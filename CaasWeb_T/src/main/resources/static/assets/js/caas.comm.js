// 사이드바 메뉴 설정
$(function () {
	var pathName = $(location).attr('pathname'); 
	var $menuTag = $(".sidebar-menu [href='"+pathName+"']").parent("li");
	var $groupTag = $menuTag.parent("ul").parent("li");
	
	if($groupTag.length == 0){
		$(".menuGroup").hide();
		return;
	}
	$(".menuName").text($("span", $menuTag).text());
	$(".menuGroup").text($("span:first", $groupTag).text()).show();
	$groupTag.attr("class","active open");
	$menuTag.attr("class","active").css("display", "block");
});

/**
 * 지정한 날짜 datepicker 셋팅
 * ex) setDate('#startDate', 30) : 30일전
 *     setDate('#endDate') 	     : 오늘
 *     setDate('#endDate', -7)   : 7일후
 */
function setDate(obj, day) {
	var date = new Date();
	if(day != undefined){
		date.setDate(date.getDate() - day);
	}
	$(obj).val(date.format('yyyy-MM-dd'));
	$(obj).datepicker();
}

function fnOnlyNumber(elementId) {
    var classNm = ".onlyNumber"; //범위 지정하지 않으면 전체에서 찾음
    if(elementId != undefined){
        classNm = "#"+elementId+" .onlyNumber";
    }
    $(classNm).keyup(function(e){ //숫자만 입력되도록
        if(e.keyCode!=37 && e.keyCode!=38 && e.keyCode!=39 && e.keyCode!=40){ //방향키 제외
            $(this).val($(this).val().replace(/[^0-9]/g,""));
        }
    });
    $(classNm).focusout(function (){ //포커스 나갔을 때 글자 남는 버그 수정
        $(this).val($(this).val().replace(/[^0-9]/g,""));
    });
}

function fnEnterForm(formId, btnId) {
    $("#"+formId+" input").keydown(function (key) { //엔터 폼전송
        if(key.keyCode == 13){
            $("#"+btnId).trigger("click");
        }
    });
}

function fnAutoDateFormat(str){
    str = str.replace(/[^0-9]/g, '');
    var tmp = '';
    if( str.length < 5){
        return str;
    }else if(str.length < 7){
        tmp += str.substr(0, 4);
        tmp += '-';
        tmp += str.substr(4);
        return tmp;
    }else{
        tmp += str.substr(0, 4);
        tmp += '-';
        tmp += str.substr(4, 2);
        tmp += '-';
        tmp += str.substr(6, 2);
        return tmp;
    }
    return str;
}

function fnAutoTimeFormat(str){
    str = str.replace(/[^0-9]/g, '');
    var tmp = '';
    if( str.length < 3){
        return str;
    }else if(str.length < 5){
        tmp += str.substr(0, 2);
        tmp += ':';
        tmp += str.substr(2);
        return tmp;
    }else{
        tmp += str.substr(0, 2);
        tmp += ':';
        tmp += str.substr(2, 2);
        tmp += ':';
        tmp += str.substr(4, 2);
        return tmp;
    }
    return str;
}

function fnAutoDttmFormat(str){
    str = str.replace(/[^0-9]/g, '');
    var date = fnAutoDateFormat(str.substr(0, 8));
    var time = fnAutoTimeFormat(str.substr(8, 6));
    if(str.length < 9){
        return date;
    }else {
        return date+" "+time;
    }
}

function fnAutoCardNoFormat(str){
    str = str.replace(/[^0-9]/g, '');
    var tmp = '';
    if( str.length < 5){
        return str;
    }else if(str.length < 9) {
        tmp += str.substr(0, 4);
        tmp += '-';
        tmp += str.substr(4);
        return tmp;
    }else if(str.length < 13){
        tmp += str.substr(0, 4);
        tmp += '-';
        tmp += str.substr(4, 4);
        tmp += '-';
        tmp += str.substr(8);
        return tmp;
    }else{
        tmp += str.substr(0, 4);
        tmp += '-';
        tmp += str.substr(4, 4);
        tmp += '-';
        tmp += str.substr(8, 4);
        tmp += '-';
        tmp += str.substr(12, 4);
        return tmp;
    }
    return str;
}


/******* 조회 날짜 값 검사 *********
 @param: 시작일자, 종료일자, 최대 조회가능 일수 (0이면 제한 없음)
 @return: true, false
 틀리면 focus()줘야해서 인자값은 document.form.startDate형태로 받는다.
 ex)
 var f = document.formSearch;
 var startDate = f.startDate;
 var endDate = f.endDate;
 if(!fnDateCheck(startDate, endDate, 30)){
                        return false;
                    }
 ***********************************/
function fnDateCheck(startDate, endDate, maxTerm) {
    var dateRegx = /^(19|20)\d{2}-(0[1-9]|1[012])-(0[1-9]|[12][0-9]|3[0-1])$/;
    if(!dateRegx.test(startDate.value)){
        alert("시작일자 날짜형식이 잘못되었습니다.(yyyy-mm-dd)");
        startDate.focus();
        return false;
    }
    if(!dateRegx.test(endDate.value)){
        alert("종료일자 날짜형식이 잘못되었습니다.(yyyy-mm-dd)");
        endDate.focus();
        return false;
    }
    var startArr = startDate.value.split('-');
    var endArr = endDate.value.split('-');
    var start = new Date(startArr[0], Number(startArr[1])-1, startArr[2]);
    var end= new Date(endArr[0], Number(endArr[1])-1, endArr[2]);
    var term = (end.getTime() - start.getTime()) /1000/60/60/24;

    if(start>end){
        alert("시작일자는 종료일자보다 작아야 합니다.");
        startDate.focus();
        return false;
    }
    if(maxTerm=="0"){
        return true; 
    }
    if(term>Number(maxTerm)-1){
        alert("조회기간은 최대 "+maxTerm+"일 입니다.");
        startDate.focus();
        return false;
    }
    return true;
}

/**************** 페이지 사용법 ******************
 @param 현재 페이지번호, 한 화면에 보여줄 목록 수, 전체목록 수, 페이징바 그릴 div아이디

 1. 페이징바를 넣을 부분에 div생성  <div id="paging"></div>

 2. 리스트를 그린 후에 페이징함수 호출(콜백함수 또는 리스트페이지 자바스크립트단에 추가)
 fnMakePaging($("#pageNo").val(), $("#countPage").val(), $("#spanTotalCnt").text(), "paging");

 3. 페이징함수 밑에 클릭이벤트 추가
 $("#paging li:not([class='disabled'])").click(function () {
                        var clickPage = $("a", this).attr("name"); //클릭한 페이지번호 가져오기
                        $("#pageNo").val(clickPage); //클릭한 페이지로 재설정 후
                        fnGetServiceList(); //다시 검색폼으로 조회(ajax)
                    });

 * 주의! 하나의 리스트마다 페이지div아이디 동일하게 설정. (1번,2번,3번 모두)
 3번에 #paging 부분은 리스트+페이징바 여러 개인 경우가 있기 때문에 범위 지정.
 ************************************************/
function fnMakePaging(pageNo, countPage, totalCnt, pageDivId) {
    if(totalCnt<1){$("#"+pageDivId).html("");return}; //리스트 없으면 페이징바 안 그림.
    var pageNo = parseInt(pageNo);
    var pageBarCnt = 10; // 페이지바 갯수
    var endPage = Math.ceil(pageNo/pageBarCnt)*pageBarCnt; //페이지바 끝번호
    var startPage = endPage - pageBarCnt + 1; //페이지바 시작번호
    var finalEndPage =  Math.ceil(totalCnt/countPage); //맨끝 번호
    var prev = pageNo==1?"disabled":""; //1페이지는 이전버튼 비활성화

    if(endPage > finalEndPage){ //페이지바 끝번호 재설정
        endPage = finalEndPage;
    }
    var next = (pageNo==finalEndPage)?"disabled":""; //맨끝 페이지는 다음버튼 비활성화

    var pageCode =  "<div class='row text-center'><ul class='pagination mgt5'>"+
		"<li class='"+prev+"'><a name='"+(pageNo-1)+"'><i class='fa fa-chevron-left'></i></a></li>";

    for(var i=startPage; i<endPage+1; i++){
        if(i==pageNo){ //현재페이지 표시
            pageCode += "<li class='active'><a name='"+i+"'>"+i+"</a></li>";
        }else{
            pageCode += "<li><a name='"+i+"'>"+i+"</a></li>";
        }
    }
    pageCode += "<li class='"+next+"'><a name='"+(pageNo+1)+"'><i class='fa fa-chevron-right'></i></a></li>";
    
    $("#"+pageDivId).html(pageCode); //페이지바 그리기
}

/*******************************************************
 @param 정렬값이 담겨있는 폼, 정렬할 테이블(범위), 정렬값 재설정 후 실행 함수
 한 화면에 리스트 여러개면 전송폼과 테이블ID를 다르게 지정해주면 된다.
 1. ORDER BY ${orderByColumn} ${ascDesc}
 2. <form name="formSearch" id="formSearch">
 <input type="hidden" name="orderByColumn" id="orderByColumn" value="registered_dttm"/>
 <input type="hidden" name="ascDesc" id="ascDesc" value="DESC"/>
 4. 테이블ID 설정, 정렬기준값 넣어주기
 <table  id="binListTable"
 <th class="text-center" value="bin_no">BIN번호<i class="fa fa-unsorted un-sorted"></i></th>
 5. 리스트 불러온 후 콜백함수에서 정렬함수 불러오기
 var form = document.formSearch;
 fnOrderByList(form, "binListTable", fnGetBinNumberList);
 *******************************************************/
function fnOrderByList(form, tableId, fnGetList) {
    var orderBy = form.orderByColumn.value;
    var ascDesc = form.ascDesc.value;
    if(ascDesc == "DESC"){ //현재 정렬기준에 맞게 아이콘 변경
        $("#"+tableId +" [value="+orderBy+"] i").attr("class", "fa fa-sort-desc down-sorted");
    }else{
        $("#"+tableId +" [value="+orderBy+"] i").attr("class", "fa fa-sort-up up-sorted");
    }

    $("#"+tableId +" i").click(function () { //정렬 아이콘 클릭(클릭한 값으로 정렬 재설정)
        var orderBy = $(this).parent("th").attr("value");
        form.orderByColumn.value = orderBy;

        if($(this).attr("class")=="fa fa-unsorted un-sorted"){
            form.ascDesc.value = "DESC"; //처음 클릭 시(내림차순)
        }else{
            if(ascDesc == "DESC"){ form.ascDesc.value = "ASC"; }
            else{ form.ascDesc.value ="DESC"; }
        }
        form.pageNo.value = "1";
        fnGetList(); //목록 다시 불러오기
    });
}

function goAjax(url, method, async, data, fnCallBack, errorCallBack){
    return $.ajax({
        type : method,
        url : url,
        data : data,
        async : async,
		success : function(response) {
			// 2017.11.14 - Ajax Status 처리 추가. 없으면 무조건 정상으로 본다.
			var status;

			if (response.status !== undefined) {
				status = response.status;
			}
			else {
				status = { code:0, message:'정상 처리했습니다.' };
			}

			// 에러면 콜백처리하지 않는다.
			if (status.code < 0) {
				if (response.redirectUrl !== undefined) {
					window.top.location.replace(response.redirectUrl);
				}
				else {
					alert(status.message);
				}
			}
			else {
				if($.isFunction(fnCallBack)) {
					fnCallBack(response);
				}
			}
		} ,
        beforeSend : function () {
            fnDrawLoading(url); //로딩바
        },
        complete : function (jqXHR, textStatus){
            $("[id='"+url+"']").remove(); //로딩바
            if(textStatus == "timeout"){ //timeout은 jqXHR.responseJSON이 없음
                alert("TIMEOUT ERROR 지정한 시간이 지나서 요청을 처리하지 못했습니다.");
                if( typeof errorCallBack != 'undefined' ){
                    // 오류시 콜백함수
                    errorCallBack();
                }
            }else if(textStatus == "parsererror") { //data타입, 인코딩,,,
                alert("PARSER ERROR 파서에러가 발생하였습니다.");
                if( typeof errorCallBack != 'undefined' ){
                    // 오류시 콜백함수
                    errorCallBack();
                }
            }else if( textStatus != "success" ){
                var response = jqXHR.responseJSON;
                if(typeof response == 'undefined'){ //방어코드
                    alert(textStatus+" 문제가 발생하였습니다."); return;
                }
                switch(response.status){
                    case 0 : alert("네트워크 오류가 발생하였습니다."); break;
                    case 400 : alert("요청에 문제가 있어 서버에서 인식할 수 없습니다."); break;
                    case 404 : alert("요청한 URL을 찾을 수 없습니다."); break;
                    case 406 : //객체변환 실패(REST방식에서 자주 나타남)
                    case 409 : //트랜잭션 오류
                    case 500 : alert("서버 처리 중에 문제가 발생하였습니다."); break;
                    default : alert(response.status+"코드에러가 발생하였습니다.\n"+response.error); break;
                }
                if( typeof errorCallBack != 'undefined' ){
                    // 오류시 콜백함수
                    errorCallBack();
                }
            }
        }
    });
}

function nvl(obj, def) {
    return isNull(obj) ? def : obj;
}

function isNull(obj) {
    if (obj == null || typeof obj == "undefined") {
        return true;
    }
    return false;
}

function isNumber(str) {
    if (isNull(str)) {
        return false;
    }
    return (/^[0-9]+$/).test(str) ? true : false;
}

function goExcelDownload(url){
    var form = document.formData;
    form.method="post";
    form.action=url;
    form.submit();
}

function goExcelDownload2(url){
    var form = document.formData2;
    form.method="post";
    form.action=url;
    form.submit();
}

function goViewAjax(url, method, async, data, destination, fnCallBack){
    $.ajax({
        type : method,
        url : url,
        data : data,
        async : async,
        beforeSend : function () {
            fnDrawLoading(url); //로딩바
        },
        complete : function () {
            $("[id='"+url+"']").remove(); //로딩바
        },
        success : function(response){
            var status;

            if (response.status !== undefined) {
            	status = response.status;
            }
            else {
            	status = { code:0, message:'정상 처리했습니다.' };
            }

            // 에러면 콜백처리하지 않는다.
            if (status.code < 0) {
                if (response.redirectUrl !== undefined) {
                    window.top.location.replace(response.redirectUrl);
                }
                else {
                    alert(status.message);
                }
            }
            else {
                $(destination).html(response);
                if(typeof fnCallBack !== 'undefined'){
                    fnCallBack(response);
                }
            }
        }
    });
}

function goViewAjaxDetail(url, method, async, data, fnCallBack){
    $.ajax({
        type : method,
        url : url,
        data : data,
        async : async,
        success : function(response){
            //$("#firstLoad").val("false");
            $("#detail").html(response);
            if(typeof fnCallBack !== 'undefined'){
                fnCallBack(response);
            }
        } ,
        error : function(e){
            console.log(e);
        }
    });
}

function fnDrawLoading(loadingId) {
    var code = "<div class=\"sk-fading-circle\" id=\""+loadingId+"\" "+
                " style=\"position: fixed; left:50%; bottom: 35%;\">\n" +
                "<div class=\"sk-circle1 sk-circle\"></div>\n" +
                "<div class=\"sk-circle2 sk-circle\"></div>\n" +
                "<div class=\"sk-circle3 sk-circle\"></div>\n" +
                "<div class=\"sk-circle4 sk-circle\"></div>\n" +
                "<div class=\"sk-circle5 sk-circle\"></div>\n" +
                "<div class=\"sk-circle6 sk-circle\"></div>\n" +
                "<div class=\"sk-circle7 sk-circle\"></div>\n" +
                "<div class=\"sk-circle8 sk-circle\"></div>\n" +
                "<div class=\"sk-circle9 sk-circle\"></div>\n" +
                "<div class=\"sk-circle10 sk-circle\"></div>\n" +
                "<div class=\"sk-circle11 sk-circle\"></div>\n" +
                "<div class=\"sk-circle12 sk-circle\"></div>\n" +
                "</div>"
    $("#divLoading").append(code);
}

//어드민 검색조건 유지
function fnSetAdminSearch(searchVO) {
    if(searchVO==null){
        return;
    }
    var searchType = searchVO.searchType;
    var searchValue = searchVO.searchValue;
    var $searchType = $("form #searchType");

    $searchType.find("option").each(function () {
        if(this.value == searchType){  //동일한 검색조건 있는 경우만 값 셋팅
            $searchType.val(searchType).trigger("change");
            if(searchType != "mobile"){
                var $divValue = $searchType.parents(".input-group").find("div:visible").last().children();
                $divValue.val(searchValue);
                $divValue.focus();
            }else{
                var mobileNo = searchValue.split("-");
                var $divMobile =  $searchType.parents(".input-group").find("div:visible").last().children();
                $divMobile.eq(0).val(mobileNo[0]);
                $divMobile.eq(1).val(mobileNo[1]);
                $divMobile.eq(2).val(mobileNo[2]);
                $divMobile.eq(2).focus();
            }
            //$("form #btnSearch").trigger("click");
            return;
        }
    });
}

// 숫자만 리턴
String.prototype.numberChar = function () {
    return this.replace(/[^\-0-9]/g, "");
};

// 숫자만 리턴 (0000025 -> 25)
String.prototype.numberChar2 = function () {
	return Number(this.numberChar()).toString();
};

// 숫자에 머니처리로 , 붙임
String.prototype.parseMoney = function () {
    try {
        var s = this;
        while ((/(-?[0-9]+)([0-9]{3})/).test(s)) {
            s = s.replace((/(-?[0-9]+)([0-9]{3})/), "$1,$2");
        }
        return s;
    }catch(e){
        return this;
    }
};

//카드번호 포맷팅
String.prototype.fmtCardNo = function () {
	var s = this;
    if(s.length == 16){
    	s = s.replace(/(.{4})(.{4})(.{4})(.{4})/, "$1-$2-$3-$4");
    }else if(s.length == 15){
    	s = s.replace(/(.{4})(.{6})(.{5})/, "$1-$2-$3");
    }
    return s;
};

// DATE 관련 함수
String.prototype.dateFormat = function (pattern, toPattern) {
    var reo = Date.formatRegExp(pattern);
    var reg = reo.regExp;
    if (!reg.test(this)) {
        return this;
    }
    var fmt = reo.format;
    var fma = fmt.split(',');
    var res = '';
    var chp = '';
    var buf = '';
    for (var ii = 0; ii < toPattern.length; ++ii) {
        var ch1 = toPattern.charAt(ii);
        var ch2 = toPattern.charAt(ii + 1);
        if (ch1 == 'y' || ch1 == 'M' || ch1 == 'd' || ch1 == 'H' || ch1 == 'h' || ch1 == 'm' || ch1 == 's') {
            if (chp != ch1) {
                buf = '';
            }
            buf += ch1;
            chp = ch1;
            if (ch2 != ch1) {
                if (buf.charAt(0) == 'y' || buf.charAt(0) == 'M' || buf.charAt(0) == 'd' || buf.charAt(0) == 'H' || buf.charAt(0) == 'h' || buf.charAt(0) == 'm' || buf.charAt(0) == 's') {
                    for (var jj = 0; jj < fma.length; ++jj) {
                        if (buf == fma[jj]) {
                            res += '$' + jj;
                        }
                    }
                } else {
                    res += buf;
                }
            }
        } else {
            res += ch1;
        }
    }
    return this.replace(reg, res);
};

String.prototype.parseDate = function (pattern) {
    var index = -1;
    var year;
    var month;
    var day;
    var hour = 0;
    var min = 0;
    var sec = 0;
    var ms = 0;
    var p = pattern || Date.InPattern;
    if ((index = p.indexOf("yyyy")) == -1) {
        index = p.indexOf("yy");
        year = "20" + this.substr(index, 2);
    } else {
        year = this.substr(index, 4);
    }
    if ((index = p.indexOf("MM")) != -1) {
        month = this.substr(index, 2);
    } else {
        month = 1;
    }
    if ((index = p.indexOf("dd")) != -1) {
        day = this.substr(index, 2);
    } else {
        day = 1;
    }
    if ((index = p.indexOf("HH")) != -1) {
        hour = this.substr(index, 2);
    }
    if ((index = p.indexOf("mm")) != -1) {
        min = this.substr(index, 2);
    }
    if ((index = p.indexOf("ss")) != -1) {
        sec = this.substr(index, 2);
    }
    return new Date(year, month - 1, day, hour, min, sec, 0);
};

Date.prototype.dateAdd = function (years, months, dates, hours, minutes, seconds) {
    var d = new Date(this);
    if (years > 0 || years < 0) {
        d.setFullYear(d.getFullYear() + years);
    }
    if (months > 0 || months < 0) {
        d.setMonth(d.getMonth() + months);
        if (d.getDate() < this.getDate()) {
            d.setDate(0);
        }
    }
    if (dates > 0 || dates < 0) {
        d.setDate(d.getDate() + dates);
    }
    if (hours > 0 || hours < 0) {
        d.setHours(d.getHours() + hours);
    }
    if (minutes > 0 || minutes < 0) {
        d.setMinutes(d.getMinutes() + minutes);
    }
    if (seconds > 0 || seconds < 0) {
        d.setSeconds(d.getSeconds() + seconds);
    }
    return d;
};

Date.prototype.dateAdd2 = function (pattern, amount) {
    if (pattern == 'y') {
        return this.dateAdd(amount);
    } else if (pattern == 'M') {
        return this.dateAdd(0, amount);
    } else if (pattern == 'h') {
        return this.dateAdd(0, 0, 0, amount);
    } else if (pattern == 'm') {
        return this.dateAdd(0, 0, 0, 0, amount);
    } else if (pattern == 's') {
        return this.dateAdd(0, 0, 0, 0, 0, amount);
    }
    return this.dateAdd(0, 0, amount);
};

Date.prototype.format = function (pattern) {
    var year = this.getFullYear();
    var month = this.getMonth() + 1;
    var dday = this.getDate();
    var hour24 = this.getHours();
    var hour12 = (hour24 > 12) ? (hour24 - 12) : hour24;
    var min = this.getMinutes();
    var sec = this.getSeconds();
    var yyyy = "" + year;
    var yy = yyyy.substr(2);
    var MM = (("" + month).length == 1) ? "0" + month : "" + month;
    var dd = (("" + dday).length == 1) ? "0" + dday : "" + dday;
    var HH = (("" + hour24).length == 1) ? "0" + hour24 : "" + hour24;
    var hh = (("" + hour12).length == 1) ? "0" + hour12 : "" + hour12;
    var mm = (("" + min).length == 1) ? "0" + min : "" + min;
    var ss = (("" + sec).length == 1) ? "0" + sec : "" + sec;
    var p = pattern || Date.InPattern;
    p = p.replace(/yyyy/g, yyyy);
    p = p.replace(/yy/g, yy);
    p = p.replace(/MM/g, MM);
    p = p.replace(/dd/g, dd);
    p = p.replace(/HH/g, HH);
    p = p.replace(/hh/g, hh);
    p = p.replace(/mm/g, mm);
    p = p.replace(/ss/g, ss);
    return p;
};

Date.prototype.dateDiff = function (pattern, date) {
    var fy = date.getFullYear() - this.getFullYear();
    if (pattern == 'y') {
        return fy;
    }
    if (pattern == 'M') {
        return (fy * 12) + (date.getMonth() - this.getMonth());
    }
    var fn = date - this;
    if (pattern == 'h') {
        return parseInt(fn / (1000 * 60 * 60), 10);
    }
    if (pattern == 'm') {
        return parseInt(fn / (1000 * 60), 10);
    }
    if (pattern == 's') {
        return parseInt(fn / (1000), 10);
    }

    return parseInt(fn / (1000 * 60 * 60 * 24), 10);
};

Date.prototype.dateDiff2 = function (pattern, dstr, dpattern) {
    return this.dateDiff(pattern, dstr.parseDate(dpattern));
};

/* 숫자만 입력되도록 */
function showKeyCode(event){
	event = event || window.event;
	var keyID = (event.which) ? event.which : event.keyCode;
	console.log(keyID);
	if( (keyID >= 48 && keyID <= 57) || (keyID >= 96 && keyID <= 105) || keyID == 8 || keyID == 46){ //8: backSpace, 46: delete
		return;
	}else{
		return false;
	}
}

/* 금액더하기 */
function fnPlus(value, form){
	$("#totalAmount", form).val(Number($("#totalAmount", form).val().numberChar())+Number(value));
	$("#totalAmount", form).val($("#totalAmount", form).val().parseMoney());
}

/* 금액 직접입력 시 comma */
function fnComma(value, form){
	value = (value == 0) ? "" : value.replace(/,/gi,"");
	$("#totalAmount", form).val(value.parseMoney());
}

/* 금액 초기화 */
function fnReset(form) {
	$("#totalAmount", form).val("0");
}

//충전하기 팝업 닫기
function _fnFlngRst(){ 
	$('#myModal').hide();
}

//보유체리 조회하기
function fnGetMyCherry(){
	$("#myCherry").text("0c");
	goAjax("/dntn/mycherry","POST", false, "", fnGetMyCherryCallBack);
	
	function fnGetMyCherryCallBack(data){
		if(data.status.code != 0){
			alert(data.status.message);
		}
		var myCherry = String(data.wltLv).parseMoney();
		$("#myCherry").text(myCherry+"c");
	}
}

//충전하기 POPUP 호출
function fnDoflng(){
	goAjax("/common/flngPopAjax","POST", false, "", fnDoflngCallBack);
	function fnDoflngCallBack(data){
		$("#flngPopAjax").html(data);
	}
}

//외부URL이동_LAYER_POPUP
function fnGoUrl(url){
	/*goAjax("/common/flngPopAjax","POST", false, "", fnDoflngCallBack);
	function fnDoflngCallBack(data){
		$("#flngPopAjax").html(data);
		
		$.ajax({ 
		  type: "get", 
		  url: url, 
		  data: "", 
		  crossOrigin:true,	// 크로스도메인 해결 플러그인
		  success: function(data){
			  $("#modalBody").html(data);
		  } 
		});
	}*/
	window.open(url);
}

//기부상세 페이지 이동
function fnMoveDntnDtl(cmpSq){
	var f = document.cmpForm;
	f.cmpSq.value = cmpSq;
	f.submit();
}

/**
 * 해당년월 마지막일자 조회
 * ex) fnLastDay('2018', '02')  => 28
 */
function fnLastDay(year, month) {
	var firstDay = new Date([year, month, '01'].join('-'));

	firstDay.setMonth(firstDay.getMonth()+1);  // 다음달 1일
	firstDay.setDate(firstDay.getDate()-1);	   // -1 일

	return firstDay.getDate();
}