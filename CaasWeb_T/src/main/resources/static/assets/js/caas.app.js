/* **************************************************************************************
 * 	E4. 공통 App <-> Web View 인터페이스
 */
//;'use strict';
(function($) {

	function func2App() {
		alert("func2App");
	}
	// Web View => App 호출
	caasToApp = {
			
		isAndroid : function() {
			return typeof(Android)!= "undefined";
		},
			
		//return true if app is valid
		isValidApp() {
			if(! isAndroid) return false;
			return Android.isValidApp();
		},
			
		//Refresh on Resume set
		setRefreshOnResume() {
			if(! isAndroid) return;
			Android.setRefreshOnResume();
		},
			
		//푸시알림 setter
		//notiStatus == boolean
		setNotificationStatus(notiStatus){
			if(! isAndroid) return;
			Android.setNotificationStatus(notiStatus);
		},
		
		//앱잠금 setter
		//lockStatus == boolean
		setAppLockStatus(lockStatus){
			if(! isAndroid) return;
			Android.setAppLockStatus(lockStatus);
		},
		
		//Get status of whether user wants to receive notifications
		//returns boolean
		getNotificationStatus() {
			if(! isAndroid) return false;
			return Android.getNotificationStatus();
		},
		
		//Get App lock status
		//returns boolean
		getAppLockStatus(){
			if(! isAndroid) return false;
			return Android.getAppLockStatus();
		},

		// 테스트용
		getString : function(){
			var sampleStr = Android.sendString();
			console.log(sampleStr);
		},

		// 테스트용
		getJson : function() {
			var sample = Android.showToast2("herro");
			console.log(sample);
		},
		
		//CID opGroupID에 저장
		getCID : function() {
			if(! isAndroid) return "";
			var CID = Android.getCID();
			console.log(CID);
			return CID;
		},
		
		//운영단체ID opGroupID에 저장
		getOpGroupID : function() {
			if(! isAndroid) return "";
			var opGroupID = Android.getOpGroupID();
			console.log(opGroupID);
			return opGroupID;
		},
			
		//
		showAndroidToast : function() {
			if(! isAndroid) return;
		    //var toast = document.getElementById("searchTxt").value;
			var toast = "Herro I am a toast"
		    Android.showToast(toast);        
		},
		
		//return firebase token string
		getFirebaseToken : function() {
			if(! isAndroid) return "";
			return Android.getFirebaseToken(); 
		},
		
		//비밀번호 설정 화면 안드로이드에서 띄우기
		startPasswordSetActivity : function() {
			if(! isAndroid) return;
			Android.startPasswordSet();
		},
		
		//비밀번호 입력(확인) 화면 안드로이드에서 띄우기
		startPasswordCheckActivity : function() {
			if(! isAndroid) return;
			Android.startPasswordCheck();
		},
		
		//CID 안드로이드에 저장
		saveCIDToPhone : function(cid){
			if(! isAndroid) return;
			Android.saveCIDToPhone(cid);
		},
		
		//DID 안드로이드에 저장
		saveDIDToPhone : function(did){
			if(! isAndroid) return;
			Android.saveDIDToPhone(did);
		},

		shareFacebook : function() {
			if(! isAndroid) return;
			//content, url
			Android.shareFacebook("http://cherry.e4net.net/ \n기부하면 체리죠! \n지금 바로 기부해보세요 ^^", "http://cherry.e4net.net/");        
		},

		shareKakaoTalk : function() {
			if(! isAndroid) return;
			Android.shareKakaoTalk("http://cherry.e4net.net/ \n기부하면 체리죠! \n지금 바로 기부해보세요 ^^", "http://cherry.e4net.net/");        
		},
		
		shareSMS : function() {
			if(! isAndroid) return;
			//only content (no url needed)
			Android.shareSMSTalk("http://cherry.e4net.net/ \n기부하면 체리죠! \n지금 바로 기부해보세요 ^^");        
		},
		
		sharePick : function() {
			if(! isAndroid) return;
			Android.shareAppLink("http://cherry.e4net.net/ \n체리로 세상을 따듯하게 합니다")
		},
		
		getPic : function() {
			if(! isAndroid) return;
			Android.getPic();        
		},
		
		//CallbackFunc == string
		//E.g. "caasFromApp.setPic"
		//Please omit brackets
		getPic2 : function(callbackFunc) {
			if(! isAndroid) return;
			Android.getPic2(callbackFunc);        
		},
		
		goToMyPage : function(url, title) {
			if(! isAndroid) return;
			Android.goToPage(url,title);        
		},
		
		openSearch : function(url, title) {
			if(! isAndroid) return;
			Android.openSearch(url);        
		}
	};

	// App => Web View 호출
	caasFromApp = {
		funcSearch : null,
		setFuncSearch : function(func) {
			caasFromApp.funcSearch = func;
		},

		buttonEvent : function(bType, url) {
			switch(bType) {
			case "SEARCH" : 
				caasToApp.openSearch(url + "/moveSrch");
				break;
			case "HOME" : 
				location.href = url + "/moveMainPag";	
				break;
			case "MYPAGE" :
				caasToApp.goToMyPage(url + "/moveMyPag", "마이페이지");
				//location.href = "https://cherry.e4net.net/moveMyPag";	
				break;
			case "ALERTS" : 
				caasToApp.goToMyPage(url + "/publish/CM-005.html", "알림");			
				break;
				
			default: alert("Invalid Button Type, bType=" + bType);
			}
		},
		
		setPic : function(base64pic) {
			$(document).ready(function() {
		    	$("#img_form_url").attr("src", "data:image/png;base64," + base64pic);
		    });
		}
	};

}(jQuery));
function func3App() {
	alert("func3App");
}


//검색 등록_srchKey : 검색키값
function fnInsSrch(srchKey, pageNo){
	pageNo = (pageNo == null || pageNo == 0) ? 1 : pageNo;
	$("#pageNo").val(pageNo);
	$("#srchKey", document.mainForm).val(srchKey);
	
	goAjax("/main/srchIns","POST", false, $("#mainForm").serialize(), fnInsSrchCallBack);
	
	function fnInsSrchCallBack(data){
		if(pageNo == 1){
			$("#dntnAjax").html(data);
		}else{
			$("#dntnAjax").append(data);
		}
		$("#btnDntnMore").click(function(){
			fnInsSrch(srchKey, Number(pageNo)+1);
		});
		
		fnGetSrchList();	//최근검색 리스트 조회 
	}
}