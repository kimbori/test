package net.e4net.caas.my.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;
import net.e4net.caas.my.vo.CardInfoPVO;
import net.e4net.caas.my.vo.CardInfoRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author hee.woo
 * @since 2018.12.03
 */
@Repository
public interface BllMgmtMapper {
	
    
    /**
     * 간편결제 카드목록 조회
     */
    public List<CardInfoRVO> selectBllCardList(CardInfoPVO pvo);
    
    
    /**
     * 간편결제 대표카드 변경
     */
    public int updateRprsCard(CardInfoPVO pvo);
	
  
}