package net.e4net.caas.my.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.my.service.BllMgmtService;
import net.e4net.caas.my.vo.CardInfoPVO;
import net.e4net.caas.my.vo.CardInfoRVO;


/**
 * 마이페이지-결제정보 관리 컨트롤러
 *
 * @author hee.woo
 * @since 2018.11.30
 * history  2018.11.30 최초작성
 */
@Controller
public class BllMgmtController extends AbstractController {
	
  
	@Autowired
	private BllMgmtService bllMgmtService;
	
    /**
     * <pre>
     * 결제정보 관리 페이지 이동
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/moveBllMgmtPag", method = RequestMethod.GET)
    public ModelAndView moveBllMgmtPag(HttpServletRequest request) throws Exception {
    	debug("===== move 결제정보 관리 페이지 =====");
    	ModelAndView mav = new ModelAndView("my/bllMgmtPag");
    	
		return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * 간편결제 카드 목록 조회
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/my/getBllCardList", method = RequestMethod.POST)
    public ModelAndView getBllCardList(@ModelAttribute CardInfoPVO pvo, HttpServletRequest request) throws Exception {
    	debug("===== select 간편결제 카드 목록 조회 =====");
    	ModelAndView mav = new ModelAndView("jsonView");
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	pvo.setCnrSq(uVO.getCnrSq());
    	List<CardInfoRVO> cardList = bllMgmtService.selectBllCardList(pvo);
    	mav.addObject("cardList", cardList);
    	
    	return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * 간편결제 대표카드 변경
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/my/updateRprsCard", method = RequestMethod.POST)
    public ModelAndView updateRprsCard(@ModelAttribute CardInfoPVO pvo, HttpServletRequest request) throws Exception {
    	debug("===== update 대표카드 변경 =====");
    	ModelAndView mav = new ModelAndView("jsonView");
    	Status status = new Status();
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	try {
        	pvo.setCnrSq(uVO.getCnrSq());
        	bllMgmtService.updateRprsCard(pvo);
    		
    	} catch (Exception e){
    		status.setFail("처리 중 오류가 발생하였습니다.");
    		return getFailModelAndView(mav, status);
    	}
    	status.setOk("결제정보가 변경되었습니다.");
    	return getOkModelAndView(mav, status);
    }
    
    
    
}