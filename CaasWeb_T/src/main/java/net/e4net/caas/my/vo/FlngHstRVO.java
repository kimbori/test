package net.e4net.caas.my.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbPgFlngDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class FlngHstRVO extends TbPgFlngDVO{
	
	private static final long serialVersionUID = 2927826803708940543L;
	
	private String lnkPrcDtFmt;
	private long flngCntCnt;
	private long flngCntSum;
}
