package net.e4net.caas.my.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbPgBllDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class CardInfoRVO extends TbPgBllDVO{

	private static final long serialVersionUID = -5958720743706296089L;
	
	private String cardCdNm;
	

}
