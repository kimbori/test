package net.e4net.caas.my.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbCmpFsprDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class FxprBllPVO extends TbCmpFsprDVO{
	
	private static final long serialVersionUID = -4237949631384168436L;
	
	
}
