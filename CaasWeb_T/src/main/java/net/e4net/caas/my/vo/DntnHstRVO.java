package net.e4net.caas.my.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbDntLstDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class DntnHstRVO extends TbDntLstDVO{
	
	private static final long serialVersionUID = -4963016249546364263L;
	
	private String cmpNm;
	private String dntnDttmFmt;
	private long dntnCntSum;
	private long dntnAmtSum;
	
}
