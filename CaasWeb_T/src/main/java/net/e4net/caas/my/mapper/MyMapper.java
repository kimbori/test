package net.e4net.caas.my.mapper;

import org.springframework.stereotype.Repository;

import net.e4net.caas.my.vo.MyInfoPVO;
import net.e4net.caas.my.vo.MyInfoRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author hee.woo
 * @since 2018.11.27
 */
@Repository
public interface MyMapper {
	
    
    /**
     * 마이페이지 회원정보
     */
    public MyInfoRVO selectMyInfo(MyInfoPVO pvo);
   
    /**
     * 프로필 정보 변경 
     */
    public int updatePrfInfo(MyInfoPVO pvo);
	
    
    
}