package net.e4net.caas.my.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbDntLstDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class DntnHstPVO extends TbDntLstDVO{
	
	private static final long serialVersionUID = -2207628890165122082L;
	
	private String startDate;
	private String endDate;
	
}
