package net.e4net.caas.my.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.my.service.FxprBllService;
import net.e4net.caas.my.vo.FxprBllPVO;
import net.e4net.caas.my.vo.FxprBllRVO;


/**
 * 마이페이지-정기기부 결제정보 컨트롤러
 *
 * @author hee.woo
 * @since 2018.11.30
 * history  2018.11.30 최초작성
 */
@Controller
public class FxprMgmtController extends AbstractController {
	
	@Autowired
	private FxprBllService fxprBllService;
	
	
    /**
     * <pre>
     * 정기기부 결제정보 페이지 이동
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/moveFxprMgmtPag", method = RequestMethod.GET)
    public ModelAndView moveFxprMgmtPag(HttpServletRequest request) throws Exception {
    	debug("===== move 정기기부 결제정보 페이지 =====");
    	ModelAndView mav = new ModelAndView("my/fxprMgmtPag");
    	
		return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * 정기결제 목록 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return List<FlngHstRVO> 충전내역 리스트
     */
    @RequestMapping(value = "/my/selectFxprBllList", method = RequestMethod.POST)
    public ModelAndView selectFxprBllList(@ModelAttribute FxprBllPVO pvo) throws Exception {
        debug("=====  select 정기결제 목록 =====");
        ModelAndView mav = new ModelAndView("my/fxprMgmtAjax :: fxprBllList");
        
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setCnrSq(uVO.getCnrSq());
        List<FxprBllRVO> resultList = fxprBllService.selectFxprBllList(pvo);
        
        int totalCnt = resultList.size() == 0 ? 0 : resultList.get(0).getTotalCnt(); 
        mav.addObject("totalCnt", totalCnt);
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
    
    
    
}