package net.e4net.caas.my.controller;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.my.service.FlngHstService;
import net.e4net.caas.my.vo.FlngHstPVO;
import net.e4net.caas.my.vo.FlngHstRVO;

/**
 * 마이페이지-충전내역 컨트롤러
 *
 * @author hee.woo
 * @since 2018.11.28
 * history  2018.11.28 최초작성
 */
@Controller
public class FlngHstController extends AbstractController {
	
	@Autowired
	private FlngHstService flngHstService;
	
    /**
     * <pre>
     * 충전내역 페이지 이동
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/moveFlngHstPag", method = RequestMethod.GET)
    public ModelAndView moveFlngHstPag(HttpServletRequest request) throws Exception {
    	debug("===== move 충전내역 페이지 =====");
    	ModelAndView mav = new ModelAndView("my/flngHstPag");
    	
		return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * 충전내역 목록 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return List<FlngHstRVO> 충전내역 리스트
     */
    @RequestMapping(value = "/my/selectFlngList", method = RequestMethod.POST)
    public ModelAndView selectFlngList(@ModelAttribute FlngHstPVO pvo) throws Exception {
        debug("=====  select 충전내역 목록 =====");
        ModelAndView mav = new ModelAndView("my/flngHstAjax :: myFlngList");
        
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	pvo.setCnrSq(uVO.getCnrSq());
    	pvo.setOrderByColumn("FLNG_ITM_SQ");
    	pvo.setAscDesc("desc");
        List<FlngHstRVO> resultList = flngHstService.selectMyFlngList(pvo);
        
        int totalCnt = resultList.size() == 0 ? 0 : resultList.get(0).getTotalCnt();
        int pageNo = StringUtils.parseInt(pvo.getPageNo(), 1);
        boolean hasNext = totalCnt-(pageNo*pvo.getCountPage()) > 0;
        
        mav.addObject("totalCnt", totalCnt);
        mav.addObject("resultList", resultList);
        mav.addObject("hasNext", hasNext);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 충전내역 집계
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return FlngHstRVO 
     */
    @RequestMapping(value = "/my/selectFlngSum", method = RequestMethod.POST)
    public ModelAndView selectFlngSum(@ModelAttribute FlngHstPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setCnrSq(uVO.getCnrSq());
    	
        FlngHstRVO flngInfo = flngHstService.selectMyFlngSum(pvo);
        mav.addObject("flngInfo", flngInfo);
        
        return getOkModelAndView(mav);
    }
    
    
}