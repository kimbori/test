package net.e4net.caas.my.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbDntPrsnDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class MyInfoPVO extends TbDntPrsnDVO{
	
	private static final long serialVersionUID = 3316434956854624081L;
	
}
