package net.e4net.caas.my.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.my.mapper.FxprBllMapper;
import net.e4net.caas.my.vo.FxprBllPVO;
import net.e4net.caas.my.vo.FxprBllRVO;

/**
 * 정기기부 결제관리 서비스
 *
 * @author hee.woo
 * @since 2018.12.04
 */
@Service
public class FxprBllService extends AbstractDao {
   
	@Autowired
    private FxprBllMapper fxprBllMapper;

	
    /**
     * 정기결제 목록 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public List<FxprBllRVO> selectFxprBllList(FxprBllPVO pvo){
    	return fxprBllMapper.selectFxprBllList(pvo);
    }
    
    
}
