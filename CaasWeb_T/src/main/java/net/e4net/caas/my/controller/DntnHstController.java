package net.e4net.caas.my.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.my.service.DntnHstService;
import net.e4net.caas.my.vo.DntnHstPVO;
import net.e4net.caas.my.vo.DntnHstRVO;

/**
 * 마이페이지-기부내역 컨트롤러
 *
 * @author hee.woo
 * @since 2018.11.27
 * history  2018.11.27 최초작성
 */
@Controller
public class DntnHstController extends AbstractController {
	
	@Autowired
	private DntnHstService dntnHstService;
	
    /**
     * <pre>
     * 기부내역 페이지 이동
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/moveDntnHstPag", method = RequestMethod.GET)
    public ModelAndView moveDntnHstPag(HttpServletRequest request) throws Exception {
    	debug("===== move 기부내역 페이지 =====");
    	ModelAndView mav = new ModelAndView("my/dntnHstPag");
    	
		return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * 기부내역 목록 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return List<DntnHstRVO> 기부내역 리스트
     */
    @RequestMapping(value = "/my/selectDntnList", method = RequestMethod.POST)
    public ModelAndView selectDntnList(@ModelAttribute DntnHstPVO pvo) throws Exception {
        debug("=====  select 기부내역 목록 =====");
        ModelAndView mav = new ModelAndView("my/dntnHstAjax :: myDntnList");
        
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setCnrSq(uVO.getCnrSq());
    	pvo.setOrderByColumn("DNTN_ITM_SQ");
    	pvo.setAscDesc("desc");
        List<DntnHstRVO> resultList = dntnHstService.selectMyDntnList(pvo);
        
        int totalCnt = resultList.size() == 0 ? 0 : resultList.get(0).getTotalCnt();
        int pageNo = StringUtils.parseInt(pvo.getPageNo(), 1);
        boolean hasNext = totalCnt-(pageNo*pvo.getCountPage()) > 0;
        
        mav.addObject("totalCnt", totalCnt);
        mav.addObject("resultList", resultList);
        mav.addObject("hasNext", hasNext);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 기부내역 집계
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return DntnHstRVO 
     */
    @RequestMapping(value = "/my/selectDntnSum", method = RequestMethod.POST)
    public ModelAndView selectDntnSum(@ModelAttribute DntnHstPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setCnrSq(uVO.getCnrSq());
    	
        DntnHstRVO dntnInfo = dntnHstService.selectMyDntnSum(pvo);
        mav.addObject("dntnInfo", dntnInfo);
        
        return getOkModelAndView(mav);
    }
    
    
    
    
}