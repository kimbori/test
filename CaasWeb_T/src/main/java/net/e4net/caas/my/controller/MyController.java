package net.e4net.caas.my.controller;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.service.BlockchainService;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.my.service.DntnHstService;
import net.e4net.caas.my.service.MyService;
import net.e4net.caas.my.vo.DntnHstPVO;
import net.e4net.caas.my.vo.MyInfoPVO;


/**
 * 마이페이지 컨트롤러
 *
 * @author hee.woo
 * @since 2018.11.27
 * history  2018.11.27 최초작성
 */
@Controller
public class MyController extends AbstractController {
	
    @Autowired
    private MyService myService;
    
    @Autowired
	private DntnHstService dntnHstService;
    
    @Autowired 
    private BlockchainService bcService;
	
	
    /**
     * <pre>
     * 마이 페이지 이동
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/moveMyPag", method = RequestMethod.GET)
    public ModelAndView moveMyPag(@ModelAttribute MyInfoPVO pvo, HttpServletRequest request) throws Exception {
    	debug("===== move 마이 페이지 =====");
    	ModelAndView mav = new ModelAndView("my/myPag");
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	// 회원 정보(닉네임, 이미지)
    	pvo.setCnrSq(uVO.getCnrSq());
		pvo.setOprGrpId(uVO.getOprGrpId());
    	mav.addObject("myInfo", myService.selectMyInfo(pvo));
    	
    	// 나의 기부 정보
    	DntnHstPVO dntnPVO = new DntnHstPVO();
    	dntnPVO.setCnrSq(uVO.getCnrSq());
    	mav.addObject("dntnInfo", dntnHstService.selectMyDntnSum(dntnPVO));
    	
    	// 나의 보유 체리
    	try {
            long wltLv = bcService.getWalletBalance(uVO.getOprGrpId(), uVO.getWltAddr());
            mav.addObject("wltLv", wltLv);
            if(wltLv<0) {
            	throw new Exception();
            }
            
        } catch(Exception e) {
        	error("### 체리 잔액조회 오류 : ", e);
        	throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
        }
    	
		return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * 프로필 설정 페이지 이동
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/moveProfilePag", method = RequestMethod.GET)
    public ModelAndView moveProfilePag(@ModelAttribute MyInfoPVO pvo, HttpServletRequest request) throws Exception {
    	debug("===== move 프로필 설정 페이지 =====");
    	ModelAndView mav = new ModelAndView("my/profilePag");
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    
    	// 회원 정보(닉네임, 이미지)
    	pvo.setCnrSq(uVO.getCnrSq());
		pvo.setOprGrpId(uVO.getOprGrpId());
    	mav.addObject("myInfo", myService.selectMyInfo(pvo));
    	
		return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * 프로필 정보 변경
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/my/updatePrfInfo", method = RequestMethod.POST)
    public ModelAndView updatePrfInfo(@ModelAttribute MyInfoPVO pvo, HttpServletRequest request) throws Exception {
    	debug("===== update 프로필 정보 변경 =====");
    	ModelAndView mav = new ModelAndView("jsonView");
    	Status status = new Status();
    	
    	Authentication currAuth = SecurityContextHolder.getContext().getAuthentication();
    	UserVO uVO = (UserVO)currAuth.getPrincipal();
    	
    	try {
        	pvo.setCnrSq(uVO.getCnrSq());
    		if(myService.updatePrfInfo(pvo) != 1) {
    			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
    		}
    		
    		//세션정보 변경(닉네임)
    		uVO.setNickNm(pvo.getNickNm());
    		UsernamePasswordAuthenticationToken newAuth = new UsernamePasswordAuthenticationToken(uVO, currAuth.getCredentials(), currAuth.getAuthorities());
    		newAuth.setDetails(currAuth.getDetails());
    		SecurityContextHolder.getContext().setAuthentication(newAuth);
    		
    	} catch (Exception e){
    		status.setFail(e.getMessage());
    		return getFailModelAndView(mav, status);
    	}
    	status.setOk("프로필 설정이 완료되었습니다.");
    	return getOkModelAndView(mav, status);
    }
    
    

    /**
     * <pre>
     * 환경설정 페이지 이동
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/moveSettingPag", method = RequestMethod.GET)
    public ModelAndView moveSettingPag(@ModelAttribute MyInfoPVO pvo, HttpServletRequest request) throws Exception {
    	debug("===== move 환경설정 페이지 =====");
    	ModelAndView mav = new ModelAndView("my/settingPag");
    	
		return getOkModelAndView(mav);
    }
    
    
}