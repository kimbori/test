package net.e4net.caas.my.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbCmpFsprDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class FxprBllRVO extends TbCmpFsprDVO{
	
	private static final long serialVersionUID = 4754981695375250891L;
	
	private String cmpNm;
	private String mrchGrpNm;
	
}
