package net.e4net.caas.my.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.my.mapper.FlngHstMapper;
import net.e4net.caas.my.vo.FlngHstPVO;
import net.e4net.caas.my.vo.FlngHstRVO;

/**
 * 충전내역 서비스
 *
 * @author hee.woo
 * @since 2018.11.28
 */
@Service
public class FlngHstService extends AbstractDao {
   
	@Autowired
    private FlngHstMapper flngHstMapper;

    /**
     * 충전내역 리스트 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public List<FlngHstRVO> selectMyFlngList(FlngHstPVO pvo){
    	return flngHstMapper.selectMyFlngList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 충전내역 집계
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public FlngHstRVO selectMyFlngSum(FlngHstPVO pvo){
    	return flngHstMapper.selectMyFlngSum(pvo);
    }
    
}
