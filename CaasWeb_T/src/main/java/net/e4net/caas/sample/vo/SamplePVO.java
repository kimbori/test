package net.e4net.caas.sample.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.SampleDVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class SamplePVO extends SampleDVO{

}
