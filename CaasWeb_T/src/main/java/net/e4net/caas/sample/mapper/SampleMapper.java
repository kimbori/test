package net.e4net.caas.sample.mapper;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import net.e4net.caas.sample.vo.SamplePVO;
import net.e4net.caas.sample.vo.SampleRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author djim
 * @since 2018.11.01
 */
@Repository
public interface SampleMapper {
	
    /**
     * SAMPLE 목록 조회
     */
    public List<SampleRVO> selectSampleList(SamplePVO pvo);
    
    /**
     * SAMPLE 등록
     */
    @Transactional
    public int insertSample(SamplePVO pvo);
    
    /**
     * SAMPLE 수정
     */
    public int updateSample(SamplePVO pvo);
    
    /**
     * SAMPLE 수정
     */
    public int deleteSample(SamplePVO pvo);
}
