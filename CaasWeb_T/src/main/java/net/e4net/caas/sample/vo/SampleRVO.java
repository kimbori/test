package net.e4net.caas.sample.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.SampleDVO;

@Data
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
public class SampleRVO extends SampleDVO {
	String sss;
}
