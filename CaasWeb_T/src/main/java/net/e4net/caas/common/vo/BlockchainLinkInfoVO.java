package net.e4net.caas.common.vo;

import lombok.Data;

@Data
public class BlockchainLinkInfoVO {
	private String blockChainKd;	// 연계 블록체인 종류 
	private String tokenId;			// 토큰심볼(Product Token)
	private String tokenSimbol;		// 토큰심볼(Product Token)
	private String apiKey;			// 블록체인 연계용 API키 (= Access Key)
	private String walletAddr;		// 운영단체 토큰지갑
}
