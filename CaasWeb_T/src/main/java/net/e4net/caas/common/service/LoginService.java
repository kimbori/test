package net.e4net.caas.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.mapper.LoginMapper;
import net.e4net.caas.common.vo.UserVO;

/**
 * LOGIN 서비스
 *
 * @author djim
 * @since 2018.11.01
 */
@Service
public class LoginService implements UserDetailsService {
    @Autowired
    private LoginMapper loginMapper;
    
    /**
     * LOGIN 서비스
     *
     * @param Sting 로그인ID
     * @return USER 정보
     */
    public UserVO selectUser(String cnrCi, String oprGrpId) {
        return loginMapper.selectUser(cnrCi, oprGrpId);
    }

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		// TODO Auto-generated method stub
		return null;
	}

}
