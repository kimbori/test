package net.e4net.caas.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class KakaoPayApiCancelRVO extends KakaoRestVO {

	private static final long serialVersionUID = -5447950025980771248L;
	
	//Request 고유 번호
	@JsonProperty("aid")
	String aid; 
	
	//결제 고유 번호. 10자.
	@JsonProperty("tid")
	String tid; 
	
	//가맹점 코드. 20자
	@JsonProperty("cid")
	String cid; 
	
	//결제상태값
	@JsonProperty("status")
	String status; 
	
	//가맹점 주문번호. 최대 100자.
	@JsonProperty("partner_order_id")
	String partnerOrderId; 
	
	//가맹점 회원 id. 최대 100자.
	@JsonProperty("partner_user_id")
	String partnerUserId; 
	
	//결제 수단. CARD, MONEY 중 하나
	@JsonProperty("payment_method_type")
	String paymentMethodType;
	
	//결제 금액 정보
	@JsonProperty("amount")
	private Amount amount;
	
	@Data
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class Amount {
		@JsonProperty("total")
		private String total;
		
		@JsonProperty("tax_free")
		private String taxFree;
		
		@JsonProperty("vat")
		private String vat;
		
		@JsonProperty("discount")
		private String discount;
	}
	
	//이번 요청으로 취소된 금액 정보
	@JsonProperty("canceled_amount")
	private CanceledAmount canceledAmount;
	
	@Data
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class CanceledAmount {
		@JsonProperty("total")
		private String total;
		
		@JsonProperty("tax_free")
		private String taxFree;
		
		@JsonProperty("vat")
		private String vat;
		
		@JsonProperty("discount")
		private String discount;
	}
	
	//이번 요청으로 취소된 금액 정보
	@JsonProperty("cancel_available_amount")
	private CancelAvailableAmount cancelAvailableAmount;
	
	@Data
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class CancelAvailableAmount {
		@JsonProperty("total")
		private String total;
		
		@JsonProperty("tax_free")
		private String taxFree;
		
		@JsonProperty("vat")
		private String vat;
		
		@JsonProperty("discount")
		private String discount;
	}
	
	//상품 이름. 최대 100자
	@JsonProperty("item_name")
	String itemName;
	
	//상품 코드. 최대 100자
	@JsonProperty("item_code")
	String itemCode;
	
	//상품 수량
	@JsonProperty("quantity")
	String quantity;
	
	//결제 준비 요청 시각
	@JsonProperty("created_at")
	String createdAt;
	
	//결제 승인 시각
	@JsonProperty("approved_at")
	String approvedAt;
	
	//결제 취소 시각
	@JsonProperty("canceled_at")
	String canceledAt;
	
	//Request로 전달한 값
	@JsonProperty("payload")
	String payload;
}
