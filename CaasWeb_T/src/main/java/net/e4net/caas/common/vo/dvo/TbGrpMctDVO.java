package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = false)
public class TbGrpMctDVO extends PageVO {
	private String mrchGrpId;		//가맹단체ID
	private String oprGrpId;		//운영단체ID
	private String mrchGrpNm;		//가맹단체명
	private String mrchGrpImg;		//가맹단체이미지
	private String wltAddr;			//지갑주소
	private int fsprStdd;			//정기결제일
	private String rprsNum;			//대표전화번호
	private String faxNum;			//FAX전화번호
	private String bnkCd;			//입금은행코드
	private String bnkAcct;			//입금계좌번호
	private int crgAdminSq;			//담당관리자SEQ
	private String fstRegDttm;		//최초등록일시
	private String fstRegUsid;		//최초등록자
	private String lstChDttm;		//최종변경일시
	private String lstChUsid;		//최종변경자
	
	private long cncrGrpSq;			//관심단체SEQ
	private String cncrYn;			//관심단체등록여부
}