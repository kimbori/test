package net.e4net.caas.common.adapter;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.service.CmmService;
import net.e4net.caas.common.util.LogObject;
import net.e4net.caas.common.vo.KakaoRestVO;
import net.e4net.caas.common.vo.RestVO;
import net.e4net.caas.common.vo.UserVO;

@Component
public class KakaoRestAdapter extends LogObject {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = -5035512889394007053L;


	protected ClientHttpRequestFactory factory = null;

	protected List<HttpMessageConverter<?>> messageConverters = null;
	
    //공통 서비스
	@Autowired 
	private CmmService cmmService;
	
	/**
	 * RestTemplate Exchange
	 *
	 * @param url
	 * @param httpMethod
	 * @param request
	 * @param responseClass
	 * @param <T1>
	 * @param <T2>
	 * @return
	 * @throws MoneyCardException
	 */
	protected <T1 extends KakaoRestVO, T2 extends KakaoRestVO> T2 exchange(String url, HttpMethod httpMethod, T1 request, Class<T2> responseClass) throws  CaasException, HttpServerErrorException, HttpClientErrorException, UnsupportedEncodingException {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);

		// GET이면 쿼리 파라미터를 추가한다.
		if (httpMethod == HttpMethod.GET) {
			builder.queryParams(convertParameter(request));
		}

		URI uri;

		uri = builder.build().encode(getCharset()).toUri();

		// RequestHeader 설정
		HttpHeaders requestHeaders = getRequestHeaders();

		try {
			ObjectMapper om = new ObjectMapper();
			String json = om.writeValueAsString(request);
			debug("REQUEST JSON : [{}]", json);
		} catch (JsonProcessingException e) {
		}

		RequestEntity<?> requestEntity;

		// GET이면 헤더만 추가한다.
		if (httpMethod == HttpMethod.GET) {
			requestEntity = new RequestEntity<>(null, requestHeaders, httpMethod, uri);
		}
		// 그 외에는 Body도 추가한다.
		else {
			if (requestHeaders.getContentType().includes(MediaType.APPLICATION_FORM_URLENCODED)) {
				requestEntity = new RequestEntity<>(convertParameter(request), requestHeaders, httpMethod, uri);

				// 메시지 컨버터 설정
				setFromMessageConverter();
			}
			else {
				requestEntity = new RequestEntity<>(request, requestHeaders, httpMethod, uri);
			}
		}

		return this.exchange(uri, httpMethod, requestEntity, responseClass);
	}

	/**
	 * RestTemplate Exchange
	 *
	 * @param uri
	 * @param httpMethod
	 * @param requestEntity
	 * @param responseClass
	 * @param <T>
	 * @return
	 * @throws Exception
	 */
	protected <T extends KakaoRestVO> T exchange(URI uri, HttpMethod httpMethod, HttpEntity<?> requestEntity, Class<T> responseClass) throws CaasException {
		RestTemplate restTemplate = new RestTemplate();

		// 메시지 컨버터 설정
		if (messageConverters != null) {
			restTemplate.setMessageConverters(messageConverters);
		}

		// 팩토리 설정
		if (factory == null) {
			setDefaultRequestFactory();
		}

		restTemplate.setRequestFactory(factory);

		debug("URI : [{}]", uri);
		debug("REQUEST ENTITY : [{}]", requestEntity.toString());

		T response = null;
		ResponseEntity<String> responseEntity = null;
		ObjectMapper om = null;
		// Json을 VO로 변환한다.
		om = new ObjectMapper();
		om.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);	// 다중 클래스 구조일 경우 오류 방지
		
		try {
			// Charset 문제로 일단 문자열로 받는다.
			responseEntity = restTemplate.exchange(uri, httpMethod, requestEntity, String.class);

			debug("RESPONSE ENTITY : [{}]", responseEntity.toString());

			// 200 OK 가 아니면 Exception.
			if (responseEntity.getStatusCode() != HttpStatus.OK) {
				//throw new CaasException("99", StringUtils.format("Status {}. {}.", responseEntity.getStatusCode().value(), responseEntity.getStatusCode().getReasonPhrase()));
			}

			response = om.readValue(responseEntity.getBody(), responseClass);
			response.setHttpCode(responseEntity.getStatusCode());
			debug("RESULT : [{}]", response);

		} catch (HttpServerErrorException | HttpClientErrorException e) {
            String responseString = e.getResponseBodyAsString();
            debug("12312313123" + responseString);
            try {
				response = om.readValue(responseString, responseClass);
			} catch(Exception e1) {
				error(e1.getMessage(), e1);
				throw new CaasException("99", e1.getMessage(), e1);
			}
            response.setHttpCode(e.getStatusCode());
            
            debug("RESPONSE ENTITY : [{}]", response);
            return response;
		} catch (ResourceAccessException e) {
			// 타임아웃시 별도의 코드로 처리한다.
			error(e.getMessage(), e);
			throw new CaasException("91", e.getMessage(), e);
		} catch (Exception e) {
			error(e.getMessage(), e);
			throw new CaasException("99", e.getMessage(), e);
		}
		return response;
	}

	/**
	 * RestTemplate Get
	 *
	 * @param url
	 * @param request
	 * @param responseClass
	 * @param <T1>
	 * @param <T2>
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public <T1 extends KakaoRestVO, T2 extends KakaoRestVO> T2 get(String url, T1 request, Class<T2> responseClass) throws CaasException, HttpServerErrorException, HttpClientErrorException, UnsupportedEncodingException {
		return exchange(url, HttpMethod.GET, request, responseClass);
	}

	/**
	 * RestTemplate Post
	 *
	 * @param url
	 * @param request
	 * @param responseClass
	 * @param <T1>
	 * @param <T2>
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public <T1 extends KakaoRestVO, T2 extends KakaoRestVO> T2 post(String url, T1 request, Class<T2> responseClass) throws CaasException, HttpServerErrorException, HttpClientErrorException, UnsupportedEncodingException {
		return exchange(url, HttpMethod.POST, request, responseClass);
	}

	/**
	 * RestTemplate Put
	 *
	 * @param url
	 * @param request
	 * @param responseClass
	 * @param <T1>
	 * @param <T2>
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public <T1 extends KakaoRestVO, T2 extends KakaoRestVO> T2 put(String url, T1 request, Class<T2> responseClass) throws CaasException, HttpServerErrorException, HttpClientErrorException, UnsupportedEncodingException {
		return exchange(url, HttpMethod.PUT, request, responseClass);
	}

	/**
	 * RestTemplate Delete
	 *
	 * @param url
	 * @param request
	 * @param responseClass
	 * @param <T1>
	 * @param <T2>
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public <T1 extends KakaoRestVO, T2 extends KakaoRestVO> T2 delete(String url, T1 request, Class<T2> responseClass) throws CaasException, HttpServerErrorException, HttpClientErrorException, UnsupportedEncodingException {
		return exchange(url, HttpMethod.DELETE, request, responseClass);
	}



	/**
	 * VO를 MultiValueMap으로 변경한다.
	 *
	 * @param vo
	 * @param <T>
	 * @return
	 */
	protected <T extends RestVO> MultiValueMap<String, String> convertParameter(T vo) {
		return vo.convertMultiValueMap();
	}

	/**
	 * ContentType 설정<br/>
	 * 기본은 application/json; charset=UTF-8
	 *
	 * @return
	 */
	protected MediaType getContentType() {
		return MediaType.APPLICATION_FORM_URLENCODED;
	}

	/**
	 * Accept 설정<br/>
	 * 기본은 application/json; charset=UTF-8
	 *
	 * @return
	 */
	protected List<MediaType> getAccept() {
//		return Arrays.asList(MediaType.APPLICATION_JSON_UTF8, MediaType.TEXT_PLAIN);
		return Collections.singletonList(MediaType.APPLICATION_JSON);
	}

	/**
	 * Charset 설정<br/>
	 * 기본은 UTF-8
	 *
	 * @return
	 */
	protected Charset getCharset() {
		return Charset.forName("UTF-8");
	}

	/**
	 * RequestHeaders 설정
	 *
	 * @return
	 */
	protected HttpHeaders getRequestHeaders() {
		UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(getContentType());
		requestHeaders.setAccept(getAccept());
		requestHeaders.set("Authorization", cmmService.getOprGrpInfo(principal.getOprGrpId()).getCpAuthKey());

		return requestHeaders;
	}

	/**
	 * MessageConverter 설정
	 */
	protected void setFromMessageConverter() {
		// StringConverter
		StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter(getCharset());
		stringHttpMessageConverter.setWriteAcceptCharset(true);

		// FormConverter
		List<HttpMessageConverter<?>> partConverters = new ArrayList<>();
		partConverters.add(new ByteArrayHttpMessageConverter());
		partConverters.add(stringHttpMessageConverter);
		partConverters.add(new ResourceHttpMessageConverter());

		FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
		formHttpMessageConverter.setCharset(getCharset());
		formHttpMessageConverter.setPartConverters(partConverters);

		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(formHttpMessageConverter);
		messageConverters.add(stringHttpMessageConverter);

		this.messageConverters = messageConverters;
	}

	/**
	 * DefaultRequestFactory 설정
	 */
	private void setDefaultRequestFactory() {
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setConnectTimeout(10 * 1000);
		requestFactory.setConnectionRequestTimeout(10 * 1000);
		requestFactory.setReadTimeout(10 * 1000);
		this.factory = requestFactory;
	}
}
