package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = false)
public class TbNotcLstDVO extends PageVO {
	private long notcSq;			//알람내역SEQ
	private long cnrSq;				//기부자SEQ
	private String notcGb;			//알림구분
	private String regDttm;			//등록일시
	private String rdYn;			//read여부
	private String rdDt;			//read일시
	private String fstRegDttm;		//최초등록일시
	private String fstRegUsid;		//최초등록자
	private String lstChDttm;		//최종변경일시
	private String lstChUsid;		//최종변경자
}