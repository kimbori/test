package net.e4net.caas.common.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.mapper.CmmCodeMapper;
import net.e4net.caas.common.mapper.CmmMapper;
import net.e4net.caas.common.vo.CmmnDetailCode;
import net.e4net.caas.common.vo.OprGrpVO;

@Slf4j
@Service
public class CmmService {

	@Autowired CmmMapper cmmMapper;
	@Autowired CmmCodeMapper cmmCodeMapper;

	public static Map<String, OprGrpVO>  oprGrpInfoList = null;
	
	/* ==================================================================================
	 * 	운영단체 정보
	 */

	private void loadOprGrpList() {
		oprGrpInfoList = cmmMapper.selectOprGrp();
		for(String key : oprGrpInfoList.keySet()) {
			log.debug(String.format("[oprGrpInfoList] load (%s), %s", key, oprGrpInfoList.get(key)) );
		}
	}
	
	/**
	 * 운영단체정보를 주기적으로 갱신한다. 
	 */
	@Scheduled(fixedDelayString = "${auto.oprGrp.delay.ms}")
	public void autoLoadOprGrpList() {
		loadOprGrpList();
	}

	/**
	 * 운영단체정보를 조회한다.
	 * @param oprGrpId 운영단체ID
	 * @return 운영단체정보
	 */
	public OprGrpVO getOprGrpInfo(String oprGrpId) {
		if(oprGrpInfoList == null) {
			loadOprGrpList();
		}
		return oprGrpInfoList.get(oprGrpId);
	}

	/* ==================================================================================
	 * 	공통코드
	 */
	// 공통코드 상세목록 조회
	public List<CmmnDetailCode> selectCmmCodeDetail(String codeId) {
		return cmmCodeMapper.selectCmmCodeDetail(codeId);
	}
}
