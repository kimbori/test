package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbKakaoPayAvLstDVO extends PageVO{
	
	private static final long serialVersionUID = -3903725600790062648L;
	 
	private long avItmSq;
	private long flngItmSq;
	private long prprItmSq;
	private String stlmMnsCd;
	private long cnrSq;
	private String authToknVl;
	private long stlmTtAmt;
	private long frTaxAmt;
	private long srtxAmt;
	private long usePnt;
	private long dscnAmt;
	private String acrKorNm;
	private String acrCd;
	private String issrKorNm;
	private String issrCd;
	private String binNo;
	private String cardTypeCd;
	private int isMcnt;
	private String avNo;
	private String acrMcno;
	private String ifimYn;
	private String cardPrdtCd;
	private String prdtNm;
	private String prdtCd;
	private int prdtQty;
	private String prprReqDttm;
	private String avPcsnDttm;
	private String cancYn;
	private String rspnCd;
	private String rspnMsg;
	private String addRspnCd;
	private String addRspnMsg;
	private String fstRegDttm;
	private String fstRegUsid;
	private String lstChDttm;
	private String lstChUsid;

}
