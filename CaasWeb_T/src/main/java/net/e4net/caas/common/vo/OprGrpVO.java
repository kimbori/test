package net.e4net.caas.common.vo;

import lombok.Data;

@Data
public class OprGrpVO {

	private String	oprGrpId;		// 운영단체ID
	private String	oprGrpNm;		// 운영단체 명
	private String	mailAddr;		// 메일주소
	private String	homeUrl;		// 홈페이지 주소(URL)

	private String	wltAddr;		// 블록체인: 지갑주소
	private String	bcKd;			// 블록체인: 종류. (01: Luniverse)
	private String	bcTokenSimbol;	// 블록체인: 토큰심볼
	private String	bcTokenId;		// 블록체인: 토큰ID
	private String	bcApiKey;		// 블록체인: 접속 API 키

	private String	ctSvcId;		// 본인인증: 서비스ID
	private String	ctEncKey;		// 본인인증: 복호화키

	private String	pgBllMid;		// PG빌링결제 : 가맹점ID
	private String	pgBillMkey;		// PG빌링결제 : 가맹점키
	private String	pgBllKeyPw;		// PG빌링결제 : 키 비밀번호
	private String	pgNormMid;		// PG일반결제 : 가맹점ID
	private String	pgGoodsName;	// 결제 : 구매토큰 상품명

	private String	cpCid;			// 카카오페이 : 접속ID
	private String	cpAuthKey;		// 카카오페이 : 권한 키

    private Data	fstRegDttm;		// 최초등록일시
    private String	fstRegUsid;		// 최초등록자ID
    private Data	lstChDttm;		// 최근갱신일시
    private String	lstChUsid;		// 최근등록자ID
}
