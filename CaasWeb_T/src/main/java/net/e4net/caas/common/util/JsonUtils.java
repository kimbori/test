package net.e4net.caas.common.util;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

public class JsonUtils {

	public static String convertvObjToJson(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			json = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			json = "";
		}
		return json;
	}
	
	public static Map<String,Object> convertJsonToMap(String json) {
		ObjectReader reader = new ObjectMapper().readerFor(Map.class);
		Map<String, Object> map = null;
		try {
			map = reader.readValue(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

}