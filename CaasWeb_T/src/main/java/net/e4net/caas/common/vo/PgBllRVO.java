package net.e4net.caas.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.TbPgBllDVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class PgBllRVO extends TbPgBllDVO {
	private String codeNm;
}