package net.e4net.caas.common.service;

import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.inicis.inipay4.INIpay;
import com.inicis.inipay4.util.INIdata;

import net.e4net.caas.common.adapter.InipayRestAdapter;
import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.mapper.CmmCodeMapper;
import net.e4net.caas.common.mapper.PgInicisMapper;
import net.e4net.caas.common.util.DateUtils;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.common.vo.CmmnDetailCode;
import net.e4net.caas.common.vo.InipayApprovePVO;
import net.e4net.caas.common.vo.OprGrpVO;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.common.vo.dvo.TbDntPrsnDVO;
import net.e4net.caas.common.vo.dvo.TbInipayAvLstDVO;
import net.e4net.caas.common.vo.dvo.TbInipayCertLstDVO;
import net.e4net.caas.common.vo.dvo.TbPgBllDVO;
import net.e4net.caas.common.vo.dvo.TbPgFlngDVO;

/**
 * INIPAY 서비스
 *
 * @author djim
 * @since 2018.11.01
 */ 
@Service
public class PgInicisService extends AbstractDao {
    

	@Value("${inipay.billKeyUrl}") private String billKeyUrl;
	//빌링키 등록 결과  callback URL
	@Value("${inipay.returnUrl}") private String returnUrl;
	//이니페이 모듈이 있는 디렉토리
	@Value("${inipay.inipayHome}") private String inipayHome;
	//이니페이 로그레벨
	@Value("${inipay.logMode}") private String logMode;
	//이니페이 IP
	@Value("${inipay.inicisSubPgIP}") private String inicisSubPgIP;
	//가상계좌, 계좌이체 송금완료에 대한 결과를 받을 URL
	@Value("${inipay.P_NOTI_URL}") private String P_NOTI_URL;
	//인증결과 수신 URL
	@Value("${inipay.P_RETURN_URL}") private String P_RETURN_URL;
	//결제 URL
	@Value("${inipay.P_NEXT_URL}") private String P_NEXT_URL;
	//BASE HOST
	@Value("${callback.api.myHost}") private String host;
	
	//충전 처리 서비스
    @Autowired
    private FlngProcService flngProcService;
    
    //블록체인 서비스
    @Autowired
    private BlockchainService blockchainService;
    
    //이니시스 API 호출 어뎁터
    @Autowired
    private InipayRestAdapter inipayRestAdapter;
	
    @Autowired
    private PgInicisMapper pgInicisMapper;
    
    //공통 서비스
	@Autowired 
	private CmmService cmmService;
	
	@Autowired 
	private CmmCodeMapper cmmCodeMapper;
	
    /**
     * PG 빌링키 시퀀스 조회
     *
     * @return PG 빌링키 시퀀스 조회
     */
    public long selectTbPgBllSeq() {
        return pgInicisMapper.selectTbPgBllSeq();
    }
    
    /**
     * PG 빌링키 등록
     *
     * @param pvo PG 빌링키 등록 정보 
     * @return 등록결과
     */
    public int insertTbPgBll(TbPgBllDVO dvo) {
        return pgInicisMapper.insertTbPgBll(dvo);
    }
    
    /**
     * PG 빌링키 수정
     *
     * @param pvo PG 빌링키 수정 정보
     * @return 수정결과
     */
    public int updateTbPgBll(TbPgBllDVO dvo) {
        return pgInicisMapper.updateTbPgBll(dvo);
    }
    
    /**
     * PG 빌링결제(충전)
     * 
     * @param pvo PG 빌링결제(충전)
     * @return 등록결과
     */
    public int insertTbPgFlng(TbPgFlngDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("insertTbPgFlng", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = pgInicisMapper.insertTbPgFlng(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
        return rtnVal;
    }
    
    /**
     * PG 빌링결제(충전)
     * 
     * @param pvo PG 빌링결제(충전)
     * @return 수정결과
     */
    public int updateTbPgFlng(TbPgFlngDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("updateTbPgFlng", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = pgInicisMapper.updateTbPgFlng(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
        return rtnVal;
    }
    
    /**
     * PG 빌링결제(충전_토큰일련번호)
     * 
     * @param pvo PG 빌링결제(충전)
     * @return 수정결과
     */
    @Transactional()
    public int updateTbPgFlngForTknSq(TbPgFlngDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("updateTbPgFlngForTknSq", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = pgInicisMapper.updateTbPgFlngForTknSq(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
        return rtnVal;
    }
  
    /**
     * PG 빌링결제 시퀀스 조회
     *
     * @return PG 빌링결제 시퀀스 조회
     */
    public long selectTbPgFlngSeq() {
        return pgInicisMapper.selectTbPgFlngSeq();
    }
    
    /**
     * PG 일반결제 인증 일련번호 조회
     *
     * @return PG 일반결제 인증 일련번호 조회
     */
    public long selectTbInipayCertSeq() {
        return pgInicisMapper.selectTbInipayCertSeq();
    }
    
    /**
     * PG 일반결제 인증 등록
     *
     * @return int
     */
    public int insertTbInipayCertLst(TbInipayCertLstDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("insertTbInipayCertLst", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = pgInicisMapper.insertTbInipayCertLst(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
        return rtnVal;
    }

    /**
     * PG 일반결제 인증 갱신
     *
     * @return int
     */
    public int updateTbInipayCertLst(TbInipayCertLstDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("updateTbInipayCertLst", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = pgInicisMapper.updateTbInipayCertLst(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
    	
        return rtnVal;
    }
    
    /**
     * PG 일반결제 인증 조회
     */
    public TbInipayCertLstDVO selectTbInipayCertLst(long inipayCertSq) {
    	return pgInicisMapper.selectTbInipayCertLst(inipayCertSq);
    }
    	
    /**
     * PG 일반결제 결제 일련번호 조회
     *
     * @return PG 일반결제 결제 일련번호 조회
     */
    public long selectTbInipayAvSeq() {
        return pgInicisMapper.selectTbInipayAvSeq();
    }
    
    /**
     * PG 일반결제 결제 등록
     *
     * @return int
     */
    public int insertTbInipayAvLst(TbInipayAvLstDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("insertTbInipayAvLst", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = pgInicisMapper.insertTbInipayAvLst(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
    	
        return rtnVal;
    }

    /**
     * PG 일반결제 결제 등록
     *
     * @return int
     */
    public int updateTbInipayAvLst(TbInipayAvLstDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("updateTbInipayAvLst", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = pgInicisMapper.updateTbInipayAvLst(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
    	
        return rtnVal;
    }

    /**
     * PG 일반결제 결제 상태 조회
     *
     * @return String
     */
    public String selectTbInipayAvLstCnt(String stlmNtno) {
        return pgInicisMapper.selectTbInipayAvLstCnt(stlmNtno);
    }
    
	/**
	 * 빌링키 요청 페이지 (이니시스 호출)
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
    public String moveBillKeyReqPag(Model model, HttpServletRequest request) throws CaasException {
    	// 세션 정보 GET
    	UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String writerID = getLoginCnrSqString();
    	
    	StringBuilder queryString = null;
    	
    	try {
    		OprGrpVO oprGrp = cmmService.getOprGrpInfo(principal.getOprGrpId());
    		
    		//빌링키 일련번호 조회
    		long pgBllSeq = selectTbPgBllSeq();

        	//상점아이디
        	String mid = oprGrp.getPgBllMid();
        	//구매자명
    		String buyername = principal.getCnrNm();
    		//상품명
    		String goodname = oprGrp.getPgGoodsName();
    		//상품금액
    		String price = "0";
    		//상점 주문번호
    		String orderid = String.valueOf(pgBllSeq);
    		//상점 응답 URL 주소
    		String returnurl = this.host + this.returnUrl;
    		//타임스탬프
    		String timestamp = DateUtils.getNowDate("yyyyMMddhhmmss");
    		String merchantkey = oprGrp.getPgBillMkey();
    		String p_noti = "";
    		String hashdata = null;
    		
    		/**
    		 *  HASH DATA 생성
    		 *  
    		 *  */
    		// SHA-256 MessageDigest의 생성
    		MessageDigest mdSHA256 = MessageDigest.getInstance("SHA-256");
    		// 문자열 바이트로 메시지 다이제스트를 갱신
    		mdSHA256.update((mid + orderid + timestamp + merchantkey).getBytes("UTF-8"));
    		// 해시 계산 반환값은 바이트 배열
    		byte[] sha256Hash = mdSHA256.digest();
    		
    		// 바이트배열을 16진수 문자열로 변환하여 표시
            StringBuilder hexSHA256hash = new StringBuilder();
            for(byte b : sha256Hash) {
                String hexString = String.format("%02x", b);
                hexSHA256hash.append(hexString);
            }
            hashdata = hexSHA256hash.toString();
            
            queryString = new StringBuilder();
            queryString.append("mid=").append(mid).append("&buyername=").append(buyername);
            queryString.append("&goodname=").append(URLEncoder.encode(goodname, StandardCharsets.UTF_8.name())).append("&price=").append(price);
            queryString.append("&orderid=").append(orderid).append("&returnurl=").append(returnurl);
    		queryString.append("&timestamp=").append(timestamp).append("&period=").append("");
    		queryString.append("&p_noti=").append(p_noti).append(orderid).append("&hashdata=").append(hashdata);
    		
    		TbPgBllDVO dvo = new TbPgBllDVO();
    		dvo.setBllAuthSq       (pgBllSeq);
    		dvo.setOprGrpId        (principal.getOprGrpId());
    		dvo.setLnkAgency       ("01");
    		dvo.setLnkPrcRst       ("");
    		dvo.setLnkReqInfo      (queryString.toString());
    		dvo.setLnkRstInfo      ("");
    		dvo.setCnrSq           (principal.getCnrSq());
    		dvo.setBllKey          ("");
    		dvo.setCardCd          ("");
    		dvo.setCardNo          ("");
    		dvo.setCardKn          ("");
    		dvo.setFstRegUsid      (writerID);
    		dvo.setLstChUsid       (writerID);
    		
    		if ( insertTbPgBll(dvo) < 1) {
    			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
    		}
    	} catch ( Exception e ) {
    		error(e.getMessage(), e);
			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
    	}    	
    	return "redirect:https://inilite.inicis.com/inibill/inibill_card.jsp?"+queryString;
    }
    
    /**
     * 이니시스 빌링키 발급결과 CALLBACK 
     * @param request 
     * @param model
     * @return
     * @throws Exception
     */
    public ModelAndView inipayReturn(HttpServletRequest request, ModelMap model) throws CaasException {
    	//세션 GET
  		//UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String writerID = getLoginCnrSqString();
  		ModelAndView mav = new ModelAndView("common/getBllKeyResultPag");
  		
  		
  		try {

  	  		model.put("resultcode", request.getParameter("resultcode"));
  	  		model.put("resultmsg", new String(request.getParameter("resultmsg").getBytes("iso-8859-1"), "UTF-8"));
  	  		model.put("orderid", request.getParameter("orderid"));
  	  		model.put("billkey", request.getParameter("billkey"));
  	  		model.put("mid", request.getParameter("mid"));
  	  		model.put("tid", request.getParameter("tid"));
  	  		model.put("authkey", request.getParameter("authkey"));
  	  		model.put("cardcd", request.getParameter("cardcd"));
  	  		model.put("cardno", request.getParameter("cardno"));
  	  		model.put("p_noti", request.getParameter("p_noti"));
  	  		model.put("merchantreserved", request.getParameter("merchantreserved"));
  	  		model.put("data1", request.getParameter("data1"));
  	  		model.put("cardkind", request.getParameter("cardkind"));
  	  		model.put("pgauthdate", request.getParameter("pgauthdate"));
  	  		model.put("pgauthtime", request.getParameter("pgauthtime"));
  	  		
  	  		/*
  	  		 * 1 step 
  	  		 * 빌링 키 등록 결과 갱신
  	  		 */
  	  		TbPgBllDVO dvo = new TbPgBllDVO();
  	  		dvo.setBllAuthSq(Integer.parseInt(request.getParameter("p_noti")));
  	  		if( "00".equals(request.getParameter("resultcode"))) {
  	  			dvo.setLnkPrcRst("S");
  	  		} else {
  	  			dvo.setLnkPrcRst("F");
  	  			dvo.setRprsCardYn("N");
  	  		}
  	  		dvo.setLnkRstInfo(model.toString());
  	  		dvo.setBllKey(request.getParameter("billkey"));
  	  		dvo.setCardCd(request.getParameter("cardcd"));
  	  		dvo.setCardNo(request.getParameter("cardno"));
  	  		dvo.setCardKn(request.getParameter("cardkind"));
  	  		dvo.setLstChUsid(writerID);
  	  		
  			if ( updateTbPgBll(dvo) < 1) {
  				throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
  			}
  			
  			mav.addObject("process", "getBllKey");
  	  		mav.addObject("resultcode", request.getParameter("resultcode"));
  	  		mav.addObject("resultmsg", model.get("resultmsg"));
  		} catch (Exception e) {
  			error(e.getMessage(), e);
			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
  		}
  		
  		return mav;
    }
    
    
	 /**
	  *  이니시스 빌링 결제 요청
	  * @param request
	  * @param model
	  * @return
	  * @throws CaasException
	  */
    public ModelAndView inicisReqBill(HttpServletRequest request, ModelMap model) throws CaasException {
    	UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
 		ModelAndView mav = new ModelAndView("common/flngResultPag");
		String writerID = getLoginCnrSqString();
 		
 		try {
 			OprGrpVO oprGrp = cmmService.getOprGrpInfo(principal.getOprGrpId());
 			
 			/*
 			 * 1 step
 			 * 결제모듈 인스턴스 생성
 			 */
 	 		INIpay inipay = new INIpay();
 	 		INIdata data = new INIdata();
 	 		
 			/*
 			 * 2 step
 			 * 충전내역 일련번호 조회
 			 */
 	 		long tbPgFlngSeq = selectTbPgFlngSeq();
 	 		
 			/*
 			 * 3 step
 			 * 빌링키 정보조회 (BY 빌링인증일련번호)
 			 */
 	 		String bllKey = flngProcService.selectBllKey(Long.parseLong(request.getParameter("bllAuthSq")));
 	 		
 	 		data.setData("type"			, "reqrealbill");                           // 결제 type, 고정 
 	 		data.setData("inipayHome"	, this.inipayHome);                         // 이니페이가 설치된 절대경로
 	 		data.setData("logMode"		, this.logMode);							// logMode
 	 		data.setData("keyPW"		, oprGrp.getPgBllKeyPw());					// 키패스워드
 	 		data.setData("subPgip"		, this.inicisSubPgIP);						// Sub PG IP (고정)
 	 		data.setData("mid"			, oprGrp.getPgBllMid());					// 상점아이디
 	 		data.setData("uid"			, oprGrp.getPgBllMid());					// INIpay User ID ( 샘플에서 mid를 전송함 )
 	 		data.setData("goodname"		, oprGrp.getPgGoodsName());					// 상품명 (최대 40자)
 	 		data.setData("currency"		, "WON");									// 화폐단위
 	 		data.setData("price"		, request.getParameter("totalAmount"));		// 가격
 	 		data.setData("buyername"	, principal.getCnrNm());					// 구매자 (최대 15자)
 	 		data.setData("buyertel"		, principal.getHpno());						// 구매자이동전화
 	 		data.setData("buyeremail"	, principal.getMailAddr());					// 구매자이메일
 	 		data.setData("paymethod"	, "Card");									// 지불방법, 카드빌링
 	 		data.setData("billkey"		, bllKey);									// 빌링등록 키(빌키)
 	 		data.setData("moid"			, String.valueOf(tbPgFlngSeq));				// 상점 주문번호
 	 		data.setData("url"			, this.host );								// 홈페이지 주소(URL)
 	 		data.setData("uip"			, request.getRemoteAddr());					// IP Addr
 	 		data.setData("cardquota"	, "00");									// 할부 기간
 	 		data.setData("authentification", "00");									// 본인인증여부 00:본인인증(카드번호,유효기간,생년월일6자리,카드비번) 01: 비인증(카드번호,유효기간)
 	 		data.setData("crypto", "execure");                                  	// Extrus 암호화 모듈 적용(고정)
 	 		//data.setData("encodecharset","utf8"); 								// 가맹점서버 UTF-8인 경우
 	 		
 			/*
 			 * 4 step
 			 * 충전요청내역 적재
 			 */
 	 		TbPgFlngDVO dvo = new TbPgFlngDVO();
 	 		dvo.setFlngItmSq(tbPgFlngSeq);
 	 		dvo.setOprGrpId(principal.getOprGrpId());
 			dvo.setLnkAgency("01");
 			dvo.setLnkReqInfo(data.toString());
 			dvo.setCnrSq(principal.getCnrSq());
 			dvo.setFlngAmt(Long.parseLong(request.getParameter("totalAmount")));
 			dvo.setStlmMd("01");
 			dvo.setStmt("01");
 			dvo.setStlmKn("01");
 			dvo.setFstRegUsid(writerID);
 			
 			/*
 			 * 5 step
 			 * 결제 요청
 			 */
 	 		if ( insertTbPgFlng(dvo) < 1 ) {
 	 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
 	 		}
 
 	 		data = inipay.payRequest(data);
 	 		  
 			/*
 			 * 6 step
 			 * 결제 요청결과 갱신
 			 */
 	 		String resultCode 	= data.getData("ResultCode"		);     // "00"이면 신용카드 빌링요청 성공
 	 		String resultMsg  	= data.getData("ResultMsg"		);     // 결과에 대한 설명
 	 		String authCode   	= data.getData("CardAuthCode"	);	   // 승인번호
 	 		String pgAuthDate 	= data.getData("PGauthdate"		);     // 이니시스 승인날짜(YYYYMMDD)
 	 		String pgAuthTime 	= data.getData("PGauthtime"		);     // 이니시스 승인시간(HHMMSS)
 	 		String tid        	= data.getData("tid"			);     // 거래번호
 	 		
 	 		model.put("resultcode"	, resultCode);
 	 		model.put("resultmsg"	, resultMsg	);
 	 		model.put("orderid"		, authCode	);
 	 		model.put("billkey"		, pgAuthDate);
 	 		model.put("mid"			, pgAuthTime);
 	 		model.put("tid"			, tid		);
 	 		dvo.setLnkRstInfo(model.toString());
 	 		dvo.setLstChUsid(writerID);
 	  		if( "00".equals(resultCode)) {
 	  			dvo.setLnkPrcRst("S");
 	  		} else {
 	  			dvo.setLnkPrcRst("F");
 	  		}
 	  		
 	 		if ( updateTbPgFlng(dvo) < 1 ) {
 	 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
 	 		}
 	 		
 	 		
 	  		if( "00".equals(resultCode)) {
 	 			/*
 	 			 * 7 step
 	 			 * 토큰충전 요청
 	 			 */
				BlockchainRetrunVO blockchainRetrunVO = blockchainService.walletTokenDeposit(principal.getOprGrpId(), principal.getWltAddr(), Long.parseLong(request.getParameter("totalAmount")), "21");
		 		//BlockchainTokenTxVO blockchainTokenTxVO = blockchainService.tokenTransferB2C(principal.getOprGrpId(), "0x8867eaca298d95f7be7f9231f69b35135deda362", Long.parseLong(request.getParameter("totalAmount")), "21", String.valueOf(principal.getCnrSq()));
				if ( ! blockchainRetrunVO.isSuccess()) {
					throw new CaasException( blockchainRetrunVO.getErrCd(), blockchainRetrunVO.getErrMsg());
				}
				
				
				dvo.setToknTrnfSq(blockchainRetrunVO.getToknTrnfSq());
		 		if ( updateTbPgFlngForTknSq(dvo) < 1 ) {
		 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
		 		}	
 	  		} 

 			mav.addObject("process", "reqBll");
 	  		mav.addObject("resultcode", resultCode);
 	  		mav.addObject("resultmsg", resultMsg);
 		} catch (Exception e) {
  			error(e.getMessage(), e);
			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
		}

  		
 		return mav;
    }
    
    /**
     * 
     * @param redirectAttributes
     * @param request
     * @return
     * @throws CaasException
     */
    public String moveIniPayMobile(RedirectAttributes redirectAttributes, HttpServletRequest request) throws CaasException {
    	UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		String writerID = getLoginCnrSqString();

 	    String method = request.getParameter("bllMethod") + "/";
 	    String stlmCd = "wcard".equals(request.getParameter("bllMethod")) ? "card" : request.getParameter("bllMethod");
 	    StringBuilder queryString = null;
 	    try {
 	    	OprGrpVO oprGrp = cmmService.getOprGrpInfo(principal.getOprGrpId());
 	    	
 			/*
 			 * 1 step
 			 * 이니페이 인증 내역 일련번호 조회
 			 */
 	 	    long inipayCertSeq = selectTbInipayCertSeq();
 	 	   
 			/*
 			 * 2 step
 			 * 이니페이 결제 창 파라미터 생성
 			 */
 	 	    queryString = new StringBuilder();
 	 	    queryString.append("&P_UNAME=").append(URLEncoder.encode(principal.getCnrNm(), "EUC-KR"))
 	   			  .append("&P_GOODS=").append(URLEncoder.encode(oprGrp.getPgGoodsName(), "EUC-KR"));
 	 	    redirectAttributes.addAttribute("P_MID", oprGrp.getPgNormMid());
 	 	    redirectAttributes.addAttribute("P_OID", inipayCertSeq);
 	 	    redirectAttributes.addAttribute("P_AMT", request.getParameter("totalAmount"));
 	 	    redirectAttributes.addAttribute("P_NEXT_URL", this.host + P_NEXT_URL);
 	 	    redirectAttributes.addAttribute("P_NOTI_URL", this.host + P_NOTI_URL);
 	 	    redirectAttributes.addAttribute("P_RETURN_URL", this.host +  P_RETURN_URL);
 	 	    redirectAttributes.addAttribute("P_RESERVED", "twotrs_isp=Y& block_isp=Y& twotrs_isp_noti= N&apprun_check=Y ");
 	 	    redirectAttributes.addAttribute("P_CHARSET", "utf8");
 	 	    redirectAttributes.addAttribute("P_NOTI", inipayCertSeq);
 	 	    
 	 	    TbInipayCertLstDVO dvo = new TbInipayCertLstDVO();
 	 	    dvo.setInipayCertSq(inipayCertSeq);
 	 	    dvo.setInipayStlmCd(stlmCd.toUpperCase());
 	 	    dvo.setStlmTtAmt(Long.parseLong(request.getParameter("totalAmount")));
 	 	    dvo.setPrdtNm(oprGrp.getPgGoodsName());
 	 	    dvo.setCnrSq(principal.getCnrSq());
 	 	    dvo.setCnrNm(principal.getCnrNm());
 	 	    dvo.setNotiUrl(this.host + P_NOTI_URL);
 	 	    dvo.setReturnUrl(this.host + P_RETURN_URL);
 	 	    dvo.setNxtUrl(this.host + P_NEXT_URL);
 	 	    dvo.setFstRegUsid(writerID);
 	 	    dvo.setLstChUsid(writerID);
 	 	    
 	 	    
 			/*
 			 * 3 step
 			 * 이니페이 결제 인증 요청 등록
 			 */
 	 	    if ( insertTbInipayCertLst(dvo) < 1) {
 	 	    	throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
 	 	    }
 	    } catch ( Exception e ) {
 	    	error(e.getMessage(), e);
			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
 	    }
 	    
 	    return "redirect:https://mobile.inicis.com/smart/"+method+"?" + queryString;
    }
    
    /**
     * 
     * @param request
     * @return
     * @throws CaasException
     */
    public ModelAndView iniPayNextUrl(HttpServletRequest request) throws CaasException{
    	UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
 		ModelAndView mav = new ModelAndView("common/flngResultPag");
		String writerID = getLoginCnrSqString();
 		
 		try {
 			OprGrpVO oprGrp = cmmService.getOprGrpInfo(principal.getOprGrpId());
 			
	 		debug("P_STATUS : " + request.getParameter("P_STATUS"));
	 		debug("P_RMESG1 : " + request.getParameter("P_RMESG1"));
	 		debug("P_TID : " + request.getParameter("P_TID"));
	 		debug("P_REQ_URL : " + request.getParameter("P_REQ_URL"));
	 		debug("P_NOTI : " + request.getParameter("P_NOTI"));
	 		debug("P_AMT  : " + request.getParameter("P_AMT"));
	 		debug("P_VACT_NUM  : " + request.getParameter("P_VACT_NUM"));
	 		debug("P_VACT_DATE  : " + request.getParameter("P_VACT_DATE"));
	 		debug("P_VACT_TIME  : " + request.getParameter("P_VACT_TIME"));
	 		debug("P_VACT_NAME  : " + request.getParameter("P_VACT_NAME"));
	 		debug("P_VACT_BANK_CODE  : " + request.getParameter("P_VACT_BANK_CODE"));
	 		debug("P_TYPE  : " + request.getParameter("P_TYPE"));
	 		String P_TID = request.getParameter("P_TID");
	 		String P_MID = oprGrp.getPgNormMid();
	 		String P_NOTI = request.getParameter("P_NOTI");
	 		
			/*
			 * 0 step
			 * 이니페이 결제 인증 정보 조회
			 */
	 		TbInipayCertLstDVO dvo = selectTbInipayCertLst(Long.parseLong(P_NOTI));
	 		
	 		
	 		
 			/*
 			 * 1 step
 			 * 이니페이 결제 인증 정보 갱신
 			 */
	 		dvo.setAvUrl(request.getParameter("P_REQ_URL"));
	 		dvo.setRspnCd(request.getParameter("P_STATUS"));
	 		dvo.setRspnMsg(request.getParameter("P_RMESG1"));
	 		dvo.setStlmNtno(P_TID);
	 	    dvo.setLstChUsid(writerID);
	 		if ( updateTbInipayCertLst(dvo) < 1) {
	 	    	throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	 	    }
 	
	 		//인증결과가 정상이고 카드결제일때만
	 		if ( "00".equals(request.getParameter("P_STATUS"))) {
	 	 		InipayApprovePVO pvo = new InipayApprovePVO();
	 	 		pvo.setPTid(P_TID);
	 	 		pvo.setPMid(P_MID);
	 	 	    dvo.setLstChUsid(writerID);
	 	 		TbInipayAvLstDVO avDvo = new TbInipayAvLstDVO();
	 	 		avDvo.setInipayAvSq( selectTbInipayCertSeq() );
	 	 		avDvo.setInipayMrchId(P_MID);
	 	 		avDvo.setInipayCertSq(Long.valueOf(P_NOTI));
	 	 		avDvo.setFstRegUsid(writerID);
	 	 		avDvo.setLstChUsid(writerID);
	 	 		if ( insertTbInipayAvLst(avDvo) < 1) {
	 	 	    	throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	 	 	    }
	 	 		
	 	 		//요청내역적재
	 	 		long tbPgFlngSeq = selectTbPgFlngSeq();
	 	 		TbPgFlngDVO flngDvo = new TbPgFlngDVO();
	 	 		flngDvo.setFlngItmSq(tbPgFlngSeq);
	 	 		flngDvo.setOprGrpId(principal.getOprGrpId());
	 	 		flngDvo.setLnkAgency("01");
	 	 		flngDvo.setLnkReqInfo(pvo.toString());
	 	 		flngDvo.setCnrSq(principal.getCnrSq());
	 	 		flngDvo.setFlngAmt(Long.parseLong(request.getParameter("P_AMT")));
	 	 		flngDvo.setStlmMd("01");
	 	 		flngDvo.setStmt("02");
	 	 		flngDvo.setStlmKn("01");
	 	 		flngDvo.setFstRegUsid(writerID);
	 			
	 	 		if ( insertTbPgFlng(flngDvo) < 1 ) {
	 	 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	 	 		}
	 	 		
	 	 		Map<String, String> result = inipayRestAdapter.post(request.getParameter("P_REQ_URL"), pvo);
				
				
				avDvo.setInipayStlmCd       (result.get("P_TYPE"));
				avDvo.setStlmNtno 			(result.get("P_TID"));
				avDvo.setAvDt               (result.get("P_AUTH_DT"));
				avDvo.setStlmTtAmt          (Long.parseLong(StringUtils.nvl(result.get("P_AMT"), "0")));
				avDvo.setCnrSq              (principal.getCnrSq());
				avDvo.setCnrNm              (result.get("P_UNAME"));
				avDvo.setRspnCd             (result.get("P_STATUS"));
				avDvo.setRspnMsg            (result.get("P_RMESG1"));
				avDvo.setNotiUrl            (result.get("P_NOTEURL"));
				avDvo.setReturnUrl          (result.get("P_RETURN_URL"));
				avDvo.setNxtUrl             (result.get("P_NEXT_URL"));
				avDvo.setCardNo             (result.get("P_CARD_NUM"));
				avDvo.setIssrCd             (result.get("P_CARD_ISSUER_CODE"));
				avDvo.setAcrMcno            (result.get("P_CARD_MEMBER_NUM"));
				avDvo.setAcrCd              (result.get("P_CARD_PURCHASE_CODE"));
				avDvo.setPorCancPosbYn      (result.get("P_CARD_PRTC_CODE"));
				avDvo.setIfimYn             (result.get("P_CARD_INTEREST"));
				avDvo.setChkCardYn          (result.get("P_CARD_CHECKFLAG"));
				if ( StringUtils.isNullOrBlank(result.get("P_RMESG2")) ) {
					avDvo.setIsMcnt             (0);
				} else {
					avDvo.setIsMcnt             (Integer.parseInt(StringUtils.nvl(result.get("P_RMESG2"), "0")));
				}
				avDvo.setCardcCd            (result.get("P_FN_CD1"));
				avDvo.setFcEngCd            (result.get("P_FN_CD2"));
				avDvo.setAvNo               (result.get("P_AUTH_NO"));
				avDvo.setVpCardCd           (result.get("P_ISP_CARDCODE"));
				avDvo.setFcKorNm            (result.get("P_FN_NM"));
				avDvo.setEvntCd             (result.get("P_EVENT_CODE"));
				avDvo.setMbphCtcoCd         (result.get("P_HPP_CORP"));
				avDvo.setStlmMbphNo         (result.get("P_HPP_NUM"));
				avDvo.setEwalletTypeCd      (result.get("P_EWALLET_TYPE"));
				avDvo.setAppLnkgCd          (result.get("P_SRC_CODE"));
				avDvo.setCashRcepStatCd     (result.get("P_CSHR_CODE"));
				avDvo.setCashRcepStatMsg    (result.get("P_CSHR_MSG"));
				avDvo.setCashRcepTtAmt      (Long.parseLong(StringUtils.nvl(result.get("P_CSHR_AMT"), "0")));
				avDvo.setCashRcepVrspAmt    (Long.parseLong(StringUtils.nvl(result.get("P_CSHR_SUP_AMT"), "0")));
				avDvo.setCashRcepTaxAmt     (Long.parseLong(StringUtils.nvl(result.get("P_CSHR_TAX"), "0")));
				avDvo.setCashRcepSrfeAmt    (Long.parseLong(StringUtils.nvl(result.get("P_CSHR_SRVC_AMT"), "0")));
				avDvo.setCashRcepTypeCd     (result.get("P_CSHR_TYPE"));
				avDvo.setCashRcepIsDttm     (result.get("P_CSHR_DT"));
				avDvo.setCashRcepIsNo       (result.get("P_CSHR_AUTH_NO"));
				avDvo.setVaccNo             (result.get("P_VACT_NUM"));
				avDvo.setRcptEndDt          (result.get("P_VACT_DATE"));
				avDvo.setRcptEndTm          (result.get("P_VACT_TIME"));
				avDvo.setAcowNm             (result.get("P_VACT_NAME"));
				avDvo.setVaccBankCd         (result.get("P_VACT_BANK_CODE"));
				avDvo.setFstRegUsid         (writerID);
				avDvo.setLstChUsid          (writerID);
				
	 	 		if ( updateTbInipayAvLst(avDvo) < 1) {
	 	 	    	throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	 	 	    }

	 	  		if ( "CARD".equals(avDvo.getInipayStlmCd()) ) {
	 	  			flngDvo.setLnkRstInfo(result.toString());
	 	  			flngDvo.setLstChUsid(writerID);
	 	  	  		if( "00".equals(avDvo.getRspnCd())) {
	 	  	  			flngDvo.setLnkPrcRst("S");
	 	  	  		} else {
	 	  	  			flngDvo.setLnkPrcRst("F");
	 	  	  		}
	 	  	  		
	 	  	 		if ( updateTbPgFlng(flngDvo) < 1 ) {
	 	  	 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	 	  	 		}
	 	  	 		
	 	  	 		//성공시
	 	  	  		if( "00".equals(avDvo.getRspnCd())) {
						BlockchainRetrunVO blockchainRetrunVO = blockchainService.walletTokenDeposit(principal.getOprGrpId(), principal.getWltAddr(), avDvo.getStlmTtAmt(), "21");
				 		//BlockchainTokenTxVO blockchainTokenTxVO = blockchainService.tokenTransferB2C(principal.getOprGrpId(), "0x8867eaca298d95f7be7f9231f69b35135deda362", Long.parseLong(request.getParameter("totalAmount")), "21", String.valueOf(principal.getCnrSq()));
						if ( ! blockchainRetrunVO.isSuccess()) {
							throw new CaasException( blockchainRetrunVO.getErrCd(), blockchainRetrunVO.getErrMsg());
						}
						
						
						flngDvo.setToknTrnfSq(blockchainRetrunVO.getToknTrnfSq());
				 		if ( updateTbPgFlngForTknSq(flngDvo) < 1 ) {
				 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
				 		}	
	 	  	  		} 
	 	  	  		
	 	  	  		
	 	  	  		
	 	  	  		
	 	 			mav.addObject("process", "reqIniPay");
	 	 	  		mav.addObject("resultcode",  request.getParameter("P_STATUS"));
	 	 	  		mav.addObject("resultmsg",  request.getParameter("P_RMESG1"));
	 	  		} else if ( "VBANK".equals(avDvo.getInipayStlmCd()) ) {
	 	 			mav.addObject("process"		, "reqIniPay");
	 	 	  		mav.addObject("resultcode"	,  request.getParameter("P_STATUS"));
	 	 	  		mav.addObject("resultmsg"	,  request.getParameter("P_RMESG1"));
	 	 	  		mav.addObject("vaccBankCd"	,  result.get("P_VACT_BANK_CODE"));
	 	 	  		List<CmmnDetailCode> bankLst= cmmCodeMapper.selectCmmCodeDetail("BNK_CD");
	 	 	  		String bankNm = "";
	 	 	  		for ( int i=0; i<bankLst.size(); i++) {
	 	 	  			if ( result.get("P_VACT_BANK_CODE").equals(bankLst.get(i).getCode()) ) {
	 	 	  				bankNm = bankLst.get(i).getCodeNm();
	 	 	  			}
	 	 	  		}
	 	 	  		mav.addObject("vaccBankNmLst",  bankLst);
	 	 	  		mav.addObject("bankNm"		, bankNm);
	 	 	  		mav.addObject("vaccNo"		,  result.get("P_VACT_NUM"));
	 	 	  		mav.addObject("rcptEndDt"	,  result.get("P_VACT_DATE"));
	 	 	  		mav.addObject("rcptEndTm"	,  result.get("P_VACT_TIME"));
	 	 	  		mav.addObject("acowNm"		,  result.get("P_VACT_NAME"));
	 	 	  		mav.addObject("rcptEndDttm" ,  DateUtils.format(result.get("P_VACT_DATE")+result.get("P_VACT_TIME"), "yyyyMMddHHmmss", "yyyy-MM-dd HH:mm:ss") );
	 	 	  		mav.setViewName("common/vactResultPag");
	 	  		}
	 		}
 		} catch (Exception e) {
 			error(e.getMessage(), e);
 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
		}
 	    return mav;
    }
    
    
    /**
     * 
     * @param request
     * @return
     */
    public String iniPayNotiUrl(HttpServletRequest request){
		String writerID = getLoginCnrSqString();
    	try {

	 		debug("*******************ini_pay_noti_url*******************");
	 		debug("P_TID   	  : " + request.getParameter("P_TID"));   
	 		debug("P_MID      : " + request.getParameter("P_MID"));   
	 		debug("P_AUTH_DT  : " + request.getParameter("P_AUTH_DT")); 
	 		debug("P_STATUS   : " + request.getParameter("P_STATUS"));  
	 		debug("P_TYPE     : " + request.getParameter("P_TYPE"));    
	 		debug("P_OID      : " + request.getParameter("P_OID"));     
	 		debug("P_FN_CD1   : " + request.getParameter("P_FN_CD1"));  
	 		debug("P_FN_CD2   : " + request.getParameter("P_FN_CD2"));  
	 		debug("P_FN_NM    : " + request.getParameter("P_FN_NM"));
	 		debug("P_UNAME    : " + request.getParameter("P_UNAME"));
	 		debug("P_AMT      : " + request.getParameter("P_AMT"));     
	 		debug("P_RMESG1   : " + request.getParameter("P_RMESG1"));
	 		debug("P_RMESG2   : " + request.getParameter("P_RMESG2"));  
	 		debug("P_NOTI     : " + request.getParameter("P_NOTI"));    
	 		debug("P_AUTH_NO  : " + request.getParameter("P_AUTH_NO")); 
	
			/*
			 * 0 step
			 * 이니페이 결제 인증 정보 조회
			 */
	 		TbInipayCertLstDVO dvo = selectTbInipayCertLst(Long.parseLong(request.getParameter("P_NOTI")));
	 		
	 		if (dvo == null) {
	 			error(" 인증되지 않은 거래 (인증번호): " + request.getParameter("P_NOTI"));
	 			return "FAIL";
	 		}
	 	
	 		
	 		//계좌이체
	 		if ( "BANK".equals( request.getParameter("P_TYPE")) ){
	 			
				/*
				 * 1 step
				 * 정상결제건이 있는지 확인 후 있으면 RETURN OK 이니시스에서 reTry를 할수도 있음
				 */
		
		 		if ( selectTbInipayAvLstCnt(request.getParameter("P_TID")) != null ) {
		 			error(" 이미처리 된 거래 " + request.getParameter("P_TID"));
		 			return "OK";
		 		}
		 		
	 			/*
	 			 * 2 step
	 			 * 이니페이 결제 인증 정보 갱신
	 			 */
	 	 		dvo.setAvUrl(request.getParameter("P_REQ_URL"));
	 	 		dvo.setRspnCd(request.getParameter("P_STATUS"));
	 	 		dvo.setRspnMsg(request.getParameter("P_RMESG1"));
	 	 		dvo.setStlmNtno(request.getParameter("P_TID"));
	 	 	    dvo.setLstChUsid(getLoginCnrSqString());
	 	 	    
	 	 		if ( updateTbInipayCertLst(dvo) < 1) {
	 	 			return "FAIL";
	 	 	    }
	 	 		
	 		//가상계좌이체
	 		} else if ( "VBANK".equals( request.getParameter("P_TYPE")) ){

	 			//입금통보 "02" 가 아니면(가상계좌 채번 : 00 또는 01 경우) 채번정보일경우는 DB 작업  후 갱신
	 			if(request.getParameter("P_STATUS").equals("02")) {
			 		if ( "02".equals(selectTbInipayAvLstCnt(request.getParameter("P_TID")))) {
			 			error(" 이미처리 된 거래 " + request.getParameter("P_TID"));
			 			return "OK";
			 		}
	 			} else {
	 				return "OK"; 
	 			}
	 		}
	
	 		
			/*
			 * 3 step
			 * 충전내역 등록
			 */
	 		//요청내역적재
	 		long tbPgFlngSeq = selectTbPgFlngSeq();
	 		TbPgFlngDVO flngDvo = new TbPgFlngDVO();
	 		flngDvo.setFlngItmSq(tbPgFlngSeq);
	 		flngDvo.setOprGrpId(getLoginOprGrpId());	// 운영단체ID (로그인 세션)
	 		flngDvo.setLnkAgency("01");
	 		flngDvo.setLnkReqInfo("");
	 		flngDvo.setCnrSq(getLoginCnrSq());			// (로그인 세션)
	 		flngDvo.setFlngAmt(Long.parseLong(request.getParameter("P_AMT")));
	 		flngDvo.setStlmMd("01");
	 		flngDvo.setStmt("02");
	 		if ( "VBANK".equals( request.getParameter("P_TYPE")) ){
	 			flngDvo.setStlmKn("03");
	 		} else if ( "BANK".equals( request.getParameter("P_TYPE")) ){
	 			flngDvo.setStlmKn("02");
	 		}
	 		flngDvo.setLnkPrcRst("S");
	 		flngDvo.setLnkRstInfo(request.getQueryString());
	 		flngDvo.setFstRegUsid(writerID);
	 		flngDvo.setLstChUsid(writerID);
	 		
	 		if ( insertTbPgFlng(flngDvo) < 1 ) {
	 			return "FAIL";
	 		}
		 		
			/*
			 * 4 step
			 * 이니페이 결제 승인정보 등록
			 */
	 		TbInipayAvLstDVO avDvo = new TbInipayAvLstDVO();
	 		avDvo.setInipayAvSq(selectTbInipayCertSeq());
	 		avDvo.setInipayMrchId(request.getParameter("P_MID"));
	 		avDvo.setInipayCertSq(Long.valueOf(request.getParameter("P_NOTI")));
			avDvo.setInipayStlmCd       (request.getParameter("P_TYPE"));
			avDvo.setStlmNtno 			(request.getParameter("P_TID"));
			avDvo.setAvDt               (request.getParameter("P_AUTH_DT"));
			avDvo.setStlmTtAmt          (Long.parseLong(StringUtils.nvl(request.getParameter("P_AMT"), "0")));
			avDvo.setCnrSq              (dvo.getCnrSq());
			avDvo.setCnrNm              (request.getParameter("P_UNAME"));
			avDvo.setRspnCd             (request.getParameter("P_STATUS"));
			avDvo.setRspnMsg            (request.getParameter("P_RMESG1"));
			avDvo.setNotiUrl            (request.getParameter("P_NOTEURL"));
			avDvo.setReturnUrl          (request.getParameter("P_RETURN_URL"));
			avDvo.setNxtUrl             (request.getParameter("P_NEXT_URL"));
			avDvo.setCardNo             (request.getParameter("P_CARD_NUM"));
			avDvo.setIssrCd             (request.getParameter("P_CARD_ISSUER_CODE"));
			avDvo.setAcrMcno            (request.getParameter("P_CARD_MEMBER_NUM"));
			avDvo.setAcrCd              (request.getParameter("P_CARD_PURCHASE_CODE"));
			avDvo.setPorCancPosbYn      (request.getParameter("P_CARD_PRTC_CODE"));
			avDvo.setIfimYn             (request.getParameter("P_CARD_INTEREST"));
			avDvo.setChkCardYn          (request.getParameter("P_CARD_CHECKFLAG"));
			if ( StringUtils.isNullOrBlank(request.getParameter("P_RMESG2")) ) {
				avDvo.setIsMcnt             (0);
			} else {
				avDvo.setIsMcnt             (Integer.parseInt(StringUtils.nvl(request.getParameter("P_RMESG2"), "0")));
			}
			avDvo.setCardcCd            (request.getParameter("P_FN_CD1"));
			avDvo.setFcEngCd            (request.getParameter("P_FN_CD2"));
			avDvo.setAvNo               (request.getParameter("P_AUTH_NO"));
			avDvo.setVpCardCd           (request.getParameter("P_ISP_CARDCODE"));
			avDvo.setFcKorNm            (request.getParameter("P_FN_NM"));
			avDvo.setEvntCd             (request.getParameter("P_EVENT_CODE"));
			avDvo.setMbphCtcoCd         (request.getParameter("P_HPP_CORP"));
			avDvo.setStlmMbphNo         (request.getParameter("P_HPP_NUM"));
			avDvo.setEwalletTypeCd      (request.getParameter("P_EWALLET_TYPE"));
			avDvo.setAppLnkgCd          (request.getParameter("P_SRC_CODE"));
			avDvo.setCashRcepStatCd     (request.getParameter("P_CSHR_CODE"));
			avDvo.setCashRcepStatMsg    (request.getParameter("P_CSHR_MSG"));
			avDvo.setCashRcepTtAmt      (Long.parseLong(StringUtils.nvl(request.getParameter("P_CSHR_AMT"), "0")));
			avDvo.setCashRcepVrspAmt    (Long.parseLong(StringUtils.nvl(request.getParameter("P_CSHR_SUP_AMT"), "0")));
			avDvo.setCashRcepTaxAmt     (Long.parseLong(StringUtils.nvl(request.getParameter("P_CSHR_TAX"), "0")));
			avDvo.setCashRcepSrfeAmt    (Long.parseLong(StringUtils.nvl(request.getParameter("P_CSHR_SRVC_AMT"), "0")));
			avDvo.setCashRcepTypeCd     (request.getParameter("P_CSHR_TYPE"));
			avDvo.setCashRcepIsDttm     (request.getParameter("P_CSHR_DT"));
			avDvo.setCashRcepIsNo       (request.getParameter("P_CSHR_AUTH_NO"));
			avDvo.setVaccNo             (request.getParameter("P_VACT_NUM"));
			avDvo.setRcptEndDt          (request.getParameter("P_VACT_DATE"));
			avDvo.setRcptEndTm          (request.getParameter("P_VACT_TIME"));
			avDvo.setAcowNm             (request.getParameter("P_VACT_NAME"));
			avDvo.setVaccBankCd         (request.getParameter("P_VACT_BANK_CODE"));
			avDvo.setFstRegUsid         (writerID);
			avDvo.setLstChUsid          (writerID);
	 		if ( insertTbInipayAvLst(avDvo) < 1) {
	 			return "FAIL"; 
	 	    }
	 		
	 		TbDntPrsnDVO tbDntPrsnDVO = pgInicisMapper.selectTbDntPrsn(dvo.getCnrSq());
	 		
			BlockchainRetrunVO blockchainRetrunVO = blockchainService.walletTokenDeposit(tbDntPrsnDVO.getOprGrpId(), tbDntPrsnDVO.getWltAddr(), avDvo.getStlmTtAmt(), "21");
	 		//BlockchainTokenTxVO blockchainTokenTxVO = blockchainService.tokenTransferB2C(principal.getOprGrpId(), "0x8867eaca298d95f7be7f9231f69b35135deda362", Long.parseLong(request.getParameter("totalAmount")), "21", String.valueOf(principal.getCnrSq()));
			if ( ! blockchainRetrunVO.isSuccess()) {
				throw new CaasException( blockchainRetrunVO.getErrCd(), blockchainRetrunVO.getErrMsg());
			}
			
			
			flngDvo.setToknTrnfSq(blockchainRetrunVO.getToknTrnfSq());
	 		if ( updateTbPgFlngForTknSq(flngDvo) < 1 ) {
	 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	 		}

 		} catch ( Exception e ) {
 			error(e.getMessage(), e);
 			return "FAIL";
 		}
 		
 	    return "OK";
    }
    
    
    public ModelAndView iniPayReturnUrl(HttpServletRequest request){
		ModelAndView mav = new ModelAndView("common/bankResultPag");

		return mav;
    }
}
