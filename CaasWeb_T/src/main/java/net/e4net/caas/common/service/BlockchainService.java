package net.e4net.caas.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.base.BaseAbstractService;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.common.vo.BlockchainTokenTxVO;
import net.e4net.caas.common.vo.BlockchainWalletVO;

@Service
@Slf4j
public class BlockchainService extends BaseAbstractService {

	@Autowired BlockchainApiService bcApiService;

	/* ==================================================================================
	 * 블록체인 지갑 잔고 조회 API
	 */

	/**
	 * 지갑주소에 대한 잔고를 조회.
	 * @param oprGrpId 운영단체ID
	 * @param walletAddr 지갑주소
	 * @return retVo.isSuccess() : 성공여부, retVo.getBalance() : 잔고
	 */
	public BlockchainRetrunVO getWalletBalanceNew(String oprGrpId, String walletAddr) {
		BlockchainRetrunVO retVo = bcApiService.getWalletBalanceSub(oprGrpId, walletAddr);
		return retVo;
	}

	@Deprecated
	public long getWalletBalance(String oprGrpId, String walletAddr) {
		BlockchainRetrunVO retVo = getWalletBalanceNew(oprGrpId, walletAddr);
		return retVo.isSuccess() ? retVo.getBalance() : -1;
	}

	/* ==================================================================================
	 * 블록체인 토큰 전송 (이체) API
	 */
	/**
	 *	토큰입금(B2C) : 결제에 의한 토큰 입급 등에 사용 
	 *		운영단체-토큰 => 기부자-토큰, 가맹단체-토큰, 캠페인-토큰
	 *		운영단체지갑은 파라미터에서 제외
	 * @param oprGrpId 운영단체ID
	 * @param wltAddr 입금 지갑주소
	 * @param trnfTokn 이체 토큰 (금액 채리)
	 * @param trnfTp "21"(결제충전), "52"(Fiat정산취소)
	 * @return retVo.isSuccess() : 성공여부, retVo.getToknTrnfSq() : 토큰전송SQ
	 */
	public BlockchainRetrunVO walletTokenDeposit(String oprGrpId, String wltAddr, long trnfTokn, String trnfTp) {
		return walletTokenSub(oprGrpId, "B2C", null, wltAddr, trnfTokn, trnfTp);
	}
	@Deprecated
	public BlockchainTokenTxVO tokenTransferB2C(String oprGrpId, String wltAddr, long trnfTokn, String trnfTp, String userid) {
		BlockchainRetrunVO retVo = walletTokenDeposit(oprGrpId, wltAddr, trnfTokn, trnfTp);
		return retVo.isSuccess() ? (BlockchainTokenTxVO)retVo.getResult() : null; 
	}

	/**
	 *	토큰출금(C2B) : Fiat 정산에 의한 토큰 출금 등에 사용
	 *		운영단체-토큰 <= 기부자-토큰, 가맹단체-토큰, 캠페인-토큰
	 *		운영단체지갑은 파라미터에서 제외
	 * @param oprGrpId 운영단체ID
	 * @param wltAddr  출금 지갑주소
	 * @param trnfTokn 이체 토큰 (금액 채리)
	 * @param trnfTp "22"(결제취소), "51"(Fiat정산요청)
	 * @return
	 */
	public BlockchainRetrunVO walletTokenWithdraw(String oprGrpId, String wltAddr, long trnfTokn, String trnfTp) {
		return walletTokenSub(oprGrpId, "C2B", wltAddr, null, trnfTokn, trnfTp);
	}
	@Deprecated
	public BlockchainTokenTxVO tokenTransferC2B(String oprGrpId, String wltAddr, long trnfTokn, String trnfTp, String userid) {
		BlockchainRetrunVO retVo = walletTokenWithdraw(oprGrpId, wltAddr, trnfTokn, trnfTp);
		return retVo.isSuccess() ? (BlockchainTokenTxVO)retVo.getResult() : null; 
	}

	/**
	 *	토큰이체 : 기부자-토큰, 가맹단체-토큰, 캠페인-토큰 상호간
	 * @param oprGrpId 운영단체ID
	 * @param frmWltAddr 출금 지갑주소
	 * @param toWltAddr 입금 지갑주소
	 * @param trnfTokn 이체 토큰 (금액 채리)
	 * @param trnfTp "31"(기부전송), "32"(기부취소), "41"(정산요청), "42"(정산취소)
	 * @return
	 */
	public BlockchainRetrunVO walletTokenTransfer(String oprGrpId, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp) {
		return walletTokenSub(oprGrpId, "C2C", frmWltAddr, toWltAddr, trnfTokn, trnfTp);
	}
	@Deprecated
	public BlockchainTokenTxVO tokenTransferC2C(String oprGrpId, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp, String userid) {
		BlockchainRetrunVO retVo = walletTokenTransfer(oprGrpId, frmWltAddr, toWltAddr, trnfTokn, trnfTp);
		return retVo.isSuccess() ? (BlockchainTokenTxVO)retVo.getResult() : null; 
	}
	
	private BlockchainRetrunVO walletTokenSub(String oprGrpId, String transferType, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp) {
		BlockchainRetrunVO retVo = bcApiService.walletTokenSub(oprGrpId, transferType, frmWltAddr, toWltAddr, trnfTokn, trnfTp);

		if(retVo.isSuccess()) {
			// 이체 성공 후 DB 잔고를 갱신해 준다.
			log.debug("[walletTokenSub] 이체후 잔고 동기화 - Async 수행");
			bcApiService.asyncUpdateWalletBalance(oprGrpId, frmWltAddr, toWltAddr);				
		}
		return retVo;
	}

	/* ==================================================================================
	 * 블록체인 지갑생성 API - 운영단체만 제외하고 지갑이 생성된다.
	 */

	// 운엳단체/가맹단체/캠페인/ 지갑생성
	//	운영단체의 경우는 B2C, C2B 이여서 지갑이 미리 생성된다. 운영자에서 등록한다.
	//public BlockchainWalletVO apiNewWalletGrpOpr(String oprGrpId) {
	//	return apiNewWallet(oprGrpId, "01", oprGrpId);
	//}
	/**
	 * 가맹단체 지갑 만들기
	 * @param oprGrpId 운영단체ID
	 * @param mrchGrpId 가맹단체ID
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainRetrunVO createWalletGrpMctNew(String oprGrpId, String mrchGrpId) {
		return bcApiService.createWalletSub(oprGrpId, "02", mrchGrpId);
	}
	@Deprecated
	public BlockchainWalletVO createWalletGrpMct(String oprGrpId, String mrchGrpId, String userid) {
		BlockchainRetrunVO retVo = createWalletGrpMctNew(oprGrpId, mrchGrpId);
		return retVo.isSuccess() ? (BlockchainWalletVO)retVo.getResult() : null; 
	}

	/**
	 * 캠페인 지갑 만들기
	 * @param oprGrpId 운영단체ID
	 * @param mrchGrpId 가맹단체ID
	 * @param cmpSq 캠페인SQ
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainRetrunVO createWalletCmpInfNew(String oprGrpId, String mrchGrpId, long cmpSq) {
		String emailGubun = mrchGrpId + "_" + String.valueOf(cmpSq);
		return bcApiService.createWalletSub(oprGrpId, "03", emailGubun);
	}
	@Deprecated
	public BlockchainWalletVO createWalletCmpInf(String oprGrpId, String mrchGrpId, long cmpSq, String userid) {
		BlockchainRetrunVO retVo = createWalletCmpInfNew(oprGrpId, mrchGrpId, cmpSq);
		return retVo.isSuccess() ? (BlockchainWalletVO)retVo.getResult() : null; 
	}

	/**
	 * 기부자 지갑 생성
	 * @param oprGrpId 운영단체ID
	 * @param 기부자SQ
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainRetrunVO createWalletDntPrsnNew(String oprGrpId, long cnrSq) {
		return bcApiService.createWalletSub(oprGrpId, "04", String.valueOf(cnrSq));
	}
	@Deprecated
	public BlockchainWalletVO createWalletDntPrsn(String oprGrpId, long cnrSq, String userid) {
		BlockchainRetrunVO retVo = createWalletDntPrsnNew(oprGrpId, cnrSq);
		return retVo.isSuccess() ? (BlockchainWalletVO)retVo.getResult() : null; 
	}




}