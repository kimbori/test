package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = false)
public class TbRcntSrchDVO extends PageVO {
	private long rcntSrchSq;		//최근검색SEQ
	private String regDttm;			//등록일시
	private String srchKey;			//검색키
	private String fstRegDttm;		//최초등록일시
	private String fstRegUsid;		//최초등록자
	private String lstChDttm;		//최종변경일시
	private String lstChUsid;		//최종변경자
}