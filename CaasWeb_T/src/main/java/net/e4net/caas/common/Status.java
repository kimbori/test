package net.e4net.caas.common;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.util.LogObject;

/**
 * Status
 * @author djim
 * @since 2018.11.01
 * <pre>
 * history
 *    2018.11.01 최초생성
 * </pre>
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class Status extends LogObject implements Serializable {

	private static final long serialVersionUID = 4501821486633810324L;

	private static final int DEFAULT_OK_CODE = 0;
	private static final int DEFAULT_FAIL_CODE = -1;

	private static final String DEFAULT_OK_MESSAGE = "정상 처리했습니다.";
	private static final String DEFAULT_FAIL_MESSAGE = "오류가 발생했습니다.";

	private int code;

	private String message;

	public static Status ok() {
		return ok(DEFAULT_OK_CODE, DEFAULT_OK_MESSAGE);
	}

	public static Status ok(int code) {
		return ok(code, Status.DEFAULT_OK_MESSAGE);
	}

	public static Status ok(String message) {
		return ok(DEFAULT_OK_CODE, message);
	}

	public static Status ok(int code, String message) {
		return new Status(code, message);
	}

	public static Status fail() {
		return fail(DEFAULT_FAIL_CODE, DEFAULT_FAIL_MESSAGE);
	}

	public static Status fail(int code) {
		return fail(code, Status.DEFAULT_FAIL_MESSAGE);
	}

	public static Status fail(String message) {
		return fail(DEFAULT_FAIL_CODE, message);
	}

	public static Status fail(int code, String message) {
		return new Status(code, message);
	}

	public Status() {
		this.code = Status.DEFAULT_FAIL_CODE;
		this.message = Status.DEFAULT_FAIL_MESSAGE;
	}

	public Status(int code) {
		this.code = code;

		if (code < 0) {
			this.message = Status.DEFAULT_FAIL_MESSAGE;
		}
		else {
			this.message = Status.DEFAULT_OK_MESSAGE;
		}
	}

	public Status(int code, String message) {
		this.code = code;
		this.message = message;
	}

	@JsonIgnore
	public boolean isFail() {
		return (code < 0);
	}

	@JsonIgnore
	public boolean isOk() {
		return !isFail();
	}

	public void setOk() {
		this.setOk(Status.DEFAULT_OK_CODE, Status.DEFAULT_OK_MESSAGE);
	}

	public void setOk(int code) {
		this.setOk(code, Status.DEFAULT_OK_MESSAGE);
	}

	public void setOk(String message) {
		this.setOk(Status.DEFAULT_OK_CODE, message);
	}

	public void setOk(int code, String message) {
		this.code = code;
		this.message = message;
	}

	public void setFail() {
		this.setFail(Status.DEFAULT_FAIL_CODE, Status.DEFAULT_FAIL_MESSAGE);
	}

	public void setFail(int code) {
		this.setFail(code, Status.DEFAULT_FAIL_MESSAGE);
	}

	public void setFail(String message) {
		this.setFail(Status.DEFAULT_FAIL_CODE, message);
	}

	public void setFail(int code, String message) {
		this.code = code;
		this.message = message;
	}

}
