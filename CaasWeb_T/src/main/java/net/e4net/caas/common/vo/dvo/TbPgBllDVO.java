package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbPgBllDVO extends PageVO{
	private static final long serialVersionUID = 8227277655478063668L;

	private long bllAuthSq;
	private String oprGrpId;
	private String lnkAgency;
	private String lnkPrcDt;
	private String lnkPrcRst;
	private String lnkReqInfo;
	private String lnkRstInfo;
	private long cnrSq;
	private String bllKey;
	private String cardCd;
	private String cardNo;
	private String cardKn;
	private String fstRegDttm;
	private String fstRegUsid;
	private String lstChDttm;
	private String lstChUsid;
	private String rprsCardYn;
}
