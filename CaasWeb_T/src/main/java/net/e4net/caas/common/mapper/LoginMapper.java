package net.e4net.caas.common.mapper;

import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.UserVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author djim
 * @since 2018.11.01
 */
@Repository
public interface LoginMapper {
	
    /** 
     * USER 정보 조회 
     */	
    public UserVO selectUser(String cnrCi, String oprGrpId);
}
