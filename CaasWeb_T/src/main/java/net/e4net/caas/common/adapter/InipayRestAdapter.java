package net.e4net.caas.common.adapter;

import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.ByteArrayHttpMessageConverter;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.ResourceHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Component;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.util.LogObject;
import net.e4net.caas.common.vo.RestVO;

@Component
public class InipayRestAdapter extends LogObject {

	@SuppressWarnings("unused")
	private static final long serialVersionUID = 2919705640657954121L;

	protected ClientHttpRequestFactory factory = null;

	protected List<HttpMessageConverter<?>> messageConverters = null;

	/**
	 * RestTemplate Exchange
	 *
	 * @param url
	 * @param httpMethod
	 * @param request
	 * @param responseClass
	 * @param <T1>
	 * @param <T2>
	 * @return
	 * @throws MoneyCardException
	 */
	protected <T1 extends RestVO, T2 extends RestVO> Map<String, String> exchange(String url, HttpMethod httpMethod, T1 request) throws  CaasException, HttpServerErrorException, HttpClientErrorException, UnsupportedEncodingException {
		UriComponentsBuilder builder = UriComponentsBuilder.fromUriString(url);

		// GET이면 쿼리 파라미터를 추가한다.
		if (httpMethod == HttpMethod.GET) {
			builder.queryParams(convertParameter(request));
		}

		URI uri;

		uri = builder.build().encode(getCharset()).toUri();

		// RequestHeader 설정
		HttpHeaders requestHeaders = getRequestHeaders();

		RequestEntity<?> requestEntity;

		// GET이면 헤더만 추가한다.
		if (httpMethod == HttpMethod.GET) {
			requestEntity = new RequestEntity<>(null, requestHeaders, httpMethod, uri);
		}
		// 그 외에는 Body도 추가한다.
		else {
			if (requestHeaders.getContentType().includes(MediaType.APPLICATION_FORM_URLENCODED)) {
				requestEntity = new RequestEntity<>(convertParameter(request), requestHeaders, httpMethod, uri);

				// 메시지 컨버터 설정
				setFromMessageConverter();
			}
			else {
				requestEntity = new RequestEntity<>(request, requestHeaders, httpMethod, uri);
			}
		}

		return this.exchange(uri, httpMethod, requestEntity);
	}

	/**
	 * RestTemplate Exchange
	 *
	 * @param uri
	 * @param httpMethod
	 * @param requestEntity
	 * @param responseClass
	 * @param <T>
	 * @return
	 * @throws Exception
	 */
	protected Map<String, String> exchange(URI uri, HttpMethod httpMethod, HttpEntity<?> requestEntity) throws CaasException, HttpServerErrorException, HttpClientErrorException {
		RestTemplate restTemplate = new RestTemplate();
		HashMap<String, String> map = new HashMap<String, String>();
		// 메시지 컨버터 설정
		if (messageConverters != null) {
			restTemplate.setMessageConverters(messageConverters);
		}

		// 팩토리 설정
		if (factory == null) {
			setDefaultRequestFactory();
		}

		restTemplate.setRequestFactory(factory);

		debug("URI : [{}]", uri);
		debug("REQUEST ENTITY : [{}]", requestEntity.toString());

		try {
			// Charset 문제로 일단 문자열로 받는다.
			ResponseEntity<String> responseEntity = restTemplate.exchange(uri, httpMethod, requestEntity, String.class);

			String[] values = new String(responseEntity.getBody()).split("&");

			for (int x = 0; x < values.length; x++) {
               debug(values[x]);  // 승인결과를 출력
               int i = values[x].indexOf("=");

               map.put(values[x].substring(0, i), values[x].substring(i + 1));
               values[x].substring(0, i);
            }
			return map;
		} catch (HttpServerErrorException | HttpClientErrorException e) {
//			error(e.getMessage(), e);
			info("RESPONSE CODE [{}], RESPONSE BODY [{}]", e.getStatusCode().toString(), new String(e.getResponseBodyAsByteArray(), Charset.forName("UTF-8")));
			throw e;
		} catch (ResourceAccessException e) {
			// 타임아웃시 별도의 코드로 처리한다.
			error(e.getMessage(), e);
			throw new CaasException("91", e.getMessage(), e);
		} catch (Exception e) {
			error(e.getMessage(), e);
			throw new CaasException("99", e.getMessage(), e);
		}
	}

	/**
	 * RestTemplate Get
	 *
	 * @param url
	 * @param request
	 * @param responseClass
	 * @param <T1>
	 * @param <T2>
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public <T1 extends RestVO, T2 extends RestVO> Map<String, String> get(String url, T1 request) throws CaasException, HttpServerErrorException, HttpClientErrorException, UnsupportedEncodingException {
		return exchange(url, HttpMethod.GET, request);
	}

	/**
	 * RestTemplate Post
	 *
	 * @param url
	 * @param request
	 * @param responseClass
	 * @param <T1>
	 * @param <T2>
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public <T1 extends RestVO, T2 extends RestVO> Map<String, String> post(String url, T1 request) throws CaasException, HttpServerErrorException, HttpClientErrorException, UnsupportedEncodingException {
		return exchange(url, HttpMethod.POST, request);
	}

	/**
	 * RestTemplate Put
	 *
	 * @param url
	 * @param request
	 * @param responseClass
	 * @param <T1>
	 * @param <T2>
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public <T1 extends RestVO, T2 extends RestVO> Map<String, String> put(String url, T1 request) throws CaasException, HttpServerErrorException, HttpClientErrorException, UnsupportedEncodingException {
		return exchange(url, HttpMethod.PUT, request);
	}

	/**
	 * RestTemplate Delete
	 *
	 * @param url
	 * @param request
	 * @param responseClass
	 * @param <T1>
	 * @param <T2>
	 * @return
	 * @throws UnsupportedEncodingException 
	 * @throws Exception
	 */
	public <T1 extends RestVO, T2 extends RestVO> Map<String, String> delete(String url, T1 request) throws CaasException, HttpServerErrorException, HttpClientErrorException, UnsupportedEncodingException {
		return exchange(url, HttpMethod.DELETE, request);
	}



	/**
	 * VO를 MultiValueMap으로 변경한다.
	 *
	 * @param vo
	 * @param <T>
	 * @return
	 */
	protected <T extends RestVO> MultiValueMap<String, String> convertParameter(T vo) {
		return vo.convertMultiValueMap();
	}

	/**
	 * ContentType 설정<br/>
	 * 기본은 application/json; charset=UTF-8
	 *
	 * @return
	 */
	protected MediaType getContentType() {
		return MediaType.APPLICATION_FORM_URLENCODED;
	}

	/**
	 * Accept 설정<br/>
	 * 기본은 application/json; charset=UTF-8
	 *
	 * @return
	 */
	protected List<MediaType> getAccept() {
		return Arrays.asList(MediaType.TEXT_HTML);
		//return Collections.singletonList(MediaType.APPLICATION_FORM_URLENCODED);
	}

	/**
	 * Charset 설정<br/>
	 * 기본은 UTF-8
	 *
	 * @return
	 */
	protected Charset getCharset() {
		return Charset.forName("UTF-8");
	}

	/**
	 * RequestHeaders 설정
	 *
	 * @return
	 */
	protected HttpHeaders getRequestHeaders() {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(getContentType());
		requestHeaders.setAccept(getAccept());

		return requestHeaders;
	}

	/**
	 * MessageConverter 설정
	 */
	protected void setFromMessageConverter() {
		// StringConverter
		StringHttpMessageConverter stringHttpMessageConverter = new StringHttpMessageConverter(getCharset());
		stringHttpMessageConverter.setWriteAcceptCharset(true);

		// FormConverter
		List<HttpMessageConverter<?>> partConverters = new ArrayList<>();
		partConverters.add(new ByteArrayHttpMessageConverter());
		partConverters.add(stringHttpMessageConverter);
		partConverters.add(new ResourceHttpMessageConverter());

		FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
		formHttpMessageConverter.setCharset(getCharset());
		formHttpMessageConverter.setPartConverters(partConverters);

		List<HttpMessageConverter<?>> messageConverters = new ArrayList<>();
		messageConverters.add(formHttpMessageConverter);
		messageConverters.add(stringHttpMessageConverter);

		this.messageConverters = messageConverters;
	}

	/**
	 * DefaultRequestFactory 설정
	 */
	private void setDefaultRequestFactory() {
		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
		requestFactory.setConnectTimeout(10 * 1000);
		requestFactory.setConnectionRequestTimeout(10 * 1000);
		requestFactory.setReadTimeout(10 * 1000);
		this.factory = requestFactory;
	}
}
