package net.e4net.caas.common.vo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class KakaoPayApiApprovePVO extends KakaoRestVO{
	private static final long serialVersionUID = 5552459882874180733L;
	
	
	@JsonIgnore
	private final transient Logger logger = LoggerFactory.getLogger(KakaoPayApiApprovePVO.class);
 
	//가맹점 코드. 10자.
	@JsonProperty("cid")
	String cid; 
	
	@JsonProperty("tid")
	String tid; 
	
	//가맹점 주문번호. 최대 100자
	@JsonProperty("partner_order_id")
	String partnerOrderId;
	
	@JsonProperty("partner_user_id")
	String partnerUserId; 
	
	@JsonProperty("pg_token")
	String pgToken; 
	
	@JsonProperty("total_amount")
	String totalAmount;
}
