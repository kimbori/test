package net.e4net.caas.common.util;

/**
 * HexUtil
 *
 * @author sangik.lee
 * @since 2017.08.28
 */
public class HexUtils {
	private static final char HEX_CHAR[] = { '0', '1', '2', '3', '4', '5', '6',
			'7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	/**
	 * byte 데이터를 Hex String을 변환
	 *
	 * @param b
	 *            Hex String으로 변환할 byte 데이터
	 * @return Hex String
	 */
	public static String toHexString(byte b) {
		StringBuffer buffer = new StringBuffer();

		buffer.append(HEX_CHAR[(b >>> 4) & 0xf]);
		buffer.append(HEX_CHAR[b & 0xf]);

		return buffer.toString();
	}

	/**
	 * char 데이터를 Hex String을 변환
	 *
	 * @param c
	 *            Hex String으로 변환할 char 데이터
	 * @return HexString
	 */
	public static String toHexString(char c) {
		StringBuffer buffer = new StringBuffer();

		buffer.append(HEX_CHAR[(c >>> 12) & 0xf]);
		buffer.append(HEX_CHAR[(c >>> 8) & 0xf]);
		buffer.append(HEX_CHAR[(c >>> 4) & 0xf]);
		buffer.append(HEX_CHAR[c & 0xf]);

		return buffer.toString();
	}

	public static String byteArrayToHex(byte[] ba) {
		if (ba == null || ba.length == 0) {
			return null;
		}

		StringBuffer sb = new StringBuffer(ba.length * 2);
		String hexNumber;
		for (int x = 0; x < ba.length; x++) {
			hexNumber = "0" + Integer.toHexString(0xff & ba[x]);

			sb.append(hexNumber.substring(hexNumber.length() - 2));
		}
		return sb.toString();
	}


	/**
	 * int 데이터를 Hex String을 변환
	 *
	 * @param i
	 *            Hex String으로 변환할 int 데이터
	 * @return HexString
	 */
	public static String toHexString(int i) {
		StringBuffer buffer = new StringBuffer();

		buffer.append(HEX_CHAR[(i >>> 28) & 0xf]);
		buffer.append(HEX_CHAR[(i >>> 24) & 0xf]);
		buffer.append(HEX_CHAR[(i >>> 20) & 0xf]);
		buffer.append(HEX_CHAR[(i >>> 16) & 0xf]);
		buffer.append(HEX_CHAR[(i >>> 12) & 0xf]);
		buffer.append(HEX_CHAR[(i >>> 8) & 0xf]);
		buffer.append(HEX_CHAR[(i >>> 4) & 0xf]);
		buffer.append(HEX_CHAR[i & 0xf]);

		return buffer.toString();
	}

	/**
	 * long 데이터를 Hex String을 변환
	 *
	 * @param l
	 *            Hex String으로 변환할 long 데이터
	 * @return HexString
	 */
	public static String toHexString(long l) {
		StringBuffer buffer = new StringBuffer();

		buffer.append(HEX_CHAR[(int) ((l >>> 60) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 56) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 52) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 48) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 44) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 40) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 36) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 32) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 28) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 24) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 20) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 16) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 12) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 8) & 0xf)]);
		buffer.append(HEX_CHAR[(int) ((l >>> 4) & 0xf)]);
		buffer.append(HEX_CHAR[(int) (l & 0xf)]);

		return buffer.toString();
	}

	/**
	 * Hex String을 byte로 변환
	 *
	 * @param hexStr
	 * @return byte
	 */
	public static byte toByte(String hexStr) {
		byte result = 0;
		String hex = hexStr.toUpperCase();

		for (int i = 0; i < hex.length(); i++) {
			char c = hex.charAt(hex.length() - i - 1);
			byte b = toByte(c);
			result |= (b & 0x0f) << (i * 4);
		}

		return result;
	}

	public static byte[] toByteArray(String hexStr) {
		String hex = hexStr.toUpperCase();
		byte[] result = new byte[hex.length()/2];

		for (int i = 0; i < hex.length(); i = i + 2) {
			String str = hex.substring(i, i+2);
			byte b = toByte(str);
			result[i/2] = b;
		}

		return result;
	}

	/**
	 * Hex String을 char 값으로 변환
	 *
	 * @param hexStr
	 * @return char
	 */
	public static char toChar(String hexStr) {
		char result = 0;
		String hex = hexStr.toUpperCase();

		for (int i = 0; i < hex.length(); i++) {
			char c = hex.charAt(hex.length() - i - 1);
			byte b = toByte(c);
			result |= (b & 0x0f) << (i * 4);
		}

		return result;
	}


	/**
	 * Hex String을 int로 변환
	 *
	 * @param hexStr
	 * @return int 값
	 */
	public static int toInt(String hexStr) {
		int result = 0;
		String hex = hexStr.toUpperCase();

		for (int i = 0; i < hex.length(); i++) {
			char c = hex.charAt(hex.length() - i - 1);
			byte b = toByte(c);
			result |= (b & 0x0f) << (i * 4);
		}

		return result;
	}

	/**
	 * Hex String을 long으로 변환
	 *
	 * @param hexStr
	 * @return long 값
	 */
	public static long toLong(String hexStr) {
		long result = 0;
		String hex = hexStr.toUpperCase();

		for (int i = 0; i < hex.length(); i++) {
			char c = hex.charAt(hex.length() - i - 1);
			byte b = toByte(c);
			result |= (long) (b & 0x0f) << (i * 4);
		}

		return result;
	}

	private static byte toByte(char c) {
		switch (c) {
			case '0':
				return 0;
			case '1':
				return 1;
			case '2':
				return 2;
			case '3':
				return 3;
			case '4':
				return 4;
			case '5':
				return 5;
			case '6':
				return 6;
			case '7':
				return 7;
			case '8':
				return 8;
			case '9':
				return 9;
			case 'A':
				return 10;
			case 'B':
				return 11;
			case 'C':
				return 12;
			case 'D':
				return 13;
			case 'E':
				return 14;
			case 'F':
				return 15;
		}

		return 0;
	}

	public static int[] toIntArray(String str) {
		int sz = str.length();
		int ret[] = new int[sz];
		for(int i=0; i<sz; i++) {
			char ch = str.charAt(i);
			String hex = HexUtils.toHexString(ch);
			byte bb = HexUtils.toByte(hex);
			ret[i] = bb;
		}
		return ret;
	}

	public static void printHexaStr(String header, byte[] data)
	{
		StringBuffer logbuf = new StringBuffer();
		logbuf.append(header).append(new String(data));
//			NLogger.info("DATA: ", logbuf.toString());
//			return null;
	}


	public static char[] bin2char(byte[] binData)
	{

		byte outBinData[] = new byte[binData.length];

		  /* 특수한 경우만 '.'로 대치.. */
		int i;
		for(i=0; i<binData.length; i++){
			if ( binData[i] < 0x20 || binData[i] >= 0x80 ){
				outBinData[i] = '.';
			}
			else {
				outBinData[i] = binData[i];
			}
		}

		return (new String(outBinData)).toCharArray();

	}


	public static String ascii2hexstr(byte[] ascii) {
		String ret = new String();
		int i = 0;

		for (i = 0; i < ascii.length; i++) {
			ret += printHex(ascii[i]);
		}
		return ret;
	}

	private static String printHex(byte data) {
		String ret = new String();
		String temp = new String();

		temp = Integer.toHexString((int) data).toUpperCase();
		if (temp.length() == 2) {
			return temp;
		}
		if (temp.length() < 2) {
			ret = "0" + temp;
			return ret;
		}
		if (temp.length() > 2) {
			ret = temp.substring(temp.length() - 2, temp.length());
		}
		return ret;
	}

}
