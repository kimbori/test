package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbPgFlngDVO extends PageVO{
	
	private static final long serialVersionUID = -2605540276664492840L;
	private long flngItmSq;
	private String oprGrpId;
	private String lnkAgency;
	private String lnkPrcDt; 
	private String lnkPrcRst;
	private String lnkReqInfo;
	private String lnkRstInfo;
	private long cnrSq;
	private String stlmMd;
	private long flngAmt;
	private String stmt;
	private String stlmKn;
	private long toknTrnfSq;
	private String fstRegDttm;
	private String fstRegUsid;
	private String lstChDttm;
	private String lstChUsid;

}
