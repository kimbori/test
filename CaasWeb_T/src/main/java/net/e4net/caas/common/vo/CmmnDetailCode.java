package net.e4net.caas.common.vo;

import java.io.Serializable;

import lombok.Data;

/**
 * 공통상세코드 모델 클래스
 * @author 공통서비스 개발팀 이중호
 */
@Data
public class CmmnDetailCode implements Serializable {

	private static final long serialVersionUID = 1L;

	/*
	 * 코드ID
	 */
    private String codeId = "";

    /*
     * 코드ID명
     */
    private String codeIdNm = "";

    /*
     * 코드
     */
	private String code = "";

	/*
	 * 코드명
	 */
    private String codeNm = "";

    /*
     * 코드설명
     */
    private String codeDc = "";

    /*
     * 사용여부
     */
    private String useAt = "";

    /*
     * 최초등록자ID
     */
    private String fstRegUsid = "";

    /*
     * 최종수정자ID
     */
    private String lstChUsid   = "";

}
