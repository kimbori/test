package net.e4net.caas.common.mapper;

import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.dvo.TbCertiPrsnDVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author yeongeun.seo 
 * @since 2018.11.30
 */ 
@Repository
public interface CertMapper {
	 
    /** 
     * 본인인증 시퀀스 조회
     */
    public long selectTbCertiPrsnSeq();
    
    /** 
     * 본인인증 내역 등록 
     */
    public int insertTbCertiPrsnInfo(TbCertiPrsnDVO dvo);
    
    /** 
     * 본인인증 정보 갱신 
     */
    public int updateTbCertiPrsnInfo(TbCertiPrsnDVO dvo);
}
