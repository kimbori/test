package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbKakaoPayRdyLstDVO extends PageVO{
	private static final long serialVersionUID = -3903725600790062648L;
	
	private long prprItmSq; 
	private long cnrSq; 
	private String prdtNm; 
	private String prdtCd; 
	private int prdtQty; 
	private long stlmTtAmt; 
	private long frTaxAmt; 
	private long srtxAmt; 
	private String sucsUrl; 
	private String cancUrl; 
	private String failUrl; 
	private String posbCcoCd; 
	private String posbStlmMnsCd; 
	private int isMcnt; 
	private String stlmNtno; 
	private String appStlmUrl; 
	private String mwebStlmUrl; 
	private String pcStlmUrl; 
	private String adrdSchmUrl; 
	private String iosSchmUrl; 
	private String prprReqDttm; 
	private String rspnCd; 
	private String rspnMsg; 
	private String addRspnCd; 
	private String addRspnMsg; 
	private String fstRegDttm;
	private String fstRegUsid; 
	private String lstChDttm; 
	private String lstChUsid;

}
