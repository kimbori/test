package net.e4net.caas.common.vo;

import java.util.Map;

import org.springframework.http.HttpMethod;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class RestVO extends AbstractVO {

	private static final long serialVersionUID = -5794748402183622416L;

	/**
	 * 요청 메서드
	 */
	@JsonIgnore
	private HttpMethod method = HttpMethod.GET;

	public Map<String, String> convertMap() {
		ObjectMapper om = new ObjectMapper();
		TypeReference<Map<String, String>> typeReference = new TypeReference<Map<String, String>>() {};

		return om.convertValue(this, typeReference);
	}

	public MultiValueMap<String, String> convertMultiValueMap() {
		MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();

		for (Map.Entry<String, String> entry : convertMap().entrySet()) {
			multiValueMap.add(entry.getKey(), entry.getValue());
		}

		return multiValueMap;
	}

}
