package net.e4net.caas.common.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.CmmnDetailCode;

/**
 * 공통코드
 *
 * @author kc.kim
 * @since 2018.11.20
 */
@Repository
public interface CmmCodeMapper {
	
	List<CmmnDetailCode> selectCmmCodeDetail(String codeId);
}
