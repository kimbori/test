package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbCertiPrsnDVO extends PageVO {

	private static final long serialVersionUID = -8633444827199221927L;
	
	private long prsnAuthSq;			// 본인인증SQ
	private String oprGrpId;			// 운영단체ID
	private String lnkAgency;			// 연계Agency
	private String lnkPrcDt;			// 연계처리일시
	private String lnkPrcRst;			// 연계처리결과
	private String lnkReqInfo;			// 연계요청정보
	private String lnkRstInfo;			// 연계결과정보
	private long cnrSq;					// 기부자SQ
	private String cnrCi;				// 기부자CI
	private String cnrDi;				// 기부자DI
	private String brthDt;				// 생년월일
	private String sex;					// 성별
	private String frmnYn;				// 외국인여부
	private String fstRegDttm;			// 최초등록일시
	private String fstRegUsid;			// 최초등록자
	private String lstChDttm;			// 최종변경일시
	private String lstChUsid;			// 최종변경자

}