package net.e4net.caas.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class CertInfoVO {
	private String svcId;		//서비스아이디
	private String mobilId;		//모빌리언스 거래번호
	private String signDate;	//결제일자
	private String tradeId;		//가맹점 거래번호
	private String name;		//이름
	private String no;			//휴대폰번호
	private String commId;		//이동통신사
	private String resultCd;	//결과코드
	private String resultMsg;	//결과메시지
	private String cryptYn;		//암호화 사용 여부 (default : y)
	private String keyGb;		//암호화 key 구분 (0 : ciSvcid 8자리, 1·2 : 가맹점 관리자 등록 후 사용)
	private String socialNo;	//생년월일
	private String sex;			//성별 (남성:m, 여성:f)
	private String foreigner;	//외국인여부 (외국인 : y)
	private String ci;			//ci
	private String di;			//di
	private String ciMode;		//ciMode 41:lms문구설정, 51:sms문구설정, 61:sms발송
	private String diCode;		//웹사이트코드
	private String mac;			//검증키
	private String mstr;		//가맹점 확장 변수
	private String oprGrpId;	//운영단체ID(MSTR[0])
	private String trmlId;		//디바이스ID(MSTR[1])
	private String mailAddr;	// 기부자 메일주소 (KGC)
}