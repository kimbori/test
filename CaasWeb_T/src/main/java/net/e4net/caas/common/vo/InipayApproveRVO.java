package net.e4net.caas.common.vo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class InipayApproveRVO extends RestVO{

	private static final long serialVersionUID = -2559214200062609898L;
	
		@JsonIgnore
		private final transient Logger logger = LoggerFactory.getLogger(InipayApproveRVO.class);
	
		//거래상태 
		@JsonProperty("P_STATUS")
		private String pStatus; 

		//거래번호
		@JsonProperty("P_TID") 
		private String pTid; 
		
		//지불수단
		@JsonProperty("P_TYPE")  
		private String pType; 
		
		//승인일자
		@JsonProperty("P_AUTH_DT")   
		private String pAuthDt; 
		
		//상점아이디
		@JsonProperty("P_MID")   
		private String pMid; 
		
		//상점 주문번호
		@JsonProperty("P_OID")   
		private String pOid; 
		
		//거래금액
		@JsonProperty("P_AMT")   
		private String pAmt; 
		
		//주문자명
		@JsonProperty("P_UNAME")   
		private String pUname; 
		
		//가맹점 이름
		@JsonProperty("P_MNAME")   
		private String pMname; 
		
		//메시지1  char(500)
		@JsonProperty("P_RMESG1") 
		private String pRmesg1; 
		
		//주문정보   char(800)
		@JsonProperty("P_NOTI") 
		private String pNoti; 
		
		//가맹점 전달 NOTI URL
		@JsonProperty("P_NOTEURL")   
		private String pNoteurl; 
		
		//가맹점 전달 NEXT URL
		@JsonProperty("P_NEXT_URL")   
		private String pNextUrl; 
		
		//카드번호
		@JsonProperty("P_CARD_NUM")  
		private String pCardNum; 
		
		//발급사 코드
		@JsonProperty("P_CARD_ISSUER_CODE")   
		private String pCardIssuerCode; 
		
		//가맹점번호
		@JsonProperty("P_CARD_MEMBER_NUM")   
		private String pCardMemberNum ; 
		
		//매입사 코드
		@JsonProperty("P_CARD_PURCHASE_CODE")   
		private String pCardPurchaseCode; 
		
		//부분취소 가능여부 1가능 0 불가능
		@JsonProperty("P_CARD_PRTC_CODE")
		private String pCardPrtcCode; 
		
		//무이자 할부여부    0 : 일반, 1 : 무이자
		@JsonProperty("P_CARD_INTEREST") 
		private String pCardInterest; 
		
		//체크카드 여부  0 : 신용카드, 1
		@JsonProperty("P_CARD_CHECKFLAG")   
		private String pCardCheckflag; 
		
		//메시지2  char(500)  신용카드 할부 개월 수
		@JsonProperty("P_RMESG2")  
		private String pRmesg2; 
		
		//카드코드    char(4)
		@JsonProperty("P_FN_CD1") 
		private String pFnCd1; 
		
		//승인번호   char(30)  신용카드거래에서만 사용합니다
		@JsonProperty("P_AUTH_NO") 
		private String pAuthNo; 
		
		//VP 카드코드
		@JsonProperty("P_ISP_CARDCODE")  
		private String pIspCardcode; 
		
		//결제카드한글명    BC카드, 
		@JsonProperty("P_FN_NM") 
		private String pFnNm; 
		
		//이벤트코드    A1,A2 등등 
		@JsonProperty("P_EVENT_CODE")
		private String pEventCode; 
		
		//통신사    char(3) 
		@JsonProperty("P_HPP_CORP")
		private String pHppCorp ; 
		
		//결제 휴대폰 번호   
		@JsonProperty("P_HPP_NUM")
		private String pHppNum ; 
		
		//전자지갑 종류   --YPAY : 옐로페이 KWPY : 뱅크월렛 
		@JsonProperty("P_EWALLET_TYP")
		private String  PEwalletTyp; 
		
		//앱연동여부   --P : 페이핀 K : 국민앱카드 C: 페이코 B: 삼성페이 L: LPAY O: 카카오페이 G: SSGPAY
		@JsonProperty("P_SRC_CODE")
		private String  PSrcCode; 
		
		//처리상태    --220000 : 정상, 그 외 : 오류 
		@JsonProperty("P_CSHR_CODE")
		private String  PCshrCode; 
		
		//처리메시지   
		@JsonProperty("P_CSHR_MSG")
		private String  PCshrMsg; 
		
		//현금영수증  총 금액   --총금액 = 공급가액+세금+봉사료 
		@JsonProperty("P_CSHR_AMT")
		private String  PCshrAmt; 
		
		//공급가액   
		@JsonProperty("P_CSHR_SUP_AMT")
		private String  PCshrSupAmt; 
		
		//세금   
		@JsonProperty("P_CSHR_TAX")
		private String  PCshrTax; 
		
		//봉사료   
		@JsonProperty("P_CSHR_SRVC_AMT")
		private String  pCshrSrvcAmt; 
		
		//용도구분   -- 0:소득공제용, 1:지출증빙용 
		@JsonProperty("P_CSHR_TYPE")
		private String  pCshrType; 
		
		//발행시간   
		@JsonProperty("P_CSHR_DT")
		private String  pCshrDt; 
		
		//발행번호  -- 가상계좌의 경우, 입금 완료 시, 생성되어 모바일 내 채번시에는 전달되지 않습니다
		@JsonProperty("P_CSHR_AUTH_NO")
		private String  pCshrAuthNo; 
}
