package net.e4net.caas.common.base;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import net.e4net.caas.common.util.LogObject;
import net.e4net.caas.common.vo.UserVO;

public abstract class BaseAbstractCommon extends LogObject {

	/**
	 * 로그인 접속자(기부자)가 소속된 운영단체ID를 얻는다.
	 * @return
	 */
	protected String getLoginOprGrpId() {
		UserVO vo = getMyUserVO();
		return vo == null ? "" : vo.getOprGrpId();
	}

	/**
	 * 접속자 로그인 기부자 SQ를 얻는다.
	 * @return 0 : 로그인 안된 경우.
	 */
	protected long getLoginCnrSq() {
		UserVO vo = getMyUserVO();
		return vo == null ? 0L : vo.getCnrSq();
	}

	/**
	 * 사용자의 경우는 ID가 없다 대신 기부자SQ를 대신 writer ID로 저장시킨다.
	 * 	없는 경우는 "system"으로 리턴
	 * @return
	 */
	protected String getLoginCnrSqString() {
		long sq = getLoginCnrSq();
		return sq == 0L ? "system" : String.valueOf(sq);
	}

	@Deprecated
	protected String getLoginWriter() {
		return getLoginCnrSqString();
	}
	
	/**
	 * 로그인한 경우 로그인 세션 정보를 얻는다.
	 * @return
	 */
	protected UserVO getMyUserVO() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if(auth == null) return  null;
		Object principal = auth.getPrincipal();
		if(principal instanceof String) return null;
	   	//UserVO vo = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	    return (UserVO)principal;
	}
}