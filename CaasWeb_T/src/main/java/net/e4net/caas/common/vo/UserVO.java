package net.e4net.caas.common.vo;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class UserVO extends AbstractVO implements UserDetails{


	private static final long serialVersionUID = -7401208445904691406L;
	
	private long	cnrSq; 
	private String	oprGrpId; 
	private String	cnrNm; 
	private String	nickNm; 
	private String	trmlId; 
	private String	passwd; 
	private String	hpno; 
	private String	prsnAuthSq; 
	private String	cnrCi; 
	private String	mailAddr; 
	private String	wltAddr;
	private boolean loginYn;
	
	
	public boolean isLogin() {
		return loginYn;
	}


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return false;
	}


	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return false;
	}
}
