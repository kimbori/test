package net.e4net.caas.common.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.service.FlngProcService;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.my.vo.CardInfoPVO;

/**
 * 충전처리 컨트롤러
 *
 * @author djim
 * @since 2018.11.01
 * history  2018.11.01 최초작성
 */
@Controller
public class FlngProcController extends AbstractController {
	
    @Autowired
    private FlngProcService flngProcService;
    
    
    /**
     * 충전하기 sample pag 로딩
     * @param request
     * @return
     */
    @RequestMapping(value = "/moveMyPage")
    public ModelAndView commonCdMgmt() {
    	ModelAndView mav = new ModelAndView("common/myPagSample");

		return getOkModelAndView(mav);
    }
    

    /**
     * 충전하기 레이어 팝업 호출 AJAX
     * @return
     * @throws CaasException
     */
    @RequestMapping(value = "/common/flngPopAjax", method = RequestMethod.POST)
    public ModelAndView selectDntnMctAjax() throws CaasException {
        ModelAndView mav = new ModelAndView("common/flngPopAjax :: flngPopAjax");
        
        try {
            //UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            //mav.addObject("resultList", flngProcService.selectBllList(principal.getCnrSq()));
        } catch (Exception e) {
        	error(e.getMessage(), e);
 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
		}
        
        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 간편결제 카드 목록 조회
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/common/getBllCardListAjax", method = RequestMethod.POST)
    public ModelAndView getBllCardList(@ModelAttribute CardInfoPVO pvo, HttpServletRequest request) throws Exception {
    	ModelAndView mav = new ModelAndView("jsonView");
    	
    	try {
            UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            mav.addObject("resultList", flngProcService.selectBllList(principal.getCnrSq()));
        } catch (Exception e) {
        	error(e.getMessage(), e);
 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
		}
    	
    	return getOkModelAndView(mav);
    }
}
