package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class SampleDVO extends PageVO {

	private static final long serialVersionUID = 1L;
	String key;
	int val1;
	int val2;
	String str1;

}
