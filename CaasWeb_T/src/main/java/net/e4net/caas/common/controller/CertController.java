package net.e4net.caas.common.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.service.CertService;
import net.e4net.caas.common.service.CmmService;
import net.e4net.caas.common.util.CommonUtils;
import net.e4net.caas.common.vo.CertInfoVO;
import net.e4net.caas.common.vo.OprGrpVO;
import net.e4net.caas.common.vo.dvo.TbCertiPrsnDVO;

/**
 * 본인인증 컨트롤러
 *
 * @author yeongeun.seo
 * @since 2018.11.29
 * history  2018.11.29 최초작성
 * 
 */
@Slf4j
@Controller
public class CertController extends AbstractController {
	
	@Value("${callback.api.myHost}") 			private String pCallbackApiMyHost;
	@Value("${mobilians.callback.okUrl}") 		private String pMobiliansCallbackOkUrl;
	@Value("${mobilians.callback.closeUrl}") 	private String pMobiliansCallbackCloseUrl;
	
	@Autowired private CmmService cmmService;
	@Autowired private CertService certService;
	//@Autowired private DntService dntService;

	/**
	 * 본인인증 호출 처리하기 위한 페이지 
	 * 	(*1) 운영단체ID는 항상 필수로 받아야 한다. 운영단체별 접속정보가 다르다.
	 * @param oprGrpId 운영단체ID (필수) - 앱으로부터 받는다.
	 * @param errCd 오류 발생된 경우에 사용
	 * @param errMsg 오류 발생된 경우에 사용
	 */
    @RequestMapping(value = {"/public/cert/certReady", "/certReady"})
    public String memberCert(@RequestParam String oprGrpId, String errCd, String errMsg, ModelMap model) {
    	log.debug("=====  KG Mobilians Cert Ready  =====");
    	OprGrpVO oprGrp = cmmService.getOprGrpInfo(oprGrpId);

    	model.addAttribute("oprGrpId", 	oprGrpId);
    	model.addAttribute("uuid", 		CommonUtils.getNewUUID());
    	model.addAttribute("ciSvcid", 	oprGrp.getCtSvcId());
    	model.addAttribute("okUrl", 	pCallbackApiMyHost + pMobiliansCallbackOkUrl + "?oprGrpId=" + oprGrpId);	// (*1)
    	model.addAttribute("closeUrl", 	pCallbackApiMyHost + pMobiliansCallbackCloseUrl + "?oprGrpId=" + oprGrpId);	// (*1)
    	model.addAttribute("siteUrl", 	oprGrp.getHomeUrl());
    	model.addAttribute("errCd", 	errCd);
    	model.addAttribute("errMsg", 	errMsg);
    	
		return "common/certReady";
    }
    
	@RequestMapping("/public/cert/certApprove")
    public String approve(String oprGrpId, HttpServletRequest request, ModelMap model) throws Exception {
		log.debug("=====  KG Mobilians Cert Return  =====");
		//request.setCharacterEncoding("utf-8");  
		String writerID = getLoginCnrSqString();
  		
  		// 본인인증서비스 결과 처리 (복화화 등)
		OprGrpVO oprGrp = cmmService.getOprGrpInfo(oprGrpId);
  		CertInfoVO certInfoVO = certService.certResultProc(request, oprGrp);
 		
  		log.debug("###############################################################################"); 
  		log.debug("certInfoVO : " + certInfoVO.toString());

  		TbCertiPrsnDVO certDVO = certService.saveUserInfo(oprGrp, certInfoVO, writerID);
 		if(certDVO == null) {
  			return "redirect:" + pMobiliansCallbackOkUrl + "?oprGrpId=" + oprGrpId + "errCd=9999&errMsg=본인인증서비스연계처리실패";
 		}
  		
  		model.addAttribute("cid", certDVO.getCnrCi());
  		model.addAttribute("did", certDVO.getCnrDi());
  		
  		log.debug("###############################################################################");
  		log.debug(model.toString());
  		log.debug("###############################################################################");
  		
  		return "common/certPinProc";
    }

	// 화면 view 테스트용. 서비스용 아님.
	@RequestMapping("/public/common/certPinProc")
    public String certPinProc(String oprGrpId, HttpServletRequest request, ModelMap model) {
  		model.addAttribute("cid", "tttt");
  		model.addAttribute("did", "yyy");
  		return "common/certPinProc";
	}
}