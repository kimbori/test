package net.e4net.caas.common.mapper;

import java.util.Map;

import org.apache.ibatis.annotations.MapKey;
import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.OprGrpVO;

/**
 * 공통
 * @author KC.KIM
 *
 */
@Repository
public interface CmmMapper {
	
	// 단체목록 조회
	@MapKey("oprGrpId") Map<String, OprGrpVO> selectOprGrp();

}
