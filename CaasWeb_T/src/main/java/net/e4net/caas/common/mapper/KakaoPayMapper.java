package net.e4net.caas.common.mapper;

import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.dvo.TbKakaoPayAvLstDVO;
import net.e4net.caas.common.vo.dvo.TbKakaoPayRdyLstDVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author djim 
 * @since 2018.11.01
 */ 
@Repository
public interface KakaoPayMapper {
	 
    /** 
     * 카카오페이 준비 시퀀스 조회
     */
    public long selectTbKakaoPayRdyLstSeq();
    
    /** 
     * 카카오페이 준비 등록 
     */
    public int insertTbKakaoPayRdyLst(TbKakaoPayRdyLstDVO dvo);
    
    /** 
     * 카카오페이 준비 갱신 
     */
    public int updateTbKakaoPayRdyLst(TbKakaoPayRdyLstDVO dvo);
    
    /** 
     * 카카오페이 준비 조회 
     */
    public TbKakaoPayRdyLstDVO selectTbKakaoPayRdyLst(String stlmNtno);
    
    /** 
     * 카카오페이 결제 시퀀스 조회
     */
    public long selectTbKakaoPayAvLstSeq();
    
    /** 
     * 카카오페이 결제 등록 
     */
    public int insertTbKakaoPayAvLst(TbKakaoPayAvLstDVO dvo);
    
    /** 
     * 카카오페이 결제 갱신 
     */
    public int updateTbKakaoPayAvLst(TbKakaoPayAvLstDVO dvo);
}
