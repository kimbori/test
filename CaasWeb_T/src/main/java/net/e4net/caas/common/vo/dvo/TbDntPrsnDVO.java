package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbDntPrsnDVO extends PageVO{
	
	private static final long serialVersionUID = -7214943067977663761L;
	
	private long cnrSq;				//기부자SQ
	private String oprGrpId;		//운영단체ID
	private String cnrNm;			//기부자명
	private String nickNm;			//닉네임
	private String prfImg;			//프로필이미지
	private String trmlId;			//단말기ID
	private String passwd;			//비밀번호
	private String hpno;			//휴대전화번호
	private long prsnAuthSq;		//본인인증SQ
	private String cnrCi;			//기부자CI
	private String mailAddr;		//메일주소
	private String wltAddr;			//지갑주소
	private String fstRegDttm;		//최초등록일시
	private String fstRegUsid;		//최초등록자
	private String lstChDttm;		//최종변경일시
	private String lstChUsid;		//최종변경자

}
