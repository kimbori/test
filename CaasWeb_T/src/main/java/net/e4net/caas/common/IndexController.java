package net.e4net.caas.common;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.service.BlockchainService;
import net.e4net.caas.common.service.DntService;
import net.e4net.caas.common.service.LoginService;
import net.e4net.caas.common.util.EncodeUtilLib;
import net.e4net.caas.common.util.FileMngUtils;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.common.vo.UserVO;

@Slf4j
@Controller
public class IndexController extends AbstractController {
	
	//@Autowired private CmmService cmmService;
	@Autowired private LoginService loginService;
	@Autowired private BlockchainService blockchainService;
	@Autowired private DntService dntService;

	@Value("${spring.profiles.active}") private String pSpringProfilesActive;
	@Value("${callback.api.myHost}") private String pCallbackApiMyHost;

	/**
	 * 모바일용 자동 로그인 처리
	 * 	(TODO) 접근에 대한 보안 대책 필요
	 * @param cnrCi
	 * @param oprGrpId
	 */
	@RequestMapping(value = {"/public/loginProc", "/loginProc"})       
	public String loginProc(@RequestParam String cnrCi, @RequestParam String oprGrpId, HttpServletRequest request) throws CaasException {
		log.debug("[loginProc] before. cnrCi=" + cnrCi);
		cnrCi = EncodeUtilLib.urlDecoder(cnrCi);  // (KGC)
		cnrCi = cnrCi.replace(" ", "+");			// (KGC)	
		log.debug("[loginProc] after. cnrCi=" + cnrCi);
		
		debug(cnrCi);
		debug(oprGrpId);

		UserVO userVO = loginService.selectUser(cnrCi, oprGrpId);
		
		if ( userVO == null) {
			throw new CaasException("9999", "인증 실패 하였습니다.");
		}

		// (KGC) 방어코드. 사용자등록은 되었는데 지갑주소가 없는 경우 생성해 준다.
		if(userVO.getWltAddr() == null || userVO.getWltAddr().isEmpty()) {
			BlockchainRetrunVO retVo = blockchainService.createWalletDntPrsnNew(oprGrpId, userVO.getCnrSq());
			if(retVo.isSuccess()) {
				dntService.changeDntWaltProc(retVo.getWalletAddr(), userVO.getCnrSq(), "system");
				userVO.setWltAddr(retVo.getWalletAddr());
			} else {
				log.error("지갑 생성 실패");	// (TODO) (KGC)실패 시 고려				
			}
		}

		userVO.setLoginYn(true);

        ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        
        Authentication authentication  = new UsernamePasswordAuthenticationToken(userVO, null, authorities);
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);

        HttpSession session = request.getSession(true);
        session.setAttribute("SPRING_SECURITY_CONTEXT",securityContext);   // 세션에 spring security context 넣음
        
		return  "redirect:/moveMainPag";   
	}

	/* ==================================================================================
	 * 	테스트용 웹용 로그인 처리 - 개발 테스트 용도로만 사용한다.
	 */
	/**
	 * 로컬 테스트용 로그인 페이지. (외부 서비스용 아님)
	 * 	(*1) (보안) "local"인 경우만 접속을 허락한다. 서버에서 접속은 불허한다.
	 * 		"dev" 등 다른 경우는 hoem("/")으로 이동.
	 */
	@RequestMapping(value = {"/public/localLogin", "/moveLoginPag"})       
	public String loginPag(Model model) { 	// (*1)  
//		if(! "local".equals(pSpringProfilesActive)) {
//			log.warn("Local이 아닌 서버에서 접속은 불허합니다.");
//			return "redirect:/";
//		}
		return "login";
	}

	/**
	 * 로컬 테스트 로그인 처리용. (외부 서비스용 아님)
	 * @param cnrCi
	 * @param oprGrpId
	 */
	@RequestMapping("/public/localLoginProc")       
	public String localLoginProc(@RequestParam String cnrCi, @RequestParam String oprGrpId, HttpServletRequest request) throws CaasException {         
//		if(! "local".equals(pSpringProfilesActive)) {	// (*1)
//			log.warn("Local이 아닌 서버에서 접속은 불허합니다.");
//			return "redirect:/";
//		}
		return loginProc(cnrCi, oprGrpId, request);
	}

	/* ==================================================================================
	 * 	홈페이지 - 안드로이드 앱 다운로드 사이트로 사용.
	 */
	/**
	 * 홈 페이지.
	 */
	@RequestMapping("/")       
	public String index(Model model){              
		log.debug("pSpringProfilesActive=" + pSpringProfilesActive + ", pCallbackApiMyHost=" + pCallbackApiMyHost);
		//OprGrpVO vo1 = cmmService.getOprGrpInfo("e4net");
		//log.debug("vo1=" + vo1);
		UserVO user = getMyUserVO();
		log.debug("user=" + user);
		return "index/index";
	}

	/**
	 * 안드로이드 앱 파일 다운로드
	 */
	@RequestMapping("/public/download/CherryAndroid.apk")       
	public void downCherryAndroid(HttpServletResponse response) throws Exception {
		String downfile = "/usr/share/nginx/html/CherryAndroid.apk";
		FileMngUtils.downFile(response, downfile, "CherryAndroid.apk");
	} 

}

