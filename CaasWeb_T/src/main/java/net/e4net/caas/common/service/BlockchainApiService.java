package net.e4net.caas.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.base.BaseAbstractService;
import net.e4net.caas.common.mapper.BlockchainMapper;
import net.e4net.caas.common.util.JsonUtils;
import net.e4net.caas.common.vo.BlockchainLinkInfoVO;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.common.vo.BlockchainTokenTxVO;
import net.e4net.caas.common.vo.BlockchainWalletVO;
import net.e4net.caas.common.vo.OprGrpVO;

@Service
@Slf4j
public class BlockchainApiService extends BaseAbstractService {

	@Value("${blockchain.base.url}") private String pBlockchainBaseUrl;
	@Value("${blockchain.callApiUrl.createWallet}") private String pApiCreateWallet;
	@Value("${blockchain.callApiUrl.userBalance}") private String pApiWalletBalance;
	@Value("${blockchain.callApiUrl.tokenTransfer}") private String pTokenTransfer;
	@Autowired BlockchainMapper mapper;
	@Autowired CmmService cmmService;

	/* ==================================================================================
	 * 블록체인 지갑 잔고 조회 API
	 */

	public BlockchainRetrunVO getWalletBalanceSub(String oprGrpId, String walletAddr) {
		try {
			return apiGetWalletBalance(oprGrpId, walletAddr);
		} catch(Exception e) {
			return getReturnFailException("getWalletBalance", e);
		}
	}

	private BlockchainRetrunVO apiGetWalletBalance(String oprGrpId, String walletAddr) {
		BlockchainLinkInfoVO bc = getBlockchainLinkInfo(oprGrpId);
		long balance = -1;
		Map<String, Object> param = new HashMap<>();
		param.put("userWalletAddress", walletAddr);
		param.put("productTokenSymbol", bc.getTokenSimbol());
		param.put("apikey", bc.getApiKey());

		Map<String,Object> result = restfulApiCallResult(pApiWalletBalance, param);
		boolean onSuccess = isApiResultSuccess(result);
		if(onSuccess) {
			Map<String,Object> data = getApiResultDataObject(result);
			balance = Long.valueOf(data.get("balance").toString());
			return getReturnSuccess(null, balance);		// null : 성공 시 no log
		} else {
			return getReturnFailApi("apiGetWalletBalance", result);
		}
	}

	/* ==================================================================================
	 * 블록체인 토큰 전송 (이체) API
	 */
	/**
	 * 토큰이체 공통 처리
	 */
	public BlockchainRetrunVO walletTokenSub(String oprGrpId, String transferType, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp) {
		try {
			BlockchainRetrunVO retVo = apiTokenTransfer(oprGrpId, transferType, frmWltAddr, toWltAddr, trnfTokn, trnfTp);
			return retVo;
		} catch(Exception e) {
			return getReturnFailException("walletTokenTransfer", e);
		}
	}

	private BlockchainRetrunVO apiTokenTransfer(String oprGrpId, String transferType, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp) {
		String logTtl = "apiTokenTransfer";
		String userid = getLoginCnrSqString();
		BlockchainLinkInfoVO bc = getBlockchainLinkInfo(oprGrpId);

		Map<String, Object> param = new HashMap<>();
		param.put("transferType", transferType);
		if("C2C".equals(transferType) || "C2B".equals(transferType)) {
			param.put("senderAddress", frmWltAddr);			
		}
		if("C2C".equals(transferType) || "B2C".equals(transferType)) {
			param.put("receiverAddress", toWltAddr);
		}

		param.put("valueAmount", trnfTokn);
		param.put("apikey", bc.getApiKey());

		Map<String,Object> result = restfulApiCallResult(pTokenTransfer, param);
		boolean onSuccess = isApiResultSuccess(result);

		BlockchainTokenTxVO vo = new BlockchainTokenTxVO();
		// ExtLinkVO
		//		DB : lnkPrcDt, fstRegDttm, lstChDttm
		//		param : fstRegUsid, lstChUsid
		vo.setOprGrpId(oprGrpId);
		vo.setLnkAgency(bc.getBlockChainKd());
		vo.setLnkPrcRst(onSuccess ? "S" : "F");
		vo.setLnkReqInfo(JsonUtils.convertvObjToJson(param));
		vo.setLnkRstInfo(JsonUtils.convertvObjToJson(result));
		vo.setFstRegUsid(userid);
		vo.setLstChUsid(userid);

		vo.setTrnfTp(trnfTp);			// 전송타입
		vo.setFrmWltAddr(frmWltAddr);
		vo.setToWltAddr(toWltAddr);
		vo.setTrnfTokn(trnfTokn);

		if(onSuccess) {
			Map<String,Object> data = getApiResultDataObject(result);
			String txHash = data.get("txHash").toString();
			vo.setTxHash(txHash);
		}

		// 실패인 경우도 로그는 남긴다.
		int ret = mapper.insertTokenSend(vo);
		if(ret <= 0) {
			return getReturnFailDb(logTtl, "Insert Token Fail");
		}

		return onSuccess ? getReturnSuccess(logTtl, vo) : getReturnFailApi(logTtl, result);
	}

	/* ==================================================================================
	 * 블록체인 지갑 잔고 동기화
	 * 	이체 후 DB 잔고를 지갑 잔고와 동기화 시킨다.
	 *	(주의) 이체 처리 후 잔고 조회 처리 주의사항.
	 * 		(1) @ASync 설정은 @Service 내의 메소드만 가능
	 * 		(2) 동일한 Service 내에서 호출은 Aync 되지 않음. 다른 Service에서는 가능
	 * 	(참조) http://heowc.tistory.com/24  Spring Boot - Async
	 */

	/**
	 * 지갑 잔고 동기화 처리는 2초 정도 후에 잔고 동기화를 실시한다.
	 * 	(1) 잔고 동기화는 지연 처리가 필요하다.
	 * 		지연이 필요한 이유는 블록체인에서 Confirm Block 시간이 1~2ch 정도 걸리기 때문이다.
	 * 	(2) ASYNC 처리 하도록 한다. ASYNC 처리 이유은 지연 처리 동안 기다리지 않기 위함이다. 
	 * 	(2) Exception이 발생되어도 catch해서 무시한다. 잔고 처리중 실패 결과는 무시한다.
	 * 		잔고 동기화는 메인 프로세스가 아니다.
	 * @param oprGrpId
	 * @param wltAddr1
	 * @param wltAddr2
	 */
	@Async
	public void asyncUpdateWalletBalance(String oprGrpId, String wltAddr) {
		asyncUpdateWalletBalanceSub(oprGrpId, wltAddr, null);
	}
	@Async
	public void asyncUpdateWalletBalance(String oprGrpId, String wltAddr1, String wltAddr2) {
		asyncUpdateWalletBalanceSub(oprGrpId, wltAddr1, wltAddr2);
	}

	private void asyncUpdateWalletBalanceSub(String oprGrpId, String wltAddr1, String wltAddr2) {
		try {
			log.debug("[asyncUpdateWalletBalance] ---------------------- 지갑 잔고 동기화 START");
			//log.debug("==================================(1) sync start");
			//apiGetWalletBalance(oprGrpId, wltAddr1);
			//apiGetWalletBalance(oprGrpId, wltAddr2);
			//log.debug("==================================(2) befire delay");
			Thread.sleep(2 * 1000);		// 2초 지연
			//log.debug("==================================(3) after delay");

			// 이체 후 바로 조회는 의미가 없다. 1~3초 후인 confirm 후에 하면 된다.
			if(wltAddr1 != null && ! wltAddr1.isEmpty()) {
				updateWalletBalance(oprGrpId, wltAddr1);
			}
			if(wltAddr2 != null && ! wltAddr2.isEmpty()) {
				updateWalletBalance(oprGrpId, wltAddr2);
			}
			//log.debug("==================================(4) sync end");
			log.debug("[asyncUpdateWalletBalance] ---------------------- 지갑 잔고 동기화 END");
		} catch(Exception e) {
			log.error("[asyncUpdateWalletBalance] ---------------------- 지갑 잔고 동기화 FAIL. errMsg= " + e.getMessage(), e);
		}
	}

	private boolean updateWalletBalance(String oprGrpId, String wltAddr) {
		BlockchainRetrunVO retVo = apiGetWalletBalance(oprGrpId, wltAddr);
		String writerid = getLoginCnrSqString();

		BlockchainWalletVO vo = new BlockchainWalletVO();
		vo.setWltAddr(wltAddr);
		vo.setWltLv(retVo.getBalance());
		vo.setLstChUsid(writerid);
		int ret = mapper.updateWalletBalance(vo);
		return ret > 0;
	}

	/* ==================================================================================
	 * 블록체인 지갑생성
	 */
	public BlockchainRetrunVO createWalletSub(String oprGrpId, String wltKn, String emailGubun) {
		try {
			//String email = oprGrpId + "-" + wltKn + "_" + CommonUtils.getNewUUID() + "@cherry.e4net.net";
			String email = oprGrpId + "_" + wltKn + "_" + emailGubun + "@cherry.e4net.net";
			return apiCreateWallet(oprGrpId, wltKn, email);
		} catch(Exception e) {
			return getReturnFailException("createWallet", e);
		}
	}

	/*
	 * (TODO) 이미 만들어진 경우는 지갑주소를 찾아 update를 시켜주는 프로세스가 필요할 수도.
	 */
	private BlockchainRetrunVO apiCreateWallet(String oprGrpId, String wltKn, String email) {
		String logTtl = "apiCreateWallet";
		String userid = getLoginCnrSqString();
		BlockchainLinkInfoVO bc = getBlockchainLinkInfo(oprGrpId);

		Map<String, Object> param = new HashMap<>();
		param.put("userEmail", email);
		param.put("apikey", bc.getApiKey());

		Map<String,Object> result = restfulApiCallResult(pApiCreateWallet, param);
		boolean onSuccess = isApiResultSuccess(result);

		BlockchainWalletVO vo = new BlockchainWalletVO();
		// ExtLinkVO
		//		DB : lnkPrcDt, fstRegDttm, lstChDttm
		//		param : fstRegUsid, lstChUsid
		vo.setOprGrpId(oprGrpId);
		vo.setLnkAgency(bc.getBlockChainKd());
		vo.setLnkPrcRst(onSuccess ? "S" : "F");
		vo.setLnkReqInfo(JsonUtils.convertvObjToJson(param));
		vo.setLnkRstInfo(JsonUtils.convertvObjToJson(result));
		vo.setFstRegUsid(userid);
		vo.setLstChUsid(userid);

		vo.setWltKn(wltKn);
		vo.setMailAddr(email);

		if(onSuccess) {
			Map<String,Object> data = getApiResultDataObject(result);
			vo.setWltAddr(data.get("address").toString());
			vo.setWltLv(0);
		}

		int ret = mapper.insertBlockchainWallet(vo);
		if(ret <= 0) {
			return getReturnFailDb(logTtl, "Insert wallet Fail");
		}

		return onSuccess ? getReturnSuccess(logTtl, vo) : getReturnFailApi(logTtl, result);
	}

	/* ==================================================================================
	 * 블록체인 인터페이스 연계 처리 공통
	 */

	/**
	 *  표준 RESTful API 호출 처리
	 * @param url 접속URL
	 * @param param 전송 파라미터
	 * @return 결과 파라미터 (JSON 문자열)
	 */
	public String restfulApiCallString(String url, Map<String, Object> param) {
		RestTemplate restTemplate = new RestTemplate();

		String parametrizedArgs = param.keySet().stream().map(k ->
			String.format("%s={%s}", k, k)
		).collect(Collectors.joining("&"));

		String callUrl = pBlockchainBaseUrl + url + "?" + parametrizedArgs;
		String result = restTemplate.getForObject(callUrl, String.class, param);
		log.debug("[restfulApiCall] url=" + url + ",\n parametrizedArgs=" + parametrizedArgs
				+ ",\n param=" + JsonUtils.convertvObjToJson(param) + ",\n result=" + result);
		return result;
	}
	
	public Map<String, Object> restfulApiCallResult(String url, Map<String, Object> param) {
		String result = this.restfulApiCallString(url, param);
		Map<String,Object> map = JsonUtils.convertJsonToMap(result);
		return map;
	}

	public boolean isApiResultSuccess(Map<String, Object> result) {
		if(result == null) return false;
		boolean success = (boolean)result.get("result");
		return success;
	}
	
	public Map<String, Object> getApiResultDataObject(Map<String, Object> result) {
		@SuppressWarnings("unchecked")
		Map<String,Object> data = (Map<String, Object>)result.get("data");
		return data;
	}

	public List<Map<String,Object>> getApiResultDataList(Map<String, Object> result) {
		@SuppressWarnings("unchecked")
		List<Map<String,Object>> data = (List<Map<String, Object>>)result.get("data");
		return data;
	}
	
	/* ==================================================================================
	 * 공통 모듈
	 */

	// 운영단체 별 블록체인 플랫폼 접속 정보 얻기.
	public BlockchainLinkInfoVO getBlockchainLinkInfo(String oprGrpId) {
		OprGrpVO vo = cmmService.getOprGrpInfo(oprGrpId);
		BlockchainLinkInfoVO infoVo = new BlockchainLinkInfoVO();
		infoVo.setBlockChainKd(vo.getBcKd());
		infoVo.setApiKey(vo.getBcApiKey());
		infoVo.setTokenId(vo.getBcTokenId());
		infoVo.setTokenSimbol(vo.getBcTokenSimbol());
		return infoVo;
	}
	
	// 성공결과 처리
	private BlockchainRetrunVO getReturnSuccess(String logTtl, Object result) {
		BlockchainRetrunVO retVo = new BlockchainRetrunVO();
		retVo.setSuccess(result);
		if(logTtl != null) {
			log.error("[" + logTtl + "] Success. retVo=" + retVo.toString());
		}
		return retVo;
	}

	// 실패결과 처리 - Api 실패 경우
	private BlockchainRetrunVO getReturnFailApi(String logTtl, Map<String, Object> result) {
		BlockchainRetrunVO retVo = new BlockchainRetrunVO();
		String errCd = (String)result.get("code");
		String errMsg = (String)result.get("message");
		retVo.setFailApi(errCd, errMsg);
		if(logTtl != null) {
			log.error("[" + logTtl + "] API Fail. errCd=" + errCd + ", errMsg=" + errMsg + ", retVo=" + retVo.toString());
		}
		return retVo;
	}

	// 실패결과 처리 - DB 처리 실패 경우
	private BlockchainRetrunVO getReturnFailDb(String logTtl, String errMsg) {
		BlockchainRetrunVO retVo = new BlockchainRetrunVO();
		retVo.setFailDb(errMsg);
		if(logTtl != null) {
			log.error("[" + logTtl + "] DB Fail. retVo=" + retVo.toString());
		}
		return retVo;
	}

	// 실패결과 처리 - Exception 실패 경우
	private BlockchainRetrunVO getReturnFailException(String logTtl, Exception e) {
		BlockchainRetrunVO retVo = new BlockchainRetrunVO();
		retVo.setFailException(e);
		if(logTtl != null) {
			log.error("[" + logTtl + "] Exception Fail. errMsg=" + e.getMessage() + ", retVo=" + retVo.toString());			
		}
		return retVo;
	}

}
