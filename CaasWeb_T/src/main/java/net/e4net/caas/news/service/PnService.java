package net.e4net.caas.news.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.news.mapper.PnMapper;
import net.e4net.caas.news.vo.TbPnPVO;
import net.e4net.caas.news.vo.TbPnRVO;

/**
 * 공지사항 서비스
 *
 * @author hee.woo
 * @since 2018.11.26
 */
@Service
public class PnService extends AbstractDao {
   
	@Autowired
    private PnMapper pnMapper;
    
    /**
     * 공지사항 목록 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public List<TbPnRVO> selectPnList(TbPnPVO pvo) {
        return pnMapper.selectPnList(pvo, MybatisUtils.pageBounds(pvo));
    }
   
    
    /**
     * 공지사항 상세조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public TbPnRVO selectPnDetail(TbPnPVO pvo) {
    	return pnMapper.selectPnDetail(pvo);
    }
    
}
