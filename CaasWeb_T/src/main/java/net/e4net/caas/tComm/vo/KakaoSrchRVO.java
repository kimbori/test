package net.e4net.caas.tComm.vo;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.KakaoRestVO;

@Data
@SuppressWarnings("serial")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class KakaoSrchRVO extends KakaoRestVO{

	//총 검색 갯수
	@JsonProperty("total_count")
	int totalCount;
	
	//total_count 중 노출가능한 검색 갯수
	@JsonProperty("pageable_count")
	int pageableCount;
	
	//마지막 페이지여부
	@JsonProperty("is_end")
	boolean isEnd;
	
	@JsonProperty("documents")
	private List<Document> documents;
	
	@Data
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class Document {
		//문서제목
		@JsonProperty("title")
		private String title;
		
		//문서본문 중 일부
		@JsonProperty("contents")
		private String contents;
		
		//문서 URL
		@JsonProperty("url")
		private String url;
		
		//문서글 작성시간. ISO 8601. [YYYY]-[MM]-[DD]T[hh]:[mm]:[ss].000+[tz]
		@JsonProperty("datetime")
		private String datetime;
	}
}