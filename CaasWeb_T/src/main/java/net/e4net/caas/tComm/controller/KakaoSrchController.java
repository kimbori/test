package net.e4net.caas.tComm.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.tComm.service.KakaoSrchService;
import net.e4net.caas.tComm.vo.KakaoSrchPVO;

/**
 * 카카오 검색 컨트롤러
 *
 * @author brkim
 * @since 2018.12.12
 * history  2018.12.12 최초작성
 */
@Controller
public class KakaoSrchController extends AbstractController {
	
	//kakao search 서비스
	@Autowired
	private KakaoSrchService service;
	
	@RequestMapping(value = "/moveKakaoSrch", method = RequestMethod.GET)
	public String moveKakaoSrh(Model model, HttpServletRequest request) throws CaasException {
		return "tComm/srchPag";
	}
	
	@RequestMapping(value = "/kakaoSrch")
	public ModelAndView search(HttpServletRequest request, KakaoSrchPVO pvo, HttpSession httpSession) throws CaasException {
		return service.search(request, pvo, httpSession);
	}
}