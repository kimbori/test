package net.e4net.caas.tComm.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.adapter.KakaoRestAdapter;
import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.tComm.vo.KakaoSrchPVO;
import net.e4net.caas.tComm.vo.KakaoSrchRVO;

/**
 * 검색 서비스
 *
 * @author brkim
 * @since 2018.12.12
 */ 

@Service
public class KakaoSrchService extends AbstractDao {
	
	//카카오 REST API 전용 어뎁터
	@Autowired
	private KakaoRestAdapter restAdapter;
	
	//카카오 Search REST API
	@Value("${kakao.srchRestUrl}") private String restUrl;
	
    public ModelAndView search(HttpServletRequest request, KakaoSrchPVO pvo, HttpSession httpSession) throws CaasException {
    	debug("###################카카오 Search REST API] START");
    	ModelAndView mav = new ModelAndView("tComm/srchAjax :: srchLst");
    	
    	debug("페이지 : " + pvo.getPage());
    	debug("검색값 : " + pvo.getQuery());
    	try {
    		/* STEP 1. 카카오 검색 REST API 호출
    		 * */
    		KakaoSrchRVO rvo = restAdapter.post(restUrl + "web", pvo, KakaoSrchRVO.class);
    		
    		debug("검색값 RVO : " + rvo.toString());

    		mav.addObject("pageNo", pvo.getPage());
    		mav.addObject("documentLst", rvo.getDocuments());
    		mav.addObject("hasNext", !rvo.isEnd());
    	} catch(Exception e) {
    		error(e.getMessage(), e);
    		throw new CaasException("9999", "kakao REST API 호출 시 오류가 발생하였습니다.");
    	}
    	debug("###################카카오 Search REST API] END");
    	
    	return mav;
    }
}