package net.e4net.caas.tComm.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.KakaoRestVO;

@Data
@SuppressWarnings("serial")
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
public class KakaoSrchPVO extends KakaoRestVO{

	//검색값
	@JsonProperty("query")
	String query;
	
	//정렬방식
	@JsonProperty("sort")
	String sort;	//accuracy:정확도순, recency:최신순
	
	//결과페이지번호(1-50 사이)
	@JsonProperty("page")
	int page;
	
	//한 페이지내 검색값 갯수(1-50 사이)
	@JsonProperty("size")
	int size;
}