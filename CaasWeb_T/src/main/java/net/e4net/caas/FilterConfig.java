package net.e4net.caas;

import java.util.Arrays;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.e4net.caas.common.filter.KrEncodingFilter;
import net.e4net.caas.common.util.LogObject;

@Configuration
public class FilterConfig extends LogObject{
	
	@SuppressWarnings("unchecked")
	@Bean
    public FilterRegistrationBean<KrEncodingFilter> myFilter(){
        @SuppressWarnings("rawtypes")
		FilterRegistrationBean bean = new FilterRegistrationBean();
        bean.setFilter(new KrEncodingFilter());
        bean.setUrlPatterns(Arrays.asList("/public/iniPayNotiUrl", "/public/cert/certApprove"));
        return bean;
    } 
}