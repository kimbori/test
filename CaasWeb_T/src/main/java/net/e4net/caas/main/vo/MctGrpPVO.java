package net.e4net.caas.main.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbGrpMctDVO;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
public class MctGrpPVO extends TbGrpMctDVO {
	private long cnrSq;				//기부자SEQ
}