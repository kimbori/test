package net.e4net.caas.main.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.vo.dvo.TbRcntSrchDVO;
import net.e4net.caas.main.mapper.SrchMapper;
import net.e4net.caas.main.vo.SrchPVO;

/**
 * 검색 서비스
 *
 * @author brkim
 * @since 2018.12.04
 */
@Service
public class SrchService extends AbstractDao {
    @Autowired
    private SrchMapper mapper;
    
    /**
     * 최근검색 리스트 조회
     *
     * @param  SrchPVO 조회조건
     * @return List<TbRcntSrchDVO> 최근검색 리스트
     */
    public List<TbRcntSrchDVO> selectRcntSrchList(SrchPVO pvo) {
        return mapper.selectRcntSrchList(pvo);
    }
    
    /**
     * 검색 정보 등록
     *
     * @param pvo 검색정보
     * @return 등록건수
     */
    public int insertSrch(SrchPVO pvo) {
    	TransactionStatus stat = null;
    	int cnt = 0;
    	
    	try {
    		stat = getTransactionStatus("insertSrch");
    		cnt = mapper.insertSrch(pvo);
    		if(cnt != 1) {
        		throw new CaasException("99999", "검색 정보 등록 시 오류발생");
        	}
    		transactionCommit(stat);
    	} catch (Exception e) {
    		error("deleteSrch", e);
    		transactionRollback(stat);
    	}
    	return cnt;
    }
    
    /**
     * 검색 정보 삭제
     *
     * @param pvo 검색정보
     * @return 삭제건수
     */
    public int deleteSrch(SrchPVO pvo) {
    	TransactionStatus stat = null;
    	int cnt = 0;
    	
    	try {
    		stat = getTransactionStatus("deleteSrch");
    		cnt = mapper.deleteSrch(pvo);
    		
    		//검색정보 전체 삭제 후, 조회 건수 0이면 정상 
    		if(pvo.getRcntSrchSq() == 0) {
    			cnt = (mapper.selectRcntSrchList(pvo).size() == 0) ? 1 : 0;
    		}else {
    			if(cnt != 1) {
            		throw new CaasException("99999", "검색 정보 삭제 시 오류발생");
            	}
    		}
    		transactionCommit(stat);
    	} catch (Exception e) {
    		error("deleteSrch", e);
    		transactionRollback(stat);
    	}
    	return cnt;
    }
}