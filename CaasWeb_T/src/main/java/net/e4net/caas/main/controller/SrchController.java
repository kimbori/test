package net.e4net.caas.main.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.common.vo.dvo.TbRcntSrchDVO;
import net.e4net.caas.dntn.vo.CmpPVO;
import net.e4net.caas.dntn.vo.CmpRVO;
import net.e4net.caas.main.service.MainService;
import net.e4net.caas.main.service.SrchService;
import net.e4net.caas.main.vo.SrchPVO;

/**
 * 검색 컨트롤러
 *
 * @author brkim
 * @since 2018.12.04
 * history  2018.12.04 최초작성
 */
@Controller
public class SrchController extends AbstractController {
	
    @Autowired
    private SrchService service;
    
    @Autowired
    private MainService mainService;
    
    /**
     * <pre>
     * 메인 페이지
     * </pre>
     *
     * @param SrchPVO 조회조건
     * @return
     */
    @RequestMapping(value = "/moveSrch", method = RequestMethod.GET)
    public String moveSrch(Model model, HttpServletRequest request) throws Exception {
		return "main/srchPag";
    }
    
    /**
     * <pre>
     * 최근검색 리스트 조회
     * </pre>
     *
     * @param SrchPVO 조회조건
     * @return List<TbRcntSrchDVO> 최근검색 리스트
     */
    @RequestMapping(value = "/main/srchAjax", method = RequestMethod.POST)
    public ModelAndView selectSrchAjax(@ModelAttribute SrchPVO pvo, HttpServletRequest request) throws Exception {
    	ModelAndView mav = new ModelAndView("main/srchAjax :: srchLst");
    	
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	if (uVO instanceof UserVO) {
    		pvo.setCnrSq(uVO.getCnrSq());
    	}
    	
    	/* 최근검색 리스트 */
    	List<TbRcntSrchDVO> srchLst = service.selectRcntSrchList(pvo);
    	
        mav.addObject("srchLst", srchLst);
        
		return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 검색하기
     * </pre>
     *
     * @param SrchPVO 입력정보
     * @return status 성공여부
     */
    @RequestMapping(value = "/main/srchIns", method = RequestMethod.POST)
    public ModelAndView insertSrch(@ModelAttribute SrchPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("main/srchDntnAjax :: dntnLst");
        Status status = new Status();

    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setCnrSq(uVO.getCnrSq());
        pvo.setFstRegUsid(uVO.getCnrNm());
        pvo.setLstChUsid(uVO.getCnrNm());
        
        try {
        	int insCnt = service.insertSrch(pvo);
        	if(insCnt != 1) {
        		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
        	}
        	
        	 /* 기부 리스트 조회 */
        	CmpPVO cmpPVO = new CmpPVO();
        	cmpPVO.setPageNo(pvo.getPageNo());
        	cmpPVO.setCountPage(pvo.getCountPage());
        	cmpPVO.setOprGrpId(uVO.getOprGrpId());
        	cmpPVO.setCmpNm(pvo.getSrchKey());
        	
            List<CmpRVO> dntnLst = mainService.selectDntnList(cmpPVO);

        	int totCnt = dntnLst.size() == 0 ? 0 : dntnLst.get(0).getTotalCnt();
            int pageNo = StringUtils.parseInt(pvo.getPageNo(), 1);
            boolean hasNext = totCnt - (pageNo * pvo.getCountPage()) > 0;
            
            mav.addObject("hasNext", hasNext);
            mav.addObject("dntnLst", dntnLst);
        } catch(Exception e) {
        	status.setFail("처리 중 오류가 발생하였습니다.");
        	return getFailModelAndView(mav, status);
        }
        status.setOk();
        return getOkModelAndView(mav, status);
    }
    
    /**
     * <pre>
     * 검색건 삭제
     * </pre>
     *
     * @param SrchPVO 입력정보
     * @return status 성공여부
     */
    @RequestMapping(value = "/main/srchDel", method = RequestMethod.POST)
    public ModelAndView deleteSrch(@ModelAttribute SrchPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();

    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setCnrSq(uVO.getCnrSq());
        
        try {

        	int insCnt = service.deleteSrch(pvo);
        	if(insCnt != 1) {
        		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
        	}
        } catch(Exception e) {
        	status.setFail("처리 중 오류가 발생하였습니다.");
        	return getFailModelAndView(mav, status);
        }
        status.setOk();
        return getOkModelAndView(mav, status);
    }
}