package net.e4net.caas.main.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.dntn.vo.CmpPVO;
import net.e4net.caas.dntn.vo.CmpRVO;
import net.e4net.caas.main.vo.MctGrpPVO;
import net.e4net.caas.main.vo.MctGrpRVO;

/**
 * 기부메인 매핑
 *
 * @author brkim
 * @since 2018.11.20
 */
@Repository
public interface MainMapper {
	
	/**
     * 관심가맹단체 리스트
     */
    public List<MctGrpRVO> selectDntnCncrList(MctGrpPVO pvo);
    
	/**
     * 가맹단체 리스트
     */
    public List<MctGrpRVO> selectDntnMctList(MctGrpPVO pvo, PageBounds pageBounds);
	
    /**
     * TODAY 참여자수, 모금액
     */
    public CmpRVO selectTodayDntn(CmpPVO pvo);
    
    /**
     * 기부 리스트
     */
    public List<CmpRVO> selectDntnList(CmpPVO pvo, PageBounds pageBounds);
    
    /**
     * 기본후원
     */
    public CmpRVO selectDntnDft(CmpPVO pvo);
    
    /**
     * 관심가맹단체 등록
     */
    public int insertDntnCncr(MctGrpPVO pvo);
    
    /**
     * 관심가맹단체 해제
     */
    public int deleteDntnCncr(MctGrpPVO pvo);
}