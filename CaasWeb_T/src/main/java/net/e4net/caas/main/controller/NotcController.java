package net.e4net.caas.main.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.common.vo.dvo.TbNotcLstDVO;
import net.e4net.caas.main.service.NotcService;
import net.e4net.caas.main.vo.NotcRVO;

/**
 * 알림 컨트롤러
 *
 * @author brkim
 * @since 2018.12.10
 * history  2018.12.10 최초작성
 */
@Controller
public class NotcController extends AbstractController {
	
    @Autowired
    private NotcService service;
    
    /**
     * <pre>
     * 메인 페이지
     * </pre>
     *
     * @param NotcPVO 조회조건
     * @return
     */
    @RequestMapping(value = "/moveNotc", method = RequestMethod.GET)
    public String moveNotc(Model model, HttpServletRequest request) throws Exception {
		return "main/NotcPag";
    }
    
    /**
     * <pre>
     * 알람 리스트 조회
     * </pre>
     *
     * @param TbNotcLstDVO 조회조건
     * @return List<TbNotcLstDVO> 알람 리스트
     */
    @RequestMapping(value = "/main/notcAjax", method = RequestMethod.POST)
    public ModelAndView selectNotcAjax(@ModelAttribute TbNotcLstDVO pvo, HttpServletRequest request) throws Exception {
    	ModelAndView mav = new ModelAndView("main/notcAjax :: notcLst");
    	
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	if (uVO instanceof UserVO) {
    		pvo.setCnrSq(uVO.getCnrSq());
    	}
    	
    	debug("#pageNo : " + pvo.getPageNo());
    	/* 알람 리스트 */
    	List<NotcRVO> notcLst = service.selectNotcList(pvo);
    	
    	int totCnt = notcLst.size() == 0 ? 0 : notcLst.get(0).getTotalCnt();
    	int pageNo = StringUtils.parseInt(pvo.getPageNo(), 1);
    	boolean hasNext = totCnt - (pageNo * pvo.getCountPage()) > 0;
    	
    	debug("hasNext : " + hasNext);
    	
        mav.addObject("notcLst", notcLst);
        mav.addObject("hasNext", hasNext);
        
		return getOkModelAndView(mav);
    }
}