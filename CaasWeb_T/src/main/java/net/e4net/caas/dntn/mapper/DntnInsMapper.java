package net.e4net.caas.dntn.mapper;

import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.dvo.TbDntLstDVO;

/**
 * 기부하기 매핑
 *
 * @author brkim
 * @since 2018.11.20
 */
@Repository
public interface DntnInsMapper {
	
    /**
     * 기부 등록
     */
    public int insertDntn(TbDntLstDVO pvo);
    
    /**
     * 기부(캠페인정보) 수정_모금토큰update
     */
    public int updateDntn(TbDntLstDVO pvo);
}