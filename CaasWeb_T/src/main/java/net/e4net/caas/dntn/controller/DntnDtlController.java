package net.e4net.caas.dntn.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.dntn.service.DntnDtlService;
import net.e4net.caas.dntn.vo.CmpPVO;
import net.e4net.caas.dntn.vo.CmpRVO;

/**
 * 기부상세 컨트롤러
 *
 * @author brkim
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
public class DntnDtlController extends AbstractController {
	
    @Autowired
    private DntnDtlService service;
    
    /**
     * <pre>
     * 기부상세 페이지 이동
     * </pre>
     *
     * @param SrchPVO 조회조건
     * @return List<CmpRVO> 기부이미지리스트, CmpRVO 기부상세, List<CmpRVO> 기부사용내역리스트
     */
    @RequestMapping(value = "/moveDntnDtl", method = RequestMethod.GET)
    public ModelAndView moveDntnPag(@ModelAttribute CmpPVO pvo, HttpServletRequest request) throws Exception {
    	ModelAndView mav = new ModelAndView("dntn/dntnDtlPag");
    	
    	/* 기부 이미지 */
    	List<CmpRVO> dntnImgLst = service.selectDntnImg(pvo);
    	
    	/* 기부소개 */
        CmpRVO dntnDtl = service.selectDntnDtl(pvo);
        
        /* 기부사용내역 */
        List<CmpRVO> dntnCostLst = service.selectDntnCost(pvo);
        
        mav.addObject("dntnImgLst", dntnImgLst);
        mav.addObject("dntnDtl", dntnDtl);
        mav.addObject("dntnCostLst", dntnCostLst);
        
		return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 기부소식 리스트 조회
     * </pre>
     *
     * @param SrchPVO 조회조건 값
     * @return List<CmpRVO> 기부소식리스트_JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/dntn/dntnNewsAjax", method = RequestMethod.POST)
    public ModelAndView selectDntnNews(@ModelAttribute CmpPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("dntn/dntnDtlNewsAjax :: dntnNewsAjax");
        
        /* 기부소식 리스트 */
        List<CmpRVO> newsLst = service.selectDntnNews(pvo);
        
        int totCnt = newsLst.size() == 0 ? 0 : newsLst.get(0).getTotalCnt();
        int pageNo = StringUtils.parseInt(pvo.getPageNo(), 1);
        boolean hasNext = totCnt - (pageNo * pvo.getCountPage()) > 0;
        
        mav.addObject("pvo", pvo);
        mav.addObject("pageNo", pageNo);
        mav.addObject("totCnt", totCnt);
        mav.addObject("hasNext", hasNext);
        mav.addObject("newsLst", newsLst);
        
        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 응원글 리스트 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return List<CmpRVO> 응원글리스트_JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/dntn/dntnCmmtAjax", method = RequestMethod.POST)
    public ModelAndView selectDntnCmmt(@ModelAttribute CmpPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("dntn/dntnDtlCmmtAjax :: dntnCmmtAjax");
        
        /* 응원글 리스트 */
        List<CmpRVO> cmmtLst = service.selectDntnCmmt(pvo);

        int totCnt = cmmtLst.size() == 0 ? 0 : cmmtLst.get(0).getTotalCnt();
        int pageNo = StringUtils.parseInt(pvo.getPageNo(), 1);
        boolean hasNext = totCnt - (pageNo * pvo.getCountPage()) > 0;
        
        mav.addObject("pageNo", pageNo);
        mav.addObject("totCnt", totCnt);
        mav.addObject("hasNext", hasNext);
        mav.addObject("cmmtLst", cmmtLst);
        
        return getOkModelAndView(mav);
    }
}