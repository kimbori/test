package net.e4net.caas.dntn.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.vo.dvo.TbDntLstDVO;
import net.e4net.caas.dntn.mapper.DntnInsMapper;

/**
 * 기부하기 서비스
 *
 * @author brkim
 * @since 2018.11.20
 */
@Service
public class DntnInsService extends AbstractDao {
    @Autowired
    private DntnInsMapper mapper;
    
    /**
     * 기부내역 등록
     *
     * @param pvo 기부등록 정보
     * @return 등록건수
     */
    public int insertDntn(TbDntLstDVO pvo) {
    	TransactionStatus stat = null;
    	int cnt = 0;
    	
    	try {
    		pvo.setNthnDntnYn("N");		//N:기본(d), Y:무기명요청
    		pvo.setRecpIssuYn("N");		//N:미발행(d), Y:발급
    		pvo.setInhgvtaxNtfyYn("N");	//N:미포함(d), Y:포함, X:제외대상
    		
    		stat = getTransactionStatus("insertDntn");
    		cnt = mapper.insertDntn(pvo); //기부내역 insert
    		if(cnt != 1) {
        		throw new CaasException("99999", "기부내역 등록 시 오류발생");
        	}
    		
    		cnt = mapper.updateDntn(pvo); //캠페인 모금토큰 update
    		if(cnt != 1) {
        		throw new CaasException("99999", "모금토큰 update 시 오류발생");
        	}
    		transactionCommit(stat);
    	} catch (Exception e) {
    		error("deleteSrch", e);
    		transactionRollback(stat);
    	}
    	return cnt;
    }
}