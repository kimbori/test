package net.e4net.caas.dntn.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.dntn.vo.CmpPVO;
import net.e4net.caas.dntn.vo.CmpRVO;

/**
 * 기부상세 매핑
 *
 * @author brkim
 * @since 2018.11.20
 */
@Repository
public interface DntnDtlMapper {
	/**
     * 기부 이미지
     */
    public List<CmpRVO> selectDntnImg(CmpPVO pvo);
	
    /**
     * 기부 상세
     */
    public CmpRVO selectDntnDtl(CmpPVO pvo);
    
    /**
     * 기부 비용 사용계획
     */
    public List<CmpRVO> selectDntnCost(CmpPVO pvo);
    
    /**
     * 기부소식 리스트
     */
    public List<CmpRVO> selectDntnNews(CmpPVO pvo, PageBounds pageBounds);
    
    /**
     * 기부 응원글 리스트
     */
    public List<CmpRVO> selectDntnCmmt(CmpPVO pvo, PageBounds pageBounds);
}