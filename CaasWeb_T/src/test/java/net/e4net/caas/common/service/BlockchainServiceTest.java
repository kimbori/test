package net.e4net.caas.common.service;

import java.io.IOException;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.CaasApplicationTests;
import net.e4net.caas.common.util.JsonUtils;
import net.e4net.caas.common.vo.BlockchainRetrunVO;

@Slf4j
public class BlockchainServiceTest extends CaasApplicationTests {

	public static final String	OPR_GRP_ID = "e4net";		// 테스트 운영단체ID
	public static final String	TEST_WALLET_USER = "0x0bfa391abc885d385db1da9e7b50b3ccb711679f";	// 테스트용 기부자 지갑
	public static final String	TEST_WALLET_CAMPAIN = "0x8867eaca298d95f7be7f9231f69b35135deda361";	// 테스트용 캠페인 지갑

	@Autowired private BlockchainService service;
	@Autowired BlockchainApiService bcApiService;

	/* ==================================================================================
	 * 	Luniverse 연계 API Test
	 */

	/*
	 * 지갑 생성 
	 */
	//@Test
	public void createWalletTest() {
		/*
		 *	운영단체 : "e4net" => "0xf91f52d9129a0117830e0066ff7837d962d13073"
		 *	가맹단체 : "e4net", "22222" => "0xa5562ecd582267c56c5a23e2ee687161435cd01b"
		 *	캠페인   : "e4net", "22222", 2 => "0x8867eaca298d95f7be7f9231f69b35135deda361"
		 *	기부자   : "e4net", 1 => "0x0bfa391abc885d385db1da9e7b50b3ccb711679f"
		 */
		//BlockchainRetrunVO vo = service.createWalletGrpMctNew(OPR_GRP_ID, "2222");		// 가맹단체 지갑
		//BlockchainRetrunVO vo = service.createWalletCmpInfNew(OPR_GRP_ID, "22222", 1);	// 캠페인 가맹단체 잘못
		BlockchainRetrunVO vo = service.createWalletCmpInfNew(OPR_GRP_ID, "22222", 2);	// 캠페인 지갑
		//BlockchainRetrunVO vo = service.createWalletDntPrsnNew(OPR_GRP_ID, 111);				// 기부자 지갑
		//BlockchainRetrunVO vo = service.createWalletSub(OPR_GRP_ID, "04", "2018120500000000074");	// 예외로 생성
		log.debug("vo=" + vo.toString());
	}

	/*
	 * 토큰 전송(이체)
	 */
	@Test
	public void tokenTransferTest() throws InterruptedException {
		//BlockchainRetrunVO retVo = service.walletTokenDeposit(OPR_GRP_ID, TEST_WALLET_USER, 1000, "21");
		//BlockchainRetrunVO retVo = service.walletTokenWithdraw(OPR_GRP_ID, TEST_WALLET_USER, 100, "51");
		BlockchainRetrunVO retVo = service.walletTokenTransfer(OPR_GRP_ID, TEST_WALLET_USER, TEST_WALLET_CAMPAIN, 300, "31");

		if(retVo.isSuccess()) {
			log.debug("success=" + retVo.isSuccess() + ", ToknTrnfSq=" + retVo.getToknTrnfSq());
		} else {
			log.error("success=" + retVo.isSuccess() + ", errMsg=" + retVo.getErrMsg());
		}
		
		log.debug("잔고동기화가 Aync로 지연 수행되므로 처리하는 동안 기다려 주어야 한다.");
		Thread.sleep(5 * 1000);
		//log.debug("vo1=" + service.convertvObjToJson(vo1));
		
		//getWalletBalanceTestSub(OPR_GRP_ID, "0x0bfa391abc885d385db1da9e7b50b3ccb711679f");		// 캠페인 지갑
		//getWalletBalanceTestSub(OPR_GRP_ID, "0x8867eaca298d95f7be7f9231f69b35135deda361");		// 캠페인 지갑
		
		//long balance = service.getWalletBalance(oprGrpId, walletAddr);
		//log.debug("balance=" + balance + ", walletAddr=" + walletAddr + ", oprGrpId=" + oprGrpId);
	}

	/*
	 * 지갑 잔고 조회
	 */
	//@Test
	public void getWalletBalanceTest() {
		//getWalletBalanceTestSub(OPR_GRP_ID, "0xf91f52d9129a0117830e0066ff7837d962d13073");		// 운영단체 지갑
		//getWalletBalanceTestSub(OPR_GRP_ID, "0xa5562ecd582267c56c5a23e2ee687161435cd01b");		// 가맹단체 지갑
		getWalletBalanceTestSub(OPR_GRP_ID, "0x8867eaca298d95f7be7f9231f69b35135deda361");		// 캠페인 지갑
		//getWalletBalanceTestSub(OPR_GRP_ID, TEST_WALLET_USER);		// 기부자 지갑
	}
	private void getWalletBalanceTestSub(String oprGrpId, String walletAddr) {
		//long balance = service.getWalletBalance(oprGrpId, walletAddr);
		BlockchainRetrunVO retVo = service.getWalletBalanceNew(oprGrpId, walletAddr);
		log.debug("success=" + retVo.isSuccess() + ", balance=" + retVo.getBalance() + ", walletAddr=" + walletAddr + ", oprGrpId=" + oprGrpId);
	}
	
	/* ==================================================================================
	 * 	Blockchain API RESTFul Test
	 *	(참조) https://stackoverflow.com/questions/8297215/spring-resttemplate-get-with-parameters
	 */
	//@Test
	public void restfulTest1() {
		RestTemplate restTemplate = new RestTemplate();

        URI uri = UriComponentsBuilder.newInstance().scheme("http").host("192.168.3.27").port("8081")
        		.path("/transAnalytics")	//.path("/cgi/{type}")
                .queryParam("fromDate", "2018-11-01")
                .queryParam("toDate", "2018-11-26")
                .queryParam("productTokenId", "304")
                .queryParam("apikey", "1xtd2ypSLgNYKA2CMJPH3PWAcz2VgbCiwdKLFbjytdgQgZU42p9UqwBCDVQqGLuHJf")
                .build()	//.expand(file.getType())
                .encode()
                .toUri();
        
		String result = restTemplate.getForObject(uri, String.class);
		Map<String, Object> map = JsonUtils.convertJsonToMap(result);
		//List<Map<String,Object>> data = (List<Map<String, Object>>)map.get("data");
		// ((List<Map<String, Object>>)map.get("data")).get(0)
		// ((List<Map<String, Object>>)map.get("data")).get(0).get("count")

		log.debug("result=" + result);
		log.debug("map=" + JsonUtils.convertvObjToJson(map));
	}

	//@Test
	public void restfulTest2() {
		RestTemplate restTemplate = new RestTemplate();
		Map<String, Object> param = new HashMap<>();
		param.put("fromDate", "2018-11-01");
		param.put("toDate", "2018-11-26");
		param.put("productTokenId", "304");
		param.put("apikey", "1xtd2ypSLgNYKA2CMJPH3PWAcz2VgbCiwdKLFbjytdgQgZU42p9UqwBCDVQqGLuHJf");

        String urlParm = "?fromDate={fromDate}&toDate={toDate}&productTokenId={productTokenId}&apikey={apikey}";
		String result = restTemplate.getForObject("http://192.168.3.27:8081/transAnalytics" + urlParm, String.class, param);
		log.debug("result=" + result);
	}

	//@Test
	public void restfulTest3() {
		RestTemplate restTemplate = new RestTemplate();

        String urlParm = "?fromDate={fromDate}&toDate={toDate}&productTokenId={productTokenId}&apikey={apikey}";
		String result = restTemplate.getForObject("http://192.168.3.27:8081/transAnalytics" + urlParm, String.class,
				"2018-11-01", "2018-11-26", "304", "1xtd2ypSLgNYKA2CMJPH3PWAcz2VgbCiwdKLFbjytdgQgZU42p9UqwBCDVQqGLuHJf");
		log.debug("result=" + result);
	}

	//@Test
	public void restfulTest4() throws IOException {
		RestTemplate restTemplate = new RestTemplate();

		Map<String, Object> param = new HashMap<>();
		param.put("fromDate", "2018-11-01");
		param.put("toDate", "2018-11-26");
		param.put("productTokenId", "304");
		param.put("apikey", "1xtd2ypSLgNYKA2CMJPH3PWAcz2VgbCiwdKLFbjytdgQgZU42p9UqwBCDVQqGLuHJf");

		String parametrizedArgs = param.keySet().stream().map(k ->
			String.format("%s={%s}", k, k)
		).collect(Collectors.joining("&"));
		
		String result = restTemplate.getForObject("http://192.168.3.27:8081/transAnalytics?" + parametrizedArgs, String.class, param);
		log.debug("result=" + result);
	}

	//@Test
	public void moduleTest() {
		Map<String, Object> param = new HashMap<>();
		param.put("fromDate", "2018-11-01");
		param.put("toDate", "2018-11-26");
		param.put("productTokenId", "304");
		param.put("apikey", "1xtd2ypSLgNYKA2CMJPH3PWAcz2VgbCiwdKLFbjytdgQgZU42p9UqwBCDVQqGLuHJf");
		
		Map<String, Object> result = bcApiService.restfulApiCallResult("/transAnalytics", param);
		List<Map<String,Object>> dataList = bcApiService.getApiResultDataList(result);
		log.debug("result=" + JsonUtils.convertvObjToJson(result));
		log.debug("success=" + bcApiService.isApiResultSuccess(result));
		for(Map<String,Object> data : dataList) {
			log.debug("data=" + JsonUtils.convertvObjToJson(data));
		}
	}

	//@Test
	public void testTemp() {
//		KakaoPayApiReadyPVO pvo = new KakaoPayApiReadyPVO();
//		KakaoPayApiReadyRVO result = restAdapter.get("http://192.168.3.27:8081/hello", pvo, KakaoPayApiReadyRVO.class);

//		String resultJson = "{\"result\":true,\"data\":{\"userId\":“xxxxxxxxxxxxxx\",\"address\":“xxxxxxxxxxxxxxxxxx\"}}";
//		JSONObject jsonObj = JSONObject.fromObject(resultJson);
//		Map<String,Object> map = restAdapter.get("http://192.168.3.27:8081/hello", vo, HashMap.class);
		//		JSONParser jsonParser = new JSONParser();
//		Map<String,Object> map = jsonParser.parseMap(resultJson);

		
//		ObjectReader reader = new ObjectMapper().readerFor(Map.class);
//		Map<String, Object> map = reader.readValue("{\"foo\":\"val\"}");		
//
//		ObjectMapper mapper = new ObjectMapper();
//		Map<String, Object> map2 = mapper.readValue("{\"foo\":\"val\"}", HashMap.class);
		
//		ObjectMapper om = new ObjectMapper().readerFor(Map.class);
//		TypeReference<HashMap<String,Object>> typeRef = new TypeReference<HashMap<String,Object>>() {};
	}

}