package net.e4net.caas;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class CaasApplicationTests {

    @Rule public TestName testName = new TestName();

    @Before
	public void beforeUnitTest() {

    	log.debug("===========================================================[before] " + testName.getMethodName());
	}

	@After
	public void afterUnitTest() {
		log.debug("===========================================================[after] " + testName.getMethodName());
	}

}
