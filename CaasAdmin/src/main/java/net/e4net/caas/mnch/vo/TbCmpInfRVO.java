package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.TbCmpInfDVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class TbCmpInfRVO extends TbCmpInfDVO{
	
	private String trgtToknFmt; 
	private String dnmrToknFmt; 
	private String dnmrStatDtFmt; 
	private String dnmrEndDtFmt; 
	private String dnmrStatNm; 
	private String dnmrTpNm; 
	private String dnmrKnNm;
	private String oprGrpNm;
	private String mrchGrpNm;
}
