package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class CmpCostAdRVO extends PageVO{
	private long cmpCostSq;
	private long cmpSq;
	private int od;
	private String usePlan;
	private String useAmt;
	private String oprGrpNm;
	private String mrchGrpNm;
	private String cmpNm;
	private String fstRegDttm;
	private String fstRegUsid;
	private String lstChDttm;
	private String lstChUsid;
}
