package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.TbDntLstDVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class TbDntLstRVO extends TbDntLstDVO{
	
	private String dntnAmtFmt;
	private String dntnDttmFmt;
	private String cmpNm;
	private String dntnKnNm;
	private String cnrNm;
	private String mrchGrpNm;
	private String oprGrpNm;
	
}
