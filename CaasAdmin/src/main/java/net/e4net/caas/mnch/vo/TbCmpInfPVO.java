package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.TbCmpInfDVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class TbCmpInfPVO extends TbCmpInfDVO{
	
	private String oprGrpId;
	private String mrchGrpId;
	private String srchCmpSq;
	private String prcLgnId;

}
