package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.TbCmpNewsDVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class TbCmpNewsRVO extends TbCmpNewsDVO{
	
	private String cmpNm;
	private String fstRegDttmFmt;
	private String lstChDttmFmt;
	private String mrchGrpNm;
	private String oprGrpNm;

}
