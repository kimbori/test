package net.e4net.caas.mnch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.mapper.FxprDntnAdMapper;
import net.e4net.caas.mnch.vo.TbCmpFsprPVO;
import net.e4net.caas.mnch.vo.TbCmpFsprRVO;

/**
 * 정기기부 관리 서비스
 *
 * @author hee.woo
 * @since 2018.11.23
 */
@SuppressWarnings("serial")
@Service
public class FxprDntnAdService extends AbstractDao {
   
	@Autowired
    private FxprDntnAdMapper fxprDntnAdMapper;
    
    /**
     * 정기기부 목록 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public List<TbCmpFsprRVO> selectFsprDntnList(TbCmpFsprPVO pvo, UserVO userEtt) {
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
    	//가맹단체관리자인 경우, 관리자레벨이 최상위 인 경우엔 고정값으로 자기가 속해있는 가맹단체ID를 셋팅
    	if( "2".equals(userEtt.getGrpGb()) ) {
    		pvo.setMrchGrpId(userEtt.getMrchGrpId());
    	}
    	
        return fxprDntnAdMapper.selectFsprDntnList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
   
}
