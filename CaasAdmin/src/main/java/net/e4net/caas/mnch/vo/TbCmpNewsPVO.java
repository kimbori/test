package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.TbCmpNewsDVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class TbCmpNewsPVO extends TbCmpNewsDVO{
	
	private String oprGrpId;
	private String mrchGrpId;
	private String srchCmpSq;
	private String cmpNm;

}
