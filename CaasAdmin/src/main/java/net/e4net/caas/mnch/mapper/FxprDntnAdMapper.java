package net.e4net.caas.mnch.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.mnch.vo.TbCmpFsprPVO;
import net.e4net.caas.mnch.vo.TbCmpFsprRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author hee.woo
 * @since 2018.11.23
 */
@Repository
public interface FxprDntnAdMapper {
	
    /**
     * 정기기부 목록 조회
     */
    public List<TbCmpFsprRVO> selectFsprDntnList(TbCmpFsprPVO pvo, PageBounds pageBounds);
    
 
}
