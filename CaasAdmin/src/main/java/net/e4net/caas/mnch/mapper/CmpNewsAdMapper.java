package net.e4net.caas.mnch.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.mnch.vo.TbCmpNewsPVO;
import net.e4net.caas.mnch.vo.TbCmpNewsRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author hee.woo
 * @since 2018.11.23
 */
@Repository
public interface CmpNewsAdMapper {
	
    /**
     * 캠페인 소식 목록 조회
     */
    public List<TbCmpNewsRVO> selectCmpNewsList(TbCmpNewsPVO pvo, PageBounds pageBounds);
    
    /**
     * 캠페인 소식 상세조회
     */
    public TbCmpNewsRVO selectCmpNewsDetail(TbCmpNewsPVO pvo);
    
    /**
     * 캠페인 소식 등록
     */
    public int insertCmpNews(TbCmpNewsPVO pvo);
    
    /**
     * 캠페인 소식 수정
     */
    public int updateCmpNews(TbCmpNewsPVO pvo);
    
    /**
     * 캠페인 소식 수정
     */
    public int deleteCmpNews(TbCmpNewsPVO pvo);
    
    /**
     * 캠페인목록 조회
     */
    public List<TbCmpNewsRVO> selectCmpList(TbCmpNewsPVO pvo);
    
}
