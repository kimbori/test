package net.e4net.caas.mnch.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.mapper.CmpImgAdMapper;
import net.e4net.caas.mnch.vo.CmpImgAdPVO;
import net.e4net.caas.mnch.vo.CmpImgAdRVO;

/**
 * 캠페인이미지 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class CmpImgAdService extends AbstractDao {
    @Autowired
    private CmpImgAdMapper mapper;
    
    /**
     * 캠페인이미지 조회
     *
     * @param pvo 캠페인이미지 VO 
     * @return 검색결과 
     */
    public List<CmpImgAdRVO> selectCmpImgAdList(CmpImgAdPVO pvo, UserVO userEtt) {
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
    	//가맹단체관리자인 경우, 관리자레벨이 최상위 인 경우엔 고정값으로 자기가 속해있는 가맹단체ID를 셋팅
    	if( "2".equals(userEtt.getGrpGb()) ) {
    		pvo.setMrchGrpId(userEtt.getMrchGrpId());
    	}
    	
        return mapper.selectCmpImgAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 캠페인이미지 조회
     *
     * @param pvo 캠페인이미지 VO 
     * @return 검색결과 
     */
    public List<CmpImgAdRVO> selectCmpImg(CmpImgAdPVO pvo) {
        return mapper.selectCmpImg(pvo);
    }
    
    /**
     * 캠페인이미지 등록 조회
     *
     * @param pvo 캠페인이미지 VO
     * @return 검색결과
     */
    public int selectCmpImgAdCnt(CmpImgAdPVO pvo) {
        return mapper.selectCmpImgAdCnt(pvo);
    }
    
    /**
     * 캠페인이미지 등록
     *
     * @param pvo 캠페인이미지 VO
     * @return 등록건수
     */
    public int insertCmpImgAd(CmpImgAdPVO pvo, HttpServletRequest request){
    	String inImgPth [] = request.getParameterValues("inImgPth");
    	String inImgInf [] = request.getParameterValues("inImgInf");
    	int procCnt = 0;
    	for( int i = 0; i<inImgPth.length; i++ ) {
    		if( !StringUtils.isNullOrBlank(inImgPth[i]+"") ) {
    			pvo.setImgPth(inImgPth[i]);
    			pvo.setImgInf(inImgInf[i]);
    			procCnt = mapper.insertCmpImgAd(pvo);
    		}
    	}
    	
        return procCnt;
    }
    
    /**
     * 캠페인이미지 수정
     *
     * @param pvo 캠페인이미지 수정 정보
     * @return 수정결과
     */
    public int updateCmpImgAd(CmpImgAdPVO pvo, HttpServletRequest request) throws Exception {
    	/*TransactionStatus status = null;
    	try {
    		status = getTransactionStatus("updateCmpImgAd");
    		int procCnt = 0;
    	
	    	//기존 데이터 삭제
    		procCnt = deleteCmpImgAd(pvo);
    		if(procCnt < 1) {
        		throw new CaasException("99999", "캠페인이미지 수정 오류");
        	}
    		
	    	//신규 데이터 추가
    		procCnt = insertCmpImgAd(pvo, request);
    		if(procCnt < 1) {
        		throw new CaasException("99999", "캠페인이미지 수정 오류");
        	}
	    	
    	}catch(Exception e) {
    		error("updateCmpImgAd", e);
    		transactionRollback(status);
    		throw e;
    	}*/
    	
    	int procCnt = 0;
    	
    	//기존 데이터 삭제
		procCnt = deleteCmpImgAd(pvo);
		if(procCnt < 1) {
    		throw new CaasException("99999", "캠페인이미지 수정 오류");
    	}
		
    	//신규 데이터 추가
		procCnt = insertCmpImgAd(pvo, request);
		if(procCnt < 1) {
    		throw new CaasException("99999", "캠페인이미지 수정 오류");
    	}
    	
        return 1;
    }
    
    /**
     * 캠페인이미지 삭제
     *
     * @param pvo 캠페인이미지 수정 정보
     * @return 수정결과
     */
    public int deleteCmpImgAd(CmpImgAdPVO pvo) {
        return mapper.deleteCmpImgAd(pvo);
    }
}
