package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class MrchAdminAdRVO extends PageVO{
	private long admSq;
	private String oprGrpId;
	private String oprGrpNm;
	private String mrchGrpId;
	private String mrchGrpNm;
	private String grpGb;
	private String grpGbNm;
	private String grpLv;
	private String grpLvNm;
	private String lgnId;
	private String passwd;
	private String mailAddr;
	private String hpno;
	private String fstRegDttm;
	private String fstRegUsid;
	private String lstChDttm;
	private String lstChUsid;
}
