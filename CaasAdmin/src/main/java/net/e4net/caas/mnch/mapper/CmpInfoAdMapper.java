package net.e4net.caas.mnch.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.mnch.vo.TbCmpInfPVO;
import net.e4net.caas.mnch.vo.TbCmpInfRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author hee.woo
 * @since 2018.11.26
 */
@Repository
public interface CmpInfoAdMapper {
	
    /**
     * 캠페인 목록 조회
     */
    public List<TbCmpInfRVO> selectCmpInfoList(TbCmpInfPVO pvo, PageBounds pageBounds);
    
    /**
     * 캠페인 목록 조회
     */
    public List<TbCmpInfRVO> selectCmpInfoList2(TbCmpInfPVO pvo);
    
    /**
     * 캠페인 목록 조회
     */
    public TbCmpInfRVO selectCmpInfoDetail(TbCmpInfPVO pvo);
    
    /**
     * 캠페인 seq 조회
     */
    public long selectCmpSq();
    
    /**
     * 캠페인 등록
     */
    public int insertCmpInfo(TbCmpInfPVO pvo);
    
    /**
     * 캠페인 수정
     */
    public int updateCmpInfo(TbCmpInfPVO pvo);
    
    /**
     * 캠페인 수정
     */
    public int deleteCmpInfo(TbCmpInfPVO pvo);
    
}
