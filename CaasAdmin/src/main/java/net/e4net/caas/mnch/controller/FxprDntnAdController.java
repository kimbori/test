package net.e4net.caas.mnch.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.service.FxprDntnAdService;
import net.e4net.caas.mnch.vo.TbCmpFsprPVO;
import net.e4net.caas.mnch.vo.TbCmpFsprRVO;
import net.e4net.caas.orp.service.MrchGrpAdService;
import net.e4net.caas.orp.vo.MrchGrpAdPVO;
import net.e4net.caas.orp.vo.MrchGrpAdRVO;
import net.e4net.caas.plt.service.OperGrpAdService;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 정기기부 관리
 *
 * @author hee.woo
 * @since 2018.11.23
 * history  2018.11.23 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class FxprDntnAdController extends AbstractController {
	
	@Autowired
	private FxprDntnAdService fxprDntnAdService;
	
	@Autowired
    private OperGrpAdService operGrpAdService;
    
    @Autowired
    private MrchGrpAdService mrchGrpAdService;
	
    /**
     * <pre>
     * method 정기기부 관리 화면 이동
     * </pre>
     * 
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/mnch/fxprDntnAdPag", method = RequestMethod.GET)
    public ModelAndView fxprDntnAdPag(Model model, HttpServletRequest request) throws Exception {
    	debug("===== move 정기기부 관리 화면 =====");
    	
    	ModelAndView mav = new ModelAndView("admin/mnch/fxprDntnAdPag");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	//운영단체 selectBox 데이터
    	OperGrpAdPVO pvo = new OperGrpAdPVO();
    	List<OperGrpAdRVO> selectResultList = operGrpAdService.selectOperGrpAd(pvo);
    	
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
	    	MrchGrpAdPVO mpvo = new MrchGrpAdPVO();
	    	mpvo.setOprGrpId(userEtt.getOprGrpId());
	    	List<MrchGrpAdRVO> selectMrchGrpAd = mrchGrpAdService.selectMrchGrpAd(mpvo);
	    	mav.addObject("selectMrchGrpAd", selectMrchGrpAd);
    	}
    	
    	mav.addObject("grpGb", userEtt.getGrpGb());
    	mav.addObject("grpLv", userEtt.getGrpLv());
    	mav.addObject("selectResultList", selectResultList);
    	
    	return getOkModelAndView(mav);
    	
    }
    
    /**
     * <pre>
     * method 정기기부 목록 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/mnch/selectFxprDntnList", method = RequestMethod.POST)
    public ModelAndView selectFxprDntnList(@ModelAttribute TbCmpFsprPVO pvo) throws Exception {
        debug("===== 정기기부 목록 조회  =====");
        ModelAndView mav = new ModelAndView("admin/mnch/fxprDntnAdAjax :: fxprDntnList");
        
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //pvo.setMrchGrpId(userEtt.getMrchGrpId()); //가맹단체ID 
        pvo.setCmpSq(StringUtils.parseLong(pvo.getSrchCmpSq(), 0));
        pvo.setCnrSq(StringUtils.parseLong(pvo.getSrchCnrSq(), 0));
        pvo.setBllAuthSq(StringUtils.parseLong(pvo.getSrchBllAuthSq(), 0));
        
        List<TbCmpFsprRVO> resultList = fxprDntnAdService.selectFsprDntnList(pvo, userEtt);
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);
        
        return getOkModelAndView(mav);
    }
    
    
}
