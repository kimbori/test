package net.e4net.caas.mnch.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.mnch.vo.MrchAdminAdPVO;
import net.e4net.caas.mnch.vo.MrchAdminAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface MrchAdminAdMapper {
	
    /**
     * 단체관리자관리 목록 조회
     */
    public List<MrchAdminAdRVO> selectMrchAdminAdList(MrchAdminAdPVO pvom, PageBounds pageBounds);
    
    /**
     * 단체관리자관리 등록여부 조회
     */
    public int selectMrchAdminAdCnt(MrchAdminAdPVO pvom);
    
    /**
     * 단체관리자관리 등록
     */
    public int insertMrchAdminAd(MrchAdminAdPVO pvo);
    
    /**
     * 단체관리자관리 수정
     */
    public int updateMrchAdminAd(MrchAdminAdPVO pvo);
    
    /**
     * 단체관리자관리 수정
     */
    public int deleteMrchAdminAd(MrchAdminAdPVO pvo);
}
