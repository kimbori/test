package net.e4net.caas.mnch.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.service.CmmService;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.service.CmpCostAdService;
import net.e4net.caas.mnch.service.CmpInfoAdService;
import net.e4net.caas.mnch.vo.CmpCostAdPVO;
import net.e4net.caas.mnch.vo.CmpCostAdRVO;
import net.e4net.caas.mnch.vo.TbCmpInfPVO;
import net.e4net.caas.mnch.vo.TbCmpInfRVO;
import net.e4net.caas.orp.service.MrchGrpAdService;
import net.e4net.caas.orp.vo.MrchGrpAdPVO;
import net.e4net.caas.orp.vo.MrchGrpAdRVO;
import net.e4net.caas.plt.service.OperGrpAdService;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 캠페인비용관리 컨트롤러
 *
 * @author e4net
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class CmpCostAdController extends AbstractController {
	
    @Autowired
    private CmpCostAdService service;
    
    @Autowired
    private OperGrpAdService operGrpAdService;
    
    @Autowired
    private MrchGrpAdService mrchGrpAdService;
    
    @Autowired
    private CmpInfoAdService cmpInfoAdService;
    
    @Autowired
    private CmmService cmmService;
    
    
    /**
     * <pre>
     * method 캠페인비용관리화면
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/mnch/cmpCostAdPag", method = RequestMethod.GET)
    public ModelAndView selectCmpCostAdList(Model model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("admin/mnch/cmpCostAdPag");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	//운영단체 selectBox 데이터
    	OperGrpAdPVO pvo = new OperGrpAdPVO();
    	List<OperGrpAdRVO> selectResultList = operGrpAdService.selectOperGrpAd(pvo);
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
	    	MrchGrpAdPVO mpvo = new MrchGrpAdPVO();
	    	mpvo.setOprGrpId(userEtt.getOprGrpId());
	    	List<MrchGrpAdRVO> selectMrchGrpAd = mrchGrpAdService.selectMrchGrpAd(mpvo);
	    	mav.addObject("selectMrchGrpAd", selectMrchGrpAd);
	    	
	    	TbCmpInfPVO cmpInfoPvo = new TbCmpInfPVO(); 
	    	List<TbCmpInfRVO> cmpInfoLst = cmpInfoAdService.selectCmpInfoList2(cmpInfoPvo, userEtt);
	    	mav.addObject("cmpInfoLst", cmpInfoLst);
    	}
    	
    	mav.addObject("grpGb", userEtt.getGrpGb());
    	mav.addObject("grpLv", userEtt.getGrpLv());
    	mav.addObject("grpLvLst", cmmService.selectCmmCodeDetail("GRP_LV"));	//관리자레벨
    	mav.addObject("selectResultList", selectResultList);
    	
    	return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 캠페인비용관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/mnch/selectCmpCostAdListAjax", method = RequestMethod.POST)
    public ModelAndView selectCmpCostAdListAjax(@ModelAttribute CmpCostAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/mnch/cmpCostAdListAjax :: default");
        
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<CmpCostAdRVO> resultList = service.selectCmpCostAdList(pvo, userEtt);
        
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 캠페인비용관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/mnch/selectCmpCost", method = RequestMethod.POST)
    public ModelAndView selectCmpCost(@ModelAttribute CmpCostAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        
        List<CmpCostAdRVO> resultList = service.selectCmpCost(pvo);
        
        mav.addObject("list", resultList);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 캠페인비용관리 등록
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/mnch/insertCmpCostAd", method = RequestMethod.POST)
    public ModelAndView insertCmpCostAd(@ModelAttribute CmpCostAdPVO pvo) {
        
    	ModelAndView mav = new ModelAndView("jsonView");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        Status status = new Status();
        pvo.setPrcLgnId(userEtt.getLgnId());
        
        try {
        	
        	int cnt = service.selectCmpCostAdCnt(pvo);
        	if( cnt != 0 ) {
        		status.setFail("이미 등록된 캠페인 비용입니다..");
                return getFailModelAndView(mav, status);
        	}
        	
            if ( service.insertCmpCostAd(pvo, userEtt) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인비용 등록에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
            status.setFail("캠페인비용 등록에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 가매단체관리자관리 수정
     * </pre>
     *
     * @param pvo 수정할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/mnch/updateCmpCostAd", method = RequestMethod.POST)
    public ModelAndView updateCmpCostAd(@ModelAttribute CmpCostAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        pvo.setPrcLgnId(userEtt.getLgnId());
        try {
            if ( service.updateCmpCostAd(pvo, userEtt) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인비용 수정에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("캠페인비용 수정에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 캠페인비용관리 삭제
     * </pre>
     *
     * @param pvo 삭제할 정보가 담겨있는 pvo
     * @return String 삭제 결과
     */
    @RequestMapping(value = "/admin/mnch/deleteCmpCostAd", method = RequestMethod.POST)
    public ModelAndView deleteCmpCostAd(@ModelAttribute CmpCostAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( service.deleteCmpCostAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인비용 삭제에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("캠페인비용 삭제에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
}
