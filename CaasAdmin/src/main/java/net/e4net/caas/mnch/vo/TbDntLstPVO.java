package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.TbDntLstDVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class TbDntLstPVO extends TbDntLstDVO{
	
	private String oprGrpId;
	private String mrchGrpId;
	private String startDate;
	private String endDate;
	private String srchCmpSq;
	private String srchCnrSq;
	private String cmpNm;
	private String cnrNm;
	 
}
