package net.e4net.caas.mnch.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.mnch.vo.TbDntLstPVO;
import net.e4net.caas.mnch.vo.TbDntLstRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author hee.woo
 * @since 2018.11.21
 */
@Repository
public interface DntnHistoryMapper {
	
    /**
     * 기부이력 리스트 조회
     */
    public List<TbDntLstRVO> selectDntnHistoryList(TbDntLstPVO pvo, PageBounds pageBounds);
    
 
}
