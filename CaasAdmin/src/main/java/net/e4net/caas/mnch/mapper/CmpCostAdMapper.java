package net.e4net.caas.mnch.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.mnch.vo.CmpCostAdPVO;
import net.e4net.caas.mnch.vo.CmpCostAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface CmpCostAdMapper {
	
    /**
     * 캠페인비용 목록 조회
     */
    public List<CmpCostAdRVO> selectCmpCostAdList(CmpCostAdPVO pvom, PageBounds pageBounds);
    
    /**
     * 캠페인비용 목록 조회
     */
    public List<CmpCostAdRVO> selectCmpCost(CmpCostAdPVO pvom);
    
    /**
     * 캠페인비용 등록여부 조회
     */
    public int selectCmpCostAdCnt(CmpCostAdPVO pvom);
    
    /**
     * 캠페인비용 등록
     */
    public int insertCmpCostAd(CmpCostAdPVO pvo);
    
    /**
     * 캠페인비용 수정
     */
    public int updateCmpCostAd(CmpCostAdPVO pvo);
    
    /**
     * 캠페인비용 수정
     */
    public int deleteCmpCostAd(CmpCostAdPVO pvo);
}
