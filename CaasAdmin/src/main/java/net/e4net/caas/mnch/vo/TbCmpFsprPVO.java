package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.TbCmpFsprDVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class TbCmpFsprPVO extends TbCmpFsprDVO{
	
	private String oprGrpId;
	private String mrchGrpId;
	private String srchCmpSq;
	private String srchCnrSq;
	private String srchBllAuthSq;
	private String cnrNm;
	private String cmpNm;
}
