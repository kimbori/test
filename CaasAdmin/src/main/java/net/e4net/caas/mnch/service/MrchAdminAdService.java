package net.e4net.caas.mnch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.mapper.MrchAdminAdMapper;
import net.e4net.caas.mnch.vo.MrchAdminAdPVO;
import net.e4net.caas.mnch.vo.MrchAdminAdRVO;

/**
 * 단체관리자관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class MrchAdminAdService extends AbstractDao {
    @Autowired
    private MrchAdminAdMapper mapper;
    
    /**
     * 단체관리자관리 조회
     *
     * @param pvo 단체관리자관리 VO 
     * @return 검색결과 
     */
    public List<MrchAdminAdRVO> selectMrchAdminAdList(MrchAdminAdPVO pvo, UserVO userEtt) {
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
    	//가맹단체관리자인 경우, 관리자레벨이 최상위 인 경우엔 고정값으로 자기가 속해있는 가맹단체ID를 셋팅
    	if( "2".equals(userEtt.getGrpGb()) ) {
    		pvo.setMrchGrpId(userEtt.getMrchGrpId());
    	}
    	
        return mapper.selectMrchAdminAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 단체관리자관리 등록 조회
     *
     * @param pvo 단체관리자관리 VO
     * @return 검색결과
     */
    public int selectMrchAdminAdCnt(MrchAdminAdPVO pvo) {
        return mapper.selectMrchAdminAdCnt(pvo);
    }
    
    /**
     * 단체관리자관리 등록
     *
     * @param pvo 단체관리자관리 VO
     * @return 등록건수
     */
    public int insertMrchAdminAd(MrchAdminAdPVO pvo, UserVO userEtt) {
    	//비밀번호 encode
    	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    	pvo.setPasswd(passwordEncoder.encode(pvo.getPasswd()));
    	
    	//플랫폼관리자가 아닌 경우 세션에 있는 값을 고정값으로 자기가 속해 있는 운영단체ID를 셋팅
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
    	//가맹단체관리자인 경우, 관리자레벨이 최상위 인 경우엔 고정값으로 자기가 속해있는 가맹단체ID를 셋팅
    	if( "2".equals(userEtt.getGrpGb()) ) {
    		pvo.setMrchGrpId(userEtt.getMrchGrpId());
    	}
    	
        return mapper.insertMrchAdminAd(pvo);
    }
    
    /**
     * 단체관리자관리 수정
     *
     * @param pvo 단체관리자관리 수정 정보
     * @return 수정결과
     */
    public int updateMrchAdminAd(MrchAdminAdPVO pvo) {
        return mapper.updateMrchAdminAd(pvo);
    }
    
    /**
     * 단체관리자관리 삭제
     *
     * @param pvo 단체관리자관리 수정 정보
     * @return 수정결과
     */
    public int deleteMrchAdminAd(MrchAdminAdPVO pvo) {
        return mapper.deleteMrchAdminAd(pvo);
    }
}
