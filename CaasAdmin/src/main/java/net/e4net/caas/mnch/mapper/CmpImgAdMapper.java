package net.e4net.caas.mnch.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.mnch.vo.CmpImgAdPVO;
import net.e4net.caas.mnch.vo.CmpImgAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface CmpImgAdMapper {
	
    /**
     * 캠페인이미지 목록 조회
     */
    public List<CmpImgAdRVO> selectCmpImgAdList(CmpImgAdPVO pvom, PageBounds pageBounds);
    
    /**
     * 캠페인이미지 목록 조회
     */
    public List<CmpImgAdRVO> selectCmpImg(CmpImgAdPVO pvom);
    
    /**
     * 캠페인이미지 등록여부 조회
     */
    public int selectCmpImgAdCnt(CmpImgAdPVO pvom);
    
    /**
     * 캠페인이미지 등록
     */
    public int insertCmpImgAd(CmpImgAdPVO pvo);
    
    /**
     * 캠페인이미지 수정
     */
    public int updateCmpImgAd(CmpImgAdPVO pvo);
    
    /**
     * 캠페인이미지 수정
     */
    public int deleteCmpImgAd(CmpImgAdPVO pvo);
}
