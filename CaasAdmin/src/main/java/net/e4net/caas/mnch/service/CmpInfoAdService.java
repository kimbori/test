package net.e4net.caas.mnch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.service.BlockchainService;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.mapper.CmpInfoAdMapper;
import net.e4net.caas.mnch.vo.MrchAdminAdPVO;
import net.e4net.caas.mnch.vo.TbCmpInfPVO;
import net.e4net.caas.mnch.vo.TbCmpInfRVO;

/**
 * 캠페인 관리 서비스
 *
 * @author hee.woo
 * @since 2018.11.26
 */
@SuppressWarnings("serial")
@Service
public class CmpInfoAdService extends AbstractDao {
   
	@Autowired
    private CmpInfoAdMapper cmpInfoAdMapper;
    
	@Autowired
	private BlockchainService blockchainService;
	
    /**
     * 캠페인 목록 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public List<TbCmpInfRVO> selectCmpInfoList(TbCmpInfPVO pvo, UserVO userEtt) {
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
    	//가맹단체관리자인 경우, 관리자레벨이 최상위 인 경우엔 고정값으로 자기가 속해있는 가맹단체ID를 셋팅
    	if( "2".equals(userEtt.getGrpGb()) ) {
    		pvo.setMrchGrpId(userEtt.getMrchGrpId());
    	}
    	
        return cmpInfoAdMapper.selectCmpInfoList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 캠페인 목록 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public List<TbCmpInfRVO> selectCmpInfoList2(TbCmpInfPVO pvo, UserVO userEtt) {
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
    	//가맹단체관리자인 경우, 관리자레벨이 최상위 인 경우엔 고정값으로 자기가 속해있는 가맹단체ID를 셋팅
    	if( "2".equals(userEtt.getGrpGb()) ) {
    		pvo.setMrchGrpId(userEtt.getMrchGrpId());
    	}
    	
        return cmpInfoAdMapper.selectCmpInfoList2(pvo);
    }
    
    /**
     * 캠페인 목록 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public TbCmpInfRVO selectCmpInfoDetail(TbCmpInfPVO pvo) {
        return cmpInfoAdMapper.selectCmpInfoDetail(pvo);
    }
    
    /**
     * 캠페인 등록
     *
     * @param pvo 단체관리자관리 VO
     * @return 등록건수
     */
    public int insertCmpInfo(TbCmpInfPVO pvo, UserVO userEtt) {
    	
    	//플랫폼관리자가 아닌 경우 세션에 있는 값을 고정값으로 자기가 속해 있는 운영단체ID를 셋팅
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
    	//가맹단체관리자인 경우, 관리자레벨이 최상위 인 경우엔 고정값으로 자기가 속해있는 가맹단체ID를 셋팅
    	if( "2".equals(userEtt.getGrpGb()) ) {
    		pvo.setMrchGrpId(userEtt.getMrchGrpId());
    	}
    	
    	long cmpSq = cmpInfoAdMapper.selectCmpSq();
    	
    	pvo.setWltAddr(blockchainService.createWalletCmpInf(pvo.getOprGrpId(), pvo.getMrchGrpId(), cmpSq, userEtt.getLgnId()).getWltAddr());
    	pvo.setCmpSq(cmpSq);
    	
    	pvo.setDnmrEndDt(StringUtils.replace(pvo.getDnmrEndDt(), "-", ""));		//모금 시작일 하이픈 제거
    	pvo.setDnmrStatDt(StringUtils.replace(pvo.getDnmrStatDt(), "-",""));	//모금 종료일 하이픈 제거
    	
    	return cmpInfoAdMapper.insertCmpInfo(pvo);
    }
    
    /**
     * 캠페인 수정
     *
     * @param pvo 단체관리자관리 수정 정보
     * @return 수정결과
     */
    public int updateCmpInfo(TbCmpInfPVO pvo) {
        return cmpInfoAdMapper.updateCmpInfo(pvo);
    }
    
    /**
     * 캠페인 삭제
     *
     * @param pvo 단체관리자관리 수정 정보
     * @return 수정결과
     */
    public int deleteCmpInfo(TbCmpInfPVO pvo) {
        return cmpInfoAdMapper.deleteCmpInfo(pvo);
    }
   
}
