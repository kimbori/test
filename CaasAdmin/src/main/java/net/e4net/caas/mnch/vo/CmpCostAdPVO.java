package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class CmpCostAdPVO extends PageVO{
	private String prcLgnId;				//등록자id, 수정자id
	
	private String oprGrpId;
	private String mrchGrpId;
	private String cmpNm;
	
	//등록 수정 삭제 
	private int inCmpSq;
	private String [] inUsePlan;
	private int [] inUseAmt;
	
	private String usePlan;
	private int useAmt;
	private int od;
}
