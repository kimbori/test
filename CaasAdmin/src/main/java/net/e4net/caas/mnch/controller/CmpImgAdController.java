package net.e4net.caas.mnch.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.service.CmmService;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.service.CmpImgAdService;
import net.e4net.caas.mnch.service.CmpInfoAdService;
import net.e4net.caas.mnch.vo.CmpImgAdPVO;
import net.e4net.caas.mnch.vo.CmpImgAdRVO;
import net.e4net.caas.mnch.vo.TbCmpInfPVO;
import net.e4net.caas.mnch.vo.TbCmpInfRVO;
import net.e4net.caas.orp.service.MrchGrpAdService;
import net.e4net.caas.orp.vo.MrchGrpAdPVO;
import net.e4net.caas.orp.vo.MrchGrpAdRVO;
import net.e4net.caas.plt.service.OperGrpAdService;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 캠페인이미지관리 컨트롤러
 *
 * @author e4net
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class CmpImgAdController extends AbstractController {
	
    @Autowired
    private CmpImgAdService service;
    
    @Autowired
    private OperGrpAdService operGrpAdService;
    
    @Autowired
    private MrchGrpAdService mrchGrpAdService;
    
    @Autowired
    private CmpInfoAdService cmpInfoAdService;
    
    @Autowired
    private CmmService cmmService;
    
    
    /**
     * <pre>
     * method 캠페인이미지관리화면
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/mnch/cmpImgAdPag", method = RequestMethod.GET)
    public ModelAndView selectCmpImgAdList(Model model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("admin/mnch/cmpImgAdPag");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	//운영단체 selectBox 데이터
    	OperGrpAdPVO pvo = new OperGrpAdPVO();
    	List<OperGrpAdRVO> selectResultList = operGrpAdService.selectOperGrpAd(pvo);
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
	    	MrchGrpAdPVO mpvo = new MrchGrpAdPVO();
	    	mpvo.setOprGrpId(userEtt.getOprGrpId());
	    	List<MrchGrpAdRVO> selectMrchGrpAd = mrchGrpAdService.selectMrchGrpAd(mpvo);
	    	mav.addObject("selectMrchGrpAd", selectMrchGrpAd);
	    	
	    	TbCmpInfPVO cmpInfoPvo = new TbCmpInfPVO(); 
	    	List<TbCmpInfRVO> cmpInfoLst = cmpInfoAdService.selectCmpInfoList2(cmpInfoPvo, userEtt);
	    	mav.addObject("cmpInfoLst", cmpInfoLst);
    	}
    	
    	mav.addObject("grpGb", userEtt.getGrpGb());
    	mav.addObject("grpLv", userEtt.getGrpLv());
    	mav.addObject("grpLvLst", cmmService.selectCmmCodeDetail("GRP_LV"));	//관리자레벨
    	mav.addObject("selectResultList", selectResultList);
    	
    	return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 캠페인이미지관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/mnch/selectCmpImgAdListAjax", method = RequestMethod.POST)
    public ModelAndView selectCmpImgAdListAjax(@ModelAttribute CmpImgAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/mnch/cmpImgAdListAjax :: default");
        
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<CmpImgAdRVO> resultList = service.selectCmpImgAdList(pvo, userEtt);
        
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 캠페인이미지관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/mnch/selectCmpImg", method = RequestMethod.POST)
    public ModelAndView selectCmpCost(@ModelAttribute CmpImgAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        
        List<CmpImgAdRVO> resultList = service.selectCmpImg(pvo);
        
        mav.addObject("list", resultList);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 캠페인이미지관리 등록
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/mnch/insertCmpImgAd", method = RequestMethod.POST)
    public ModelAndView insertCmpImgAd(@ModelAttribute CmpImgAdPVO pvo, HttpServletRequest request) {
        
    	ModelAndView mav = new ModelAndView("jsonView");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        Status status = new Status();
        pvo.setPrcLgnId(userEtt.getLgnId());
        
        try {
        	
        	int cnt = service.selectCmpImgAdCnt(pvo);
        	if( cnt != 0 ) {
        		status.setFail("해당 캠페인에 등록된 이미지가 존재합니다.");
                return getFailModelAndView(mav, status);
        	}
        	
            if ( service.insertCmpImgAd(pvo, request) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인이미지 등록에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	e.printStackTrace();
            status.setFail("캠페인이미지 등록에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 가매단체관리자관리 수정
     * </pre>
     *
     * @param pvo 수정할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/mnch/updateCmpImgAd", method = RequestMethod.POST)
    public ModelAndView updateCmpImgAd(@ModelAttribute CmpImgAdPVO pvo, HttpServletRequest request) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        pvo.setPrcLgnId(userEtt.getLgnId());
        try {
            if ( service.updateCmpImgAd(pvo, request) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인이미지 수정에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	e.printStackTrace();
        	status.setFail("캠페인이미지 수정에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 캠페인이미지관리 삭제
     * </pre>
     *
     * @param pvo 삭제할 정보가 담겨있는 pvo
     * @return String 삭제 결과
     */
    @RequestMapping(value = "/admin/mnch/deleteCmpImgAd", method = RequestMethod.POST)
    public ModelAndView deleteCmpImgAd(@ModelAttribute CmpImgAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( service.deleteCmpImgAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인이미지 삭제에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("캠페인이미지 삭제에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
}
