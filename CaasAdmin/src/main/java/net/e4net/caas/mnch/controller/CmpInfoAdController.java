package net.e4net.caas.mnch.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.service.CmmService;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.service.CmpInfoAdService;
import net.e4net.caas.mnch.vo.TbCmpInfPVO;
import net.e4net.caas.mnch.vo.TbCmpInfRVO;
import net.e4net.caas.orp.service.MrchGrpAdService;
import net.e4net.caas.orp.vo.MrchGrpAdPVO;
import net.e4net.caas.orp.vo.MrchGrpAdRVO;
import net.e4net.caas.plt.service.OperGrpAdService;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 캠페인 관리
 *
 * @author hee.woo
 * @since 2018.11.26
 * history  2018.11.26 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class CmpInfoAdController extends AbstractController {
	
	@Autowired
	private CmpInfoAdService service;
	
	@Autowired
    private OperGrpAdService operGrpAdService;
    
    @Autowired
    private MrchGrpAdService mrchGrpAdService;
	
    @Autowired
    private CmmService cmmService;
    
    /**
     * <pre>
     * method 캠페인 관리 화면 이동
     * </pre>
     * 
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/mnch/cmpInfoAdPag", method = RequestMethod.GET)
    public ModelAndView cmpInfoAdPag(Model model, HttpServletRequest request) throws Exception {
    	debug("===== move 캠페인 관리 화면 =====");
    	
		ModelAndView mav = new ModelAndView("admin/mnch/cmpInfoAdPag");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	//운영단체 selectBox 데이터
    	OperGrpAdPVO pvo = new OperGrpAdPVO();
    	List<OperGrpAdRVO> selectResultList = operGrpAdService.selectOperGrpAd(pvo);
    	
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
	    	MrchGrpAdPVO mpvo = new MrchGrpAdPVO();
	    	mpvo.setOprGrpId(userEtt.getOprGrpId());
	    	List<MrchGrpAdRVO> selectMrchGrpAd = mrchGrpAdService.selectMrchGrpAd(mpvo);
	    	mav.addObject("selectMrchGrpAd", selectMrchGrpAd);
    	}
    	
    	mav.addObject("grpGb", userEtt.getGrpGb());
    	mav.addObject("grpLv", userEtt.getGrpLv());
    	mav.addObject("dnmrKnLst", cmmService.selectCmmCodeDetail("DNMR_KN"));	//모금종류
    	mav.addObject("dnmrStatLst", cmmService.selectCmmCodeDetail("DNMR_STAT"));	//모금상태
    	mav.addObject("dnmrTpLst", cmmService.selectCmmCodeDetail("DNMR_TP"));	//모금유형
    	mav.addObject("selectResultList", selectResultList);
    	
    	return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 캠페인 목록 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/mnch/selectCmpInfoList", method = RequestMethod.POST)
    public ModelAndView selectCmpInfoList(@ModelAttribute TbCmpInfPVO pvo) throws Exception {
        debug("===== 캠페인 목록 조회  =====");
        ModelAndView mav = new ModelAndView("admin/mnch/cmpInfoAdAjax :: cmpInfoList");
        
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        List<TbCmpInfRVO> resultList = service.selectCmpInfoList(pvo, userEtt);
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);
        
        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 캠페인 목록 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/mnch/selectCmpInfoList2", method = RequestMethod.POST)
    public ModelAndView selectCmpInfoList2(@ModelAttribute TbCmpInfPVO pvo) throws Exception {
        debug("===== 캠페인 목록 조회  =====");
        ModelAndView mav = new ModelAndView("jsonView");
        
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        List<TbCmpInfRVO> resultList = service.selectCmpInfoList2(pvo, userEtt);
        
        mav.addObject("resultList", resultList);
        
        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 캠페인 상세 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/mnch/selectCmpInfoDetail", method = RequestMethod.POST)
    public ModelAndView selectCmpInfoDetail(@ModelAttribute TbCmpInfPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        
        TbCmpInfRVO result = service.selectCmpInfoDetail(pvo);
        
        mav.addObject("result", result);
        
        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 캠페인관리 등록
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/mnch/insertCmpInfo", method = RequestMethod.POST)
    public ModelAndView insertCmpInfo(@ModelAttribute TbCmpInfPVO pvo){
        
    	ModelAndView mav = new ModelAndView("jsonView");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        Status status = new Status();
        pvo.setPrcLgnId(userEtt.getLgnId());
        
        try {
        	
            if ( service.insertCmpInfo(pvo, userEtt) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인 등록에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	e.printStackTrace();
            status.setFail("캠페인 등록에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 캠페인관리 수정
     * </pre>
     *
     * @param pvo 수정할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/mnch/updateCmpInfo", method = RequestMethod.POST)
    public ModelAndView updateCmpInfo(@ModelAttribute TbCmpInfPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        pvo.setPrcLgnId(userEtt.getLgnId());
        try {
            if ( service.updateCmpInfo(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인 수정에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("캠페인 수정에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 캠페인관리 삭제
     * </pre>
     *
     * @param pvo 삭제할 정보가 담겨있는 pvo
     * @return String 삭제 결과
     */
    @RequestMapping(value = "/admin/mnch/deleteCmpInfo", method = RequestMethod.POST)
    public ModelAndView deleteCmpInfo(@ModelAttribute TbCmpInfPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( service.deleteCmpInfo(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인 삭제에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("캠페인 삭제에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
}
