package net.e4net.caas.mnch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.mapper.CmpCostAdMapper;
import net.e4net.caas.mnch.vo.CmpCostAdPVO;
import net.e4net.caas.mnch.vo.CmpCostAdRVO;

/**
 * 단체관리자관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class CmpCostAdService extends AbstractDao {
    @Autowired
    private CmpCostAdMapper mapper;
    
    /**
     * 캠페인비용 조회
     *
     * @param pvo 캠페인비용 VO 
     * @return 검색결과 
     */
    public List<CmpCostAdRVO> selectCmpCostAdList(CmpCostAdPVO pvo, UserVO userEtt) {
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
    	//가맹단체관리자인 경우, 관리자레벨이 최상위 인 경우엔 고정값으로 자기가 속해있는 가맹단체ID를 셋팅
    	if( "2".equals(userEtt.getGrpGb()) ) {
    		pvo.setMrchGrpId(userEtt.getMrchGrpId());
    	}
    	
        return mapper.selectCmpCostAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 캠페인비용 조회
     *
     * @param pvo 캠페인비용 VO 
     * @return 검색결과 
     */
    public List<CmpCostAdRVO> selectCmpCost(CmpCostAdPVO pvo) {
        return mapper.selectCmpCost(pvo);
    }
    
    /**
     * 캠페인비용 등록 조회
     *
     * @param pvo 캠페인비용 VO
     * @return 검색결과
     */
    public int selectCmpCostAdCnt(CmpCostAdPVO pvo) {
        return mapper.selectCmpCostAdCnt(pvo);
    }
    
    /**
     * 캠페인비용 등록
     *
     * @param pvo 캠페인비용 VO
     * @return 등록건수
     */
    public int insertCmpCostAd(CmpCostAdPVO pvo, UserVO userEtt) {
    	int j = 1;
    	for( int i = 0; i<pvo.getInUsePlan().length; i++ ) {
    		if( !StringUtils.isNullOrBlank(pvo.getInUseAmt()[i]+"") 
    				&& !StringUtils.isNullOrBlank(pvo.getInUsePlan()[i]) ) {
    			
    			pvo.setUseAmt(StringUtils.parseInt(pvo.getInUseAmt()[i]+"", 0));
    			pvo.setUsePlan(pvo.getInUsePlan()[i]);
    			pvo.setOd(j++);
    			
    			mapper.insertCmpCostAd(pvo);    			
    		}
    	}
    	
        return 1;
    }
    
    /**
     * 캠페인비용 수정
     *
     * @param pvo 캠페인비용 수정 정보
     * @return 수정결과
     */
    public int updateCmpCostAd(CmpCostAdPVO pvo, UserVO userEtt)throws Exception {
    	/*TransactionStatus status = null;
    	try {
    		status = getTransactionStatus("updateCmpCostAd");
    		int procCnt = 0;
    	
	    	//기존 데이터 삭제
    		procCnt = deleteCmpCostAd(pvo);
	    	
    		if(procCnt < 1) {
        		throw new CaasException("99999", "캠페인비용 수정 오류");
        	}
    		
	    	//신규 데이터 추가
    		procCnt = insertCmpCostAd(pvo, userEtt);
	    	
    		if(procCnt < 1) {
        		throw new CaasException("99999", "캠페인비용 수정 오류");
        	}
    		
	    	transactionCommit(status);
    	}catch(Exception e) {
    		error("updateCmpCostAd", e);
    		transactionRollback(status);
    		throw e;
    	}*/
    	
    	int procCnt = 0;
    	
    	//기존 데이터 삭제
		procCnt = deleteCmpCostAd(pvo);
    	
		if(procCnt < 1) {
    		throw new CaasException("99999", "캠페인비용 수정 오류");
    	}
		
    	//신규 데이터 추가
		procCnt = insertCmpCostAd(pvo, userEtt);
    	
		if(procCnt < 1) {
    		throw new CaasException("99999", "캠페인비용 수정 오류");
    	}
    	
        return 1;
    }
    
    /**
     * 캠페인비용 삭제
     *
     * @param pvo 캠페인비용 정보
     * @return 수정결과
     */
    public int deleteCmpCostAd(CmpCostAdPVO pvo) {
        return mapper.deleteCmpCostAd(pvo);
    }
}
