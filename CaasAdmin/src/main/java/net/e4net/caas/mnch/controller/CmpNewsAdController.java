package net.e4net.caas.mnch.controller;


import java.util.List;

import javax.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.service.CmpNewsAdService;
import net.e4net.caas.mnch.vo.TbCmpNewsPVO;
import net.e4net.caas.mnch.vo.TbCmpNewsRVO;
import net.e4net.caas.orp.service.MrchGrpAdService;
import net.e4net.caas.orp.vo.MrchGrpAdPVO;
import net.e4net.caas.orp.vo.MrchGrpAdRVO;
import net.e4net.caas.plt.service.OperGrpAdService;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 캠페인소식 관리
 *
 * @author hee.woo
 * @since 2018.11.23
 * history  2018.11.23 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class CmpNewsAdController extends AbstractController {
	
	@Autowired
	private CmpNewsAdService cmpNewsAdService;
	
	@Autowired
    private OperGrpAdService operGrpAdService;
    
    @Autowired
    private MrchGrpAdService mrchGrpAdService;
	
    /**
     * <pre>
     * method 캠페인소식 관리 화면 이동
     * </pre>
     * 
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/mnch/cmpNewsAdPag", method = RequestMethod.GET)
    public ModelAndView cmpNewsAdPag(Model model, HttpServletRequest request) throws Exception {
    	debug("===== move 캠페인소식 관리 화면 =====");
    	
    	ModelAndView mav = new ModelAndView("admin/mnch/cmpNewsAdPag");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	//운영단체 selectBox 데이터
    	OperGrpAdPVO pvo = new OperGrpAdPVO();
    	List<OperGrpAdRVO> selectResultList = operGrpAdService.selectOperGrpAd(pvo);
    	
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
	    	MrchGrpAdPVO mpvo = new MrchGrpAdPVO();
	    	mpvo.setOprGrpId(userEtt.getOprGrpId());
	    	List<MrchGrpAdRVO> selectMrchGrpAd = mrchGrpAdService.selectMrchGrpAd(mpvo);
	    	mav.addObject("selectMrchGrpAd", selectMrchGrpAd);
    	}
    	
    	mav.addObject("grpGb", userEtt.getGrpGb());
    	mav.addObject("grpLv", userEtt.getGrpLv());
    	mav.addObject("selectResultList", selectResultList);
    	
    	return getOkModelAndView(mav);
    	
    }
    
    /**
     * <pre>
     * method 캠페인소식 목록 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/mnch/selectCmpNewsList", method = RequestMethod.POST)
    public ModelAndView selectCmpNewsList(@ModelAttribute TbCmpNewsPVO pvo) throws Exception {
        debug("===== 캠페인소식 목록 조회  =====");
        ModelAndView mav = new ModelAndView("admin/mnch/cmpNewsAdAjax :: cmpNewsList");
        
        //UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //pvo.setMrchGrpId(userEtt.getMrchGrpId()); //가맹단체ID 
        pvo.setCmpSq(StringUtils.parseLong(pvo.getSrchCmpSq(), 0));
        
        List<TbCmpNewsRVO> resultList = cmpNewsAdService.selectCmpNewsList(pvo);
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);
        
        return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * method 캠페인소식 상세조회
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 조회 결과
     */
    @RequestMapping(value = "/admin/CmpNews/selectCmpNewsDetail", method = RequestMethod.POST)
    public ModelAndView selectCmpNewsDetail(@ModelAttribute TbCmpNewsPVO pvo) {
    	debug("===== 캠페인소식 상세조회  =====");
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        TbCmpNewsRVO rvo = cmpNewsAdService.selectCmpNewsDetail(pvo);
        if(rvo == null){
        	status.setFail("캠페인소식이 존재하지 않습니다.");
            return getFailModelAndView(mav);
        }
        mav.addObject("rvo", rvo);
        return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * method 캠페인소식 등록
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 등록 결과
     */
    @RequestMapping(value = "/admin/CmpNews/insertCmpNews", method = RequestMethod.POST)
    public ModelAndView insertCmpNews(@ModelAttribute TbCmpNewsPVO pvo) {
    	debug("=====  insert 캠페인소식  =====");
    	ModelAndView mav = new ModelAndView("jsonView");
    	Status status = new Status();
    	
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        pvo.setFstRegUsid(userEtt.getLgnId());
    	
    	try {
    		if ( cmpNewsAdService.insertCmpNews(pvo) > 0 ) {
    			status.setOk(0);
    			return getOkModelAndView(mav, status);
    		} else {
    			status.setFail("캠페인소식 등록에 실패하였습니다.");
    			return getFailModelAndView(mav, status);
    		}
    	} catch (Exception e){
    		status.setFail("캠페인소식 등록에 실패하였습니다.");
    		return getFailModelAndView(mav, status);
    	}
    }
    
    /**
     * <pre>
     * method 캠페인소식 수정
     * </pre>
     *
     * @param pvo 수정할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/CmpNews/updateCmpNews", method = RequestMethod.POST)
    public ModelAndView updateCmpNews(@ModelAttribute TbCmpNewsPVO pvo) throws Exception {
    	debug("=====  update 캠페인소식  =====");
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();

    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        pvo.setLstChUsid(userEtt.getLgnId());
        
        try {
            if ( cmpNewsAdService.updateCmpNews(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인소식 수정에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("캠페인소식 수정에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 캠페인소식 삭제
     * </pre>
     *
     * @param pvo 삭제할 정보가 담겨있는 pvo
     * @return String 삭제 결과
     */
    @RequestMapping(value = "/admin/CmpNews/deleteCmpNews", method = RequestMethod.POST)
    public ModelAndView deleteCmpNews(@ModelAttribute TbCmpNewsPVO pvo) throws Exception {
    	debug("=====  delete 캠페인소식  =====");
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( cmpNewsAdService.deleteCmpNews(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("캠페인소식 삭제에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("캠페인소식 삭제에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 캠페인 목록조회
     * </pre>
     *
     * @param pvo 검색조건 pvo
     * @return String  결과
     */
    @RequestMapping(value = "/admin/CmpNews/selectCmpList", method = RequestMethod.POST)
    public ModelAndView selectCmpList(@ModelAttribute TbCmpNewsPVO pvo) {
    	ModelAndView mav = new ModelAndView("jsonView");
    	
    	//UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        //pvo.setMrchGrpId(userEtt.getMrchGrpId()); //가맹단체ID 
    	
    	List<TbCmpNewsRVO> list = cmpNewsAdService.selectCmpList(pvo);
    	mav.addObject("list", list);
    	return getOkModelAndView(mav);
    }
    
}
