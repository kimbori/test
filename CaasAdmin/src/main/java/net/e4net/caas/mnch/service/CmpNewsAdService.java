package net.e4net.caas.mnch.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.mnch.mapper.CmpNewsAdMapper;
import net.e4net.caas.mnch.vo.TbCmpNewsPVO;
import net.e4net.caas.mnch.vo.TbCmpNewsRVO;

/**
 * 캠페인소식 관리 서비스
 *
 * @author hee.woo
 * @since 2018.11.23
 */
@SuppressWarnings("serial")
@Service
public class CmpNewsAdService extends AbstractDao {
   
	@Autowired
    private CmpNewsAdMapper cmpNewsAdMapper;
    
    /**
     * 캠페인소식 목록 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public List<TbCmpNewsRVO> selectCmpNewsList(TbCmpNewsPVO pvo) {
        return cmpNewsAdMapper.selectCmpNewsList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 캠페인소식 상세조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public TbCmpNewsRVO selectCmpNewsDetail(TbCmpNewsPVO pvo) {
    	return cmpNewsAdMapper.selectCmpNewsDetail(pvo);
    }
    
    /**
     * 캠페인소식 등록
     *
     * @param pvo 등록 정보
     * @return 등록결과
     */
    public int insertCmpNews(TbCmpNewsPVO pvo) {
        return cmpNewsAdMapper.insertCmpNews(pvo);
    }
    
    /**
     * 캠페인소식 수정
     *
     * @param pvo 수정 정보
     * @return 수정결과
     */
    public int updateCmpNews(TbCmpNewsPVO pvo) {
        return cmpNewsAdMapper.updateCmpNews(pvo);
    }
    
    /**
     * 캠페인소식 삭제
     *
     * @param pvo 삭제 정보
     * @return 삭제결과
     */
    public int deleteCmpNews(TbCmpNewsPVO pvo) {
        return cmpNewsAdMapper.deleteCmpNews(pvo);
    }
    
    /**
     * 캠페인목록 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public List<TbCmpNewsRVO> selectCmpList(TbCmpNewsPVO pvo) {
        return cmpNewsAdMapper.selectCmpList(pvo);
    }
}
