package net.e4net.caas.mnch.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.dvo.TbCmpFsprDVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class TbCmpFsprRVO extends TbCmpFsprDVO{
	
	private String cmpNm;
	private String otaAmtFmt;
	private String statYmFmt;
	private String endYmFmt;
	private String regDttmFmt;
	private String cnrNm;
	private String mrchGrpNm;
	private String oprGrpNm;
}
