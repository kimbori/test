package net.e4net.caas.admin.sample.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.admin.sample.service.SampleService;
import net.e4net.caas.admin.sample.vo.SamplePVO;
import net.e4net.caas.admin.sample.vo.SampleRVO;
import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.vo.UserVO;

/**
 * SAMPLE 컨트롤러
 *
 * @author djim
 * @since 2018.11.01
 * history  2018.11.01 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class SampleController extends AbstractController {
	
    @Autowired
    private SampleService sampleService;
    
    /**
     * <pre>
     * method sample조회 화면 이동
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/sample/moveSamplePag", method = RequestMethod.GET)
    public String commonCdMgmt(Model model, HttpServletRequest request) throws Exception {
    	debug("===== move sample page =====");
    	UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();

    	if (principal instanceof UserVO) {
    	    String name = principal.getLgnId();
    	    debug(principal.toString());
    	}
		return "admin/sample/samplePag";
    }
    
    /**
     * <pre>
     * method sample 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/sample/selectSampleList", method = RequestMethod.POST)
    public ModelAndView selectSampleList(@ModelAttribute SamplePVO pvo) throws Exception {
        debug("=====  select sample List =====");
        
        ModelAndView mav = new ModelAndView("admin/sample/sampleListAjax :: sampleListAjax");
        List<SampleRVO> resultList = sampleService.selectSampleList(pvo);
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method sample 등록
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/sample/insertSample", method = RequestMethod.POST)
    public ModelAndView insertSample(@ModelAttribute SamplePVO pvo) {
    	debug("=====  insert sample  =====");
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( sampleService.insertSample(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("샘플 등록에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
            status.setFail("샘플 등록에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method sample 수정
     * </pre>
     *
     * @param pvo 수정할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/sample/updateSample", method = RequestMethod.POST)
    public ModelAndView updateSample(@ModelAttribute SamplePVO pvo) throws Exception {
    	debug("=====  update sample  =====");
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( sampleService.updateSample(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("샘플 수정에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("샘플 수정에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method sample 삭제
     * </pre>
     *
     * @param pvo 삭제할 정보가 담겨있는 pvo
     * @return String 삭제 결과
     */
    @RequestMapping(value = "/admin/sample/deleteSample", method = RequestMethod.POST)
    public ModelAndView deleteSample(@ModelAttribute SamplePVO pvo) throws Exception {
    	debug("=====  delete sample  =====");
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( sampleService.deleteSample(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("샘플 삭제에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("샘플 삭제에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
}
