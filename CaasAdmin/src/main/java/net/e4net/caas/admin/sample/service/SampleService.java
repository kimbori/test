package net.e4net.caas.admin.sample.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.admin.sample.mapper.SampleMapper;
import net.e4net.caas.admin.sample.vo.SamplePVO;
import net.e4net.caas.admin.sample.vo.SampleRVO;
import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;

/**
 * SAMPLE 서비스
 *
 * @author djim
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class SampleService extends AbstractDao {
    @Autowired
    private SampleMapper sampleMapper;
    
    /**
     * SAMPLE 조회
     *
     * @param pvo SAMPLE 조회 조건
     * @return 검색결과
     */
    public List<SampleRVO> selectSampleList(SamplePVO pvo) {
        return sampleMapper.selectSampleList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * SAMPLE 등록
     *
     * @param pvo SAMPLE 등록 정보
     * @return 등록결과
     */
    public int insertSample(SamplePVO pvo) {
        return sampleMapper.insertSample(pvo);
    }
    
    /**
     * SAMPLE 수정
     *
     * @param pvo SAMPLE 수정 정보
     * @return 수정결과
     */
    public int updateSample(SamplePVO pvo) {
        return sampleMapper.updateSample(pvo);
    }
    
    /**
     * SAMPLE 삭제
     *
     * @param pvo SAMPLE 수정 정보
     * @return 수정결과
     */
    public int deleteSample(SamplePVO pvo) {
        return sampleMapper.deleteSample(pvo);
    }
}
