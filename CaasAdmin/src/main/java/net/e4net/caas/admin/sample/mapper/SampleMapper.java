package net.e4net.caas.admin.sample.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.admin.sample.vo.SamplePVO;
import net.e4net.caas.admin.sample.vo.SampleRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author djim
 * @since 2018.11.01
 */
@Repository
public interface SampleMapper {
	
    /**
     * SAMPLE 목록 조회
     */
    public List<SampleRVO> selectSampleList(SamplePVO pvom, PageBounds pageBounds);
    
    /**
     * SAMPLE 등록
     */
    public int insertSample(SamplePVO pvo);
    
    /**
     * SAMPLE 수정
     */
    public int updateSample(SamplePVO pvo);
    
    /**
     * SAMPLE 수정
     */
    public int deleteSample(SamplePVO pvo);
}
