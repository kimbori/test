package net.e4net.caas.plt.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.plt.vo.PlfAdminAdPVO;
import net.e4net.caas.plt.vo.PlfAdminAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface PlfAdminAdMapper {
	
    /**
     * 플랫폼관리자관리 목록 조회
     */
    public List<PlfAdminAdRVO> selectPlfAdminAdList(PlfAdminAdPVO pvom, PageBounds pageBounds);
    
    /**
     * 플랫폼관리자관리 등록여부 조회
     */
    public int selectPlfAdminAdCnt(PlfAdminAdPVO pvom);
    
    /**
     * 플랫폼관리자관리 등록
     */
    public int insertPlfAdminAd(PlfAdminAdPVO pvo);
    
    /**
     * 플랫폼관리자관리 수정
     */
    public int updatePlfAdminAd(PlfAdminAdPVO pvo);
    
    /**
     * 플랫폼관리자관리 수정
     */
    public int deletePlfAdminAd(PlfAdminAdPVO pvo);
    
}
