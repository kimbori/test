package net.e4net.caas.plt.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class CodeAdRVO extends PageVO {
	private String codeId;
	private String codeIdNm;
	private String codeIdDc;
	
	private String code;
	private String codeNm;
	private String codeDc;
	private String useAt;
}
