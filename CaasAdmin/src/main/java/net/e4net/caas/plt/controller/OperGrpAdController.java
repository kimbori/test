package net.e4net.caas.plt.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.service.CmmService;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.plt.service.OperGrpAdService;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 운영단체관리 컨트롤러
 *
 * @author e4net
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class OperGrpAdController extends AbstractController {
	
    @Autowired
    private OperGrpAdService service;
    
    @Autowired
    private CmmService cmmService;
    
    /**
     * <pre>
     * method 운영단체관리화면
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/plt/operGrpAdPag", method = RequestMethod.GET)
    public ModelAndView selectOperGrpAdList(Model model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("admin/plt/operGrpAdPag");
    	mav.addObject("bkChnKdLst", cmmService.selectCmmCodeDetail("BK_CHN_KD"));	//블록체인종류
    	
    	return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 운영단체관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/plt/selectOperGrpAdListAjax", method = RequestMethod.POST)
    public ModelAndView selectOperGrpAdListAjax(@ModelAttribute OperGrpAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/plt/operGrpAdListAjax :: default");
        List<OperGrpAdRVO> resultList = service.selectOperGrpAdList(pvo);
        
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 운영단체 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/plt/selectOperGrpAdDetail", method = RequestMethod.POST)
    public ModelAndView selectOperGrpAdDetail(@ModelAttribute OperGrpAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        OperGrpAdRVO result = service.selectOperGrpAdDetail(pvo);
        
        mav.addObject("result", result);

        return getOkModelAndView(mav);
    }    
    
    /**
     * <pre>
     * method 운영단체관리 등록
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/plt/insertOperGrpAd", method = RequestMethod.POST)
    public ModelAndView insertOperGrpAd(@ModelAttribute OperGrpAdPVO pvo) {
        
    	ModelAndView mav = new ModelAndView("jsonView");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        Status status = new Status();
        pvo.setPrcLgnId(userEtt.getLgnId());
        
        try {
        	
        	int cnt = service.selectOperGrpAdCnt(pvo);
        	if( cnt != 0 ) {
        		status.setFail("이미 운영단체에 등록된 ID입니다..");
                return getFailModelAndView(mav, status);
        	}
        	
            if ( service.insertOperGrpAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("운영단체 등록에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
            status.setFail("운영단체 등록에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 운영단체관리 수정
     * </pre>
     *
     * @param pvo 수정할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/plt/updateOperGrpAd", method = RequestMethod.POST)
    public ModelAndView updateOperGrpAd(@ModelAttribute OperGrpAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        pvo.setPrcLgnId(userEtt.getLgnId());
        try {
            if ( service.updateOperGrpAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("운영단체관리 수정에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	e.printStackTrace();
        	status.setFail("운영단체관리 수정에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 운영단체관리 삭제
     * </pre>
     *
     * @param pvo 삭제할 정보가 담겨있는 pvo
     * @return String 삭제 결과
     */
    @RequestMapping(value = "/admin/plt/deleteOperGrpAd", method = RequestMethod.POST)
    public ModelAndView deleteOperGrpAd(@ModelAttribute OperGrpAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( service.deleteOperGrpAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("운영단체관리 삭제에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("운영단체관리 삭제에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
}
