package net.e4net.caas.plt.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class AdminAdPVO extends PageVO{
	private long admSq;
	private String oprGrpId;
	private String mrchGrpId;
	private String grpGb;
	private String grpLv;
	private String lgnId;
	private String passwd;
	private String mailAddr;
	private String hpno;
	private String fstRegDttm;
	private String fstRegUsid;
	private String lstChDttm;
	private String lstChUsid;
}
