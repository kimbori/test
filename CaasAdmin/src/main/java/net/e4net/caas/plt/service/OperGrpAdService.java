package net.e4net.caas.plt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.plt.mapper.OperGrpAdMapper;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 운영단체관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class OperGrpAdService extends AbstractDao {
    @Autowired
    private OperGrpAdMapper mapper;
    
    /**
     * 운영단체관리 조회
     *
     * @param pvo 운영단체관리 VO 
     * @return 검색결과 
     */
    public List<OperGrpAdRVO> selectOperGrpAdList(OperGrpAdPVO pvo) {
        return mapper.selectOperGrpAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 운영단체관리 조회
     *
     * @param pvo 운영단체관리 VO 
     * @return 검색결과 
     */
    public OperGrpAdRVO selectOperGrpAdDetail(OperGrpAdPVO pvo) {
        return mapper.selectOperGrpAdDetail(pvo);
    }

    /**
     * 운영단체관리 조회
     *
     * @param pvo 운영단체관리 VO 
     * @return 검색결과 
     */
    public List<OperGrpAdRVO> selectOperGrpAd(OperGrpAdPVO pvo) {
        return mapper.selectOperGrpAd(pvo);
    }
    
    /**
     * 운영단체관리 등록 조회
     *
     * @param pvo 운영단체관리 VO
     * @return 검색결과
     */
    public int selectOperGrpAdCnt(OperGrpAdPVO pvo) {
        return mapper.selectOperGrpAdCnt(pvo);
    }
    
    /**
     * 운영단체관리 등록
     *
     * @param pvo 운영단체관리 VO
     * @return 등록건수
     */
    public int insertOperGrpAd(OperGrpAdPVO pvo) {
        return mapper.insertOperGrpAd(pvo);
    }
    
    /**
     * 운영단체관리 수정
     *
     * @param pvo 운영단체관리 수정 정보
     * @return 수정결과
     */
    public int updateOperGrpAd(OperGrpAdPVO pvo) {
        return mapper.updateOperGrpAd(pvo);
    }
    
    /**
     * 운영단체관리 삭제
     *
     * @param pvo 운영단체관리 수정 정보
     * @return 수정결과
     */
    public int deleteOperGrpAd(OperGrpAdPVO pvo) {
        return mapper.deleteOperGrpAd(pvo);
    }
}
