package net.e4net.caas.plt.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class OperGrpAdPVO extends PageVO{
	private String oprGrpId;            //운영단체ID    
	private String oprGrpNm;            //운영단체명
	private String mailAddr;            //메일 주소
	private String wltAddr;             //지갑주소
	private String homeUrl;             //홈페이지 주소
	private String ctSvcId;             //본인인증 서비스ID
	private String ctEncKey;            //본인인증 복호화키
	private String cpCid;               //카카오페이ID
	private String cpAuthKey;           //카카오페이권한키
	private String pgNormMid;           //일반PG가맹점ID         
	private String pgBllMid;            //빌링PG가맹점ID         
	private String pgBillMkey;          //빌링PG가맹점KEY
	private String pgBllKeyPw;          //빌링PG키비밀번호
	private String pgGoodsName;         //상품PG명
	private String bcKd;                //블록체인 종류
	private String bcTokenSimbol;       //블록체인 토큰심볼
	private String bcApiKey;            //블록체인API키
	private String bcTokenId;           //블록체인 토큰 ID
	private String lstChDttm;
	
	private String prcLgnId;				//등록자id, 수정자id
}
