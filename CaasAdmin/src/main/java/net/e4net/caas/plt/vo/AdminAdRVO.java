package net.e4net.caas.plt.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class AdminAdRVO extends PageVO {
	private long admSq;
	private String oprGrpId;
	private String mrchGrpId;
	private String grpGb;
	private String grpGbNm;
	private String grpLv;
	private String grpLvNm;
	private String lgnId;
	private String passwd;
	private String mailAddr;
	private String hpno;
	private String fstRegDttm;
	private String fstRegUsid;
	private String lstChDttm;
	private String lstChUsid;
	private String oprGrpNm;
	private String mrchGrpNm;
}
