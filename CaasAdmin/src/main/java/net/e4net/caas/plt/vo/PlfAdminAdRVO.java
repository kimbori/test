package net.e4net.caas.plt.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class PlfAdminAdRVO extends PageVO {
	private long admSq;
	private String grpGb;
	private String lgnId;
	private String passwd;
	private String mailAddr;
	private String hpno;
}
