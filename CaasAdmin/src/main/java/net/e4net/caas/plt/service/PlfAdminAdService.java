package net.e4net.caas.plt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.plt.mapper.PlfAdminAdMapper;
import net.e4net.caas.plt.vo.PlfAdminAdPVO;
import net.e4net.caas.plt.vo.PlfAdminAdRVO;

/**
 * 플랫폼관리자관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class PlfAdminAdService extends AbstractDao {
    @Autowired
    private PlfAdminAdMapper mapper;
    
    /**
     * 플랫폼관리자관리 조회
     *
     * @param pvo 플랫폼관리자관리 VO 
     * @return 검색결과 
     */
    public List<PlfAdminAdRVO> selectPlfAdminAdList(PlfAdminAdPVO pvo) {
        return mapper.selectPlfAdminAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 플랫폼관리자관리 등록 조회
     *
     * @param pvo 플랫폼관리자관리 VO
     * @return 검색결과
     */
    public int selectPlfAdminAdCnt(PlfAdminAdPVO pvo) {
        return mapper.selectPlfAdminAdCnt(pvo);
    }
    
    /**
     * 플랫폼관리자관리 등록
     *
     * @param pvo 플랫폼관리자관리 VO
     * @return 등록건수
     */
    public int insertPlfAdminAd(PlfAdminAdPVO pvo) {
    	//비밀번호 encode
    	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    	pvo.setPasswd(passwordEncoder.encode(pvo.getPasswd()));
        return mapper.insertPlfAdminAd(pvo);
    }
    
    /**
     * 플랫폼관리자관리 수정
     *
     * @param pvo 플랫폼관리자관리 수정 정보
     * @return 수정결과
     */
    public int updatePlfAdminAd(PlfAdminAdPVO pvo) {
        return mapper.updatePlfAdminAd(pvo);
    }
    
    /**
     * 플랫폼관리자관리 삭제
     *
     * @param pvo 플랫폼관리자관리 수정 정보
     * @return 수정결과
     */
    public int deletePlfAdminAd(PlfAdminAdPVO pvo) {
        return mapper.deletePlfAdminAd(pvo);
    }
}
