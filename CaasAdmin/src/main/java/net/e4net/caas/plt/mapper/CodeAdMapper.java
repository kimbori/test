package net.e4net.caas.plt.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.plt.vo.CodeAdPVO;
import net.e4net.caas.plt.vo.CodeAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface CodeAdMapper {
	
    /**
     * 공통코드관리 목록 조회
     */
    public List<CodeAdRVO> selectCodeAdList(CodeAdPVO pvom, PageBounds pageBounds);
    
    /**
     * 공통코드관리 목록 조회
     */
    public List<CodeAdRVO> selectCodeAdDetail(CodeAdPVO pvom);
    
    /**
     * 공통코드관리 등록여부 조회
     */
    public int selectCodeAdCnt(CodeAdPVO pvom);
    
    /**
     * 공통코드관리 등록
     */
    public int insertCodeId(CodeAdPVO pvo);
    
    /**
     * 공통코드관리 등록
     */
    public int insertCodeDtl(CodeAdPVO pvo);
    
    /**
     * 공통코드관리 수정
     */
    public int updateCodeAd(CodeAdPVO pvo);
    
    /**
     * 공통코드관리 수정
     */
    public int deleteCodeId(CodeAdPVO pvo);
    
    /**
     * 공통코드관리 수정
     */
    public int deleteCodeDtl(CodeAdPVO pvo);
    
}
