package net.e4net.caas.plt.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface OperGrpAdMapper {
	
    /**
     * 운영단체관리 목록 조회
     */
    public List<OperGrpAdRVO> selectOperGrpAdList(OperGrpAdPVO pvom, PageBounds pageBounds);
    
    /**
     * 운영단체관리 조회
     */
    public OperGrpAdRVO selectOperGrpAdDetail(OperGrpAdPVO pvom);

    /**
     * 운영단체관리 목록 조회
     */
    public List<OperGrpAdRVO> selectOperGrpAd(OperGrpAdPVO pvom);
    
    /**
     * 운영단체관리 등록여부 조회
     */
    public int selectOperGrpAdCnt(OperGrpAdPVO pvom);
    
    /**
     * 운영단체관리 등록
     */
    public int insertOperGrpAd(OperGrpAdPVO pvo);
    
    /**
     * 운영단체관리 수정
     */
    public int updateOperGrpAd(OperGrpAdPVO pvo);
    
    /**
     * 운영단체관리 수정
     */
    public int deleteOperGrpAd(OperGrpAdPVO pvo);
}
