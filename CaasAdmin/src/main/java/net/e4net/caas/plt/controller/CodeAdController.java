package net.e4net.caas.plt.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.plt.service.CodeAdService;
import net.e4net.caas.plt.vo.CodeAdPVO;
import net.e4net.caas.plt.vo.CodeAdRVO;

/**
 * 공통코드관리 컨트롤러
 *
 * @author e4net
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class CodeAdController extends AbstractController {
	
    @Autowired
    private CodeAdService service;
    
    /**
     * <pre>
     * method 공통코드관리화면
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/plt/codeAdPag", method = RequestMethod.GET)
    public String selectCodeAdList(Model model, HttpServletRequest request) throws Exception {
		
    	return "admin/plt/codeAdPag";
    }
    
    /**
     * <pre>
     * method 공통코드관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/plt/selectCodeAdListAjax", method = RequestMethod.POST)
    public ModelAndView selectCodeAdListAjax(@
    		ModelAttribute CodeAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/plt/codeAdListAjax :: default");
        List<CodeAdRVO> resultList = service.selectCodeAdList(pvo);
        
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 공통코드관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/plt/selectCodeDetailAjax", method = RequestMethod.POST)
    public ModelAndView selectCodeDetailAjax(@
    		ModelAttribute CodeAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/plt/codeAdListAjax :: detail");
        List<CodeAdRVO> resultList = service.selectCodeAdDetail(pvo);
        
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }    
    
    /**
     * <pre>
     * method 공통코드관리 등록
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/plt/insertCodeAd", method = RequestMethod.POST)
    public ModelAndView insertCodeAd(@ModelAttribute CodeAdPVO pvo) {
        
    	ModelAndView mav = new ModelAndView("jsonView");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        Status status = new Status();
        pvo.setPrcLgnId(userEtt.getLgnId());
        
        try {
        	
        	int cnt = service.selectCodeAdCnt(pvo);
        	if( cnt != 0 ) {
        		status.setFail("이미 등록된 코드ID입니다..");
                return getFailModelAndView(mav, status);
        	}
        	
            if ( service.insertCodeAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("공통코드 등록에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	e.printStackTrace();
            status.setFail("공통코드 등록에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 공통코드관리 수정
     * </pre>
     *
     * @param pvo 수정할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/plt/updateCodeAd", method = RequestMethod.POST)
    public ModelAndView updateCodeAd(@ModelAttribute CodeAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( service.updateCodeAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("공통코드 수정에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	e.printStackTrace();
        	status.setFail("공통코드 수정에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 공통코드관리 삭제
     * </pre>
     *
     * @param pvo 삭제할 정보가 담겨있는 pvo
     * @return String 삭제 결과
     */
    @RequestMapping(value = "/admin/plt/deleteCodeAd", method = RequestMethod.POST)
    public ModelAndView deleteCodeAd(@ModelAttribute CodeAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( service.deleteCodeAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("공통코드 삭제에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	status.setFail("공통코드 삭제에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
}
