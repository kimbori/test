package net.e4net.caas.plt.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.plt.service.AdminAdService;
import net.e4net.caas.plt.service.OperGrpAdService;
import net.e4net.caas.plt.vo.AdminAdPVO;
import net.e4net.caas.plt.vo.AdminAdRVO;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 관리자관리 컨트롤러
 *
 * @author e4net
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class AdminAdController extends AbstractController {
	
    @Autowired
    private AdminAdService service;
    
    @Autowired
    private OperGrpAdService operGrpAdService;
    
    /**
     * <pre>
     * method 관리자관리화면
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/plt/adminAdPag", method = RequestMethod.GET)
    public ModelAndView selectAdminAdList(Model model, HttpServletRequest request) throws Exception {
		
    	ModelAndView mav = new ModelAndView("admin/plt/adminAdPag");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	//운영단체 selectBox 데이터
    	OperGrpAdPVO pvo = new OperGrpAdPVO();
    	List<OperGrpAdRVO> selectResultList = operGrpAdService.selectOperGrpAd(pvo);
    	
    	mav.addObject("grpGb", userEtt.getGrpGb());
    	mav.addObject("grpLv", userEtt.getGrpLv());
    	mav.addObject("selectResultList", selectResultList);
    	
    	return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 관리자관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/plt/selectAdminAdListAjax", method = RequestMethod.POST)
    public ModelAndView selectAdminAdListAjax(@ModelAttribute AdminAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/plt/adminAdListAjax :: default");
        List<AdminAdRVO> resultList = service.selectAdminAdList(pvo);
        
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
}
