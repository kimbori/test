package net.e4net.caas.plt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.plt.mapper.AdminAdMapper;
import net.e4net.caas.plt.vo.AdminAdPVO;
import net.e4net.caas.plt.vo.AdminAdRVO;

/**
 * 관리자관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class AdminAdService extends AbstractDao {
    @Autowired
    private AdminAdMapper mapper;
    
    /**
     * 관리자관리 조회
     *
     * @param pvo 관리자관리 VO 
     * @return 검색결과 
     */
    public List<AdminAdRVO> selectAdminAdList(AdminAdPVO pvo) {
        return mapper.selectAdminAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
}
