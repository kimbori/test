package net.e4net.caas.plt.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.plt.mapper.CodeAdMapper;
import net.e4net.caas.plt.vo.CodeAdPVO;
import net.e4net.caas.plt.vo.CodeAdRVO;

/**
 * 공통코드 관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class CodeAdService extends AbstractDao {
    @Autowired
    private CodeAdMapper mapper;
    
    /**
     * 공통코드 관리 조회
     *
     * @param pvo 공통코드 관리 VO 
     * @return 검색결과 
     */
    public List<CodeAdRVO> selectCodeAdList(CodeAdPVO pvo) {
        return mapper.selectCodeAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 공통코드 관리 조회
     *
     * @param pvo 공통코드 관리 VO 
     * @return 검색결과 
     */
    public List<CodeAdRVO> selectCodeAdDetail(CodeAdPVO pvo) {
        return mapper.selectCodeAdDetail(pvo);
    }
    
    /**
     * 공통코드 관리 등록 조회
     *
     * @param pvo 공통코드 관리 VO
     * @return 검색결과
     */
    public int selectCodeAdCnt(CodeAdPVO pvo) {
        return mapper.selectCodeAdCnt(pvo);
    }
    
    /**
     * 공통코드 관리 등록
     *
     * @param pvo 공통코드 관리 VO
     * @return 등록건수
     */
    public int insertCodeAd(CodeAdPVO pvo) throws Exception{
    	/*TransactionStatus status = null;
    	try {
    		status = getTransactionStatus("insertCodeAd");
    		int procCnt = 0;
	    	//코드등록
    		procCnt = mapper.insertCodeId(pvo);
	    	
    		if(procCnt < 1) {
        		throw new CaasException("99999", "공통코드 등록 오류");
        	}
    		procCnt = 0;
	    	//코드상세등록
	    	for( int i=0; i<pvo.getInCode().length; i++ ) {
	    		pvo.setCode(pvo.getInCode()[i]);
	    		pvo.setCodeNm(pvo.getInCodeNm()[i]);
	    		pvo.setCodeDc(pvo.getInCodeDc()[i]);
	    		pvo.setUseAt(pvo.getInUseAt()[i]);
	    		
	    		procCnt = mapper.insertCodeDtl(pvo);
	    	}
	    	
	    	if(procCnt < 1) {
        		throw new CaasException("99999", "공통코드 등록 오류");
        	}
	    	
	    	transactionCommit(status);
    	} catch(Exception e) {
    		error("insertCodeAd", e);
    		transactionRollback(status);
    		throw e;
    	}*/
    	
    	
    	//코드등록
		int procCnt = mapper.insertCodeId(pvo);
    	
		if(procCnt < 1) {
    		throw new CaasException("99999", "공통코드 등록 오류");
    	}
		
		procCnt = 0;
    	//코드상세등록
    	for( int i=0; i<pvo.getInCode().length; i++ ) {
    		pvo.setCode(pvo.getInCode()[i]);
    		pvo.setCodeNm(pvo.getInCodeNm()[i]);
    		pvo.setCodeDc(pvo.getInCodeDc()[i]);
    		pvo.setUseAt(pvo.getInUseAt()[i]);
    		
    		procCnt = mapper.insertCodeDtl(pvo);
    	}
    	
    	if(procCnt < 1) {
    		throw new CaasException("99999", "공통코드 등록 오류");
    	}
    	
    	
        return 1;
    }
    
    /**
     * 공통코드 관리 수정
     *
     * @param pvo 공통코드 관리 수정 정보
     * @return 수정결과
     */
    public int updateCodeAd(CodeAdPVO pvo) throws Exception {
    	/*TransactionStatus status = null;
    	try {
    		status = getTransactionStatus("updateCodeAd");
    		int procCnt = 0;
    		procCnt = mapper.deleteCodeDtl(pvo);
	    	
	    	if(procCnt < 1) {
        		throw new CaasException("99999", "공통코드 수정 오류");
        	}
	    	
	    	procCnt = 0;
	    	//코드상세등록
	    	for( int i=0; i<pvo.getInCode().length; i++ ) {
	    		pvo.setCode(pvo.getInCode()[i]);
	    		pvo.setCodeNm(pvo.getInCodeNm()[i]);
	    		pvo.setCodeDc(pvo.getInCodeDc()[i]);
	    		pvo.setUseAt(pvo.getInUseAt()[i]);
	    		
	    		procCnt = mapper.insertCodeDtl(pvo);
	    	}
	    	
	    	if(procCnt < 1) {
        		throw new CaasException("99999", "공통코드 수정 오류");
        	}
	    	
	    	transactionCommit(status);
    	}catch(Exception e) {
    		error("updateCodeAd", e);
    		transactionRollback(status);
    		throw e;
    	}*/
    	
    	int procCnt = 0;
		procCnt = mapper.deleteCodeDtl(pvo);
    	
    	if(procCnt < 1) {
    		throw new CaasException("99999", "공통코드 수정 오류");
    	}
    	
    	procCnt = 0;
    	//코드상세등록
    	for( int i=0; i<pvo.getInCode().length; i++ ) {
    		pvo.setCode(pvo.getInCode()[i]);
    		pvo.setCodeNm(pvo.getInCodeNm()[i]);
    		pvo.setCodeDc(pvo.getInCodeDc()[i]);
    		pvo.setUseAt(pvo.getInUseAt()[i]);
    		
    		procCnt = mapper.insertCodeDtl(pvo);
    	}
    	
    	if(procCnt < 1) {
    		throw new CaasException("99999", "공통코드 수정 오류");
    	}
    	
        return 1;
    }
    
    /**
     * 공통코드 관리 삭제
     *
     * @param pvo 공통코드 관리 수정 정보
     * @return 수정결과
     */
    public int deleteCodeAd(CodeAdPVO pvo) throws Exception{
    	/*TransactionStatus status = null;
    	try {
    		status = getTransactionStatus("deleteCodeAd");
    		int procCnt = 0;
    		
    		procCnt = mapper.deleteCodeId(pvo);
    		if(procCnt < 1) {
        		throw new CaasException("99999", "공통코드 삭제 오류");
        	}
    		
    		procCnt = mapper.deleteCodeDtl(pvo);
    		if(procCnt < 1) {
        		throw new CaasException("99999", "공통코드 삭제 오류");
        	}
	    	
    	}catch (Exception e) {
    		error("deleteCodeAd", e);
    		transactionRollback(status);
    		throw e;
    	}*/
    	
    	int procCnt = 0;
		
		procCnt = mapper.deleteCodeId(pvo);
		if(procCnt < 1) {
    		throw new CaasException("99999", "공통코드 삭제 오류");
    	}
		
		procCnt = mapper.deleteCodeDtl(pvo);
		if(procCnt < 1) {
    		throw new CaasException("99999", "공통코드 삭제 오류");
    	}
    	
        return 1;
    }
}
