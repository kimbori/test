package net.e4net.caas.plt.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.plt.vo.AdminAdPVO;
import net.e4net.caas.plt.vo.AdminAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface AdminAdMapper {
	
    /**
     * 관리자관리 목록 조회
     */
    public List<AdminAdRVO> selectAdminAdList(AdminAdPVO pvom, PageBounds pageBounds);
    
}
