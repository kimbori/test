package net.e4net.caas.plt.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.plt.service.PlfAdminAdService;
import net.e4net.caas.plt.vo.PlfAdminAdPVO;
import net.e4net.caas.plt.vo.PlfAdminAdRVO;

/**
 * 플랫폼관리자관리 컨트롤러
 *
 * @author e4net
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class PlfAdminAdController extends AbstractController {
	
    @Autowired
    private PlfAdminAdService service;
    
    /**
     * <pre>
     * method 플랫폼관리자관리화면
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/plt/plfAdminAdPag", method = RequestMethod.GET)
    public String selectPlfAdminAdList(Model model, HttpServletRequest request) throws Exception {
		
    	return "admin/plt/plfAdminAdPag";
    }
    
    /**
     * <pre>
     * method 플랫폼관리자관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/plt/selectPlfAdminAdListAjax", method = RequestMethod.POST)
    public ModelAndView selectPlfAdminAdListAjax(@ModelAttribute PlfAdminAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/plt/plfAdminAdListAjax :: default");
        List<PlfAdminAdRVO> resultList = service.selectPlfAdminAdList(pvo);
        
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 플랫폼관리자 등록
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/plt/insertPlfAdminAd", method = RequestMethod.POST)
    public ModelAndView insertPlfAdminAd(@ModelAttribute PlfAdminAdPVO pvo) {
        
    	ModelAndView mav = new ModelAndView("jsonView");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        Status status = new Status();
        pvo.setPrcLgnId(userEtt.getLgnId());
        
        try {
        	
        	int cnt = service.selectPlfAdminAdCnt(pvo);
        	if( cnt != 0 ) {
        		status.setFail("이미 등록된 ID입니다..");
                return getFailModelAndView(mav, status);
        	}
        	
            if ( service.insertPlfAdminAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("플랫폼관리자 등록에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
            status.setFail("플랫폼관리자 등록에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 플랫폼관리자 수정
     * </pre>
     *
     * @param pvo 수정할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/plt/updatePlfAdminAd", method = RequestMethod.POST)
    public ModelAndView updatePlfAdminAd(@ModelAttribute PlfAdminAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( service.updatePlfAdminAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("플랫폼관리자 수정에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("플랫폼관리자 수정에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 플랫폼관리자 삭제
     * </pre>
     *
     * @param pvo 삭제할 정보가 담겨있는 pvo
     * @return String 삭제 결과
     */
    @RequestMapping(value = "/admin/plt/deletePlfAdminAd", method = RequestMethod.POST)
    public ModelAndView deletePlfAdminAd(@ModelAttribute PlfAdminAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( service.deletePlfAdminAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("플랫폼관리자 삭제에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("플랫폼관리자 삭제에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
}
