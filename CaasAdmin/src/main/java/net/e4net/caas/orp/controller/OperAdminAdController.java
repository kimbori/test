package net.e4net.caas.orp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.service.CmmService;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.orp.service.OperAdminAdService;
import net.e4net.caas.orp.vo.OperAdminAdPVO;
import net.e4net.caas.orp.vo.OperAdminAdRVO;
import net.e4net.caas.plt.service.OperGrpAdService;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 단체관리자관리 컨트롤러
 *
 * @author e4net
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class OperAdminAdController extends AbstractController {
	
    @Autowired
    private OperAdminAdService service;
    
    @Autowired
    private OperGrpAdService operGrpAdService;
    
    @Autowired
    private CmmService cmmService;
    
    
    /**
     * <pre>
     * method 단체관리자관리화면
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/orp/operAdminAdPag", method = RequestMethod.GET)
    public ModelAndView selectOperAdminAdList(Model model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("admin/orp/operAdminAdPag");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	//운영단체 selectBox 데이터
    	OperGrpAdPVO pvo = new OperGrpAdPVO();
    	List<OperGrpAdRVO> selectResultList = operGrpAdService.selectOperGrpAd(pvo);
    	
    	mav.addObject("grpGb", userEtt.getGrpGb());
    	mav.addObject("grpLv", userEtt.getGrpLv());
    	mav.addObject("grpLvLst", cmmService.selectCmmCodeDetail("GRP_LV"));	//관리자레벨
    	mav.addObject("selectResultList", selectResultList);
    	
    	return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 단체관리자관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/orp/selectOperAdminAdListAjax", method = RequestMethod.POST)
    public ModelAndView selectOperAdminAdListAjax(@ModelAttribute OperAdminAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/orp/operAdminAdListAjax :: default");
        
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<OperAdminAdRVO> resultList = service.selectOperAdminAdList(pvo, userEtt);
        
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 단체관리자관리 등록
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/orp/insertOperAdminAd", method = RequestMethod.POST)
    public ModelAndView insertOperAdminAd(@ModelAttribute OperAdminAdPVO pvo) {
        
    	ModelAndView mav = new ModelAndView("jsonView");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        Status status = new Status();
        pvo.setPrcLgnId(userEtt.getLgnId());
        
        try {
        	
        	int cnt = service.selectOperAdminAdCnt(pvo);
        	if( cnt != 0 ) {
        		status.setFail("이미 등록된 로그인 ID입니다..");
                return getFailModelAndView(mav, status);
        	}
        	
            if ( service.insertOperAdminAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("운영단체관리자 등록에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
            status.setFail("운영단체관리자 등록에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 단체관리자관리 수정
     * </pre>
     *
     * @param pvo 수정할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/orp/updateOperAdminAd", method = RequestMethod.POST)
    public ModelAndView updateOperAdminAd(@ModelAttribute OperAdminAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        pvo.setPrcLgnId(userEtt.getLgnId());
        try {
            if ( service.updateOperAdminAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("운영단체관리자 수정에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("운영단체관리자 수정에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 단체관리자관리 삭제
     * </pre>
     *
     * @param pvo 삭제할 정보가 담겨있는 pvo
     * @return String 삭제 결과
     */
    @RequestMapping(value = "/admin/orp/deleteOperAdminAd", method = RequestMethod.POST)
    public ModelAndView deleteOperAdminAd(@ModelAttribute OperAdminAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( service.deleteOperAdminAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("운영단체관리자 삭제에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("운영단체관리자 삭제에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
}
