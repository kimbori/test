package net.e4net.caas.orp.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class DntPrsnAdPVO extends PageVO{
	private String oprGrpId;
	private String cnrNm;
	private long cnrSq;
}
