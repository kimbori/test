package net.e4net.caas.orp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.orp.mapper.DntPrsnAdMapper;
import net.e4net.caas.orp.vo.DntPrsnAdPVO;
import net.e4net.caas.orp.vo.DntPrsnAdRVO;

/**
 * 단체관리자관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class DntPrsnAdService extends AbstractDao {
    @Autowired
    private DntPrsnAdMapper mapper;
    
    /**
     * 단체관리자관리 조회
     *
     * @param pvo 단체관리자관리 VO 
     * @return 검색결과 
     */
    public List<DntPrsnAdRVO> selectDntPrsnAdList(DntPrsnAdPVO pvo, UserVO userEtt) {
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
        return mapper.selectDntPrsnAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 단체관리자관리 조회
     *
     * @param pvo 단체관리자관리 VO 
     * @return 검색결과 
     */
    public DntPrsnAdRVO selectDntPrsn(DntPrsnAdPVO pvo) {
        return mapper.selectDntPrsn(pvo);
    }
}
