package net.e4net.caas.orp.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class DntPrsnAdRVO extends PageVO{
	private String cnrSq;
	private String oprGrpId;
	private String oprGrpNm;
	private String cnrNm;
	private String nickNm;
	private String prfImg;
	private String trmlId;
	private String passwd;
	private String hpno;
	private String prsnAuthSq;
	private String cnrCi;
	private String mailAddr;
	private String wltAddr;
}
