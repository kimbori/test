package net.e4net.caas.orp.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class MrchGrpAdRVO extends PageVO{
	private String mrchGrpId;
	private String mrchGrpNm;
	private String oprGrpId;
	private String oprGrpNm;
	private String mrchGrpImg;
	private String mailAddr;
	private String wltAddr;
	private int fxprStdd;
	private String rprsNum;
	private String faxNum;
	private String bnkCd;
	private String bnkNm;
	private String bnkAcct;
	private long crgAdminSq;
	private String fstRegDttm;
	private String fstRegUsid;
	private String lstChDttm;
	private String lstChUsid;
}
