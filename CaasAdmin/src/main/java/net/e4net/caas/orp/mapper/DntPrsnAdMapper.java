package net.e4net.caas.orp.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.orp.vo.DntPrsnAdPVO;
import net.e4net.caas.orp.vo.DntPrsnAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface DntPrsnAdMapper {
	
    /**
     * 단체관리자관리 목록 조회
     */
    public List<DntPrsnAdRVO> selectDntPrsnAdList(DntPrsnAdPVO pvom, PageBounds pageBounds);
    
    /**
     * 단체관리자관리 목록 조회
     */
    public DntPrsnAdRVO selectDntPrsn(DntPrsnAdPVO pvom);
    
}
