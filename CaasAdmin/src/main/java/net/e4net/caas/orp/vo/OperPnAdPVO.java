package net.e4net.caas.orp.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class OperPnAdPVO extends PageVO{
	private String prcLgnId;				//등록자id, 수정자id
	
	private long pnSq;
	private String oprGrpId;
	private String title;
	private String ctnt;
	private String notcYn;
	
	

}
