package net.e4net.caas.orp.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class OperPnAdRVO extends PageVO{
	private long pnSq;
	private String oprGrpId;
	private String oprGrpNm;
	private String title;
	private String ctnt;
	private String notcYn;
	private String fstRegDttm;
	private String fstRegUsid;
	private String lstChDttm;
	private String lstChUsid;
}
