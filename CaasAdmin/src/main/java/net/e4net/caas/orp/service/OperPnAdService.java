package net.e4net.caas.orp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.orp.mapper.OperPnAdMapper;
import net.e4net.caas.orp.vo.OperPnAdPVO;
import net.e4net.caas.orp.vo.OperPnAdRVO;

/**
 * 공지사항 관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class OperPnAdService extends AbstractDao {
    @Autowired
    private OperPnAdMapper mapper;
    
    /**
     * 공지사항 관리 조회
     *
     * @param pvo 공지사항 관리 VO 
     * @return 검색결과 
     */
    public List<OperPnAdRVO> selectOperPnAdList(OperPnAdPVO pvo, UserVO userEtt) {
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
        return mapper.selectOperPnAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 공지사항 관리 조회
     *
     * @param pvo 공지사항 관리 VO 
     * @return 검색결과 
     */
    public OperPnAdRVO selectOperPn(OperPnAdPVO pvo) {
        return mapper.selectOperPn(pvo);
    }
    
    /**
     * 공지사항 관리 조회
     *
     * @param pvo 공지사항 관리 VO 
     * @return 검색결과 
     */
    public List<OperPnAdRVO> selectOperPnAd(OperPnAdPVO pvo) {
        return mapper.selectOperPnAd(pvo);
    }
    
    /**
     * 공지사항 관리 등록 조회
     *
     * @param pvo 공지사항 관리 VO
     * @return 검색결과
     */
    public int selectOperPnAdCnt(OperPnAdPVO pvo) {
        return mapper.selectOperPnAdCnt(pvo);
    }
    
    /**
     * 공지사항 관리 등록
     *
     * @param pvo 공지사항 관리 VO
     * @return 등록건수
     */
    public int insertOperPnAd(OperPnAdPVO pvo) {
    	
    	int procCnt = mapper.insertOperPnAd(pvo);
    	//알림여부가 Y 인 경우 운영단체 소속 기부자 수 만큼 알림내역  insert 처리
    	if( "Y".equals(pvo.getNotcYn()) ) {
    		procCnt = mapper.insertOperPnNotcLst(pvo);
    	}
    	
    	return procCnt;
    }
    
    /**
     * 공지사항 관리 수정
     *
     * @param pvo 공지사항 관리 수정 정보
     * @return 수정결과
     */
    public int updateOperPnAd(OperPnAdPVO pvo) {
        return mapper.updateOperPnAd(pvo);
    }
    
    /**
     * 공지사항 관리 삭제
     *
     * @param pvo 공지사항 관리 수정 정보
     * @return 수정결과
     */
    public int deleteOperPnAd(OperPnAdPVO pvo) {
        return mapper.deleteOperPnAd(pvo);
    }
}
