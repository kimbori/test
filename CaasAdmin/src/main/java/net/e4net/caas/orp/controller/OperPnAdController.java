package net.e4net.caas.orp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.orp.service.OperPnAdService;
import net.e4net.caas.orp.vo.OperPnAdPVO;
import net.e4net.caas.orp.vo.OperPnAdRVO;
import net.e4net.caas.plt.service.OperGrpAdService;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 공지사항관리 컨트롤러
 *
 * @author e4net
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class OperPnAdController extends AbstractController {
	
    @Autowired
    private OperPnAdService service;
    
    @Autowired
    private OperGrpAdService operGrpAdService;

    /**
     * <pre>
     * method 공지사항관리화면
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/orp/operPnAdPag", method = RequestMethod.GET)
    public ModelAndView selectOperPnAdList(Model model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("admin/orp/operPnAdPag");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	//운영단체 selectBox 데이터
    	OperGrpAdPVO pvo = new OperGrpAdPVO();
    	List<OperGrpAdRVO> selectResultList = operGrpAdService.selectOperGrpAd(pvo);
    	
    	mav.addObject("grpGb", userEtt.getGrpGb());
    	mav.addObject("grpLv", userEtt.getGrpLv());
    	mav.addObject("selectResultList", selectResultList);
    	
    	return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 공지사항관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/orp/selectOperPnAdListAjax", method = RequestMethod.POST)
    public ModelAndView selectOperPnAdListAjax(@ModelAttribute OperPnAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/orp/operPnAdListAjax :: default");
        
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<OperPnAdRVO> resultList = service.selectOperPnAdList(pvo, userEtt);
        
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 공지사항관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/orp/selectOperPn", method = RequestMethod.POST)
    public ModelAndView selectOperPn(@ModelAttribute OperPnAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        
        OperPnAdRVO result = service.selectOperPn(pvo);
        
        mav.addObject("result", result);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 공지사항관리 등록
     * </pre>
     *
     * @param pvo 저장할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/orp/insertOperPnAd", method = RequestMethod.POST)
    public ModelAndView insertOperPnAd(@ModelAttribute OperPnAdPVO pvo) {
        
    	ModelAndView mav = new ModelAndView("jsonView");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        Status status = new Status();
        pvo.setPrcLgnId(userEtt.getLgnId());
        
        try {
            if ( service.insertOperPnAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("가맹단체 등록에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	e.printStackTrace();
            status.setFail("가맹단체 등록에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 공지사항관리 수정
     * </pre>
     *
     * @param pvo 수정할 정보가 담겨있는 pvo
     * @return String 수정 결과
     */
    @RequestMapping(value = "/admin/orp/updateOperPnAd", method = RequestMethod.POST)
    public ModelAndView updateOperPnAd(@ModelAttribute OperPnAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        
        pvo.setPrcLgnId(userEtt.getLgnId());
        try {
            if ( service.updateOperPnAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("가맹단체 수정에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("가맹단체 수정에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
    
    /**
     * <pre>
     * method 공지사항관리 삭제
     * </pre>
     *
     * @param pvo 삭제할 정보가 담겨있는 pvo
     * @return String 삭제 결과
     */
    @RequestMapping(value = "/admin/orp/deleteOperPnAd", method = RequestMethod.POST)
    public ModelAndView deleteOperPnAd(@ModelAttribute OperPnAdPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();
        
        try {
            if ( service.deleteOperPnAd(pvo) > 0 ) {
            	status.setOk(0);
            	return getOkModelAndView(mav, status);
            } else {
                status.setFail("가맹단체 삭제에 실패하였습니다.");
                return getFailModelAndView(mav, status);
            }
        } catch (Exception e){
        	debug(e.toString());
        	status.setFail("가맹단체 삭제에 실패하였습니다.");
            return getFailModelAndView(mav, status);
        }
    }
}
