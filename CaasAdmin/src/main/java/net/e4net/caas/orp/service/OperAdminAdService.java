package net.e4net.caas.orp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.orp.mapper.OperAdminAdMapper;
import net.e4net.caas.orp.vo.OperAdminAdPVO;
import net.e4net.caas.orp.vo.OperAdminAdRVO;

/**
 * 단체관리자관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class OperAdminAdService extends AbstractDao {
    @Autowired
    private OperAdminAdMapper mapper;
    
    /**
     * 단체관리자관리 조회
     *
     * @param pvo 단체관리자관리 VO 
     * @return 검색결과 
     */
    public List<OperAdminAdRVO> selectOperAdminAdList(OperAdminAdPVO pvo, UserVO userEtt) {
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
        return mapper.selectOperAdminAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 단체관리자관리 등록 조회
     *
     * @param pvo 단체관리자관리 VO
     * @return 검색결과
     */
    public int selectOperAdminAdCnt(OperAdminAdPVO pvo) {
        return mapper.selectOperAdminAdCnt(pvo);
    }
    
    /**
     * 단체관리자관리 등록
     *
     * @param pvo 단체관리자관리 VO
     * @return 등록건수
     */
    public int insertOperAdminAd(OperAdminAdPVO pvo) {
    	//비밀번호 encode
    	BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
    	pvo.setPasswd(passwordEncoder.encode(pvo.getPasswd()));
        return mapper.insertOperAdminAd(pvo);
    }
    
    /**
     * 단체관리자관리 수정
     *
     * @param pvo 단체관리자관리 수정 정보
     * @return 수정결과
     */
    public int updateOperAdminAd(OperAdminAdPVO pvo) {
        return mapper.updateOperAdminAd(pvo);
    }
    
    /**
     * 단체관리자관리 삭제
     *
     * @param pvo 단체관리자관리 수정 정보
     * @return 수정결과
     */
    public int deleteOperAdminAd(OperAdminAdPVO pvo) {
        return mapper.deleteOperAdminAd(pvo);
    }
}
