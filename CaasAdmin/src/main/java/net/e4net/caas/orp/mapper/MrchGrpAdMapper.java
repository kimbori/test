package net.e4net.caas.orp.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.orp.vo.MrchGrpAdPVO;
import net.e4net.caas.orp.vo.MrchGrpAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface MrchGrpAdMapper {
	
    /**
     * 단체관리자관리 목록 조회
     */
    public List<MrchGrpAdRVO> selectMrchGrpAdList(MrchGrpAdPVO pvom, PageBounds pageBounds);
    
    /**
     * 단체관리자관리 목록 조회
     */
    public List<MrchGrpAdRVO> selectMrchGrpAd(MrchGrpAdPVO pvom);
    
    /**
     * 단체관리자관리 등록여부 조회
     */
    public int selectMrchGrpAdCnt(MrchGrpAdPVO pvom);
    
    /**
     * 단체관리자관리 등록
     */
    public int insertMrchGrpAd(MrchGrpAdPVO pvo);
    
    /**
     * 단체관리자관리 수정
     */
    public int updateMrchGrpAd(MrchGrpAdPVO pvo);
    
    /**
     * 단체관리자관리 수정
     */
    public int deleteMrchGrpAd(MrchGrpAdPVO pvo);
}
