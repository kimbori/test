package net.e4net.caas.orp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.orp.mapper.SttlFiatAdMapper;
import net.e4net.caas.orp.vo.SttlFiatAdPVO;
import net.e4net.caas.orp.vo.SttlFiatAdRVO;

/**
 * 단체관리자관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class SttlFiatAdService extends AbstractDao {
    @Autowired
    private SttlFiatAdMapper mapper;
    
    /**
     * 단체관리자관리 조회
     *
     * @param pvo 단체관리자관리 VO 
     * @return 검색결과 
     */
    public List<SttlFiatAdRVO> selectSttlFiatAdList(SttlFiatAdPVO pvo, UserVO userEtt) {
    	pvo.setSttlDttmSt(StringUtils.replace(pvo.getSttlDttmSt(), "-", ""));
    	pvo.setSttlDttmEd(StringUtils.replace(pvo.getSttlDttmEd(), "-", ""));
        return mapper.selectSttlFiatAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
}
