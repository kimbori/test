package net.e4net.caas.orp.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class SttlFiatAdPVO extends PageVO{
	private String prcLgnId;		//등록자id, 수정자id
	
	private String oprGrpId;		//운영단체id
	private String mrchGrpId;		//가맹단체id
	private String sttlDttmSt;		//정산일시 시작일
	private String sttlDttmEd;		//정산일시 종료일
	private String sttlTp;			//정산타입
	
}
