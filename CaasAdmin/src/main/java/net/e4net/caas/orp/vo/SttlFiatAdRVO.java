package net.e4net.caas.orp.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import net.e4net.caas.common.vo.PageVO;

@SuppressWarnings("serial")
@Data
@EqualsAndHashCode(callSuper = true)
public class SttlFiatAdRVO extends PageVO{
	private String sttlItmSq;	//정산내역 sq
    private String mrchGrpId;	//가맹단체 Id
    private String mrchGrpNm;	//가맹단체명
    private String sttlDttm;	//정산일시
    private String sttlTp;		//정산타입
    private String sttlAmt;		//정상금액
    private String fstRegDttm;	//최초등록일시
    private String fstRegUsid;	//최초등록자
    private String lstChDttm;	//최종변경일시
    private String lstChUsid;	//최종변경자
}
