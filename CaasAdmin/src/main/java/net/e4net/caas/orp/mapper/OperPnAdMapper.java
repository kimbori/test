package net.e4net.caas.orp.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.orp.vo.OperPnAdPVO;
import net.e4net.caas.orp.vo.OperPnAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface OperPnAdMapper {
	
    /**
     * 공지사항관리 목록 조회
     */
    public List<OperPnAdRVO> selectOperPnAdList(OperPnAdPVO pvom, PageBounds pageBounds);
    
    /**
     * 공지사항관리 목록 조회
     */
    public OperPnAdRVO selectOperPn(OperPnAdPVO pvom);
    
    /**
     * 공지사항관리 목록 조회
     */
    public List<OperPnAdRVO> selectOperPnAd(OperPnAdPVO pvom);
    
    /**
     * 공지사항관리 등록여부 조회
     */
    public int selectOperPnAdCnt(OperPnAdPVO pvom);
    
    /**
     * 공지사항관리 등록
     */
    public int insertOperPnAd(OperPnAdPVO pvo);
    
    /**
     * 공지사항관리 수정
     */
    public int updateOperPnAd(OperPnAdPVO pvo);
    
    /**
     * 공지사항관리 수정
     */
    public int deleteOperPnAd(OperPnAdPVO pvo);

    /**
     * 공지사항 등록 시 알림내역을 기부자건수만큼  insert
     */
    public int insertOperPnNotcLst(OperPnAdPVO pvo);
    
}
