package net.e4net.caas.orp.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.service.BlockchainService;
import net.e4net.caas.common.util.DateUtils;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.mnch.service.CmpInfoAdService;
import net.e4net.caas.mnch.vo.TbCmpInfPVO;
import net.e4net.caas.orp.mapper.MrchGrpAdMapper;
import net.e4net.caas.orp.vo.MrchGrpAdPVO;
import net.e4net.caas.orp.vo.MrchGrpAdRVO;

/**
 * 단체관리자관리 서비스
 *
 * @author e4net
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class MrchGrpAdService extends AbstractDao {
    @Autowired
    private MrchGrpAdMapper mapper;
    
    @Autowired 
    private BlockchainService blockChainService;
    
    @Autowired 
    private CmpInfoAdService cmpInfoAdService;
    
    
    /**
     * 단체관리자관리 조회
     *
     * @param pvo 단체관리자관리 VO 
     * @return 검색결과 
     */
    public List<MrchGrpAdRVO> selectMrchGrpAdList(MrchGrpAdPVO pvo, UserVO userEtt) {
    	
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	
        return mapper.selectMrchGrpAdList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 단체관리자관리 조회
     *
     * @param pvo 단체관리자관리 VO 
     * @return 검색결과 
     */
    public List<MrchGrpAdRVO> selectMrchGrpAd(MrchGrpAdPVO pvo) {
        return mapper.selectMrchGrpAd(pvo);
    }
    
    /**
     * 단체관리자관리 등록 조회
     *
     * @param pvo 단체관리자관리 VO
     * @return 검색결과
     */
    public int selectMrchGrpAdCnt(MrchGrpAdPVO pvo) {
        return mapper.selectMrchGrpAdCnt(pvo);
    }
    
    /**
     * 단체관리자관리 등록
     *
     * @param pvo 단체관리자관리 VO
     * @return 등록건수
     */
    public int insertMrchGrpAd(MrchGrpAdPVO pvo, UserVO userEtt) {
    	//초기값 셋팅
    	pvo.setCrgAdminSq(StringUtils.parseLong(pvo.getCrgAdminSq()+"", 0));
    	//해당 단체에 대해서만 조회할 수 있도록 수정
    	if( !"0".equals(userEtt.getGrpGb()) ) {
    		pvo.setOprGrpId(userEtt.getOprGrpId());
    	}
    	//가맹단체 지갑주소 획득
    	pvo.setWltAddr(blockChainService.createWalletGrpMct(pvo.getOprGrpId(), pvo.getMrchGrpId(), pvo.getOprGrpId()).getWltAddr());
        
    	int insCnt = mapper.insertMrchGrpAd(pvo);

    	if( insCnt > 0 ) {
    		//캠페인 등록 (캠페인유형 : 기본후원)
    		TbCmpInfPVO inVo = new TbCmpInfPVO();
    		inVo.setOprGrpId(pvo.getOprGrpId());
    		inVo.setMrchGrpId(pvo.getMrchGrpId());
    		inVo.setCmpNm(pvo.getMrchGrpNm());
    		inVo.setCmpCn(pvo.getMrchGrpNm());
    		inVo.setDnmrStat("B");	//진행(중)
    		inVo.setDnmrTp("01");	//기본후원
    		inVo.setDnmrKn("03");	//둘다
    		inVo.setShowYn("Y");
    		inVo.setDnmrStatDt(DateUtils.getNowDate("yyyyMMdd"));
    		inVo.setDnmrEndDt("99991231");
    		
    		cmpInfoAdService.insertCmpInfo(inVo, userEtt);
    	}
    	
    	return 1;
    }
    
    /**
     * 단체관리자관리 수정
     *
     * @param pvo 단체관리자관리 수정 정보
     * @return 수정결과
     */
    public int updateMrchGrpAd(MrchGrpAdPVO pvo) {
        return mapper.updateMrchGrpAd(pvo);
    }
    
    /**
     * 단체관리자관리 삭제
     *
     * @param pvo 단체관리자관리 수정 정보
     * @return 수정결과
     */
    public int deleteMrchGrpAd(MrchGrpAdPVO pvo) {
        return mapper.deleteMrchGrpAd(pvo);
    }
}
