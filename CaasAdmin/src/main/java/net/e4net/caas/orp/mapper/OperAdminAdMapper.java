package net.e4net.caas.orp.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.orp.vo.OperAdminAdPVO;
import net.e4net.caas.orp.vo.OperAdminAdRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author e4net
 * @since 2018.11.01
 */
@Repository
public interface OperAdminAdMapper {
	
    /**
     * 단체관리자관리 목록 조회
     */
    public List<OperAdminAdRVO> selectOperAdminAdList(OperAdminAdPVO pvom, PageBounds pageBounds);
    
    /**
     * 단체관리자관리 등록여부 조회
     */
    public int selectOperAdminAdCnt(OperAdminAdPVO pvom);
    
    /**
     * 단체관리자관리 등록
     */
    public int insertOperAdminAd(OperAdminAdPVO pvo);
    
    /**
     * 단체관리자관리 수정
     */
    public int updateOperAdminAd(OperAdminAdPVO pvo);
    
    /**
     * 단체관리자관리 수정
     */
    public int deleteOperAdminAd(OperAdminAdPVO pvo);
}
