package net.e4net.caas.orp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.orp.service.DntPrsnAdService;
import net.e4net.caas.orp.vo.DntPrsnAdPVO;
import net.e4net.caas.orp.vo.DntPrsnAdRVO;
import net.e4net.caas.plt.service.OperGrpAdService;
import net.e4net.caas.plt.vo.OperGrpAdPVO;
import net.e4net.caas.plt.vo.OperGrpAdRVO;

/**
 * 기부자관리 컨트롤러
 *
 * @author e4net
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class DntPrsnAdController extends AbstractController {
	
    @Autowired
    private DntPrsnAdService service;
    
    @Autowired
    private OperGrpAdService operGrpAdService;
    
    /**
     * <pre>
     * method 기부자관리화면
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/admin/orp/dntPrsnAdPag", method = RequestMethod.GET)
    public ModelAndView selectDntPrsnAdList(Model model, HttpServletRequest request) throws Exception {
    	
    	ModelAndView mav = new ModelAndView("admin/orp/dntPrsnAdPag");
    	UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	//운영단체 selectBox 데이터
    	OperGrpAdPVO pvo = new OperGrpAdPVO();
    	List<OperGrpAdRVO> selectResultList = operGrpAdService.selectOperGrpAd(pvo);
    	
    	mav.addObject("grpGb", userEtt.getGrpGb());
    	mav.addObject("grpLv", userEtt.getGrpLv());
    	mav.addObject("selectResultList", selectResultList);
    	
    	return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * method 기부자관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/orp/selectDntPrsnAdListAjax", method = RequestMethod.POST)
    public ModelAndView selectDntPrsnAdListAjax(@ModelAttribute DntPrsnAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/orp/dntPrsnAdListAjax :: default");
        
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<DntPrsnAdRVO> resultList = service.selectDntPrsnAdList(pvo, userEtt);
        
        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
        mav.addObject("resultList", resultList);

        return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * method 기부자 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/orp/selectDntPrsn", method = RequestMethod.POST)
    public ModelAndView selectDntPrsn(@ModelAttribute DntPrsnAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("jsonView");
        
        DntPrsnAdRVO result = service.selectDntPrsn(pvo);
        
        mav.addObject("result", result);
        
        return getOkModelAndView(mav);
    }
    
}
