package net.e4net.caas.common.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.util.LogObject;

/**
 * AbstractController
 * @author djim
 * @since 2018.11.01
 * <pre>
 * history
 *    2018.11.01 최초생성
 * </pre>
 */
@Controller
@SuppressWarnings("serial")
public class AbstractController extends LogObject {

	protected void setOkStatus(Model model) {
		setStatus(model, Status.ok());
	}

	protected void setFailStatus(Model model) {
		setStatus(model, Status.fail());
	}

	protected void setStatus(Model model, Status status) {
		model.addAttribute("status", status);
	}

	protected ModelAndView getOkModelAndView(ModelAndView mav) {
		return getOkModelAndView(mav, Status.ok());
	}

	protected ModelAndView getOkModelAndView(ModelAndView mav, Status status) {
		mav.addObject("status", status);
		return mav;
	}

	protected ModelAndView getFailModelAndView(ModelAndView mav) {
		return getFailModelAndView(mav, Status.fail());
	}

	protected ModelAndView getFailModelAndView(ModelAndView mav, Status status) {
		mav.addObject("status", status);
		return mav;
	}

}
