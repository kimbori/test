package net.e4net.caas.common;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
    private AuthProvider authProvider;
  
	@Autowired
	private Environment environment;
	
    @Override
    protected void configure(HttpSecurity http) throws Exception {
    	
        http.authorizeRequests()
        		.antMatchers("/assets/**").permitAll()
        		.antMatchers("/moveLoginPag").permitAll()
                .antMatchers("/**").hasRole("ADMIN")
                .anyRequest().authenticated();
        http.csrf().disable();
        
        http.formLogin()
                .loginPage("/moveLoginPag") // default
                .loginProcessingUrl("/authenticate")
                .failureUrl("/error") // default
                .defaultSuccessUrl("/moveIndexPag")
                .usernameParameter("id")
                .passwordParameter("password")
                .permitAll();
 
        http.logout()
                .logoutUrl("/logout") // default
                .logoutSuccessUrl("/moveLoginPag")
                .permitAll();
        
        String[] profiles = environment.getActiveProfiles();
        
        /*if (!"local".equals(profiles[0])) {
        	http.requiresChannel().antMatchers("/moveLoginPag").requiresSecure();
        }*/
    }
 
    @Override
    protected void configure(AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authProvider);
    }
}