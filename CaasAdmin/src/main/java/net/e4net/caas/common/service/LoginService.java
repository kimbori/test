package net.e4net.caas.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.mapper.LoginMapper;
import net.e4net.caas.common.vo.UserVO;

/**
 * LOGIN 서비스
 *
 * @author djim
 * @since 2018.11.01
 */
@SuppressWarnings("serial")
@Service
public class LoginService extends AbstractDao {
    @Autowired
    private LoginMapper loginMapper;
    
    /**
     * LOGIN 서비스
     *
     * @param Sting 로그인ID
     * @return USER 정보
     */
    public UserVO selectUser(String lgnId) {
        return loginMapper.selectUser(lgnId);
    }
 
}
