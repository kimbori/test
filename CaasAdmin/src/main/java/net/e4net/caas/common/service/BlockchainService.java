package net.e4net.caas.common.service;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.mapper.BlockchainMapper;
import net.e4net.caas.common.vo.BlockchainLinkInfoVO;
import net.e4net.caas.common.vo.BlockchainTokenTxVO;
import net.e4net.caas.common.vo.BlockchainWalletVO;

@Service
@Slf4j
public class BlockchainService {
	// 임시 luniverse PD 정보. ActionName은 고정시키기로 한다.
	//	ActionName은 Token 별로 이체(토큰전송)를 위한 smart contract 명이다.
	//	
	public String TOKEN_SYMBOL	= "CHY";
	public String TOKEN_ID		= "361";
	public String API_KEY 	= "1xWggjfSFJDvgn15G1w3jsnx6Dats2Mb5q9zffLpL6UYThiQx7UjTvYEXj4Z9hX1Cm";

	public static final String	BC_API_CREATE_WALLET = "/createWallet";
	public static final String	BC_API_WALLET_BALANCE = "/userBalance";
	public static final String	BC_API_TOKEN_TRANSFER = "/tokenTransfer";
	
	@Autowired BlockchainMapper mapper;
	@Value("${blockchain.base.url}") private String blockchainBaseUrl;


	/* ==================================================================================
	 * 블록체인 지갑 잔고 조회 API
	 */
	public long getWalletBalance(String oprGrpId, String walletAddr) {
		BlockchainLinkInfoVO bc = getBlockchainLinkInfo(oprGrpId);
		return apiGetWalletBalance(oprGrpId, walletAddr, bc);
	}

	private long apiGetWalletBalance(String oprGrpId, String walletAddr, BlockchainLinkInfoVO bc) {
		long balance = -1;
		Map<String, Object> param = new HashMap<>();
		param.put("userWalletAddress", walletAddr);
		param.put("productTokenSymbol", bc.getTokenSimbol());
		param.put("apikey", bc.getApiKey());

		Map<String,Object> result = restfulApiCallResult(BC_API_WALLET_BALANCE, param);
		boolean onSuccess = isApiResultSuccess(result);
		if(onSuccess) {
			Map<String,Object> data = getApiResultDataObject(result);
			balance = Long.valueOf(data.get("balance").toString());
		} else {
			log.error("[getWalletBalance] api fail. onSuccess=" + onSuccess);
		}

		return balance;
	}

	/* ==================================================================================
	 * 블록체인 토큰 전송 (이체) API
	 */
	/**
	 *	토큰이체 : 운영단체-토큰 => 기부자-토큰, 가맹단체-토큰, 캠페인-토큰
	 *		운영단체지갑은 파라미터에서 제외
	 * @param oprGrpId 운영단체ID
	 * @param toWltAddr 입금 지갑주소
	 * @param trnfTokn 이체 토큰 (금액 채리)
	 * @param trnfTp "21"(결제충전), "52"(Fiat정산취소)
	 * @param userid
	 * @return
	 */
	public BlockchainTokenTxVO tokenTransferB2C(String oprGrpId, String toWltAddr, long trnfTokn, String trnfTp, String userid) {
		return apiTokenTransfer(oprGrpId, "B2C", null, toWltAddr, trnfTokn, trnfTp, userid);
	}

	/**
	 *	토큰이체 : 운영단체-토큰 <= 기부자-토큰, 가맹단체-토큰, 캠페인-토큰
	 *		운영단체지갑은 파라미터에서 제외
	 * @param oprGrpId 운영단체ID
	 * @param frmWltAddr  출금 지갑주소
	 * @param trnfTokn 이체 토큰 (금액 채리)
	 * @param trnfTp "22"(결제취소), "51"(Fiat정산요청)
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainTokenTxVO tokenTransferC2B(String oprGrpId, String frmWltAddr, long trnfTokn, String trnfTp, String userid) {
		return apiTokenTransfer(oprGrpId, "C2B", frmWltAddr, null, trnfTokn, trnfTp, userid);
	}

	/**
	 *	토큰이체 : 기부자-토큰, 가맹단체-토큰, 캠페인-토큰 상호간
	 * @param oprGrpId 운영단체ID
	 * @param frmWltAddr 출금 지갑주소
	 * @param toWltAddr 입금 지갑주소
	 * @param trnfTokn 이체 토큰 (금액 채리)
	 * @param trnfTp "31"(기부전송), "32"(기부취소), "41"(정산요청), "42"(정산취소)
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainTokenTxVO tokenTransferC2C(String oprGrpId, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp, String userid) {
		return apiTokenTransfer(oprGrpId, "C2C", frmWltAddr, toWltAddr, trnfTokn, trnfTp, userid);
	}
	
	
	private BlockchainTokenTxVO apiTokenTransfer(String oprGrpId, String transferType, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp, String userid) {
		BlockchainLinkInfoVO bc = getBlockchainLinkInfo(oprGrpId);

		Map<String, Object> param = new HashMap<>();
		param.put("transferType", transferType);
		if("C2C".equals(transferType) || "C2B".equals(transferType)) {
			param.put("senderAddress", frmWltAddr);			
		}
		if("C2C".equals(transferType) || "B2C".equals(transferType)) {
			param.put("receiverAddress", toWltAddr);
		}

		param.put("valueAmount", trnfTokn);
		param.put("apikey", bc.getApiKey());

		Map<String,Object> result = restfulApiCallResult(BC_API_TOKEN_TRANSFER, param);
		boolean onSuccess = isApiResultSuccess(result);

		BlockchainTokenTxVO vo = new BlockchainTokenTxVO();
		// ExtLinkVO
		//		DB : lnkPrcDt, fstRegDttm, lstChDttm
		//		param : fstRegUsid, lstChUsid
		vo.setOprGrpId(oprGrpId);
		vo.setLnkAgency(bc.getBlockChainKd());
		vo.setLnkPrcRst(onSuccess ? "S" : "F");
		vo.setLnkReqInfo(convertvObjToJson(param));
		vo.setLnkRstInfo(convertvObjToJson(result));
		vo.setFstRegUsid(userid);
		vo.setLstChUsid(userid);

		vo.setTrnfTp(trnfTp);			// 전송타입
		vo.setFrmWltAddr(frmWltAddr);
		vo.setToWltAddr(toWltAddr);
		vo.setTrnfTokn(trnfTokn);

		if(onSuccess) {
			Map<String,Object> data = getApiResultDataObject(result);
			String txHash = data.get("txHash").toString();
			vo.setTxHash(txHash);
		} else {
			log.error("[apiTokenTransfer] api fail. onSuccess=" + onSuccess);
		}
		
		int ret = mapper.insertTokenSend(vo);
		if(ret <= 0) {
			log.error("[apiTokenTransfer] insertBlockchainWallet error. ret=" + ret + "\n");
		}

		if("C2C".equals(transferType) || "C2B".equals(transferType)) {
			long frmWltBalance = apiGetWalletBalance(oprGrpId, frmWltAddr, bc);
			updateWalletBalance(frmWltAddr, frmWltBalance, userid);
		}
		if("C2C".equals(transferType) || "B2C".equals(transferType)) {
			long toWltBalance = apiGetWalletBalance(oprGrpId, toWltAddr, bc);
			updateWalletBalance(toWltAddr, toWltBalance, userid);
		}
		
		return vo;
	}
	
	private boolean updateWalletBalance(String wltAddr, long balance, String userid) {
		BlockchainWalletVO vo = new BlockchainWalletVO();
		vo.setWltAddr(wltAddr);
		vo.setWltLv(balance);
		vo.setLstChUsid(userid);
		int ret = mapper.updateWalletBalance(vo);
		return ret > 0;
	}

	/* ==================================================================================
	 * 블록체인 지갑생성 API - 운영단체만 제외하고 지갑이 생성된다.
	 */

	// 운엳단체/가맹단체/캠페인/ 지갑생성
	//	운영단체의 경우는 B2C, C2B 이여서 지갑이 미리 생성된다. 운영자에서 등록한다.
//	public BlockchainWalletVO apiNewWalletGrpOpr(String oprGrpId) {
//		return apiNewWallet(oprGrpId, "01", oprGrpId);
//	}
	/**
	 * 가맹단체 지갑 만들기
	 * @param oprGrpId 운영단체ID
	 * @param mrchGrpId 가맹단체ID
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainWalletVO createWalletGrpMct(String oprGrpId, String mrchGrpId, String userid) {
		return createWalletSub(oprGrpId, "02", mrchGrpId, userid);
	}

	/**
	 * 캠페인 지갑 만들기
	 * @param oprGrpId 운영단체ID
	 * @param mrchGrpId 가맹단체ID
	 * @param cmpSq 캠페인SQ
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainWalletVO createWalletCmpInf(String oprGrpId, String mrchGrpId, long cmpSq, String userid) {
		String emailGubun = mrchGrpId + "_" + String.valueOf(cmpSq);
		return createWalletSub(oprGrpId, "03", emailGubun, userid);
	}

	/**
	 * 기부자 지갑 생성
	 * @param oprGrpId 운영단체ID
	 * @param 기부자SQ
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainWalletVO createWalletDntPrsn(String oprGrpId, long cnrSq, String userid) {
		return createWalletSub(oprGrpId, "04", String.valueOf(cnrSq), userid);
	}

	private BlockchainWalletVO createWalletSub(String oprGrpId, String wltKn, String emailGubun, String userid) {
		//String email = oprGrpId + "-" + wltKn + "_" + CommonUtils.getNewUUID() + "@cherry.e4net.net";
		String email = oprGrpId + "_" + wltKn + "_" + emailGubun + "@cherry.e4net.net";
		return apiCreateWallet(oprGrpId, wltKn, email, userid);
	}

	/*
	 * (TODO) 이미 만들어진 경우는 지갑주소를 찾아 update를 시켜주는 프로세스가 필요할 수도.
	 */
	public BlockchainWalletVO apiCreateWallet(String oprGrpId, String wltKn, String email, String userid) {
		BlockchainLinkInfoVO bc = getBlockchainLinkInfo(oprGrpId);

		Map<String, Object> param = new HashMap<>();
		param.put("userEmail", email);
		param.put("apikey", bc.getApiKey());
		
		Map<String,Object> result = restfulApiCallResult(BC_API_CREATE_WALLET, param);
		boolean onSuccess = isApiResultSuccess(result);

		BlockchainWalletVO vo = new BlockchainWalletVO();
		// ExtLinkVO
		//		DB : lnkPrcDt, fstRegDttm, lstChDttm
		//		param : fstRegUsid, lstChUsid
		vo.setOprGrpId(oprGrpId);
		vo.setLnkAgency(bc.getBlockChainKd());
		vo.setLnkPrcRst(onSuccess ? "S" : "F");
		vo.setLnkReqInfo(convertvObjToJson(param));
		vo.setLnkRstInfo(convertvObjToJson(result));
		vo.setFstRegUsid(userid);
		vo.setLstChUsid(userid);

		vo.setWltKn(wltKn);
		vo.setMailAddr(email);
		if(onSuccess) {
			Map<String,Object> data = getApiResultDataObject(result);
			vo.setWltAddr(data.get("address").toString());
			vo.setWltLv(0);
		} else {
			log.error("[apiNewWallet] api fail. onSuccess=" + onSuccess);
		}

		int ret = mapper.insertBlockchainWallet(vo);
		if(ret <= 0) {
			log.error("[apiNewWallet] insertBlockchainWallet error. ret=" + ret + "\n");
		}

		return vo;
	}

	/*
	 * 블록체인 별 공통
	 */

	/**
	 *  표준 RESTful API 호출 처리
	 * @param url 접속URL
	 * @param param 전송 파라미터
	 * @return 결과 파라미터 (JSON 문자열)
	 */
	public String restfulApiCallString(String url, Map<String, Object> param) {
		RestTemplate restTemplate = new RestTemplate();

		String parametrizedArgs = param.keySet().stream().map(k ->
			String.format("%s={%s}", k, k)
		).collect(Collectors.joining("&"));

		String callUrl = blockchainBaseUrl + url + "?" + parametrizedArgs;
		String result = restTemplate.getForObject(callUrl, String.class, param);
		log.debug("[restfulApiCall] url=" + url + ",\n parametrizedArgs=" + parametrizedArgs
				+ ",\n param=" + convertvObjToJson(param) + ",\n result=" + result);
		return result;
	}
	
	public Map<String, Object> restfulApiCallResult(String url, Map<String, Object> param) {
		String result = this.restfulApiCallString(url, param);
		Map<String,Object> map = convertJsonToMap(result);
		return map;
	}

	public boolean isApiResultSuccess(Map<String, Object> result) {
		if(result == null) return false;
		boolean success = (boolean)result.get("result");
		return success;
	}
	
	public Map<String, Object> getApiResultDataObject(Map<String, Object> result) {
		@SuppressWarnings("unchecked")
		Map<String,Object> data = (Map<String, Object>)result.get("data");
		return data;
	}

	public List<Map<String,Object>> getApiResultDataList(Map<String, Object> result) {
		@SuppressWarnings("unchecked")
		List<Map<String,Object>> data = (List<Map<String, Object>>)result.get("data");
		return data;
	}
	
	public String convertvObjToJson(Object obj) {
		ObjectMapper mapper = new ObjectMapper();
		String json;
		try {
			json = mapper.writeValueAsString(obj);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			json = "error";
		}
		return json;
	}
	
	public Map<String,Object> convertJsonToMap(String json) {
		ObjectReader reader = new ObjectMapper().readerFor(Map.class);
		Map<String, Object> map = null;
		try {
			map = reader.readValue(json);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

	// (TODO) 일단 상수 처리 하지만 차후 DB에서 읽어 온다.
	public BlockchainLinkInfoVO getBlockchainLinkInfo(String oprGrpId) {
		//BlockchainLinkInfoVO infoVo = mapper.getBlockchainLinkInfo(oprGrpId);
		BlockchainLinkInfoVO infoVo = new BlockchainLinkInfoVO();
		infoVo.setBlockChainKd("01");		// "01" : Luniverse
		infoVo.setApiKey(API_KEY);
		infoVo.setTokenId(TOKEN_ID);
		infoVo.setTokenSimbol(TOKEN_SYMBOL);
		return infoVo;
	}
}