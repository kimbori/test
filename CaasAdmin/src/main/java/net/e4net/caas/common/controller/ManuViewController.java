package net.e4net.caas.common.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.plt.vo.AdminAdPVO;

/**
 * 메뉴조회 컨트롤러
 *
 * @author e4net
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
@SuppressWarnings("serial")
public class ManuViewController extends AbstractController {
    
    /**
     * <pre>
     * method 관리자관리화면 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return ModelAndView  JSON으로 리턴되는 조회결과
     */
    @RequestMapping(value = "/admin/cmm/menuAjax", method = RequestMethod.POST)
    public ModelAndView menuAjax(@ModelAttribute AdminAdPVO pvo) throws Exception {
        
        ModelAndView mav = new ModelAndView("admin/cmm/menuAjax :: default");
        
        UserVO userEtt = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	
    	mav.addObject("grpGb", userEtt.getGrpGb());

    	return getOkModelAndView(mav);
    }
}
