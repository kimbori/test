package net.e4net.caas.common.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;

/**
 * LogObject
 *   상속하여 로그를 남기는데 사용함
 * @author djim
 * @version 1.0.0
 */
public class LogObject implements Serializable {

	private static final long serialVersionUID = -6405185726106282890L;

	private transient Logger logger;

	protected final String loggerName;

	/**
	 * LogObject 생성자
	 */
	protected LogObject() {
		this.loggerName = getClass().getName();
	}

	protected LogObject(String loggerName) {
		this.loggerName = loggerName;
	}

	@JsonIgnore
	public boolean isTraceEnabled() {
		initLogger();
		return logger.isTraceEnabled();
	}

	public void trace(String message) {
		if( isTraceEnabled() ) logger.trace(message);
	}

	public void trace(String message, Object... objects) {
		if( isTraceEnabled() ) logger.trace(message, objects);
	}

	public void trace(String message, Throwable t) {
		if( isTraceEnabled() ) logger.trace(message, t);
	}

	@JsonIgnore
	public boolean isDebugEnabled() {
		initLogger();
		return logger.isDebugEnabled();
	}

	public void debug(String message) {
		if( isDebugEnabled() ) logger.debug(message);
	}

	public void debug(String message, Object... objects) {
		if( isDebugEnabled() ) logger.debug(message, objects);
	}

	public void debug(String message, Throwable t) {
		if( isDebugEnabled() ) logger.debug(message, t);
	}

	@JsonIgnore
	public boolean isInfoEnabled() {
		initLogger();
		return logger.isInfoEnabled();
	}

	public void info(String message) {
		if( isInfoEnabled() ) logger.info(message);
	}

	public void info(String message, Object... objects) {
		if( isInfoEnabled() ) logger.info(message, objects);
	}

	public void info(String message, Throwable t) {
		if( isInfoEnabled() ) logger.info(message, t);
	}

	@JsonIgnore
	public boolean isWarnEnabled() {
		initLogger();
		return logger.isWarnEnabled();
	}

	public void warn(String message) {
		if( isWarnEnabled() ) logger.warn(message);
	}

	public void warn(String message, Object... objects) {
		if( isWarnEnabled() ) logger.warn(message, objects);
	}

	public void warn(String message, Throwable t) {
		if( isWarnEnabled() ) logger.warn(message, t);
	}

	@JsonIgnore
	public boolean isErrorEnabled() {
		initLogger();
		return logger.isErrorEnabled();
	}

	public void error(String message) {
		if( isErrorEnabled() ) logger.error(message);
	}

	public void error(String message, Object... objects) {
		if( isErrorEnabled() ) logger.error(message, objects);
	}

	public void error(String message, Throwable t) {
		if( isErrorEnabled() ) logger.error(message, t);
	}

	private void initLogger() {
		if (logger == null) {
			logger = LoggerFactory.getLogger(loggerName);
		}
	}

}
