package net.e4net.caas.common;

import java.util.ArrayList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import groovy.util.logging.Slf4j;
import net.e4net.caas.common.service.LoginService;
import net.e4net.caas.common.vo.UserVO;

@Configuration
@Slf4j
public class AuthProvider implements AuthenticationProvider {
	
	private static final Logger logger = LoggerFactory.getLogger(AuthProvider.class);
	
    @Autowired
    LoginService loginService;
 
    @Override
    public Authentication authenticate(Authentication authentication) 
            throws AuthenticationException {
        String id = authentication.getName();
        String password = (String) authentication.getCredentials();
 
        UserVO userVO = loginService.selectUser(id);
        
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        if (null == userVO || !passwordEncoder.matches(password, userVO.getPasswd() )) {
        	logger.debug(userVO.getPasswd().equals(password)+"");
        	throw new BadCredentialsException("Login Error !!");
        } 
        logger.debug(userVO.toString());
        userVO.setLoginYn(true);
        //super admin
        if ( (userVO.getOprGrpId() == null || "".equals(userVO.getOprGrpId()))
        		&& (userVO.getMrchGrpId() == null || "".equals(userVO.getMrchGrpId()))) {
        	userVO.setAdminRole("0");
        } else {
        	//운영단체 관리자
        	if (userVO.getOprGrpId() == null || "".equals(userVO.getOprGrpId() )) {
        		userVO.setAdminRole("1");
        	} 
        	//가맹단체 관리자
        	else if (userVO.getMrchGrpId() == null || "".equals(userVO.getMrchGrpId())) {
        		userVO.setAdminRole("2");
        	}
        }
        
        
        userVO.setPasswd(null);
        ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();
        
        authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
       
        return new UsernamePasswordAuthenticationToken(userVO, null, authorities);
    }
 
    @Override
    public boolean supports(Class authentication) {
        return authentication.equals(UsernamePasswordAuthenticationToken.class);
    }
}