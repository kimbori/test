package net.e4net.caas.common;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import net.e4net.caas.common.controller.AbstractController;

@Controller
public class IndexController extends AbstractController {
	
	private static final long serialVersionUID = -2720771006257348677L;

	@RequestMapping("/moveLoginPag")       
	public String loginPag(Model model){            
		return "login";         
	}
	
	@RequestMapping("/moveIndexPag")       
	public String index(Model model){              
		return "index";         
	} 
}

