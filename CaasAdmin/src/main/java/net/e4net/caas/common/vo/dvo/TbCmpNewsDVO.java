package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbCmpNewsDVO extends PageVO{

	private static final long serialVersionUID = -7737881506646882763L;
	
	private long cmpNewsSq;		//캠페인소식sq
	private long cmpSq;			//캠페인sq
	private String regDttm;		//등록일시
	private String img;			//이미지
	private String cn;			//내용
	private String title;		//제목
	private String fstRegDttm;	//최초등록일시
	private String fstRegUsid;	//최초등록자
	private String lstChDttm;	//최종변경일시
	private String lstChUsid;	//최종변경자

}
