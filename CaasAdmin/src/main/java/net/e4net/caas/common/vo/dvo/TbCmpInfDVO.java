package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbCmpInfDVO extends PageVO{

	private static final long serialVersionUID = 6260338757434909162L;

	private long cmpSq;				//캠페인SQ
	private String mrchGrpId;		//가맹단체ID
	private String cmpNm;			//캠페인명
	private String cmpImg;			//캠페인이미지
	private String cmpCn;			//캠페인내용
	private String cmpNews;			//캠페인소식
	private String mailAddr;		//메일주소
	private String wltAddr;			//지갑주소
	private String dnmrStat;		//모금상태
	private String showYn;			//SHOW여부
	private String dnmrTp;			//모금유형
	private String dnmrKn;			//모금종류
	private String dnmrStatDt;		//모금시작일
	private String dnmrEndDt;		//모금종료일
	private int ptcpCnt;			//참여자수
	private long trgtTokn;			//목표토큰
	private long dnmrTokn;			//모금토큰
	private String fstRegDttm;		//최초등록일시
	private String fstRegUsid;		//최초등록자
	private String lstChDttm;		//최종변경일시
	private String lstChUsid;		//최종변경자


}
