package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbCmpFsprDVO extends PageVO{

	private static final long serialVersionUID = -7737881506646882763L;
	
	private long fxprDntnSq;		//정기기부sq
	private long cmpSq;				//캠페인sq
	private long cnrSq;				//기부자sq
	private long bllAuthSq;			//빌링인증sq
	private String statYm;			//개시연월
	private String endYm;			//종료연월
	private String otaDt;			//출금일
	private long otaAmt;			//출금금액
	private String regDttm;			//등록일시
	private String fstRegDttm;		//최초등록일시
	private String fstRegUsid;		//최초등록자
	private String lstChDttm;		//최종변경일시
	private String lstChUsid;		//최종변경자

}
