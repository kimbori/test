package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbDntLstDVO extends PageVO{
	
	private static final long serialVersionUID = 1842790950784922570L;

	private long dntnItmSq;			//기부내역SQ
	private long cmpSq;				//캠페인SQ
	private long cnrSq;				//기부자SQ
	private String dntnDttm;		//기부일시
	private String dntnKn;			//기부종류
	private long dntnAmt;			//기부금액
	private long toknTrnfSq;		//토큰전송SQ
	private String nthnDntnYn;		//무기명기부여부
	private String recpIssuYn;		//영수증발급여부
	private String inhgvtaxNtfyYn;	//상증세신고여부
	private long dntnRecpSq;		//기부영수증SQ
	private String cmmt;			//응원글
	private String fstRegDttm;		//최초등록일시
	private String fstRegUsid;		//등록자ID
	private String lstChDttm;		//최종변경일시
	private String lstChUsid;		//변경자ID

}
