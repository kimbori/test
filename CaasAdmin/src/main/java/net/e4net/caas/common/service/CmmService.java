package net.e4net.caas.common.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.mapper.CmmCodeMapper;
import net.e4net.caas.common.vo.CmmnDetailCode;

@Service
public class CmmService {

	@Autowired CmmCodeMapper cmmCodeMapper;
		
	public List<CmmnDetailCode> selectCmmCodeDetail(String codeId) {
		return cmmCodeMapper.selectCmmCodeDetail(codeId);
	}
}
