package net.e4net.caas.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class PageVO extends AbstractVO{

    private String rowNo;
    private String pageNo = "1";
    private int countPage = 20;
    private int startCount;
    private int totalCnt = 0;
    private String orderByColumn;
    private String ascDesc;

    public int getStartCount() {
        startCount = (Integer.parseInt(pageNo) - 1) * countPage;
        return startCount;
    }

}
