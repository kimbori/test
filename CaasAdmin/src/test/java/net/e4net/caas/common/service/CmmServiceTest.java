package net.e4net.caas.common.service;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.CaasApplicationTests;
import net.e4net.caas.common.vo.CmmnDetailCode;

@Slf4j
public class CmmServiceTest extends CaasApplicationTests {

	@Autowired CmmService service;
	
	@Test
	public void selectCmmCodeDetailTet() {
    	log.debug("test2");
		List<CmmnDetailCode> result = service.selectCmmCodeDetail("test");
		assertTrue(result.size() > 0);
	}

}
