
-- 공통코드 구분

INSERT INTO public.tb_cmm_codeid(
	code_id, code_id_nm, code_id_dc, fst_reg_dttm, fst_reg_usid, lst_ch_dttm, lst_ch_usid)
	VALUES ('test', '테스트코드', '테스트코드', now(), 'system', now(), 'system');

INSERT INTO public.tb_cmm_codedtl(
	code_id, code, code_nm, code_dc, fst_reg_dttm, fst_reg_usid, lst_ch_dttm, lst_ch_usid)
	VALUES ('test', '01', '테스트01', '테스트01', now(), 'system', now(), 'system');
INSERT INTO public.tb_cmm_codedtl(
	code_id, code, code_nm, code_dc, fst_reg_dttm, fst_reg_usid, lst_ch_dttm, lst_ch_usid)
	VALUES ('test', '02', '테스트02', '테스트02', now(), 'system', now(), 'system');