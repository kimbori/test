package org.rex.e4net.cherryandroid.Activities;

import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.Toast;

import org.rex.e4net.cherryandroid.R;


public abstract class BaseActivity extends AppCompatActivity {
    ImageButton adminBtn;

    protected void onCreate(Bundle savedInstanceState, int layoutId) {
//        setTheme(R.style.NoActionBar);
//
//        final Handler handler = new Handler();
//        handler.postDelayed(new Runnable() {
//            @Override
//            public void run() {
//
//            }
//        }, 5000);

        super.onCreate(savedInstanceState);
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        setContentView(layoutId);

        //profileBtn = (ImageButton) findViewById(R.id.profileBtn);
       // alertsBtn = (ImageButton) findViewById(R.id.alertsBtn);

        adminBtn = (ImageButton) findViewById(R.id.adminBtn);



        adminBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(getApplicationContext(), "adminBtn!" , Toast.LENGTH_SHORT).show();
            }
        });


        Toolbar myToolbar = (Toolbar) findViewById(R.id.ToolBarID);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        assert myToolbar != null;
        //myToolbar.setLogo(R.drawable.cherry_logo_sm_01);

//        LinearLayout rexlinLayout = new LinearLayout(this);
//        rexlinLayout.setOrientation(LinearLayout.HORIZONTAL);
//        rexlinLayout.setGravity(Gravity.RIGHT);
//
//        ImageButton ib1 = new ImageButton(this);
//        ib1.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.alerts));
//        ImageButton ib2 = new ImageButton(this);
//        ib2.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.profile));
//        ImageButton ib3 = new ImageButton(this);
//        ib3.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.search));
//
//        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(
//                LinearLayout.LayoutParams.WRAP_CONTENT,
//                LinearLayout.LayoutParams.WRAP_CONTENT, 1
//        );
//
//
//        rexlinLayout.setLayoutParams(layoutParams);
//
//        ib1.setScaleType(ScaleType.CENTER);
//        ib2.setScaleType(ScaleType.CENTER);
//        ib3.setScaleType(ScaleType.CENTER);
//        ib1.setBackgroundColor(Color.TRANSPARENT);
//        ib2.setBackgroundColor(Color.TRANSPARENT);
//        ib3.setBackgroundColor(Color.TRANSPARENT);
//        ib1.setLayoutParams(layoutParams);
//        ib2.setLayoutParams(layoutParams);
//        ib3.setLayoutParams(layoutParams);
//        ib1.setForegroundGravity(Gravity.RIGHT);
//        ib2.setForegroundGravity(Gravity.RIGHT);
//        ib3.setForegroundGravity(Gravity.RIGHT);
//        rexlinLayout.addView(ib3);
//        rexlinLayout.addView(ib2);
//        rexlinLayout.addView(ib1);
//
//
//        myToolbar.addView(rexlinLayout);



        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(getApplicationContext(), R.color.colorPrimary)));
        //myToolbar.setNavigationIcon();
    }

//    public void setBtnVisible(){
//        searchBtn.setVisibility(View.VISIBLE);
////        alertsBtn.setVisibility(View.VISIBLE);
////        profileBtn.setVisibility(View.VISIBLE);
//    }
//
//    public void setBtnInvisible(){
//        searchBtn.setVisibility(View.INVISIBLE);
////        alertsBtn.setVisibility(View.INVISIBLE);
////        profileBtn.setVisibility(View.INVISIBLE);
//    }
}
