package org.rex.e4net.cherryandroid.Activities;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.rex.e4net.cherryandroid.R;
import org.rex.e4net.cherryandroid.Utils.ShareUtils;

public class NotificationCheckActivity extends AppCompatActivity {
    public static final String TAG = "MsgFirebaseServ";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification_check);

        final TextView tokenTV = (TextView) findViewById(R.id.fbTokenTV);

        Button subscribeButton = (Button) findViewById(R.id.checkFBTokenBtn);
        subscribeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Get token
                FirebaseInstanceId.getInstance().getInstanceId()
                        .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                            @Override
                            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                                if (!task.isSuccessful()) {
                                    Log.w(TAG, "getInstanceId failed", task.getException());
                                    return;
                                }
                                // Get new Instance ID token
                                String token = task.getResult().getToken();

                                // Log and toast
                                Log.d(TAG, token);
                                Toast.makeText(NotificationCheckActivity.this, token, Toast.LENGTH_SHORT).show();
                                tokenTV.setText(token);
                            }
                        });
            }
        });

        Button sendKakao = (Button) findViewById(R.id.sendFBTokenKakaoBtn);
        sendKakao.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ShareUtils.shareKakao(NotificationCheckActivity.this, tokenTV.getText().toString(), "" );
            }
        });
    }
}
