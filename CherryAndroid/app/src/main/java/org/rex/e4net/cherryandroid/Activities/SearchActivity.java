package org.rex.e4net.cherryandroid.Activities;

import android.content.Intent;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import org.rex.e4net.cherryandroid.Interfaces.JSInterface;
import org.rex.e4net.cherryandroid.R;
import org.rex.e4net.cherryandroid.WebViewClient.MyWebChromeClient;
import org.rex.e4net.cherryandroid.WebViewClient.MyWebViewClient;

public class SearchActivity extends AppCompatActivity {

    private ImageButton searchBtn;
    private ImageButton cancelBtn;
    private EditText searchBar;
    private WebView wv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        Intent intent = getIntent();
        String url = intent.getStringExtra("url");

        searchBtn = (ImageButton) findViewById(R.id.activeSearchBtn);
        cancelBtn = (ImageButton) findViewById(R.id.serachCancelBtn);
        searchBar = (EditText) findViewById(R.id.searchET);
        wv = (WebView) findViewById(R.id.searchWebView);

        //webview setup
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        wv.setWebChromeClient(new MyWebChromeClient(this));
        wv.setWebViewClient(new MyWebViewClient(this));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            wv.setWebContentsDebuggingEnabled(true);
        }
        wv.addJavascriptInterface(new JSInterface(this), "Android");

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            wv.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        final RotateAnimation anim = new RotateAnimation(0.0f, 360.0f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        anim.setInterpolator(new DecelerateInterpolator());
        anim.setRepeatCount(Animation.ABSOLUTE);
        anim.setDuration(400);

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(wv.canGoBack()){
                    wv.goBack();
                } else {
                    cancelBtn.startAnimation(anim);
                    finish();
                }
            }
        });

        anim.setInterpolator(new LinearInterpolator());
        cancelBtn.startAnimation(anim);

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), searchBar.getText().toString() + " 검색합니다", Toast.LENGTH_LONG).show();
            }
        });
        searchBar.requestFocus();
        wv.loadUrl(url);
    }
}
