package org.rex.e4net.cherryandroid.Activities;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.rex.e4net.cherryandroid.Constants.Constants;
import org.rex.e4net.cherryandroid.Interfaces.JSInterface;
import org.rex.e4net.cherryandroid.R;
import org.rex.e4net.cherryandroid.Utils.PictureUtils;
import org.rex.e4net.cherryandroid.WebViewClient.MyWebChromeClient;
import org.rex.e4net.cherryandroid.WebViewClient.MyWebViewClient;

import java.io.File;

public class MyActivity extends AppCompatActivity {

    LinearLayout leftBtnContainer;
    LinearLayout rightBtnContainer;
    LinearLayout homeBtnContainer;
    LinearLayout mypageBtnContainer;
    LinearLayout alertsBtnContainer;
    WebView wv;

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor sharedEditor;
    int resumeCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        resumeCounter = 0;

        sharedPref = this.getSharedPreferences("rex_pref", Context.MODE_PRIVATE);
        sharedEditor = sharedPref.edit();

        ImageButton backBtn = findViewById(R.id.backFromTitleBtn);
        TextView titleTV = (TextView) findViewById(R.id.titleTitleTv);

        leftBtnContainer = (LinearLayout) findViewById(R.id.leftBtnContainer);
        rightBtnContainer = (LinearLayout) findViewById(R.id.rightBtnContainer);
        homeBtnContainer = (LinearLayout) findViewById(R.id.homeBtnContainer);
        mypageBtnContainer = (LinearLayout) findViewById(R.id.mypageBtnContainer);
        alertsBtnContainer = (LinearLayout) findViewById(R.id.alertsBtnContainer);

        Intent intent = getIntent();

        final String title = intent.getStringExtra("title");
        final String url = intent.getStringExtra("url");

        wv = (WebView) findViewById(R.id.WebView2ID);
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        wv.setWebChromeClient(new MyWebChromeClient(this));
        wv.setWebViewClient(new MyWebViewClient(this));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            wv.setWebContentsDebuggingEnabled(true);
        }
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
       webSettings.setSupportMultipleWindows(true);
        wv.addJavascriptInterface(new JSInterface(this), "Android");
        //wv.setRendererPriorityPolicy(WebView.RENDERER_PRIORITY_BOUND,false);
        wv.getSettings().setDomStorageEnabled(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            wv.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }

        wv.loadUrl(url);

        rightBtnContainer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                wv.goForward();
            }
        });
        leftBtnContainer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                goBack(wv);
            }
        });
        homeBtnContainer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        mypageBtnContainer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(!title.equals("마이페이지")){
                    wv.loadUrl("javascript:caasFromApp.buttonEvent('" + "MYPAGE','" + sharedPref.getString(getResources().getString(R.string.SP_admin_url), "https://cherry.e4net.net") +"')");
                    finish();
                }
            }
        });
        alertsBtnContainer.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                if(!title.equals("알림")) {
                    if(sharedPref.getBoolean(getResources().getString(R.string.SP_dev_mode), false)){
                        wv.loadUrl("javascript:caasFromApp.buttonEvent('" + "ALERTS','" + sharedPref.getString(getResources().getString(R.string.SP_admin_url), "https://cherry.e4net.net") + "')");
                    } else {
                        Toast.makeText(getApplicationContext(), "개발중입니다" , Toast.LENGTH_SHORT).show();
                    }
                    //finish();
                }
            }
        });
        titleTV.setText(title);
        //titleTV.setText("");
        backBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //goBack(wv);
                finish();
            }
        });
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
                goBack(wv);
                return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("REXYREX","ON RESUME");
        if(sharedPref.getBoolean(getResources().getString(R.string.SP_reload_on_resume), true)){
            Log.i("REXYREX","ON RESUME REFRESH");
            wv.reload();
        } else {
            resumeCounter++;
            //Counter를 체크하는 이유는 프로필 사진 설정하고 돌아오는 화면이 refresh되면 안돼서 적용시킨거임
            //getPic2() 호출시 onResume이 2번 콜되기 때문에 카운터를 두고 잡아야한다.
            //그외 카카오페이같은경우 onResume이 불렸을때 refresh 돼야 로딩 애니메이션이 사라진다
            if(resumeCounter >= 2){
                sharedEditor.putBoolean(getResources().getString(R.string.SP_reload_on_resume), true);
                sharedEditor.apply();
                resumeCounter = 0;
            }
        }
        //wv.reload();
    }

    private void goBack(WebView wv){
        if(wv.canGoBack()){
            wv.goBack();
        } else {
            finish();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String base64pic = data.getStringExtra("base64pic");
                Toast.makeText(this, base64pic, Toast.LENGTH_LONG);
                Log.d("mainmsg", "mainactivity says base 64 received!");
                Log.d("mainmsg", "mainactivity says base 64 is " + base64pic);
                //JSInterface jsInt = new JSInterface(this);
                //jsInt.setBase64Pic(base64pic);

                //wv.loadUrl("javascript:caasFromApp.setPic('" + base64pic +"')");
                Log.d("mainmsg", "pic upload finished");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.d("mainmsg", "critical photo error");
            }
        }

        if (requestCode == Constants.GALLERY_ACTIVITY_CODE) {
            if(resultCode == Activity.RESULT_OK){
                String picturePath = data.getStringExtra("picturePath");
                //perform Crop on the Image Selected from Gallery
                performCrop(picturePath);
            }
        }

        if (requestCode == Constants.RESULT_CROP ) {
            if(resultCode == Activity.RESULT_OK){
                Bundle extras = data.getExtras();
                Bitmap selectedBitmap = extras.getParcelable("data");

                Log.i("IMAGE_CROP", "IMAGE SIZE : " + selectedBitmap.getHeight());

                String encoded = PictureUtils.picTo64(selectedBitmap);
                String callbackFunc = sharedPref.getString(getResources().getString(R.string.SP_gallery_callback_func), "null");

                Log.i("javascript","REXYREX callback func is " + callbackFunc);
                wv.loadUrl("javascript:" + callbackFunc + "('" + encoded +"')");
                //Toast.makeText(this,"crop complete", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void performCrop(String picUri) {
        try {
            //Start Crop Activity
            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 1500);
            cropIntent.putExtra("outputY", 1500);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, Constants.RESULT_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }



}
