package org.rex.e4net.cherryandroid.Interfaces;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.rex.e4net.cherryandroid.Activities.MainActivity;
import org.rex.e4net.cherryandroid.Activities.MyActivity;
import org.rex.e4net.cherryandroid.Activities.SearchActivity;
import org.rex.e4net.cherryandroid.Activities.PictureSaveActivity;
import org.rex.e4net.cherryandroid.Activities.SecureKeyboardActivity;
import org.rex.e4net.cherryandroid.Constants.Constants;
import org.rex.e4net.cherryandroid.R;
import org.rex.e4net.cherryandroid.Utils.DeviceInfoUtils;
import org.rex.e4net.cherryandroid.Utils.ShareUtils;
import org.rex.e4net.cherryandroid.Utils.GalleryUtils;

public class JSInterface {
    Context mContext;
    SharedPreferences sharedPref;
    SharedPreferences.Editor sharedEditor;

    public JSInterface(Context c) {
        mContext = c;
        Activity ma = (Activity) mContext;

        sharedPref = ma.getSharedPreferences("rex_pref", Context.MODE_PRIVATE);
        sharedEditor = sharedPref.edit();
    }

//    @JavascriptInterface
//    public void loadMain(String url) {
//        MainActivity ma = (MainActivity) mContext;
//
//    }

    //cannot send like this because javascript interface only supports primitive datatypes
    @JavascriptInterface
    public JSONObject showToast2(String toast) {

        JSONObject postData = new JSONObject();

        try {
            postData.accumulate("cid", "111111");
            postData.accumulate("did", "222222");
            return postData;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postData;
    }

    @JavascriptInterface
    public String sendString() {
        Activity ma = (Activity) mContext;
        return sharedPref.getString(ma.getResources().getString(R.string.SP_admin_url), "http://192.168.");
    }

    @JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
        //MainActivity ma = (MainActivity) mContext;
    }

    @JavascriptInterface
    public void shareFacebook(String content, String url) {
        Activity ma = (Activity) mContext;
        ShareUtils.shareFacebook(ma,content, url);
    }

    @JavascriptInterface
    public void shareKakaoTalk(String content, String url) {
        Activity ma = (Activity) mContext;
        ShareUtils.shareKakao(ma,content,url);
    }

    @JavascriptInterface
    public void shareSMS(String content) {
        Activity ma = (Activity) mContext;
        ShareUtils.shareSMS(ma,content);
    }

    @JavascriptInterface
    public void getPic() {
        Activity ma = (Activity) mContext;
        Intent i = new Intent(ma, PictureSaveActivity.class);
        ma.startActivityForResult(i, 1);
    }

    @JavascriptInterface
    public void getPic2(String callbackFunc) {
        Activity ma = (Activity) mContext;

        sharedEditor.putString(ma.getResources().getString(R.string.SP_gallery_callback_func),callbackFunc);
        sharedEditor.putBoolean(ma.getResources().getString(R.string.SP_reload_on_resume), false);
        sharedEditor.apply();

        Intent i = new Intent(ma, GalleryUtils.class);
        ma.startActivityForResult(i, Constants.GALLERY_ACTIVITY_CODE);
    }

    @JavascriptInterface
    public void startPasswordSet() {
        Activity ma = (Activity) mContext;
        Intent i = new Intent(ma, SecureKeyboardActivity.class);
        i.putExtra("mode", 1);
        ma.startActivity(i);
        ma.finish();
    }

    @JavascriptInterface
    public void startPasswordCheck() {
        Activity ma = (Activity) mContext;
        Intent i = new Intent(ma, SecureKeyboardActivity.class);
        i.putExtra("mode", 3);
        ma.startActivity(i);
        ma.finish();
    }

    @JavascriptInterface
    public String getOpGroupID() {
        Activity ma = (Activity) mContext;
        return ma.getResources().getString(R.string.operating_group);
    }

    @JavascriptInterface
    public String getCID() {

        Activity ma = (Activity) mContext;
        return sharedPref.getString(ma.getResources().getString(R.string.SP_CID), "null");
    }

    @JavascriptInterface
    public String getFirebaseToken() {
        Activity ma = (Activity) mContext;
        return sharedPref.getString(ma.getResources().getString(R.string.SP_fb_token), "null");
    }


    @JavascriptInterface
    public void saveCIDToPhone(String cid) {
        Activity ma = (Activity) mContext;
        Log.i("REXYREX", "REXYREX CID IS : " + cid );
        Log.i("REXYREX", "REXYREX CID SAVING TO : " + ma.getResources().getString(R.string.SP_CID) );
        sharedEditor.putString(ma.getResources().getString(R.string.SP_CID),cid);
        sharedEditor.apply();
        Log.i("REXYREX", "REXYREX CID AFTER SAVE : " + sharedPref.getString(ma.getResources().getString(R.string.SP_CID), "null"));

    }

    @JavascriptInterface
    public void saveDIDToPhone(String did) {
        Activity ma = (Activity) mContext;
        sharedEditor.putString(ma.getResources().getString(R.string.SP_DID),did);
        sharedEditor.apply();
    }



    @JavascriptInterface
    public void goToPage(String url, String title) {
        Activity ma = (Activity) mContext;
        Intent i = new Intent(ma, MyActivity.class);
        i.putExtra("title", title);
        i.putExtra("url", url);
        ma.startActivity(i);
    }

    @JavascriptInterface
    public void openSearch(String url) {
        Activity ma = (Activity) mContext;
        Intent i = new Intent(ma, SearchActivity.class);
        i.putExtra("url", url);
        ma.startActivity(i);
    }

    @JavascriptInterface
    public boolean getNotificationStatus(){
        Activity ma = (Activity) mContext;
        return sharedPref.getBoolean(ma.getResources().getString(R.string.SP_notification_status), false);
        //return true;
    }

    @JavascriptInterface
    public boolean getAppLockStatus(){
        Activity ma = (Activity) mContext;
        if(sharedPref.getBoolean(ma.getResources().getString(R.string.SP_app_lock_status), true)){
            Log.i("sharedPref", "REXYREX app lock status true");
        }

        return sharedPref.getBoolean(ma.getResources().getString(R.string.SP_app_lock_status), true);
        //return true;
    }

    @JavascriptInterface
    public void setNotificationStatus(boolean notiStatus){
        Activity ma = (Activity) mContext;
        DeviceInfoUtils.toggleNotificationSetting(notiStatus, mContext);
    }

    @JavascriptInterface
    public void setAppLockStatus(boolean lockStatus){
        Activity ma = (Activity) mContext;
        DeviceInfoUtils.toggleAppLockSetting(lockStatus, mContext);
    }

    @JavascriptInterface
    public void shareAppLink(String msg){
        Activity ma = (Activity) mContext;

        sharedEditor.putBoolean(ma.getResources().getString(R.string.SP_reload_on_resume), true);
        sharedEditor.apply();

        ShareUtils.shareGeneral(ma, msg);
    }

    @JavascriptInterface
    public boolean isValidApp(){
        return true;
    }

    @JavascriptInterface
    public void setRefreshOnResume(){
        Activity ma = (Activity) mContext;
        Log.i("refresh set", "REXYREX REFRESH SET ON EDITOR");

        sharedEditor.putBoolean(ma.getResources().getString(R.string.SP_reload_on_resume), true);
        sharedEditor.apply();
    }
}
