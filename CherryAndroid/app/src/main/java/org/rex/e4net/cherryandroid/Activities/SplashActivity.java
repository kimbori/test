package org.rex.e4net.cherryandroid.Activities;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;

import org.rex.e4net.cherryandroid.Activities.MainActivity;
import org.rex.e4net.cherryandroid.R;

public class SplashActivity extends AppCompatActivity {
    ProgressBar pb;

    SharedPreferences sharedPref;
    SharedPreferences.Editor sharedPrefEditor;

    ImageButton adminBtn;
    boolean isAdminPressed;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);
        setContentView(R.layout.activity_splash);
        sharedPref = this.getSharedPreferences("rex_pref", Context.MODE_PRIVATE);
        sharedPrefEditor = sharedPref.edit();
        adminBtn = findViewById(R.id.splashAdminBtn);
        isAdminPressed = false;
        adminBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                isAdminPressed = true;
                View view = (LayoutInflater.from(SplashActivity.this)).inflate(R.layout.splash_admin_popup, null);

                Button serverBtn = view.findViewById(R.id.rexServerBtn);
                Button loadBtn = view.findViewById(R.id.rexLoadBtn);
                Button connectBtn = view.findViewById(R.id.rexConnectBtn);
                final EditText rexUrlEditText = view.findViewById(R.id.rexUrlEditText);

                rexUrlEditText.setText(sharedPref.getString(getResources().getString(R.string.SP_admin_url), "https://cherry.e4net.net"));

                serverBtn.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        rexUrlEditText.setText("https://cherry.e4net.net");
                    }
                });

                loadBtn.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        rexUrlEditText.setText(sharedPref.getString(getResources().getString(R.string.SP_admin_url), "https://cherry.e4net.net"));
                    }
                });

                connectBtn.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        sharedPrefEditor.putString(getResources().getString(R.string.SP_admin_url), rexUrlEditText.getText().toString());
                        sharedPrefEditor.apply();
                        startActivity(getRexIntent());
                        finish();
                    }
                });

                AlertDialog.Builder rexAlertBuilder = new AlertDialog.Builder(SplashActivity.this, R.style.PauseDialog2);
                rexAlertBuilder.setView(view);
                rexAlertBuilder.setCancelable(true);
                final AlertDialog dialog = rexAlertBuilder.create();
                dialog.show();
            }
        });

        pb = (ProgressBar) findViewById(R.id.splashProgressBar);
        pb.setProgress(0);
        pb.setMax(100*100);
        setProgressAnimate(pb, 100);
        //setupWindowAnimations();
        scheduleSplashScreen();

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("FIREBASE", "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        sharedPrefEditor.putString(getResources().getString(R.string.SP_fb_token),token);
                        sharedPrefEditor.apply();
                    }
                });
    }

    private void setProgressAnimate(ProgressBar pb, int progressTo)
    {
        ObjectAnimator animation = ObjectAnimator.ofInt(pb, "progress", pb.getProgress(), progressTo * 100);
        animation.setDuration(3770);
        animation.setInterpolator(new DecelerateInterpolator(2));
        animation.start();
    }

//    private void setupWindowAnimations() {
//        Slide slide = new Slide();
//        slide.setDuration(1000);
//        getWindow().setExitTransition(slide);
//    }

    private void fakeLoad(){
        long splashScreenDuration = 400L;
        final Intent intent = new Intent(getApplicationContext(),MainActivity.class);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                pb.incrementProgressBy(17);
                if(pb.getProgress() > 80){
                    scheduleSplashScreen();
                }
                fakeLoad();
            }
        }, splashScreenDuration);
    }

    private Intent getRexIntent(){
        final Intent intent;

        //Intent types
        if(sharedPref.getString(getResources().getString(R.string.SP_CID), "none").equals("none")){
            //저장된 CID가 없으면 무조건 sign up
            intent = new Intent(getApplicationContext(),SignUpActivity.class);
        } else if(sharedPref.getBoolean(getResources().getString(R.string.SP_app_lock_status), true) != true) {
            //CID는 있고 앱잠금 설정이 안돼있다 --> main
            intent = new Intent(getApplicationContext(), MainActivity.class);
        } else if(sharedPref.getString(getResources().getString(R.string.SP_pin), null)!=null) {
            //CID도 있고 앱잠금 설정돼있고 핀설정이 돼있다 --> 로그인을 위해 비번 입력
            intent = new Intent(getApplicationContext(),SecureKeyboardActivity.class);
            intent.putExtra("mode",3);
        } else {
            //cid는 있는데 핀번호 설정이 안돼있다
            intent = new Intent(getApplicationContext(),SecureKeyboardActivity.class);
            intent.putExtra("mode",1);
        }

        return intent;
    }

    private void scheduleSplashScreen() {
        long splashScreenDuration = 1500L;

        final Intent intent = getRexIntent();

        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                ViewGroup mRootView = (ViewGroup) findViewById(R.id.splashView);
//                Fade fade = new Fade();
//                fade.setDuration(1000);
//                TransitionManager.beginDelayedTransition(mRootView,fade);
                if(!isAdminPressed){
                    startActivity(intent);
                    finish();
                }

            }
        }, splashScreenDuration);
    }
}