package org.rex.e4net.cherryandroid.Activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.DownloadListener;
import android.webkit.WebSettings;
import android.webkit.WebView;

import org.rex.e4net.cherryandroid.Interfaces.JSInterface;
import org.rex.e4net.cherryandroid.R;
import org.rex.e4net.cherryandroid.WebViewClient.MyWebChromeClient;
import org.rex.e4net.cherryandroid.WebViewClient.MyWebViewClient;

public class UpdateAppActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_app);

        WebView wv = findViewById(R.id.updateWV);
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        wv.setWebChromeClient(new MyWebChromeClient(this));
        wv.setWebViewClient(new MyWebViewClient(this));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            wv.setWebContentsDebuggingEnabled(true);
        }
        //webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);
        wv.addJavascriptInterface(new JSInterface(this), "Android");
        //wv.setRendererPriorityPolicy(WebView.RENDERER_PRIORITY_BOUND,false);
        wv.getSettings().setDomStorageEnabled(true);

        wv.setDownloadListener(new DownloadListener() {
            @Override
            public void onDownloadStart(String url, String userAgent, String contentDisposition, String mimetype, long contentLength) {
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
                finish();
            }
        });
        wv.clearCache(true);
        wv.loadUrl("https://cherry.e4net.net/");
    }
}
