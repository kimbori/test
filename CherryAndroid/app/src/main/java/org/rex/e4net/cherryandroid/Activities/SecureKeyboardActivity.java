package org.rex.e4net.cherryandroid.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import org.rex.e4net.cherryandroid.R;

import java.util.ArrayList;
import java.util.Random;

public class SecureKeyboardActivity extends AppCompatActivity {

    Button sec_key1;
    Button sec_key2;
    Button sec_key3;
    Button sec_key4;
    Button sec_key5;
    Button sec_key6;
    Button sec_key7;
    Button sec_key8;
    Button sec_key9;
    Button sec_key0;
    Button sec_keyBack;
    Button sec_keyOneBack;

    ImageView sec_text1;
    ImageView sec_text2;
    ImageView sec_text3;
    ImageView sec_text4;
    ImageView sec_text5;
    ImageView sec_text6;

    TextView secureTV;

    ArrayList<ImageView> pass;
    ArrayList<Button> keys;
    int pass_index;
    ArrayList<Integer> password;

    Random random;

    SharedPreferences sharedPref;
    SharedPreferences.Editor sharedPrefEditor;

    // 1. new password
    // 2. confirm password
    // 3. login password
    int mode;

    String lastPassword ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secure_keyboard);

        sharedPref = this.getSharedPreferences("rex_pref",Context.MODE_PRIVATE);
        sharedPrefEditor = sharedPref.edit();

        Intent gIntent = getIntent();
        mode = gIntent.getExtras().getInt("mode");


        random = new Random();
        password = new ArrayList<Integer>();

        secureTV = findViewById(R.id.secureTV);

        switch(mode){
            case 1:
                secureTV.setText("새 핀번호를 입력하세요"); break;
            case 2:
                lastPassword = gIntent.getExtras().getString("lastPassword");
                secureTV.setText("핀번호를 다시 입력하세요"); break;
            case 3:
                secureTV.setText("핀번호를 입력하세요"); break;
            default:
                secureTV.setText("- 오류 -"); break;
        }

        sec_key1 = (Button) findViewById(R.id.sec_key1);
        sec_key2 = (Button) findViewById(R.id.sec_key2);
        sec_key3 = (Button) findViewById(R.id.sec_key3);
        sec_key4 = (Button) findViewById(R.id.sec_key4);
        sec_key5 = (Button) findViewById(R.id.sec_key5);
        sec_key6 = (Button) findViewById(R.id.sec_key6);
        sec_key7 = (Button) findViewById(R.id.sec_key7);
        sec_key8 = (Button) findViewById(R.id.sec_key8);
        sec_key9 = (Button) findViewById(R.id.sec_key9);
        sec_key0 = (Button) findViewById(R.id.sec_key0);
        sec_keyBack = (Button) findViewById(R.id.sec_keyDel);
        sec_keyOneBack = findViewById(R.id.sec_keyOneBack);

        sec_text1 = findViewById(R.id.sec_text1);
        sec_text2 = findViewById(R.id.sec_text2);
        sec_text3 = findViewById(R.id.sec_text3);
        sec_text4 = findViewById(R.id.sec_text4);
        sec_text5 = findViewById(R.id.sec_text5);
        sec_text6 = findViewById(R.id.sec_text6);

        keys = new ArrayList<Button>();
        pass = new ArrayList<ImageView>();

        pass.add(sec_text1);
        pass.add(sec_text2);
        pass.add(sec_text3);
        pass.add(sec_text4);
        pass.add(sec_text5);
        pass.add(sec_text6);

        //make text black
        for(int i=0; i<pass.size(); i++){
            //pass.get(i).setTextColor(Color.argb(27,255,0,0));
            //pass.get(i).setAlpha(0.2f);
            passImageUpdate(pass.get(i), false);
        }


        keys.add(sec_key1);
        keys.add(sec_key2);
        keys.add(sec_key3);
        keys.add(sec_key4);
        keys.add(sec_key5);
        keys.add(sec_key6);
        keys.add(sec_key7);
        keys.add(sec_key8);
        keys.add(sec_key9);
        keys.add(sec_key0);

        mixBtns();

        sec_keyBack.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                password.clear();
                pass_index = 0;
                for( int i=0; i<pass.size(); i++){
                    //pass.get(i).setAlpha(0.2f);
                    passImageUpdate(pass.get(i), false);
                }
            }
        });

        sec_keyOneBack.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                if(pass_index==0){
                    //pass.get(pass_index).setTextColor(Color.argb(27, 255, 0, 0));
                    //pass.get(pass_index).setAlpha(0.2f);
                    passImageUpdate(pass.get(pass_index), false);
                } else if(pass_index>0) {
                    pass_index--;
                    //pass.get(pass_index).setTextColor(Color.argb(27, 255, 0, 0));
                    //pass.get(pass_index).setAlpha(0.2f);
                    passImageUpdate(pass.get(pass_index), false);
                    password.remove(password.size() - 1);
                }
            }
        });


        for(int i=0; i<keys.size(); i++){
            final int iz = i;
            keys.get(i).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(pass_index<6) {
                        Log.i("Rexyrex", "REXYREX pass_index is " + pass_index);
                        Log.i("Rexyrex", "REXYREX iz is " + iz);
                        password.add(pass_index, Integer.parseInt(keys.get(iz).getText().toString()));
                        //pass.get(pass_index).setTextColor(Color.argb(255, 255, 0, 0));
                        //pass.get(pass_index).setAlpha(1f);
                        passImageUpdate(pass.get(pass_index), true);
                        //pass.get(pass_index).setText(keys.get(iz).getText().toString());

                        boolean shouldIncrement = true;

                        if(pass_index==5){
                            //Toast.makeText(getApplicationContext(), getPasswordAsString(), Toast.LENGTH_SHORT).show();
                            //6 numbers pressed
                            switch(mode){
                                case 1:
                                    Intent i = new Intent(getApplicationContext(), SecureKeyboardActivity.class);
                                    i.putExtra("lastPassword", getPasswordAsString());
                                    i.putExtra("mode", 2);
                                    startActivity(i);
                                    finish();
                                    break;
                                case 2:
                                    if(lastPassword.equals(getPasswordAsString())){
                                        //success -> login
                                        sharedPrefEditor.putString(getResources().getString(R.string.SP_pin),getPasswordAsString());
                                        sharedPrefEditor.apply();
                                        Intent i2 = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(i2);
                                        finish();
                                    } else {

                                        secureTV.setText("비밀번호가 틀렸습니다. 다시 입력해주세요.");
                                        secureTV.setTextColor(Color.RED);
                                        Toast.makeText(getApplicationContext(), "비밀번호가 틀렸습니다. 다시 입력해주세요.", Toast.LENGTH_SHORT).show();
                                        shouldIncrement =  false;
                                        resetPass();
                                    }
                                    break;
                                case 3:
                                    if(getPasswordAsString().equals(sharedPref.getString(getResources().getString(R.string.SP_pin),"null"))){
                                        //sharedPrefEditor.putString(getResources().getString(R.string.SP_pin),getPasswordAsString());
                                        //sharedPrefEditor.apply();

                                        secureTV.setText("로그인 중입니다. 잠시만 기다려주세요.");
                                        secureTV.setTextColor(Color.rgb(71, 150, 44));
                                        secureTV.setTextSize(20f);

                                        Intent i2 = new Intent(getApplicationContext(), MainActivity.class);
                                        startActivity(i2);
                                        finish();
                                    } else {

                                        secureTV.setText("비밀번호가 틀렸습니다. 다시 입력해주세요.");
                                        secureTV.setTextColor(Color.RED);
                                        Toast.makeText(getApplicationContext(), "비밀번호가 틀렸습니다. 다시 입력해주세요.", Toast.LENGTH_SHORT).show();
                                        shouldIncrement =  false;
                                        resetPass();
                                    }
                                    break;
                                default:
                                    secureTV.setText("- 오류 -"); break;
                            }
                        }

                        if(shouldIncrement) {
                            pass_index++;
                        }

                    } else {
                        Toast.makeText(getApplicationContext(), "비밀번호가 틀렸습니다. 다시 입력해주세요.", Toast.LENGTH_SHORT).show();
                        //Toast.makeText(getApplicationContext(), getPasswordAsString(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }
    }

    private void resetPass(){
        password.clear();
        pass_index = 0;
        for( int i=0; i<pass.size(); i++){
            //pass.get(i).setAlpha(0.2f);
            passImageUpdate(pass.get(i), false);
        }
    }

    private void passImageUpdate(ImageView btnIV, boolean activated){
        if(activated){
            btnIV.setImageDrawable(getDrawable(R.drawable.password_circle_filled));
        } else {
            btnIV.setImageDrawable(getDrawable(R.drawable.password_circle));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public String getPasswordAsString(){
        String res = "";
        for(int i=0; i<password.size(); i++){
            res += String.valueOf(password.get(i));
        }
        return res;
    }

    public void mixBtns(){
        ArrayList<Integer> nums = new ArrayList<Integer>();
        for(int i=0; i<10; i++){
            nums.add(i);
        }
        int tmpIndex = 0;
        while(nums.size() > 0){
            int randomNumber = random.nextInt(nums.size());
            keys.get(tmpIndex).setText(String.valueOf(nums.remove(randomNumber)));
            tmpIndex++;
        }
    }
}
