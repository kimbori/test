package org.rex.e4net.cherryandroid.Activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.webkit.WebSettings;
import android.webkit.WebView;

import org.rex.e4net.cherryandroid.Interfaces.JSInterface;
import org.rex.e4net.cherryandroid.R;
import org.rex.e4net.cherryandroid.Utils.DeviceInfoUtils;
import org.rex.e4net.cherryandroid.WebViewClient.MyWebChromeClient;
import org.rex.e4net.cherryandroid.WebViewClient.MyWebViewClient;

public class SignUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        SharedPreferences sharedPref = this.getSharedPreferences("rex_pref", Context.MODE_PRIVATE);
        WebView wv = findViewById(R.id.signUpWV);
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        wv.setWebChromeClient(new MyWebChromeClient(this));
        wv.setWebViewClient(new MyWebViewClient(this));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            wv.setWebContentsDebuggingEnabled(true);
        }
        //webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);
        wv.addJavascriptInterface(new JSInterface(this), "Android");
        //wv.setRendererPriorityPolicy(WebView.RENDERER_PRIORITY_BOUND,false);
        wv.getSettings().setDomStorageEnabled(true);
        wv.loadUrl(sharedPref.getString(getResources().getString(R.string.SP_admin_url), "https://cherry.e4net.net") + "/public/cert/certReady?oprGrpId=" + getResources().getString(R.string.operating_group));
        //wv.loadUrl("http://192.168.10.210:8080/certReady");
        DeviceInfoUtils.toggleNotificationSetting(true,this);
        DeviceInfoUtils.toggleAppLockSetting(true,this);
        DeviceInfoUtils.toggleDevMode(false,this);


    }
}
