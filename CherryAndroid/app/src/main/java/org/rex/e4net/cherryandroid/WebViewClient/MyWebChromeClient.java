package org.rex.e4net.cherryandroid.WebViewClient;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Message;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.webkit.ConsoleMessage;
import android.webkit.CookieManager;
import android.webkit.JsResult;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import org.rex.e4net.cherryandroid.Activities.MainActivity;
import org.rex.e4net.cherryandroid.Activities.MyActivity;
import org.rex.e4net.cherryandroid.Interfaces.JSInterface;
import org.rex.e4net.cherryandroid.R;


import java.net.URISyntaxException;

public class MyWebChromeClient extends WebChromeClient {

    Context c;

    public MyWebChromeClient(Context c){
        this.c = c;
    }

    @Override
    public boolean onJsAlert(WebView view, String url, String message, final JsResult result) {

        //Required functionality here
        //return super.onJsAlert(view, url, message, result);

        final AlertDialog rex_dialog = new AlertDialog.Builder(c)
                .setTitle("title")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                result.confirm();
                            }
                        })
                .setCancelable(false)
                .create();

        rex_dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                rex_dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
                rex_dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
                rex_dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.BLACK);
            }
        });

        rex_dialog.show();

        return true;
    }

    @Override
    public boolean onJsConfirm(WebView view, String url, String message, final JsResult result) {

        //Required functionality here
        //return super.onJsAlert(view, url, message, result);

        final AlertDialog rex_dialog = new AlertDialog.Builder(c)
                .setTitle("title")
                .setMessage(message)
                .setPositiveButton(android.R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                result.confirm();
                            }
                        })
                .setNegativeButton(android.R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                result.cancel();
                            }
                        })
                .setCancelable(false)
                .create();

        rex_dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                rex_dialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(Color.BLACK);
                rex_dialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(Color.BLACK);
                rex_dialog.getButton(AlertDialog.BUTTON_NEUTRAL).setTextColor(Color.BLACK);
            }
        });

        rex_dialog.show();

        return true;
    }

    @Override
    public void onCloseWindow(WebView window){
        Log.i("info","REXYREX OnCloseWindow");
    }

//    @Override
//    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
//        Log.i("WEB", "ON CREATE WINDOW : " + view);
//        return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
//    }
//
//    @Override
//    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
//        Message href = view.getHandler().obtainMessage();
//        view.requestFocusNodeHref(href);
//
//        String url = href.getData().getString("url");
//        Log.i("url to load", "REXYREX URL TO LOAD: "+url);
//        CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
//        CustomTabsIntent customTabsIntent = builder.build();
//        customTabsIntent.launchUrl(c, Uri.parse(url));
//
//        return super.onCreateWindow(view, isDialog, isUserGesture, resultMsg);
//    }


        @Override
    public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
        Log.i("info","REXYREX OnCreateWindow");
        View v = (LayoutInflater.from(c)).inflate(R.layout.alert_layout1, null);

        Button closeBtn = (Button) v.findViewById(R.id.button_close);

        Log.i("log", "REXYREX VIEW URL: " + view.getUrl());

        final WebView newWebView = new WebView((Activity) c);
        WebSettings webSettings = newWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setAppCacheEnabled(true);
        webSettings.setDomStorageEnabled(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        //webSettings.setSafeBrowsingEnabled(false);
            newWebView.addJavascriptInterface(new JSInterface(c), "Android");

            CookieManager cookieManager = CookieManager.getInstance();
            cookieManager.setAcceptCookie(true);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                cookieManager.setAcceptThirdPartyCookies(newWebView, true);
                webSettings.setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);
            }

            //newWebView.setWebViewClient(new MyWebViewClient(c));
        newWebView.setWebViewClient(new WebViewClient(){
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Log.i("OVERRIDE","REXYREX OVERRIDE url : " + url);
                if (url != null && url.startsWith("intent://")) {

                    try {
                        Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                        Intent existPackage = c.getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                        if (existPackage != null) {
                            Log.i("OVERRIDE","REXYREX OVERRIDE found package : ");
                            c.startActivity(intent);return true;
                        } else {
                            Log.i("OVERRIDE","REXYREX OVERRIDE NOT found package : ");
                            Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                            marketIntent.setData(Uri.parse("market://details?id="+intent.getPackage()));
                            c.startActivity(marketIntent);
                        }
                        return true;
                    }catch (Exception e) {
                        Log.i("OVERRIDE","REXYREX OVERRIDE EXCEPTION : ");
                        e.printStackTrace();
                    }
                } else if (url != null && url.startsWith("market://")) {
                    try {
                        Log.i("OVERRIDE","REXYREX OVERRIDE Market : ");
                        Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                        if (intent != null) {
                            c.startActivity(intent);
                        }
                        return true;
                    } catch (URISyntaxException e) {
                        Log.i("OVERRIDE","REXYREX OVERRIDE Market EXCEPTION : ");
                        e.printStackTrace();
                    }
                }
                Log.i("OVERRIDE","REXYREX OVERRIDE end : ");
                view.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
                view.invalidate();
                //view.loadUrl(url);
                return super.shouldOverrideUrlLoading(view,url);
                //return true;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                //super.onPageStarted(view, url, favicon);
                Log.i("OVERRIDE","REXYREX OVERRIDE PAGE START: "+url);
                //view.setVisibility(view.GONE);
                //dialog.show();
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                //super.onPageFinished(view, url);
                Log.i("OVERRIDE","REXYREX OVERRIDE PAGE FINISH: "+url);
                //view.setVisibility(view.VISIBLE);
                //dialog.hide();
                //view.loadUrl(url);
            }

            @Override
            public boolean onRenderProcessGone(WebView view, RenderProcessGoneDetail detail) {
                Log.e("err","REXYREX RENDER PROCESS GONE");
                return super.onRenderProcessGone(view, detail);
            }
        });
            //newWebView.setRendererPriorityPolicy(WebView.RENDERER_PRIORITY_IMPORTANT,false);

        //newWebView.addJavascriptInterface(new JSInterface(c), "Android");
        // Other configuration comes here, such as setting the WebViewClient

        final Dialog dialog = new Dialog(c,android.R.style.Theme_Light_NoTitleBar_Fullscreen);
        //dialog.setContentView(v);

        dialog.setContentView(newWebView);
        dialog.show();

        //newWebView.setWebChromeClient(new WebChromeClient());

        newWebView.setWebChromeClient(new WebChromeClient() {
            @Override
            public boolean onCreateWindow(WebView view, boolean isDialog, boolean isUserGesture, Message resultMsg) {
                Log.i("wut", "REXYREX creating ANOTHER popup window");
                newWebView.loadUrl(view.getUrl());
                return true;
            }

            @Override
            public void onCloseWindow(WebView window) {
                Log.i("wut", "REXYREX closing popup window");
                newWebView.destroy();
                dialog.dismiss();
            }

            @Override
            public boolean onConsoleMessage(ConsoleMessage consoleMessage) {
                Log.i("rexymsg", "REXYREX OVERRIDE CONSOLE MSG: "+consoleMessage.message());
                return true;
            }
        });

        closeBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Log.i("wut", "REXYREX closing popup window");
                newWebView.destroy();
                dialog.dismiss();
            }
        });

        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                Log.i("wut", "REXYREX closing popup window");
                newWebView.destroy();
            }
        });

        ((WebView.WebViewTransport)resultMsg.obj).setWebView(newWebView);
        resultMsg.sendToTarget();
        return true;
    }
}

