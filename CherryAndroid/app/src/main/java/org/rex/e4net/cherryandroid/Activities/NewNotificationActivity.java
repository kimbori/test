package org.rex.e4net.cherryandroid.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import org.rex.e4net.cherryandroid.R;

public class NewNotificationActivity extends AppCompatActivity {

    TextView titleTV;
    TextView msgTV;
    ImageButton exitAppBtn;
    ImageButton goToCherryBtn;
    Button updateBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_notification);

        titleTV = (TextView) findViewById(R.id.NotiTitleTV);
        msgTV = (TextView) findViewById(R.id.NotiContentsTV);
        exitAppBtn = (ImageButton) findViewById(R.id.exitNotiViewBtn);
        goToCherryBtn = (ImageButton) findViewById(R.id.goToCherryBtn);
        updateBtn = findViewById(R.id.goUpdateAppBtn);

        exitAppBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        goToCherryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mainIntent = new Intent(NewNotificationActivity.this, SplashActivity.class);
                NewNotificationActivity.this.startActivity(mainIntent);
                finish();
            }
        });

        Intent intent = getIntent();
        String msgTitle = intent.getStringExtra("title");
        String msgContents = intent.getStringExtra("message");
        String msgDetails = intent.getStringExtra("details");
        String msgType = intent.getStringExtra("type");

        if(msgType.equals("update")){
            updateBtn.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View v) {
                    Intent mainIntent = new Intent(NewNotificationActivity.this, UpdateAppActivity.class);
                    NewNotificationActivity.this.startActivity(mainIntent);
                    finish();
                }
            });
        } else {
            updateBtn.setVisibility(View.INVISIBLE);
        }

        titleTV.setText(msgTitle);
        msgTV.setText(msgDetails);
    }
}
