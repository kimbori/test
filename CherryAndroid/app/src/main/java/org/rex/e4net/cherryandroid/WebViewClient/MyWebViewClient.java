package org.rex.e4net.cherryandroid.WebViewClient;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.ClientCertRequest;
import android.webkit.RenderProcessGoneDetail;
import android.webkit.SafeBrowsingResponse;
import android.webkit.SslErrorHandler;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import org.rex.e4net.cherryandroid.Activities.MainActivity;
import org.rex.e4net.cherryandroid.R;

import java.net.URISyntaxException;
import java.util.ArrayList;

public class MyWebViewClient extends WebViewClient {

    Context c;
    final AlertDialog dialog;
    private WebView mWebView;

    @Override
    public void onSafeBrowsingHit(WebView view, WebResourceRequest request, int threatType, SafeBrowsingResponse callback) {
        Log.e("safe browsing", "safe browsing hit");
    }

//    @Override
//    public boolean onRenderProcessGone(WebView view, RenderProcessGoneDetail detail) {
//        if (!detail.didCrash()) {
//            // Renderer was killed because the system ran out of memory.
//            // The app can recover gracefully by creating a new WebView instance
//            // in the foreground.
//            Log.e("MY_APP_TAG", "System killed the WebView rendering process " +
//                    "to reclaim memory. Recreating...");
//
//            if (mWebView != null) {
////                ViewGroup webViewContainer =
////                        (ViewGroup) findViewById(R.id.my_web_view_container);
////                webViewContainer.removeView(mWebView);
////                mWebView.destroy();
////                mWebView = null;
//            }
//
//            // By this point, the instance variable "mWebView" is guaranteed
//            // to be null, so it's safe to reinitialize it.
//
//            return true; // The app continues executing.
//        }
//
//        // Renderer crashed because of an internal error, such as a memory
//        // access violation.
//        Log.e("MY_APP_TAG", "The WebView rendering process crashed!");
//
//        // In this example, the app itself crashes after detecting that the
//        // renderer crashed. If you choose to handle the crash more gracefully
//        // and allow your app to continue executing, you should 1) destroy the
//        // current WebView instance, 2) specify logic for how the app can
//        // continue executing, and 3) return "true" instead.
//        return false;
//    }

    @Override
    public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);
        Log.i("rexError", "REXYREX OVERRIDE onReceivedError");
    }

    @Override
    public void onReceivedHttpError(WebView view, WebResourceRequest request, WebResourceResponse errorResponse) {
        super.onReceivedHttpError(view, request, errorResponse);
        Log.i("rexError", "REXYREX OVERRIDE onReceivedHttpError: " );
        ArrayList<String> errArr = new ArrayList<String>();
        errArr.add("getEncoding: " + errorResponse.getEncoding());
        errArr.add("getData: " + errorResponse.getData());
        errArr.add("getMimeType: " + errorResponse.getMimeType());
        //errArr.add("getResponseHeaders: " + errorResponse.getResponseHeaders().);
        errArr.add("getStatusCode: " + errorResponse.getStatusCode());
        errArr.add("request.getUrl: " + request.getUrl());

        for(int i=0; i<errArr.size(); i++){
            Log.i("rexError", "REXYREX OVERRIDE onReceivedHttpError detail: [" + errArr.get(i) + "]");
        }
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        super.onReceivedSslError(view, handler, error);
        Log.i("rexError", "REXYREX OVERRIDE onReceivedSslError");
    }

    @Override
    public void onReceivedClientCertRequest(WebView view, ClientCertRequest request) {
        super.onReceivedClientCertRequest(view, request);
        Log.i("rexError", "REXYREX OVERRIDE onReceivedClientCertRequest");
    }

    @Override
    public boolean onRenderProcessGone(WebView view, RenderProcessGoneDetail detail) {
        //dialog.hide();
        return super.onRenderProcessGone(view, detail);

    }

    public MyWebViewClient(Context c){
       this.c = c;
        View progressview = (LayoutInflater.from(c)).inflate(R.layout.webview_load_progress_bar, null);

        ProgressBar pb = progressview.findViewById(R.id.wv_progressbar);

        AlertDialog.Builder rexAlertBuilder = new AlertDialog.Builder(c, R.style.WebProgressBarStyle);
        rexAlertBuilder.setView(progressview);
        rexAlertBuilder.setCancelable(true);
        dialog = rexAlertBuilder.create();
        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

        pb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog.hide();
            }
        });
        //ProgressBar pb = progressview.findViewById(R.id.progressBar);
        //pb.setProgress(50);
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {
        Log.i("OVERRIDE","REXYREX OVERRIDE url : " + url);
        if (url != null && url.startsWith("intent://")) {
            try {
                Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                Intent existPackage = c.getPackageManager().getLaunchIntentForPackage(intent.getPackage());
                if (existPackage != null) {
                    Log.i("OVERRIDE","REXYREX OVERRIDE found package : ");
                    c.startActivity(intent);return true;
                } else {
                    Log.i("OVERRIDE","REXYREX OVERRIDE NOT found package : ");
                    Intent marketIntent = new Intent(Intent.ACTION_VIEW);
                    marketIntent.setData(Uri.parse("market://details?id="+intent.getPackage()));
                    c.startActivity(marketIntent);
                }
                return true;
            }catch (Exception e) {
                Log.i("OVERRIDE","REXYREX OVERRIDE EXCEPTION : ");
                e.printStackTrace();
            }
        } else if (url != null && url.startsWith("market://")) {
            try {
                Log.i("OVERRIDE","REXYREX OVERRIDE Market : ");
                Intent intent = Intent.parseUri(url, Intent.URI_INTENT_SCHEME);
                if (intent != null) {
                    c.startActivity(intent);
                }
                return true;
            } catch (URISyntaxException e) {
                Log.i("OVERRIDE","REXYREX OVERRIDE Market EXCEPTION : ");
                e.printStackTrace();
            }
        }
        Log.i("OVERRIDE","REXYREX OVERRIDE end : ");
        //view.setLayerType(View.LAYER_TYPE_SOFTWARE,null);
        //view.invalidate();
        view.loadUrl(url);
        return true;
    }

    @Override
    public void onPageStarted(WebView view, String url, Bitmap favicon) {
        //super.onPageStarted(view, url, favicon);
        Log.i("OVERRIDE","REXYREX OVERRIDE PAGE START: "+url);
        //view.setVisibility(view.GONE);
        dialog.show();
    }

    @Override
    public void onPageFinished(WebView view, String url) {
        //super.onPageFinished(view, url);
        Log.i("OVERRIDE","REXYREX OVERRIDE PAGE FINISH: "+url);
        //view.setVisibility(view.VISIBLE);
        dialog.hide();
        //view.loadUrl(url);
    }
}