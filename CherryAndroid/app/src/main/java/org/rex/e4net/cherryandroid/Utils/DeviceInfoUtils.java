package org.rex.e4net.cherryandroid.Utils;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.rex.e4net.cherryandroid.Activities.MainActivity;
import org.rex.e4net.cherryandroid.Activities.NotificationCheckActivity;
import org.rex.e4net.cherryandroid.Constants.Constants;
import org.rex.e4net.cherryandroid.R;

public class DeviceInfoUtils {

    public static void toggleNotificationSetting(boolean notiSetting, Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("rex_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedEditor = sharedPref.edit();
        sharedEditor.putBoolean(context.getResources().getString(R.string.SP_notification_status), notiSetting);
        sharedEditor.apply();
        if(notiSetting){
            FirebaseMessaging.getInstance().subscribeToTopic("rexTopic");
        } else {
            FirebaseMessaging.getInstance().unsubscribeFromTopic("rexTopic");
        }
    }

    public static void toggleAppLockSetting(boolean appLockSetting, Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("rex_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedEditor = sharedPref.edit();
        sharedEditor.putBoolean(context.getResources().getString(R.string.SP_app_lock_status), appLockSetting);
        sharedEditor.apply();
    }

    public static void toggleDevMode(boolean devModeSetting, Context context){
        SharedPreferences sharedPref = context.getSharedPreferences("rex_pref", Context.MODE_PRIVATE);
        SharedPreferences.Editor sharedEditor = sharedPref.edit();
        sharedEditor.putBoolean(context.getResources().getString(R.string.SP_dev_mode), devModeSetting);
        sharedEditor.apply();
    }

    public static void askForPermission(Context c, String permission, int permissionID){
        if (ContextCompat.checkSelfPermission((Activity)c,
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale((Activity)c,
                    permission)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions((Activity)c,
                        new String[]{permission},
                        permissionID);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    public static void permissionDeviceID(Context c) throws SecurityException{
        askForPermission(c,Manifest.permission.READ_PHONE_STATE,Constants.REX_PERMISSIONS_REQUEST_DEVICE_INFO);

//        if (ContextCompat.checkSelfPermission((Activity)c, Manifest.permission.READ_PHONE_STATE)
//                != PackageManager.PERMISSION_GRANTED) {
//            // Permission is not granted
//            return "ERROR";
//        } else {
//            TelephonyManager tm = (TelephonyManager) c.getSystemService(Context.TELEPHONY_SERVICE);
//            return tm.getDeviceId();
//        }
    }
}
