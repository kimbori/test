package org.rex.e4net.cherryandroid.Activities;


import android.Manifest;
import android.app.Activity;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JsResult;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;

import org.rex.e4net.cherryandroid.Constants.Constants;
import org.rex.e4net.cherryandroid.Interfaces.JSInterface;
import org.rex.e4net.cherryandroid.Utils.DeviceInfoUtils;
import org.rex.e4net.cherryandroid.Utils.PictureUtils;
import org.rex.e4net.cherryandroid.WebViewClient.MyWebChromeClient;
import org.rex.e4net.cherryandroid.WebViewClient.MyWebViewClient;
import org.rex.e4net.cherryandroid.R;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.Date;

public class MainActivity extends BaseActivity {

    public static final String TAG = "MsgFirebaseServ";
    public final int GALLERY_ACTIVITY_CODE=Constants.GALLERY_ACTIVITY_CODE;
    public  final int RESULT_CROP = 400;

    private WebView wv;
    private String mainURL;
    private ImageButton adminBtn;

    private ImageButton searchBtn;

    private SharedPreferences sharedPref;
    private SharedPreferences.Editor sharedEditor;

    private LinearLayout leftBtnContainer;
    private LinearLayout rightBtnContainer;
    private LinearLayout homeBtnContainer;
    private LinearLayout mypageBtnContainer;
    private LinearLayout alertsBtnContainer;

    private long lastBackAttemptTime;

    private TextView currentURLTV;

    protected void onCreate(Bundle savedInstanceState) {
        //super.onCreate(savedInstanceState);
        super.onCreate(savedInstanceState, R.layout.activity_main);

        lastBackAttemptTime = System.currentTimeMillis();

        //enable strictmode to allow for pic url reading
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());

        //instantiate shared preferences (for dev ip and extension saving)
        sharedPref = this.getSharedPreferences("rex_pref", Context.MODE_PRIVATE);
        sharedEditor = sharedPref.edit();

        adminBtn = (ImageButton) findViewById(R.id.adminBtn);

        //URL to connect to
        mainURL = "https://cherry.e4net.net";

        searchBtn = (ImageButton) findViewById(R.id.searchBtn);

        //LinearLayouts can be pushed like buttons
        leftBtnContainer = (LinearLayout) findViewById(R.id.leftBtnContainer);
        rightBtnContainer = (LinearLayout) findViewById(R.id.rightBtnContainer);
        homeBtnContainer = (LinearLayout) findViewById(R.id.homeBtnContainer);
        mypageBtnContainer = (LinearLayout) findViewById(R.id.mypageBtnContainer);
        alertsBtnContainer = (LinearLayout) findViewById(R.id.alertsBtnContainer);

//        DeviceInfoUtils.permissionDeviceID(MainActivity.this);
//
//        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
//                != PackageManager.PERMISSION_GRANTED) {
//            // Permission is not granted
//            Log.i("deviceid", "REXYREX Device ID: NOT GRANTED");
//        } else {
//            TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
//            Log.i("deviceid", "REXYREX Device ID: " + tm.getDeviceId());
//        }

        //Notification Channels for Firebase msg's
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // Create channel to show notifications.
            String channelId = "rexid2";
            String channelName = "rexid2";
            NotificationManager notificationManager =
                    getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(new NotificationChannel(channelId,
                    channelName, NotificationManager.IMPORTANCE_LOW));
        }

        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                Object value = getIntent().getExtras().get(key);
                Log.d(TAG, "Key: " + key + " Value: " + value);
            }
        }

        //firebase topic subscription


        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "getInstanceId failed", task.getException());
                            return;
                        }
                        // Get new Instance ID token
                        String token = task.getResult().getToken();

                        sharedEditor.putString(getResources().getString(R.string.SP_fb_token),token);
                        sharedEditor.apply();
                    }
                });

        Log.i("instanceid","REXYREX firebase instance id: " + FirebaseInstanceId.getInstance().getId());

        //fade animation
        overridePendingTransition(android.R.anim.fade_in, android.R.anim.fade_out);

        //main webview
        wv = (WebView) findViewById(R.id.WebViewID);
        WebSettings webSettings = wv.getSettings();
        webSettings.setJavaScriptEnabled(true);
        webSettings.setGeolocationEnabled(true);
        //webSettings.setSupportMultipleWindows(true); //Override URL doesnt work if this is enabled (dont know why)
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportMultipleWindows(true);
        webSettings.setDomStorageEnabled(true);

        wv.getSettings().setLoadWithOverviewMode(true);
        //wv.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        //enable web debugging
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            wv.setLayerType(View.LAYER_TYPE_HARDWARE, null);
        } else {
            wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        }


        wv.addJavascriptInterface(new JSInterface(this), "Android");
        wv.setWebViewClient(new MyWebViewClient(this));
        wv.setWebChromeClient(new MyWebChromeClient(this));


//        if (Build.VERSION.SDK_INT >= 19) {
//            wv.setLayerType(View.LAYER_TYPE_HARDWARE, null);
//        }
//        else {
//            wv.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
//        }

        //wv.addJavascriptInterface(this, "Android");

        //버튼을 누르고있을때 색바뀜
//        searchBtn.setOnTouchListener(new View.OnTouchListener() {
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//                if(event.getAction() == MotionEvent.ACTION_DOWN) {
//                    searchBtn.setImageDrawable(getResources().getDrawable(R.drawable.btn_search_on));
//                } else if (event.getAction() == MotionEvent.ACTION_UP) {
//                    searchBtn.setImageDrawable(getResources().getDrawable(R.drawable.btn_search));
//                    wv.loadUrl("javascript:caasFromApp.buttonEvent('" + "SEARCH" + "')");
//                }
//                return true;
//            }
//        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Toast.makeText(getApplicationContext(), "Search Button!" , Toast.LENGTH_SHORT).show();
//                Intent searchIntent = new Intent(MainActivity.this, SearchActivity.class);
//                MainActivity.this.startActivity(searchIntent);
                if(sharedPref.getBoolean(getResources().getString(R.string.SP_dev_mode), false)){
                    wv.loadUrl("javascript:caasFromApp.buttonEvent('" + "SEARCH','" + sharedPref.getString(getResources().getString(R.string.SP_admin_url), "https://cherry.e4net.net") + "')");
                } else {
                    Toast.makeText(getApplicationContext(), "개발중입니다" , Toast.LENGTH_SHORT).show();
                }
            }
        });
        leftBtnContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wv.goBack();
            }
        });
        rightBtnContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wv.goForward();
            }
        });
        homeBtnContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wv.loadUrl("javascript:caasFromApp.buttonEvent('" + "HOME','" + sharedPref.getString(getResources().getString(R.string.SP_admin_url), "https://cherry.e4net.net") + "')");
                //Toast.makeText(getApplicationContext(), "homeBtn!" , Toast.LENGTH_SHORT).show();
            }
        });
        mypageBtnContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                wv.loadUrl("javascript:caasFromApp.buttonEvent('" + "MYPAGE','" + sharedPref.getString(getResources().getString(R.string.SP_admin_url), "https://cherry.e4net.net") + "')");
                //Toast.makeText(getApplicationContext(), "mypageBtn!" , Toast.LENGTH_SHORT).show();
            }
        });
        alertsBtnContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sharedPref.getBoolean(getResources().getString(R.string.SP_dev_mode), false)){
                    wv.loadUrl("javascript:caasFromApp.buttonEvent('" + "ALERTS','" + sharedPref.getString(getResources().getString(R.string.SP_admin_url), "https://cherry.e4net.net") + "')");
                } else {
                    Toast.makeText(getApplicationContext(), "개발중입니다" , Toast.LENGTH_SHORT).show();
                }

//                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.KITKAT) {
//                    wv.evaluateJavascript("javascript:func1App();", null);
//                } else {
//                    wv.loadUrl("javascript:func1App()");
//                }
                //Toast.makeText(getApplicationContext(), "alertsBtn!" , Toast.LENGTH_SHORT).show();
            }
        });
        adminBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view = (LayoutInflater.from(MainActivity.this)).inflate(R.layout.test_url_popup, null);
                currentURLTV = (TextView) view.findViewById(R.id.currentURLTV);
                currentURLTV.setText(wv.getUrl());


                final Switch devModeSwitch = view.findViewById(R.id.devModeSwitch);
                boolean isDevModeSet = sharedPref.getBoolean(getResources().getString(R.string.SP_dev_mode), false);
                devModeSwitch.setChecked(isDevModeSet);
                sharedEditor.putBoolean(getResources().getString(R.string.SP_dev_mode), devModeSwitch.isChecked());
                sharedEditor.apply();

                if(devModeSwitch.isChecked()){
                    devModeSwitch.setText("Dev Mode On");
                    devModeSwitch.setTextColor(Color.GREEN);

                } else {
                    devModeSwitch.setText("Dev Mode Off");
                    devModeSwitch.setTextColor(Color.RED);
                }

                //devModeSwitch.setTextOn("Dev Mode On");
                //devModeSwitch.setTextOff("Dev Mode Off");
                //dev mode 설정이 안돼있을경우 dev mode on
                devModeSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        sharedEditor.putBoolean(getResources().getString(R.string.SP_dev_mode), isChecked);
                        Log.i("devmode","devmode set to " + isChecked);
                        sharedEditor.apply();

                        if(isChecked){
                            //dev mode checked true
                            devModeSwitch.setText("Dev Mode On");
                            devModeSwitch.setTextColor(Color.GREEN);
                        } else {
                            //dev mode checked false
                            devModeSwitch.setText("Dev Mode Off");
                            devModeSwitch.setTextColor(Color.RED);
                        }
                    }
                });


                Button refreshURLBtn = (Button) view.findViewById(R.id.popupRefreshBtn);
                Button serverConnectBtn = (Button) view.findViewById(R.id.serverConnectBtn);
                Button ipConnectBtn = (Button) view.findViewById(R.id.customIPConnectBtn);
                Button testPageBtn = (Button) view.findViewById(R.id.activityActivateBtn);
                TextView opGroup = view.findViewById(R.id.operating_groupTV);
                final EditText newUrlET = (EditText) view.findViewById(R.id.urlEditText);
                final EditText newExtensionET = (EditText) view.findViewById(R.id.extensionET);

                newUrlET.setText(sharedPref.getString(getResources().getString(R.string.SP_admin_url), "http://192.168."));
                newExtensionET.setText(sharedPref.getString(getResources().getString(R.string.SP_admin_extension), "/publish/MN-001.html"));
                final Spinner popupSpinner = (Spinner) view.findViewById(R.id.popup_spinner);
                final String[] items = new String[]{"Android JS Interface (test.html)", "Password Activity", "Base64 Picture Activity", "Firebase", "Animation Test"};
                ArrayAdapter<String> adapter = new ArrayAdapter<>(view.getContext(), android.R.layout.simple_spinner_dropdown_item, items);
                popupSpinner.setAdapter(adapter);

                String usrCid = sharedPref.getString(getResources().getString(R.string.SP_CID), "null");
                opGroup.setText("운영단체ID: "+getResources().getString(R.string.operating_group));
                //opGroup.setText(usrCid);

                AlertDialog.Builder rexAlertBuilder = new AlertDialog.Builder(MainActivity.this, R.style.PauseDialog);
                rexAlertBuilder.setView(view);
                rexAlertBuilder.setCancelable(true);
                final AlertDialog dialog = rexAlertBuilder.create();
                dialog.setOnShowListener(new DialogInterface.OnShowListener() {
                    @Override
                    public void onShow(DialogInterface dialog) {
//                        Button btn = ((AlertDialog)dialog).getButton(DialogInterface.BUTTON_POSITIVE);
//                        btn.setBackgroundColor(Color.BLACK);
//                        btn.setTextColor(Color.WHITE);
                    }
                });
                dialog.show();

                serverConnectBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "서버 연결중...", Toast.LENGTH_LONG).show();
                        mainURL = "https://cherry.e4net.net/publish/MN-001.html";
                        updateURL(mainURL);
                        dialog.cancel();
                    }
                });
                ipConnectBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mainURL = newUrlET.getText().toString();

                        sharedEditor.putString(getResources().getString(R.string.SP_admin_url), mainURL);
                        sharedEditor.putString(getResources().getString(R.string.SP_admin_extension), newExtensionET.getText().toString());
                        sharedEditor.apply();
                        mainURL += newExtensionET.getText().toString();
                        updateURL(mainURL);
                        Toast.makeText(getApplicationContext(), "Connecting to : " + mainURL, Toast.LENGTH_SHORT).show();
                        dialog.cancel();
                    }
                });
                refreshURLBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Toast.makeText(getApplicationContext(), "새로고침 중입니다", Toast.LENGTH_LONG).show();
                        wv.clearCache(true);
                        wv.reload();
                        dialog.cancel();
                    }
                });
                testPageBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        switch (popupSpinner.getSelectedItem().toString()) {
                            case "Android JS Interface (test.html)":
                                Toast.makeText(getApplicationContext(), "Loading Test Page...", Toast.LENGTH_LONG).show();
                                mainURL = "http://192.168.10.207:8080/publish/rex_test.html";
                                updateURL(mainURL);
                                dialog.cancel();
                                break;
                            case "Password Activity":
                                Intent passIntent = new Intent(MainActivity.this, SecureKeyboardActivity.class);
                                MainActivity.this.startActivity(passIntent);
                                break;
                            case "Base64 Picture Activity":
                                Intent pictureIntent = new Intent(MainActivity.this, PictureSaveActivity.class);
                                MainActivity.this.startActivity(pictureIntent);
                                break;
                            case "Firebase":
                                Intent NotiIntent = new Intent(MainActivity.this, NotificationCheckActivity.class);
                                MainActivity.this.startActivity(NotiIntent);
                                break;
                            case "Animation Test":
                                Toast.makeText(getApplicationContext(), "Loading Test Page...", Toast.LENGTH_LONG).show();
                                mainURL = "http://192.168.10.207:8080/";
                                updateURL(mainURL);
                                dialog.cancel();
                                break;
                            default:
                                break;
                        }
                    }
                });
            }
        });

        //updateURL("https://cherry.e4net.net/moveMainPag");
        //wv.loadUrl("javascript:caasFromApp.buttonEvent('" + "HOME" + "')");
        //updateURL("https://cherry.e4net.net/publish/MN-001.html");

        //"/loginProc?oprGrpId=운영단체ID&cnrCi=기부자CID
        String usrCid = sharedPref.getString(getResources().getString(R.string.SP_CID), "null");
        try {
            usrCid= URLEncoder.encode(usrCid, "UTF-8");
            Log.i("encode success", "REXYREX ENCODE SUCCESS");
        } catch(UnsupportedEncodingException e) {
            Log.e("encode error", e.toString());
        }

        updateURL(sharedPref.getString(getResources().getString(R.string.SP_admin_url), "https://cherry.e4net.net") + "/public/loginProc?oprGrpId="+getResources().getString(R.string.operating_group) + "&cnrCi=" + usrCid);
        //updateURL("http://192.168.10.202:8080/loginProc?oprGrpId="+getResources().getString(R.string.operating_group) + "&cnrCi=" + usrCid);
        //updateURL("http://192.168.10.210:8080/loginProc?oprGrpId="+getResources().getString(R.string.operating_group) + "&cnrCi=" + usrCid);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.i("deviceid", "REXYREX Device ID: called");
        switch (requestCode) {
            case Constants.REX_PERMISSIONS_REQUEST_DEVICE_INFO: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                    TelephonyManager tm = (TelephonyManager) getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
                    if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE)
                            != PackageManager.PERMISSION_GRANTED) {
                        // Permission is not granted
                        Log.i("deviceid", "REXYREX Device ID: NOT GRANTED");
                    } else {
                        Log.i("deviceid", "REXYREX Device ID: " + tm.getDeviceId());
                    }
                } else {
                    Log.i("deviceid", "REXYREX Device ID: DENIED");
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

    public WebView getWebView(){
        return wv;
    }

    public void updateURL(String newURL){
        mainURL = newURL;
        if(currentURLTV!=null){
            currentURLTV.setText(newURL);
        }
        wv.loadUrl(newURL);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            if(wv.canGoBack() && !wv.getUrl().equals("https://cherry.e4net.net/publish/MN-001.html") && !wv.getUrl().equals("https://cherry.e4net.net/moveMainPag")){
                wv.goBack();
            } else {
                long timeNow = System.currentTimeMillis();
                long tPassed = timeNow - lastBackAttemptTime;
                if(tPassed >1000){
                    lastBackAttemptTime = System.currentTimeMillis();
                    Toast.makeText(this, "뒤로 버튼을 한번 더 누르면 종료됩니다", Toast.LENGTH_SHORT).show();
                } else {
                    MainActivity.this.finish();
                }

//                View view = (LayoutInflater.from(MainActivity.this)).inflate(R.layout.exit_confirm, null);
//                Button exitAppBtn = (Button) view.findViewById(R.id.exitAppBtn);
//                Button cancelExitAppBtn = (Button) view.findViewById(R.id.cancelExitAppBtn);
//
//                AlertDialog.Builder rexAlertBuilder = new AlertDialog.Builder(MainActivity.this,R.style.ExitConfirmDialog);
//                rexAlertBuilder.setView(view);
//                final AlertDialog dialog = rexAlertBuilder.create();
//                dialog.show();
//                exitAppBtn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.cancel();
//                        MainActivity.this.finish();
//                    }
//                });
//                cancelExitAppBtn.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.cancel();
//                    }
//                });
            }
            //onBackPressed();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    private boolean appInstalledOrNot(String uri) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("package error", "PACKAGE NOT FOUND ERROR " + e);
        }

        return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            if(resultCode == Activity.RESULT_OK){
                String base64pic = data.getStringExtra("base64pic");
                Toast.makeText(this, base64pic, Toast.LENGTH_LONG);
                Log.d("mainmsg", "mainactivity says base 64 received!");
                Log.d("mainmsg", "mainactivity says base 64 is " + base64pic);
                //JSInterface jsInt = new JSInterface(this);
                //jsInt.setBase64Pic(base64pic);

                wv.loadUrl("javascript:caasFromApp.setPic('" + base64pic +"')");
                Log.d("mainmsg", "pic upload finished");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                Log.d("mainmsg", "critical photo error");
            }
        }

        if (requestCode == GALLERY_ACTIVITY_CODE) {
            if(resultCode == Activity.RESULT_OK){
                String picturePath = data.getStringExtra("picturePath");
                //perform Crop on the Image Selected from Gallery
                performCrop(picturePath);
            }
        }

        if (requestCode == RESULT_CROP ) {
            if(resultCode == Activity.RESULT_OK){
                Bundle extras = data.getExtras();
                Bitmap selectedBitmap = extras.getParcelable("data");

                Log.i("IMAGE_CROP", "IMAGE SIZE : " + selectedBitmap.getHeight());

                String encoded = PictureUtils.picTo64(selectedBitmap);
                wv.loadUrl("javascript:caasFromApp.setPic('" + encoded +"')");
                //Toast.makeText(this,"crop complete", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void performCrop(String picUri) {
        try {
            //Start Crop Activity

            Intent cropIntent = new Intent("com.android.camera.action.CROP");
            // indicate image type and Uri
            File f = new File(picUri);
            Uri contentUri = Uri.fromFile(f);

            cropIntent.setDataAndType(contentUri, "image/*");
            // set crop properties
            cropIntent.putExtra("crop", "true");
            // indicate aspect of desired crop
            cropIntent.putExtra("aspectX", 1);
            cropIntent.putExtra("aspectY", 1);
            // indicate output X and Y
            cropIntent.putExtra("outputX", 1500);
            cropIntent.putExtra("outputY", 1500);

            // retrieve data on return
            cropIntent.putExtra("return-data", true);
            // start the activity - we handle returning in onActivityResult
            startActivityForResult(cropIntent, RESULT_CROP);
        }
        // respond to users whose devices do not support the crop action
        catch (ActivityNotFoundException anfe) {
            // display an error message
            String errorMessage = "your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
        }
    }
}
