﻿Luniverse REST API Server
=====================

* Users
 * Retrieve User Balance
     * Service
         * /userBalance
     * Request Param
         * userWalletAddress
         * productTokenSymbol (optional, 없을 경우 MT잔액조회)
         * apikey
        * Response
         * {"result":true,"data":{"balance":"99999200"}}

 * Create User Account Data (Wallet)
     * Service
         * /createWallet
     * Request Param
         * userEmail
        * Response
         * {"result":true,"data":{"userId":“xxxxxxxxxxxxxx","address":“0xxxxxxxxxxxx"}
         * {"result":false,"code":"CONFLICT","message":"Deplicated resource found Can't complete the request"}

* Transactions
 * Transactions Analytics
     * Service
         * /transAnalytics
     * Request Param
         * fromDate
         * toDate
         * productTokenId
         * apikey
     * Response
         * { "result": true, "data": [{ "actionId": 8, "productTokenId": 2, "createdAt": "2018-08-24", "count": 2 }, { "actionId": 8, "productTokenId": 2, "createdAt": "2018-08-23", "count": 10 }]}

 * Transactions History
     * Service
         * /transHistory
     * Request Param
         * TxHash (optional)
         * ProductTokenId (optional)
         * Page (optional)
         * PerPage (optional)
         * apikey
     * Response
         * {"result":true,"data":[{"createdAt":"2018-11-28","txHash":"0x2f4b2776460a6ba874fda33b4a0172dbd9ea231850f5c3cd4ce4e303bdb649c9","senderAddress":"0x8867eaca298d95f7be7f9231f69b35135deda361","receiverAddress":"0x8867eaca298d95f7be7f9231f69b35135deda361","valueType":null,"valueAmount":"10000","mtId":106,"mtSymbol":"E4T","ptId":361,"ptSymbol":"CHY","actionId":677,"actionName":"DefaultTransfer","attributions":"{}","pdTs":1543398575322}]}

 * Token Transfer
     * Service
         * /tokenTransfer
     * Request Param
         * transferType (C2C ,B2C, C2B)
         * sendAddress
         * receiverAddress
         * valueAmount
         * apikey
     * Response
         *{ "result": true, "data": { "txId": 98, "txHash": "0xf1d4cc51521bc930277f663f8c86ad4e09307a55432f8401b8ef0747d3d61d44", "pdTs": 1535110345921 }}