/*
*
* author    yeongeun.seo ( yeongeun.seo@e4net.net )
* since     2018.11.01
* history   2018.11.01 최초작성
*
*/
const express = require('express');
const ipfilter = require('express-ipfilter').IpFilter;
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const request = require('request');
const requestIp = require('request-ip');
const format = require('string-format');
const logger = require('./logger');

const app = express();

/*
범위 사용 예시
192.168.0.10 ~ 192.168.0.20 안의 범위와 192.168.0.100 차단 or 허용,
var ips = [['192.168.0.10', '192.168.0.20'], '192.168.0.100'];
const ips = [['192.168.10.1', '192.168.10.253'], '192.168.10.202'];
['192.168.10.1', '192.168.10.254'],
app.use(ipfilter(ips, {mode: 'allow'})); // ips 목록의 ip들만 허용
app.use(ipfilter(ips)); // ips 목록의 ip들 차단
*/

let ips = [['192.168.10.1', '192.168.10.254'], '192.168.3.27', '127.0.0.1', 'localhost', '::1'];
app.use(ipfilter(ips, { mode: 'allow' }));
app.use(morgan('combined'));
app.use(bodyParser.json());
app.use(cors());

app.get('/hello', (req, res) => {
    const reqIP = requestIp.getClientIp(req);
    logger.debug('IP : ' + reqIP);
    logger.debug('IP : ::: ' + reqIP);

    const testData = '{"result":true,"data":[{"createdAt":"2018-11-20","count":2,"valueAmount":20 0,"actionId":585,"ptId":193},{"createdAt":"2018-11-07","count":4,"valueAmount":400,"actionId":585,"ptId":193},{"createdAt":"2018-11-07","count":3,"valueAmount":1600,"actionId":586,"ptId":193},{"createdAt":"2018-11-07","count":3,"valueAmount":160,"actionId":588,"ptId":304}]}';
    res.send(testData);
});

// Create User Account Data (Wallet)
app.get('/createWallet', (req, res) => {
    const baseURL = 'https://pre-api.luniverse.io/api/v1.0/wallet/users';
    const userEmail = req.query.userEmail; // User Email
    const apikey = req.query.apikey; // Side Token API
    const params = {
        'email': userEmail
    };

    logger.debug('============ Create User Account Data API Request ============');
    logger.debug('userEmail : ' + userEmail);
    logger.debug('api-key : ' + apikey);
    logger.debug('==============================================================');

    if(checkNull(userEmail)) { res.send(errMessage('checkNull', 'userEmail')); return false; }
    if(checkNull(apikey)) { res.send(errMessage('checkNull', 'apikey')); return false; }

    const apiOptions = {
        method: 'POST',
        url: baseURL,
        headers: {
            'Host': 'internal-tx-api.luniverse.com',
            'api-key': apikey,
            'Content-Type': 'application/json'
        },
        rejectUnauthorized: false,
        body: JSON.stringify(params)
    };

    request(apiOptions, function (error, response) {
        if (error) throw new Error(error);
        res.send(response.body);
        logger.debug('============ Create User Account Data API Response ============');
        logger.debug(response.body);
        logger.debug('==============================================================');
    });
});

// Token Transfer
app.get('/tokenTransfer', (req, res) => {
    const baseURL = 'https://pre-api.luniverse.io/tx-api/api/v1.0/transactions/action';
    const transferType = req.query.transferType; // Transfer Transaction Type
    const senderAddress = req.query.senderAddress; // senderAddress
    const receiverAddress = req.query.receiverAddress; // receiverAddress
    const valueAmount = req.query.valueAmount; // valueAmount
    const feePercent = 0; // feePercent
    let actionName = ''; // actionName req.query.actionName
    const apikey = req.query.apikey; // Side Token API
    let params = '';

    logger.debug('============ Token Transfer API Request ============');
    if (transferType === 'C2C') {
        actionName = 'DefaultTransfer';
        params = {
            'senderAddress': senderAddress,
            'receiverAddress': receiverAddress,
            'valueAmount': valueAmount,
            'feePercent': feePercent,
            'actionName': actionName
        };

        logger.debug('senderAddress : ' + senderAddress);
        logger.debug('receiverAddress : ' + receiverAddress);
        logger.debug('valueAmount : ' + valueAmount);
        logger.debug('feePercent : ' + feePercent);
        logger.debug('actionName : ' + actionName);
        logger.debug('api-key : ' + apikey);

        if(checkNull(senderAddress)) { res.send(errMessage('checkNull', 'senderAddress')); return false; }
        if(checkNull(receiverAddress)) { res.send(errMessage('checkNull', 'receiverAddress')); return false; }
        if(checkNull(valueAmount)) { res.send(errMessage('checkNull', 'valueAmount')); return false; }
        if(checkNull(apikey)) { res.send(errMessage('checkNull', 'apikey')); return false; }
    } else if (transferType === 'B2C') {
        actionName = 'B2CTransfer';
        params = {
            'receiverAddress': receiverAddress,
            'valueAmount': valueAmount,
            'actionName': actionName
        };

        logger.debug('receiverAddress : ' + receiverAddress);
        logger.debug('valueAmount : ' + valueAmount);
        logger.debug('actionName : ' + actionName);
        logger.debug('api-key : ' + apikey);

        if(checkNull(receiverAddress)) { res.send(errMessage('checkNull', 'receiverAddress')); return false; }
        if(checkNull(valueAmount)) { res.send(errMessage('checkNull', 'valueAmount')); return false; }
        if(checkNull(apikey)) { res.send(errMessage('checkNull', 'apikey')); return false; }
    } else if (transferType === 'C2B') {
        actionName = 'C2BTransfer';
        params = {
            'senderAddress': senderAddress,
            'valueAmount': valueAmount,
            'actionName': actionName
        };

        logger.debug('senderAddress : ' + senderAddress);
        logger.debug('valueAmount : ' + valueAmount);
        logger.debug('actionName : ' + actionName);
        logger.debug('api-key : ' + apikey);

        if(checkNull(senderAddress)) { res.send(errMessage('checkNull', 'senderAddress')); return false; }
        if(checkNull(valueAmount)) { res.send(errMessage('checkNull', 'valueAmount')); return false; }
        if(checkNull(apikey)) { res.send(errMessage('checkNull', 'apikey')); return false; }
    } else {
        res.send('Invalid transferType : ' + transferType);
        return false;
    }
    logger.debug('====================================================');

    const apiOptions = {
        method: 'POST',
        url: baseURL,
        headers: {
            'api-key': apikey,
            'Content-Type': 'application/json'
        },
        rejectUnauthorized: false,
        body: JSON.stringify(params)
    };

    request(apiOptions, function (error, response) {
        if (error) throw new Error(error);
        res.send(response.body);
        logger.debug('============ Token Transfer API Response ============');
        logger.debug(response.body);
        logger.debug('=====================================================');
    });
});

// Transactions Analytics
app.get('/transAnalytics', (req, res) => {
    var baseURL = 'https://pre-api.luniverse.io/tx-api/api/v1.0/transactions/analytics/';

    const mainTokenId = '106'; // E4T MainToken (FIX)
    const fromDate = req.query.fromDate; // FromDate
    const toDate = req.query.toDate; // ToDate
    const actionId = req.query.actionId; // actionId (optional)
    const productTokenId = req.query.productTokenId; // productTokenId (optional)
    const apikey = req.query.apikey; // Side Token API
    const params = {
        'actionId': actionId,
        'productTokenId': productTokenId
    };

    logger.debug('============ Transactions Analytics API Request ============');
    logger.debug('fromDate : ' + fromDate);
    logger.debug('toDate : ' + toDate);
    logger.debug('actionId : ' + actionId);
    logger.debug('productTokenId : ' + productTokenId);
    logger.debug('api-key : ' + apikey);
    logger.debug('============================================================');

    if(checkNull(fromDate)) { res.send(errMessage('checkNull', 'fromDate')); return false; }
    if(checkNull(toDate)) { res.send(errMessage('checkNull', 'toDate')); return false; }
    if(checkNull(apikey)) { res.send(errMessage('checkNull', 'apikey')); return false; }

    const apiOptions = {
        method: 'GET',
        url: baseURL + mainTokenId + '/' + fromDate + '/' + toDate,
        headers: {
            'api-key': apikey,
            'Content-Type': 'application/json'
        },
        rejectUnauthorized: false,
        params: params
    };

    request(apiOptions, function (error, response) {
        if (error) throw new Error(error);
        res.send(response.body);
        logger.debug('============ Transactions Analytics API Response ============');
        logger.debug(response.body);
        logger.debug('=============================================================');
    });
});

// Transactions History
app.get('/transHistory', (req, res) => {
    const baseURL = 'https://pre-api.luniverse.io/tx-api/api/v1.0/transactions/history/';
    const mainTokenId = '106'; // E4T MainToken (FIX)
    const txHash = req.query.txHash; // TxHash (optional)
    const productTokenId = req.query.productTokenId; // ProductTokenId (optional)
    const page = req.query.page; // Page (optional) Default 1
    const perPage = req.query.perPage; // PerPage (optional) Default 10 (up to 990)
    const apikey = req.query.apikey; // Side Token API

    const params = {
        'txHash': txHash,
        'productTokenId': productTokenId,
        'page': page,
        'perPage': perPage
    };

    logger.debug('============ Transactions History API Request ============');
    logger.debug('txHash : ' + txHash);
    logger.debug('productTokenId : ' + productTokenId);
    logger.debug('page : ' + page);
    logger.debug('perPage : ' + perPage);
    logger.debug('api-key : ' + apikey);
    logger.debug('==========================================================');

    if(checkNull(apikey)) { res.send(errMessage('checkNull', 'apikey')); return false; }

    const apiOptions = {
        method: 'GET',
        url: baseURL + mainTokenId,
        headers: {
            'api-key': apikey,
            'Content-Type': 'application/json'
        },
        rejectUnauthorized: false,
        params: params
    };

    request(apiOptions, function (error, response) {
        if (error) throw new Error(error);
        res.send(response.body);
        logger.debug('============ Transactions History API Response ============');
        logger.debug(response.body);
        logger.debug('===========================================================');
    });
});

// Retrieve User Balance
app.get('/userBalance', (req, res) => {
    const baseURL = 'https://pre-api.luniverse.io/tx-api/api/v1.0/wallet/users/';
    const sideURL = '/balances/token/';
    const userWalletAddress = req.query.userWalletAddress; // User's account address to inquire
    const mainTokenSymbol = 'E4T'; // Main Token Symbol (FIX)
    const productTokenSymbol = req.query.productTokenSymbol; // Product Token Symbol (optional)
    const apikey = req.query.apikey; // Side Token API

    logger.debug('============ Retrieve User Balance API Request ============');
    logger.debug('userWalletAddress : ' + userWalletAddress);
    logger.debug('mainTokenSymbol : ' + mainTokenSymbol);
    logger.debug('productTokenSymbol : ' + productTokenSymbol);
    logger.debug('api-key : ' + apikey);
    logger.debug('==========================================================');

    if(checkNull(userWalletAddress)) { res.send(errMessage('checkNull', 'userWalletAddress')); return false; }
    if(checkNull(productTokenSymbol)) { res.send(errMessage('checkNull', 'productTokenSymbol')); return false; }
    if(checkNull(apikey)) { res.send(errMessage('checkNull', 'apikey')); return false; }

    let lastURL = baseURL + userWalletAddress + sideURL;

    if(checkNull(productTokenSymbol)) {
        lastURL = lastURL + mainTokenSymbol;
    } else {
        lastURL = lastURL + mainTokenSymbol + '/' + productTokenSymbol;
    }

    const apiOptions = {
        method: 'GET',
        url: lastURL,
        headers: {
            'api-key': apikey,
            'Content-Type': 'application/json'
        },
        rejectUnauthorized: false
    };

    request(apiOptions, function (error, response) {
        if (error) throw new Error(error);
        res.send(response.body);
        logger.debug('============ Retrieve User Balance API Response ============');
        logger.debug(response.body);
        logger.debug('===========================================================');
    });
});

// Null Check
function checkNull(value) {
    if(value === '' || value === null || value === undefined || (value != null && typeof value === 'object' && !Object.keys(value).length)) {
        return true;
    } else {
        return false;
    }
}

// Error Message 처리
function errMessage(type, value) {
    let msg = '';

    if (type === 'checkNull') {
        msg = { 'result': false, 'code': 'CHERRY', 'message': format("Request Parameter is Null ( '{0}' )", value) };
        logger.error('ERROR | ' + JSON.stringify(msg));
    }

    return msg;
}

app.listen(process.env.PORT || 8081);