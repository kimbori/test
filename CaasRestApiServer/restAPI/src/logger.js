/*
*
* author    yeongeun.seo ( yeongeun.seo@e4net.net )
* since     2018.11.29
* history   2018.11.29 최초작성
*
*/

const winston = require('winston');
const winstonDaily = require('winston-daily-rotate-file');
require('date-utils');

const logger = winston.createLogger({
    level: 'debug', // 최소 레벨
    // 파일저장
    transports: [
        new winston.transports.DailyRotateFile({
            filename: 'logs/CaasRestApiServer.log',
            zippedArchive: true, // 압축여부
            format: winston.format.printf(
                info => `${new Date().toFormat('[YYYY/MM/DD HH24:MM:SS]')} [${info.level.toUpperCase()}] - ${info.message}`)
        }),
        // 콘솔 출력
        new winston.transports.Console({
            format: winston.format.printf(
                info => `${new Date().toFormat('[YYYY/MM/DD HH24:MM:SS]')} [${info.level.toUpperCase()}] - ${info.message}`)
        })
    ]
});

module.exports = logger;