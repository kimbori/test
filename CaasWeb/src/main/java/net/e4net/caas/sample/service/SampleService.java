package net.e4net.caas.sample.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.miemiedev.mybatis.paginator.domain.PageList;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.sample.mapper.SampleMapper;
import net.e4net.caas.sample.vo.SamplePVO;
import net.e4net.caas.sample.vo.SampleRVO;
import net.e4net.caas.common.base.BaseAbstractService;
import net.e4net.caas.common.service.BlockchainService;
import net.e4net.caas.common.util.MybatisUtils;

/**
 * SAMPLE 서비스
 *
 * @author djim
 * @since 2018.11.01
 */
@Service
@Slf4j
public class SampleService extends BaseAbstractService {

	@Autowired private SampleMapper sampleMapper;
	@Autowired private BlockchainService blockchainService;
    
    /**
     * SAMPLE 조회
     *
     * @param pvo SAMPLE 조회 조건
     * @return 검색결과
     */
    public PageList<SampleRVO> selectSampleList(SamplePVO pvo) {
        return sampleMapper.selectSampleList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * SAMPLE 등록
     *
     * @param pvo SAMPLE 등록 정보
     * @return 등록결과
     */
    public boolean insertSample(SamplePVO pvo) {
        return sampleMapper.insertSample(pvo) > 0;
    }
    
    /**
     * SAMPLE 수정
     *
     * @param pvo SAMPLE 수정 정보
     * @return 수정결과
     */
    public boolean updateSample(SamplePVO pvo) {
        return sampleMapper.updateSample(pvo) > 0;
    }
    
    /**
     * SAMPLE 삭제
     *
     * @param pvo SAMPLE 수정 정보
     * @return 수정결과
     */
    public boolean deleteSample(SamplePVO pvo) {
        return sampleMapper.deleteSample(pvo) > 0;
    }

    /* ==================================================================================
     * 	트랜젝션 테스트
     */
    public void transactionRollbackTest(boolean onFail) {
    	transactionRollbackTestModule("External", "TEST_ROLLBACK", 1, 0);
    	blockchainService.transactionRollbackTest();
    	if(onFail) {
    		throw new RuntimeException();
    	}
    }

    /*
     * 트렌젝션을 테스트 하기 위한 모듈이다.
     * 	외부 메소드에서 rollback이 발생되는 경우
     * 	일반적으로는 외부에서 호출한 내부 메소드도 rollback 한다.
     * 	반면에 내부 메소드에서는 rollback이 일어나질 않길 바라는 경우도 있다.
     * 참조
     * 	http://redgura.tistory.com/422 - @Transaction 사용 시 내부 메소드 간의 호출
     */
    public void transactionRollbackTestModule(String logTtl, String key, int addVal1, int addVal2) {
    	int ret = 0;
    	SampleRVO vo = getTransactionRollbackTestVo(key);
    	SamplePVO pvo = new SamplePVO();
    	pvo.setKey(key);
    	if(vo == null) {
    		pvo.setVal1(10);
    		pvo.setVal2(10);
    		ret = sampleMapper.insertSample(pvo);
    	} else {
    		pvo.setVal1(vo.getVal1() + addVal1);
    		pvo.setVal2(vo.getVal2() + addVal2);
    		ret = sampleMapper.updateSample(pvo);    		
    	}
    	transactionRollbackTestVoLog(logTtl + " ret=" + ret, key, vo);
    }
 
    public SampleRVO getTransactionRollbackTestVo(String key) {
    	return sampleMapper.getSample(key);
    }

    public void transactionRollbackTestVoLog(String logTtl, String key, SampleRVO vo) {
    	SampleRVO vo2 = getTransactionRollbackTestVo(key);
    	String logOut = " ";
    	if(vo == null) logOut= "before=empty";
    	else logOut = "before=(" + vo.getVal1() + "," + vo.getVal2() + ")";
    	logOut += ", after=(" + vo2.getVal1() + "," + vo2.getVal2() + ")";
		log.debug("########## " + logTtl + ", key=" + key + logOut);
    }
   
}
