package net.e4net.caas.sample.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.sample.service.SampleService;
import net.e4net.caas.sample.vo.SamplePVO;
import net.e4net.caas.sample.vo.SampleRVO;
import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;


/**
 * SAMPLE 컨트롤러
 *
 * @author djim
 * @since 2018.11.01
 * history  2018.11.01 최초작성
 */
@Controller
@Slf4j
public class SampleController extends AbstractController {
	
    @Autowired private SampleService sampleService;
    
    /**
     * method sample조회 화면 이동
     */
    @RequestMapping("/sample/moveSamplePag")
    public String commonCdMgmt() {
    	log.debug("===== move sample page =====");
		return "sample/samplePag";
    }
//    @RequestMapping(value = "/sample/moveSamplePag", method = RequestMethod.GET)
//    public String commonCdMgmt(Model model, HttpServletRequest request) throws Exception {
//    	debug("===== move sample page =====");
//		return "sample/samplePag";
//    }

    /**
     * method sample 조회
     */
    @RequestMapping("/sample/selectSampleList")
    public String selectSampleList(SamplePVO pvo, ModelMap model) {
        log.debug("=====  select sample List =====");

        //pvo.setCountPage(3);
        //pvo.setPageNo("2");
        PageList<SampleRVO> resultList = sampleService.selectSampleList(pvo);
        model.addAttribute("totalCnt", resultList.getPaginator().getTotalCount());
        model.addAttribute("resultList", resultList);

        return "sample/sampleListAjax :: sampleListAjax";
    }
//    @RequestMapping(value = "/sample/selectSampleList", method = RequestMethod.POST)
//    public ModelAndView selectSampleList(@ModelAttribute SamplePVO pvo) throws Exception {
//        debug("=====  select sample List =====");
//        
//        ModelAndView mav = new ModelAndView("sample/sampleListAjax :: sampleListAjax");
//        List<SampleRVO> resultList = sampleService.selectSampleList(pvo);
//        mav.addObject("totalCnt", resultList.size() == 0 ? "0" : resultList.get(0).getTotalCnt());
//        mav.addObject("resultList", resultList);
//
//        return getOkModelAndView(mav);
//    }
   
    /**
     * method sample 등록
     */
    @RequestMapping("/sample/insertSample")
    public String insertSample(SamplePVO pvo, ModelMap model) {
    	debug("=====  insert sample  =====");
        Status status = new Status();

        try {
	        if(sampleService.insertSample(pvo)) {
	        	status.setOk();
	        } else {
	            status.setFail("샘플 등록에 실패하였습니다.");
	        }
        } catch(DuplicateKeyException e) {
        	log.error("Exception. msg=", e.getMessage());
            status.setFail("중복된 데이터가 존재합이다.");
        }
    	model.addAttribute(status);
        return "jsonView";
    }
//    @RequestMapping(value = "/sample/insertSample", method = RequestMethod.POST)
//    public ModelAndView insertSample(@ModelAttribute SamplePVO pvo) {
//    	debug("=====  insert sample  =====");
//        ModelAndView mav = new ModelAndView("jsonView");
//        Status status = new Status();
//        
//        try {
//            if ( sampleService.insertSample(pvo) > 0 ) {
//            	status.setOk(0);
//            	return getOkModelAndView(mav, status);
//            } else {
//                status.setFail("샘플 등록에 실패하였습니다.");
//                return getFailModelAndView(mav, status);
//            }
//        } catch (Exception e){
//            status.setFail("샘플 등록에 실패하였습니다.");
//            return getFailModelAndView(mav, status);
//        }
//    }
    
    /**
     * method sample 수정
     */
    @RequestMapping("/sample/updateSample")
    public String updateSample(SamplePVO pvo, ModelMap model) {
    	log.debug("=====  update sample  =====");
        Status status = new Status();
        
        if(sampleService.updateSample(pvo)) {
        	status.setOk();
        } else {
            status.setFail("샘플 수정에 실패하였습니다.");
        }
    	model.addAttribute(status);
        return "jsonView";
    }
    
    /**
     * method sample 삭제
     */
    @RequestMapping("/sample/deleteSample")
    public String deleteSample(SamplePVO pvo, ModelMap model) {
    	log.debug("=====  delete sample  =====");
        Status status = new Status();
        
        if(sampleService.deleteSample(pvo)) {
        	status.setOk();
        } else {
            status.setFail("샘플 삭제에 실패하였습니다.");
        }
    	model.addAttribute(status);
        return "jsonView";
    }
}
