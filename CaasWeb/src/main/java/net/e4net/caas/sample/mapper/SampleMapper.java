package net.e4net.caas.sample.mapper;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;

import net.e4net.caas.sample.vo.SamplePVO;
import net.e4net.caas.sample.vo.SampleRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author djim
 * @since 2018.11.01
 */
@Repository
public interface SampleMapper {
	
    PageList<SampleRVO> selectSampleList(SamplePVO pvo, PageBounds pageBounds);
    SampleRVO getSample(String key);
    
    int insertSample(SamplePVO pvo);
 	int updateSample(SamplePVO pvo);
    int deleteSample(SamplePVO pvo);
}
