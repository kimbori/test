package net.e4net.caas.main.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.dntn.vo.CmpPVO;
import net.e4net.caas.dntn.vo.CmpRVO;
import net.e4net.caas.main.service.MainService;
import net.e4net.caas.main.vo.MctGrpPVO;
import net.e4net.caas.main.vo.MctGrpRVO;

/**
 * 기부메인 컨트롤러
 *
 * @author brkim
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
public class MainController extends AbstractController {
	
    @Autowired private MainService service;
    
    /**
     * <pre>
     * 메인 페이지
     * </pre>
     *
     * @param SrchPVO 조회조건
     * @return
     */
    @RequestMapping("/moveMainPag")
    public String moveMainPag(ModelMap model) {
    	model.addAttribute("oprGrpId", getLoginOprGrpId());
		return "main/mainPag";
    }
    
    /**
     * <pre>
     * 관심단체(=가맹단체) 조회
     * </pre>
     *
     * @param RcepPVO 조회조건 값(현재 default: cherry)
     * @return List<DntnRVO> 관심단체 리스트
     */
    @RequestMapping(value = "/main/cncrAjax", method = RequestMethod.POST)
    public ModelAndView selectCncrAjax(@ModelAttribute MctGrpPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("main/mainCncrAjax :: cncrAjax");
        
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	if (uVO instanceof UserVO) {
    		pvo.setOprGrpId(uVO.getOprGrpId());
    		pvo.setCnrSq(uVO.getCnrSq());
    	}
        List<MctGrpRVO> dntnCncrLst = service.selectDntnCncrList(pvo);
        
        mav.addObject("dntnCncrLst", dntnCncrLst);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 가맹단체 조회
     * </pre>
     *
     * @param RcepPVO 조회조건 값(현재 default: cherry)
     * @return List<DntnRVO> 가맹단체 리스트
     */
    @RequestMapping(value = "/main/mctAjax", method = RequestMethod.POST)
    public ModelAndView selectMctAjax(@ModelAttribute MctGrpPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("main/mainMctAjax :: mctAjax");
        
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	if (uVO instanceof UserVO) {
    	    pvo.setOprGrpId(uVO.getOprGrpId());
    	    pvo.setCnrSq(uVO.getCnrSq());
    	}
        
        List<MctGrpRVO> dntnMctLst = service.selectDntnMctList(pvo);
        
        mav.addObject("totCnt", dntnMctLst.size() == 0 ? "0" : dntnMctLst.get(0).getTotalCnt());
        mav.addObject("dntnMctLst", dntnMctLst);

        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 관심가맹단체 등록
     * </pre>
     *
     * @param RcepPVO 입력정보
     * @return status 가맹단체등록성공여부
     */
    @RequestMapping(value = "/main/cncrIns", method = RequestMethod.POST)
    public ModelAndView insertCncr(@ModelAttribute MctGrpPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();

    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setCnrSq(uVO.getCnrSq());
        
        try {
        	if(pvo.getMrchGrpId() == null || "".equals(pvo.getMrchGrpId())) {
        		status.setFail("등록할 단체가 선택되지 않았습니다.<br>선택 후 다시 시도해 주시기 바랍니다.");
        		return getFailModelAndView(mav, status);
        	}
        	int cnt = service.insertDntnCncr(pvo);
        	if(cnt <= 0) {
        		status.setFail("관심 단체 등록에 실패하였습니다.");
            	return getFailModelAndView(mav, status);
        	}
        } catch(Exception e) {
        	status.setFail("관심 단체 등록에 실패하였습니다.");
        	return getFailModelAndView(mav, status);
        }
        status.setOk("관심 단체 등록이 완료되었습니다.");
        return getOkModelAndView(mav, status);
    }
    
    /**
     * <pre>
     * 관심가맹단체 해지
     * </pre>
     *
     * @param RcepPVO 입력정보
     * @return status 가맹단체해지성공여부
     */
    @RequestMapping(value = "/main/cncrDel", method = RequestMethod.POST)
    public ModelAndView deleteCncr(@ModelAttribute MctGrpPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();

    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setCnrSq(uVO.getCnrSq());
        
        try {
        	if(pvo.getCncrGrpSq() == 0) {
        		status.setFail("해제할 단체가 선택되지 않았습니다.<br>선택 후 다시 시도해 주시기 바랍니다.");
        		return getFailModelAndView(mav, status);
        	}
        	int cnt = service.deleteDntnCncr(pvo);
        	if(cnt <= 0) {
        		status.setFail("관심 단체 해지에 실패하였습니다.");
            	return getFailModelAndView(mav, status);
        	}
        } catch(Exception e) {
        	status.setFail("관심 단체 해지에 실패하였습니다.");
        	return getFailModelAndView(mav, status);
        }
        status.setOk("관심 단체 해지를 완료하였습니다.");
        return getOkModelAndView(mav, status);
    }
    
    /**
     * <pre>
     * 기부 TODAY 참여자, 모금현황 조회
     * </pre>
     *
     * @param SrchPVO 조회조건
     * @return CmpRVO 기부 TODAY 참여자수, 모금액
     */
    @RequestMapping(value = "/main/tdyAjax", method = RequestMethod.POST)
    public ModelAndView selectTdyAjax(@ModelAttribute CmpPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("main/mainTdyAjax :: tdyInf");
        
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	if (uVO instanceof UserVO) {
    	    pvo.setOprGrpId(uVO.getOprGrpId());
    	}
    	
    	/* TODAY 참여자수, 모금액 */
    	CmpRVO dntnTdy = service.selectTodayDntn(pvo);
        
        mav.addObject("dntnTdy", dntnTdy);
        
        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 기부 리스트 조회
     * </pre>
     *
     * @param SrchPVO 조회조건 
     * @return List<CmpRVO> 기부 리스트, CmpRVO 기본후원
     */
    @RequestMapping(value = "/main/dntnAjax", method = RequestMethod.POST)
    public ModelAndView selectDntnAjax(@ModelAttribute CmpPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("main/mainAjax :: dntnLst");
        
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	if (uVO instanceof UserVO) {
    	    pvo.setOprGrpId(uVO.getOprGrpId());
    	}
    	
        /* 기부 리스트 조회 */
        List<CmpRVO> dntnLst = service.selectDntnList(pvo);
        
        int totCnt = dntnLst.size() == 0 ? 0 : dntnLst.get(0).getTotalCnt();
        int pageNo = StringUtils.parseInt(pvo.getPageNo(), 1);
        boolean hasNext = totCnt - (pageNo * pvo.getCountPage()) > 0;
        
        /* 기본후원 조회 */
    	CmpRVO dntnDft = service.selectDntnDft(pvo);
        boolean existYn = (dntnDft == null) ? false : ("".equals(pvo.getMrchGrpId())) ? false : true;
        
        mav.addObject("dntnDft", dntnDft);
        mav.addObject("existYn", existYn);
        mav.addObject("hasNext", hasNext);
        mav.addObject("dntnLst", dntnLst);
        mav.addObject("mrchGrpNm", pvo.getMrchGrpNm());

        return getOkModelAndView(mav);
    }
}