package net.e4net.caas.main.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.vo.dvo.TbNotcLstDVO;
import net.e4net.caas.main.mapper.NotcMapper;
import net.e4net.caas.main.vo.NotcRVO;

/**
 * 알람 서비스
 *
 * @author brkim
 * @since 2018.12.10
 */
@Service
public class NotcService extends AbstractDao {
    @Autowired
    private NotcMapper mapper;
    
    /**
     * 알람 리스트 조회
     *
     * @param  SrchPVO 조회조건
     * @return List<TbRcntSrchDVO> 최근검색 리스트
     */
    public List<NotcRVO> selectNotcList(TbNotcLstDVO pvo) {
    	pvo.setOrderByColumn("FST_REG_DTTM"); //정렬 : 날짜 역순 표시
    	pvo.setAscDesc("desc");
        return mapper.selectNotcList(pvo, MybatisUtils.pageBounds(pvo));
    }
}