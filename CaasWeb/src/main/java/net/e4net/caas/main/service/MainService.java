package net.e4net.caas.main.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.dntn.vo.CmpPVO;
import net.e4net.caas.dntn.vo.CmpRVO;
import net.e4net.caas.main.mapper.MainMapper;
import net.e4net.caas.main.vo.MctGrpPVO;
import net.e4net.caas.main.vo.MctGrpRVO;

/**
 * 기부메인 서비스
 *
 * @author brkim
 * @since 2018.11.20
 */
@Service
public class MainService extends AbstractDao {
    @Autowired
    private MainMapper mapper;
    
    /**
     * 관심가맹단체(=자선단체) 조회
     *
     * @param  pvo 조회조건 값(현재 default: cherry)
     * @return 관심가맹단체 리스트
     */
    public List<MctGrpRVO> selectDntnCncrList(MctGrpPVO pvo) {
        return mapper.selectDntnCncrList(pvo);
    }
    
    /**
     * 가맹단체(=자선단체) 조회
     *
     * @param  pvo 조회조건 값(현재 default: cherry)
     * @return 가맹단체 리스트
     */
    public List<MctGrpRVO> selectDntnMctList(MctGrpPVO pvo) {
    	pvo.setOrderByColumn("FST_REG_DTTM"); //정렬 : 단체 등록한 일시 순
    	pvo.setAscDesc("asc");	
        return mapper.selectDntnMctList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * TODAY 참여자수, 모금액
     */
    public CmpRVO selectTodayDntn(CmpPVO pvo) {
    	return mapper.selectTodayDntn(pvo);
    }
    
    /**
     * 기부 리스트 조회
     *
     * @param pvo 기부 리스트 조회 조건
     * @return 기부(캠페인) 리스트
     */
    public List<CmpRVO> selectDntnList(CmpPVO pvo) {
    	pvo.setOrderByColumn("FST_REG_DTTM"); //정렬 : 프로젝트(기부) 등록 역순
    	pvo.setAscDesc("desc");	
        return mapper.selectDntnList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 기본후원 조회
     */
    public CmpRVO selectDntnDft(CmpPVO pvo) {
    	return mapper.selectDntnDft(pvo);
    }
    
    /**
     * 관심가맹단체 등록
     *
     * @param pvo 가맹단체정보
     * @return 등록건수
     */
    public int insertDntnCncr(MctGrpPVO pvo) {
    	TransactionStatus stat = null;
    	int cnt = 0;
    	
    	try {
    		stat = getTransactionStatus("insertCncr");
    		cnt = mapper.insertDntnCncr(pvo);
    		if(cnt != 1) {
        		throw new CaasException("99999", "관심가맹단체 등록 시 오류발생");
        	}
    		transactionCommit(stat);
    	} catch (Exception e) {
    		error("deleteSrch", e);
    		transactionRollback(stat);
    	}
    	return cnt;
    }
    
    /**
     * 관심가맹단체 해지
     *
     * @param pvo 가맹단체정보
     * @return 해지건수
     */
    public int deleteDntnCncr(MctGrpPVO pvo) {
    	TransactionStatus stat = null;
    	int cnt = 0;
    	
    	try {
    		stat = getTransactionStatus("deleteCncr");
    		cnt = mapper.deleteDntnCncr(pvo);
    		if(cnt != 1) {
        		throw new CaasException("99999", "관심가맹단체 해지 시 오류발생");
        	}
    		transactionCommit(stat);
    	} catch (Exception e) {
    		error("deleteSrch", e);
    		transactionRollback(stat);
    	}
    	return cnt;
    }
}