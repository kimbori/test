package net.e4net.caas.main.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbNotcLstDVO;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
public class NotcRVO extends TbNotcLstDVO {
	private String notcGbVal;			//알림구분명
}