package net.e4net.caas.main.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;


import net.e4net.caas.common.vo.dvo.TbRcntSrchDVO;
import net.e4net.caas.main.vo.SrchPVO;

/**
 * 검색 매핑
 *
 * @author brkim
 * @since 2018.12.04
 */
@Repository
public interface SrchMapper {
	
	/**
     * 최근검색 리스트
     */
    public List<TbRcntSrchDVO> selectRcntSrchList(SrchPVO pvo);
    
    /**
     * 검색정보 등록
     */
    public int insertSrch(SrchPVO pvo);
    
    /**
     * 검색정보 삭제
     */
    public int deleteSrch(SrchPVO pvo);
}