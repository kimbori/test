package net.e4net.caas.main.vo;

import lombok.Data;

import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbGrpMctDVO;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
public class MctGrpRVO extends TbGrpMctDVO {
	private String strtTag;	//시작태그
	private String endTag;	//종료태그
}