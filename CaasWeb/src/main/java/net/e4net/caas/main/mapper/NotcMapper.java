package net.e4net.caas.main.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.common.vo.dvo.TbNotcLstDVO;
import net.e4net.caas.main.vo.NotcRVO;

/**
 * 알람 매핑
 *
 * @author brkim
 * @since 2018.12.10
 */
@Repository
public interface NotcMapper {
	
	/**
     * 알람 리스트
     */
    public List<NotcRVO> selectNotcList(TbNotcLstDVO pvo, PageBounds pageBounds);
}