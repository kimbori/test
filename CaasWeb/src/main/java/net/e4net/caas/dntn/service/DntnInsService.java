package net.e4net.caas.dntn.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.service.BlockchainService;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.common.vo.dvo.TbDntLstDVO;
import net.e4net.caas.dntn.mapper.DntnInsMapper;
import net.e4net.caas.my.mapper.FxprBllMapper;
import net.e4net.caas.my.service.BllMgmtService;
import net.e4net.caas.my.vo.CardInfoPVO;
import net.e4net.caas.my.vo.CardInfoRVO;
import net.e4net.caas.my.vo.FxprBllHstDVO;
import net.e4net.caas.my.vo.FxprBllPVO;

/**
 * 기부하기 서비스
 *
 * @author brkim
 * @since 2018.11.20
 */
@Service
public class DntnInsService extends AbstractDao {
    @Autowired
    private DntnInsMapper mapper;
    
    @Autowired
    private FxprBllMapper fxprBllMapper;
    
    @Autowired
	private BllMgmtService bllMgmtService;
    
    @Autowired 
    private BlockchainService bcService;
    
    /**
     * 기부내역 등록
     *
     * @param pvo 기부등록 정보
     * @return 등록건수
     */
    public int insertDntn(UserVO uVO, TbDntLstDVO pvo) {
    	TransactionStatus stat = null;
    	int cnt = 0;
    	
    	try {
    		/* 지갑잔액조회 */
        	BlockchainRetrunVO rlt = bcService.getWalletBalance(uVO.getOprGrpId(), uVO.getWltAddr());
        	if(!rlt.isSuccess()) {
        		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
        	}
        	long wltLv = rlt.getBalance();
        	if(wltLv < pvo.getDntnAmt()) {
        		throw new CaasException("99999", "보유하신 체리가 부족합니다.\\n부족한 체리를 충전 후 기부를 진행해 주시기 바랍니다.");
        	}
        	
        	/* 블록체인 체리전송(기부자->캠페인_c2c=31) */
        	BlockchainRetrunVO vo = bcService.walletTokenTransfer(uVO.getOprGrpId(), uVO.getWltAddr(), pvo.getWltAddr(), pvo.getDntnAmt(), "31");
        	if(!vo.isSuccess()) {
        		throw new CaasException("99999", "블록체인 API 연계 시 오류발생-!!");
        	}
        	
        	pvo.setToknTrnfSq(vo.getToknTrnfSq()); //토큰전송 SEQ
        	
    		pvo.setNthnDntnYn("N");		//N:기본(d), Y:무기명요청
    		pvo.setRecpIssuYn("N");		//N:미발행(d), Y:발급
    		pvo.setInhgvtaxNtfyYn("N");	//N:미포함(d), Y:포함, X:제외대상
    		
    		stat = getTransactionStatus("insertDntn");
    		cnt = mapper.insertDntn(pvo); //기부내역 insert
    		if(cnt != 1) {
        		throw new CaasException("99999", "기부내역 등록 시 오류발생");
        	}
    		
    		cnt = mapper.updateDntn(pvo); //캠페인 모금토큰 update
    		if(cnt != 1) {
        		throw new CaasException("99999", "모금토큰 update 시 오류발생");
        	}
    		transactionCommit(stat);
    	} catch (Exception e) {
    		error("insertDntn", e);
    		transactionRollback(stat);
    	}
    	return cnt;
    }
    
    /**
     * 정기기부 등록
     *
     * @param pvo 기부등록 정보
     * @return 등록건수
     */
    public int insertDntnFxpr(FxprBllPVO pvo) {
    	TransactionStatus stat = null;
    	int cnt = 0;
    	try {
    		stat = getTransactionStatus("insertDntnFxpr");
    		
    		//대표카드 빌링SQ 조회
    		CardInfoPVO cdPvo = new CardInfoPVO();
    		cdPvo.setCnrSq(pvo.getCnrSq());
    		CardInfoRVO rvo = bllMgmtService.selectBllCard(cdPvo);
    		if(rvo == null) {
    			throw new CaasException("9999", "등록된 대표카드 없음");
    		}

    		//SEQ 조회
    		long fxprDntnSq = mapper.selectFxprDntnSq();
    		
    		pvo.setFxprDntnSq(fxprDntnSq);
    		pvo.setFxprStat("01");
    		pvo.setBllAuthSq(rvo.getBllAuthSq());
    		pvo.setStatYm(pvo.getStatYm().replaceAll("\\.", "").substring(0, 6));
    		pvo.setEndYm(pvo.getEndYm().replaceAll("\\.", "").substring(0, 6));
    		
    		cnt = mapper.insertDntnFxpr(pvo);
    		if(cnt != 1) {
    			throw new CaasException("9999", "정기기부 등록 시 오류발생");
    		}
    		
    		FxprBllHstDVO dvo = fxprBllMapper.selectFxprBllInfo(pvo);
    		dvo.setFxprDntnSq(fxprDntnSq);
    		dvo.setCnrSq(pvo.getCnrSq());
    		dvo.setFxprPcsnStat("01");
    		dvo.setStatYm(dvo.getStatYm().replaceAll("\\.", "").substring(0, 6));
    		dvo.setEndYm(dvo.getEndYm().replaceAll("\\.", "").substring(0, 6));
    		cnt = fxprBllMapper.insertFxprBllHst(dvo);
    		if(cnt != 1) {
    			throw new CaasException("9999", "정기기부 이력 등록 시 오류발생");
    		}
    		transactionCommit(stat);
    	} catch (Exception e) {
    		error("insertDntnFxpr", e);
    		transactionRollback(stat);
    	}
    	return cnt;
    }
}