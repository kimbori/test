package net.e4net.caas.dntn.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
public class CmpPVO extends PageVO {
	private long cmpSq;				//캠페인 SEQ
	private String cmpNm;			//캠페인명
	private String oprGrpId;		//운영단체ID
	private String mrchGrpId;		//가맹단체 ID
	private String mrchGrpNm;		//가맹단체명
}