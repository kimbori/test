package net.e4net.caas.dntn.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbCmpInfDVO;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
public class CmpRVO extends TbCmpInfDVO {
	private int no;					//NO
	private String dnmrKnNm;		//모금종류(한글명)
	private String dnmrKnVal;		//모금종류(화면노출)
	private int pctTokn;			//토큰 퍼센트
	
	/* 비용사용계획 */
	private int od;					//순서
	private String usePlan;			//사용계획
	private int useAmt;				//사용금액
	
	/* 캠페인 이미지 */
	private String imgPth;			//이미지패스
	private String imgInf;			//이미지정보
	
	/* 캠페인소식 */
	private String regDttm;			//등록일시
	private String title;			//제목
	private String cn;				//내용
	
	/* 응원글 */
	private String cntNm;			//기부자명
	private String nickNm;			//기부자닉네임
	private String prfImg;			//프로필이미지
	private String cmmt;			//응원글
	private String dntnDttm;		//기부일시
}