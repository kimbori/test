package net.e4net.caas.dntn.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.dntn.mapper.DntnDtlMapper;
import net.e4net.caas.dntn.vo.CmpPVO;
import net.e4net.caas.dntn.vo.CmpRVO;

/**
 * 기부상세 서비스
 *
 * @author brkim
 * @since 2018.11.20
 */
@Service
public class DntnDtlService extends AbstractDao {
    @Autowired
    private DntnDtlMapper mapper;
    
    /**
     * 기부 이미지 리스트 조회
     *
     * @param pvo seq
     * @return 기부 응원글 리스트
     */
    public List<CmpRVO> selectDntnImg(CmpPVO pvo) {
        return mapper.selectDntnImg(pvo);
    }
    
    /**
     * 기부 상세 조회
     *
     * @param pvo seq
     * @return 기부상세정보
     */
    public CmpRVO selectDntnDtl(CmpPVO pvo) {
        return mapper.selectDntnDtl(pvo);
    }
    
    /**
     * 기부 비용 사용계획 조회
     *
     * @param pvo seq
     * @return 기부 비용 사용계획 리스트
     */
    public List<CmpRVO> selectDntnCost(CmpPVO pvo) {
        return mapper.selectDntnCost(pvo);
    }
    
    /**
     * 기부 소식 리스트 조회
     *
     * @param pvo seq
     * @return 기부 소식 리스트
     */
    public List<CmpRVO> selectDntnNews(CmpPVO pvo) {
    	pvo.setOrderByColumn("FST_REG_DTTM");	//정렬 : 프로젝트(기부) 등록 역순
    	pvo.setAscDesc("desc");
        return mapper.selectDntnNews(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 기부 응원글 조회
     *
     * @param pvo seq
     * @return 기부 응원글 리스트
     */
    public List<CmpRVO> selectDntnCmmt(CmpPVO pvo) {
    	pvo.setOrderByColumn("FST_REG_DTTM");	//정렬 : 기부일시 역순
    	pvo.setAscDesc("desc");
        return mapper.selectDntnCmmt(pvo, MybatisUtils.pageBounds(pvo));
    }
}