package net.e4net.caas.dntn.mapper;

import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.dvo.TbDntLstDVO;
import net.e4net.caas.my.vo.FxprBllPVO;

/**
 * 기부하기 매핑
 *
 * @author brkim
 * @since 2018.11.20
 */
@Repository
public interface DntnInsMapper {
	
    /**
     * 기부 등록
     */
    public int insertDntn(TbDntLstDVO pvo);
    
    /**
     * 정기기부 등록
     */
    public int insertDntnFxpr(FxprBllPVO pvo);
    
    /**
     * SEQ 조회
     */
    public long selectFxprDntnSq();
    
    /**
     * 기부(캠페인정보) 수정_모금토큰update
     */
    public int updateDntn(TbDntLstDVO pvo);
}