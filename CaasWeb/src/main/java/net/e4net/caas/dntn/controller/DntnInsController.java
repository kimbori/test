package net.e4net.caas.dntn.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.Status;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.service.BlockchainService;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.common.vo.dvo.TbDntLstDVO;
import net.e4net.caas.dntn.service.DntnDtlService;
import net.e4net.caas.dntn.service.DntnInsService;
import net.e4net.caas.dntn.vo.CmpPVO;
import net.e4net.caas.dntn.vo.CmpRVO;
import net.e4net.caas.my.vo.FxprBllPVO;

/**
 * 기부하기 컨트롤러
 *
 * @author brkim
 * @since 2018.11.20
 * history  2018.11.20 최초작성
 */
@Controller
public class DntnInsController extends AbstractController {
	
	@Autowired
	private DntnDtlService dtlService;
	 
    @Autowired
    private DntnInsService service;
    
    @Autowired 
    private BlockchainService bcService;
    
    /**
     * <pre>
     * 기부하기 페이지 이동
     * </pre>
     *
     * @param SrchPVO 조회조건
     * @return CmpRVO 기부상세
     */
    @RequestMapping(value = "/moveDntnIns", method = RequestMethod.GET)
    public ModelAndView moveDntnPag(@ModelAttribute CmpPVO pvo) throws Exception {
    	ModelAndView mav = new ModelAndView("dntn/dntnInsPag");
    	
    	/* 기부소개 */
        CmpRVO dntnDtl = dtlService.selectDntnDtl(pvo);
        mav.addObject("dntnDtl", dntnDtl);
        
		return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 보유체리조회
     * </pre>
     *
     * @param 
     * @return wltLv 체리
     */
    @RequestMapping(value = "/dntn/mycherry", method = RequestMethod.POST)
    public ModelAndView selectMyCerry(Model model) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();

    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        try {
        	/* 지갑잔액조회 */
        	BlockchainRetrunVO rlt = bcService.getWalletBalance(uVO.getOprGrpId(), uVO.getWltAddr());
        	if(!rlt.isSuccess()) {
        		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
        	}
        	long wltLv = rlt.getBalance();

            mav.addObject("wltLv", wltLv);
        } catch(Exception e) {
        	status.setFail("잔액이 정상적으로 조회되지 않았습니다.");
        	return getFailModelAndView(mav, status);
        }
        status.setOk();
        return getOkModelAndView(mav);
    }
    
    /**
     * <pre>
     * 기부하기(일시후원)
     * </pre>
     *
     * @param TbDntLstDVO 입력정보
     * @return status 기부성공여부
     */
    @RequestMapping(value = "/dntn/dntnIns", method = RequestMethod.POST)
    public ModelAndView insertDntnCncr(@ModelAttribute TbDntLstDVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();

    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setCnrSq(uVO.getCnrSq());
        
        try {
        	int insCnt = service.insertDntn(uVO, pvo);
        	if(insCnt != 1) {
        		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
        	}
        } catch(Exception e) {
        	status.setFail("\"기부가 정상적으로 처리되지 않았습니다.\"");
        	return getFailModelAndView(mav, status);
        }
        status.setOk("\"기부가 완료되었습니다.\"");
        return getOkModelAndView(mav, status);
    }
    
    /**
     * <pre>
     * 기부하기(정기후원)
     * </pre>
     *
     * @param TbDntLstDVO 입력정보
     * @return status 기부성공여부
     */
    @RequestMapping(value = "/dntn/dntnFxprIns", method = RequestMethod.POST)
    public ModelAndView insertDntnFxpr(@ModelAttribute FxprBllPVO pvo) throws Exception {
        ModelAndView mav = new ModelAndView("jsonView");
        Status status = new Status();

    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setCnrSq(uVO.getCnrSq());
        
        try {
        	int insCnt = service.insertDntnFxpr(pvo);
        	if(insCnt != 1) {
        		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
        	}
        } catch(Exception e) {
        	status.setFail("\"기부가 정상적으로 처리되지 않았습니다.\"");
        	return getFailModelAndView(mav, status);
        }
        status.setOk("\"기부가 완료되었습니다.\"");
        return getOkModelAndView(mav, status);
    }
}