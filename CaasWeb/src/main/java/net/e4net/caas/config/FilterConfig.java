package net.e4net.caas.config;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import net.e4net.caas.common.filter.KrEncodingFilter;
import net.e4net.caas.common.util.LogObject;

@Configuration
public class FilterConfig extends LogObject{

	@Value("${inipay.P_NOTI_URL}") private String pInipayPNotiUrl;
	@Value("${mobilians.callback.okUrl}") private String pMobiliansCallbackOkUrl;

	@SuppressWarnings("unchecked")
	@Bean
    public FilterRegistrationBean<KrEncodingFilter> myFilter() {
        @SuppressWarnings("rawtypes")
		FilterRegistrationBean bean = new FilterRegistrationBean();
        bean.setFilter(new KrEncodingFilter());
        bean.setUrlPatterns(Arrays.asList(pInipayPNotiUrl, pMobiliansCallbackOkUrl));
        return bean;
    } 
}