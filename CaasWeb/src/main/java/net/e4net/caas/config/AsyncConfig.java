package net.e4net.caas.config;

import java.util.concurrent.Executor;

import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import lombok.extern.slf4j.Slf4j;

/**
 * ASYNC 동기화 설정
 *	(참조) https://heowc.github.io/2018/02/10/spring-boot-async/
 * @author KC.KIM
 */
@Configuration
@EnableAsync
@Slf4j
public class AsyncConfig extends AsyncConfigurerSupport {

	@Override
	public Executor getAsyncExecutor() {
		log.debug("[KGC] AsyncConfig start");
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(2);
		executor.setMaxPoolSize(10);
		executor.setQueueCapacity(500);
		//executor.setThreadNamePrefix("heowc-async-");
		executor.initialize();
		return executor;
	}
}