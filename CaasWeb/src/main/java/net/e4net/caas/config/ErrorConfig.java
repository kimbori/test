package net.e4net.caas.config;

import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.server.ErrorPage;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;

import net.e4net.caas.common.util.LogObject;


public class ErrorConfig extends LogObject{
	@Bean
	public ConfigurableServletWebServerFactory webServerFactory() {
		TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
		
		factory.addErrorPages(new ErrorPage(HttpStatus.NOT_FOUND, "/error"));
		return factory;
	}
}