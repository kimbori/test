package net.e4net.caas.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.session.SessionRegistry;
import org.springframework.security.core.session.SessionRegistryImpl;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private Environment environment;

    @Override
    protected void configure(HttpSecurity http) throws Exception {	
        http.authorizeRequests()
        		.antMatchers("/assets/**").permitAll()
        		.antMatchers("/publish/**").permitAll()
        		.antMatchers("/public/**").permitAll()
        		.antMatchers("/moveLoginPag").permitAll()
        		.antMatchers("/").permitAll()
                .antMatchers("/**").hasRole("USER")
                .anyRequest().authenticated();
        		
        http.csrf().disable();
        http.formLogin().loginPage("/moveLoginPag");
 
        http.logout();
        http.sessionManagement()
//	      	.invalidSessionUrl("/public/invalidSessionUrl")			// session out 시
		   //.sessionAuthenticationErrorUrl("/spring/error?error_code=2")
			.maximumSessions(1)
			//.expiredUrl("/public/invalidSessionUrl")
			.maxSessionsPreventsLogin(true)
			.sessionRegistry(sessionRegistry());
        
        String[] profiles = environment.getActiveProfiles();

        if (!"local".equals(profiles[0])) {
        	http.requiresChannel().antMatchers("/moveLoginPag").requiresSecure();
        }
        //http.requiresChannel().antMatchers("/moveLoginPag").requiresSecure();
    }
    
    @Bean
    public SessionRegistry sessionRegistry() {
        return new SessionRegistryImpl();
    }
}