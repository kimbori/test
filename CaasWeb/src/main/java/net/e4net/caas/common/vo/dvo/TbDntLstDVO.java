package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = false)
public class TbDntLstDVO extends TbCmpInfDVO {
	private long dntnItmSq;			//기부내역sq
	private long cmpSq;				//캠페인sq
	private long cnrSq;				//기부자sq
	private String dntnDttm;		//기부일시
	private String dntnKn;			//기부종류
	private long dntnAmt;			//기부금액
	private String cmmt;			//응원글
	private long toknTrnfSq;		//토큰전송sq
	private String nthnDntnYn;		//무기명기부여부
	private String recpIssuYn;		//영수증신고여부
	private String inhgvtaxNtfyYn;	//상증세신고여부
	private long dntnRecpSq;		//기부영수증sq
	private String fstRegDttm;		//최초등록일시
	private String fstRegUsid;		//최초등록자
	private String lstChDttm;		//최종변경일시
	private String lstChUsid;		//최종변경자
}