package net.e4net.caas.common.mapper;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.MapKey;
import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.OprGrpVO;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.my.vo.FxprBllRVO;

/**
 * 공통
 * @author KC.KIM
 *
 */
@Repository
public interface CmmMapper {
	
	// 단체목록 조회
	@MapKey("oprGrpId") Map<String, OprGrpVO> selectOprGrp();

	// cnrSq로 userInfo 조회
	public UserVO selectUserInfo(long cnrSq);
	
	/**
     * 해당일 정기결제 대상자 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
	public List<FxprBllRVO> selectFxprBllRcp();
	
	/**
     * 캠페인 상태정보 업데이트
     */
	public int updateDntnStat();
	
	/**
     * 정기결제 상태정보 업데이트
     */
	public int updateFxprStat();
}