package net.e4net.caas.common.service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.adapter.KakaoRestAdapter;
import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.mapper.KakaoPayMapper;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.common.vo.KakaoPayApiApprovePVO;
import net.e4net.caas.common.vo.KakaoPayApiApproveRVO;
import net.e4net.caas.common.vo.KakaoPayApiReadyPVO;
import net.e4net.caas.common.vo.KakaoPayApiReadyRVO;
import net.e4net.caas.common.vo.OprGrpVO;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.common.vo.dvo.TbKakaoPayAvLstDVO;
import net.e4net.caas.common.vo.dvo.TbKakaoPayRdyLstDVO;
import net.e4net.caas.common.vo.dvo.TbPgFlngDVO;

/**
 * INIPAY 서비스
 *
 * @author djim
 * @since 2018.11.01
 */ 


@Service
public class KakaoPayService extends AbstractDao {
	
	//카카오  PAY REST API restUrl
	@Value("${kakaoPay.restUrl}") private String restUrl;
	//카카오  PAY REST API 승인  callbackURL
	@Value("${kakaoPay.approvalUrl}") private String approvalUrl;
	//카카오  PAY REST API 실패  callbackURL
	@Value("${kakaoPay.failUrl}") private String failUrl;
	//카카오  PAY REST API 취소  callbackURL
	@Value("${kakaoPay.cancelUrl}") private String cancelUrl;
	//BASE HOST
	@Value("${callback.api.myHost}") private String host;
	
    @Autowired
    private KakaoPayMapper kakaoPayMapper;
    
	//kakao REST API 전용 어뎁터
    @Autowired
    private KakaoRestAdapter restAdapter;
    
    //kakao REST API 전용 어뎁터
    @Autowired
    private PgInicisService pgInicisService;
    
    //kakao REST API 전용 어뎁터
    @Autowired
    private BlockchainService blockchainService;
    
    //공통 서비스
	@Autowired 
	private CmmService cmmService;
	
    /**
     * 카카오페이 준비 시퀀스 조회
     *
     * @return 카카오페이 준비 시퀀스 조회
     */
    public long selectTbKakaoPayRdyLstSeq() {
        return kakaoPayMapper.selectTbKakaoPayRdyLstSeq();
    }
    
    /**
     * 카카오페이 준비  등록
     *
     * @param 카카오페이 준비  등록 정보 
     * @return 등록결과
     */
    public int insertTbKakaoPayRdyLst(TbKakaoPayRdyLstDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("insertTbKakaoPayRdyLst", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = kakaoPayMapper.insertTbKakaoPayRdyLst(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
        return rtnVal;
    }
    
    /**
     *카카오페이 준비  수정
     *
     * @param 카카오페이 준비 수정 정보
     * @return 수정결과
     */
    public int updateTbKakaoPayRdyLst(TbKakaoPayRdyLstDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("updateTbKakaoPayRdyLst", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = kakaoPayMapper.updateTbKakaoPayRdyLst(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
        return rtnVal;
    }
    
    /**
     * 카카오페이 준비  조회
     *
     * @return 카카오페이 결제 시퀀스 조회
     */
    public TbKakaoPayRdyLstDVO selectTbKakaoPayRdyLst(String stlmNtno) {
        return kakaoPayMapper.selectTbKakaoPayRdyLst(stlmNtno);
    }

    
    /**
     * 카카오페이 결제 시퀀스 조회
     *
     * @return 카카오페이 결제 시퀀스 조회
     */
    public long selectTbKakaoPayAvLstSeq() {
        return kakaoPayMapper.selectTbKakaoPayAvLstSeq();
    }
    
    /**
     * 카카오페이 결제  등록
     *
     * @param 카카오페이 결제  등록 정보 
     * @return 등록결과
     */
    public int insertTbKakaoPayAvLst(TbKakaoPayAvLstDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("insertTbKakaoPayAvLst", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = kakaoPayMapper.insertTbKakaoPayAvLst(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
        return rtnVal;
    }
    
    /**
     *카카오페이 결제  수정
     *
     * @param 카카오페이 결제 수정 정보
     * @return 수정결과
     */
    public int updateTbKakaoPayAvLst(TbKakaoPayAvLstDVO dvo) {
    	TransactionStatus status = super.getTransactionStatus("updateTbKakaoPayAvLst", TransactionDefinition.PROPAGATION_REQUIRES_NEW, TransactionDefinition.ISOLATION_SERIALIZABLE);
    	int rtnVal = -1;
    	try {
        	rtnVal = kakaoPayMapper.updateTbKakaoPayAvLst(dvo);
        	super.transactionCommit(status);
    	} catch (Exception e) {
    		super.transactionRollback(status);
			error(e.getMessage(), e);
    	}
        return rtnVal;
    }
    
    
    /**
     *  카카오페이  준비요청
     * @param request
     * @param pvo
     * @param httpSession
     * @return
     * @throws CaasException
     */
    public ModelAndView ready( HttpServletRequest request, KakaoPayApiReadyPVO pvo, HttpSession httpSession ) throws CaasException {
    	ModelAndView mav = new ModelAndView("common/flngResultPag");
    	
    	//세션정보(스프링시큐리티)
    	UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	OprGrpVO oprGrp = cmmService.getOprGrpInfo(principal.getOprGrpId());
    	try {
    		
	    	/*
	    	 * 1 step : 카카오페이결제 준비일련번호 채번
	    	 */
	    	long tbKakaoPayRdyLstSeq = selectTbKakaoPayRdyLstSeq();
	    	
	    	
	    	/*
	    	 * 2 step : 카카오페이결제 준비내역 등록
	    	 */
	    	pvo.setCid(oprGrp.getCpCid());
	    	pvo.setPartnerOrderId(String.valueOf(tbKakaoPayRdyLstSeq));
	    	pvo.setPartnerUserId(String.valueOf(principal.getCnrSq()));
	    	pvo.setItemName(oprGrp.getPgGoodsName());
	    	pvo.setTotalAmount(pvo.getTotalAmount());
	    	pvo.setTaxFreeAmount("0");
	    	pvo.setApprovalUrl(host+this.approvalUrl);
	    	pvo.setFailUrl(host+this.failUrl);
	    	pvo.setCancelUrl(host+this.cancelUrl);
	    	
	    	KakaoPayApiReadyRVO result = null;
	    	
	    	TbKakaoPayRdyLstDVO dvo = new TbKakaoPayRdyLstDVO();
	    	dvo.setPrprItmSq(tbKakaoPayRdyLstSeq);
	    	dvo.setCnrSq(principal.getCnrSq());
	    	dvo.setPrdtNm(oprGrp.getPgGoodsName());
	    	dvo.setPrdtQty(1);
	    	dvo.setStlmTtAmt(Long.parseLong(pvo.getTotalAmount()));
	    	dvo.setFrTaxAmt(0);
	    	dvo.setSrtxAmt(0);
	    	dvo.setSucsUrl(host+approvalUrl);
	    	dvo.setCancUrl(host+cancelUrl);
	    	dvo.setFailUrl(host+failUrl);
	    	dvo.setFstRegUsid(String.valueOf(principal.getCnrSq()));
	    	dvo.setLstChUsid(String.valueOf(principal.getCnrSq()));
	    	
	    	//카카오페이결제 준비 등록
	    	if (insertTbKakaoPayRdyLst(dvo) < 1 ) {
	    		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	    	}
			
	    	
	    	/*
	    	 * 3 step : 카카오페이결제 준비 REST API 호출
	    	 */
			result = restAdapter.post(restUrl+"ready", pvo, KakaoPayApiReadyRVO.class);
			
			debug(result.toString());
			
	    	dvo.setStlmNtno(result.getTid());
	    	dvo.setAppStlmUrl(result.getNextRedirectAppUrl());
	    	dvo.setMwebStlmUrl(result.getNextRedirectMobileUrl());
	    	dvo.setPcStlmUrl(result.getNextRedirectPcUrl());;
	    	dvo.setAdrdSchmUrl(result.getAndroidAppScheme());
	    	dvo.setIosSchmUrl(result.getIosAppScheme());
	    	dvo.setPrprReqDttm(result.getCreatedAt());
	    	
	    	if( result.getHttpCode() == HttpStatus.OK) {
	    		dvo.setRspnCd("00");
	    	} else {
	    		dvo.setRspnCd(result.getCode());
		    	dvo.setRspnMsg(result.getMsg());
		    	if (result.getExtras() != null) {
			    	dvo.setAddRspnCd(result.getExtras().getMethodResultCode());
			    	dvo.setAddRspnMsg(result.getExtras().getMethodResultMessage());
		    	}
	    	}
	    	dvo.setLstChUsid(String.valueOf(principal.getCnrSq()));
	    	
	    	
	    	/*
	    	 * 4 step : 카카오페이결제 준비 REST API 응답결과 갱신 
	    	 */
	    	if ( updateTbKakaoPayRdyLst(dvo) < 1 ) {
	    		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	    	}
	    	
	    	
	    	/*
	    	 * 5 step : 카카오페이 일련번호를 session에 기록
	    	 */
			httpSession.removeAttribute("kakao_tid");
			httpSession.setAttribute("kakao_tid", result.getTid());
			
			if( ! "00".equals(dvo.getRspnCd()) ) {
				mav.addObject("process", "kakaoBll");
				mav.addObject("resultcode", dvo.getRspnCd());
				mav.addObject("resultmsg", dvo.getRspnMsg());
			} else {
				//mav.setViewName("redirect:"+dvo.getMwebStlmUrl());
				mav.setViewName("redirect:"+dvo.getMwebStlmUrl());
				
			}
		} catch (Exception e) {
			error(e.getMessage(), e);
			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
        }
		return mav;
    }
    
    public ModelAndView approve(HttpSession httpSession, String pgToken) throws CaasException {
    	//세션정보(스프링시큐리티) GET
    	UserVO principal = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	ModelAndView mav = new ModelAndView("common/flngResultPag");
    	
    	TbKakaoPayAvLstDVO avDvo = null;
    	try {
    		OprGrpVO oprGrp = cmmService.getOprGrpInfo(principal.getOprGrpId());
    		
	    	/*
	    	 * 1 step : 카카오  거래 고유번호로 준비내역 조회
	    	 */
	    	KakaoPayApiApprovePVO pvo = new KakaoPayApiApprovePVO();
	    	TbKakaoPayRdyLstDVO dvo = selectTbKakaoPayRdyLst(httpSession.getAttribute("kakao_tid").toString());
	    	
	    	pvo.setCid(oprGrp.getCpCid());
	    	pvo.setTid(httpSession.getAttribute("kakao_tid").toString());
	    	pvo.setPartnerOrderId(String.valueOf(dvo.getPrprItmSq()));
	    	pvo.setPartnerUserId(String.valueOf(principal.getCnrSq()));
	    	pvo.setPgToken(pgToken);
	    	pvo.setTotalAmount(String.valueOf(dvo.getStlmTtAmt()));
	    	
	    	KakaoPayApiApproveRVO result = null; 
	    	
	    	//층전내역일련번호
	    	long tbPgFlngSeq = pgInicisService.selectTbPgFlngSeq();
	    	long tbKakaoPayAvLstSeq = selectTbKakaoPayAvLstSeq();
	    	
	    	
	    	avDvo = new TbKakaoPayAvLstDVO();
	    	avDvo.setAvItmSq(tbKakaoPayAvLstSeq);
	    	avDvo.setFlngItmSq(tbPgFlngSeq);
	    	avDvo.setPrprItmSq(dvo.getPrprItmSq());
	    	avDvo.setCnrSq(principal.getCnrSq());
	    	avDvo.setAuthToknVl(pgToken);
	    	avDvo.setFstRegUsid(String.valueOf(principal.getCnrSq()));
	    	avDvo.setLstChUsid(String.valueOf(principal.getCnrSq()));
	
	    	/*
	    	 * 2 step : 카카오  결제 요청 정보 등록
	    	 */
	    	if( insertTbKakaoPayAvLst(avDvo) < 1 ) {
	    		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	    	}
	    	
	    	
			//요청내역적재
	 		TbPgFlngDVO tbPgDvo = new TbPgFlngDVO();
	 		
	 		tbPgDvo.setFlngItmSq(tbPgFlngSeq);
	 		tbPgDvo.setOprGrpId(principal.getOprGrpId());
	 		tbPgDvo.setLnkAgency("02");
	 		tbPgDvo.setLnkReqInfo(pvo.toString());
	 		tbPgDvo.setCnrSq(principal.getCnrSq());
	 		tbPgDvo.setFlngAmt(dvo.getStlmTtAmt());
	 		tbPgDvo.setStlmMd("01");
	 		tbPgDvo.setStmt("03");
	 		tbPgDvo.setFstRegUsid(String.valueOf(principal.getCnrSq()));
	    	/*
	    	 * 3 step : 충전 요청내역  정보 등록
	    	 */
	 		if ( pgInicisService.insertTbPgFlng(tbPgDvo) < 1 ) {
	 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	 		}
 		
	    	/*
	    	 * 4 step : 카카오  결제 REST API 호출
	    	 */
			result = restAdapter.post(restUrl + "approve", pvo, KakaoPayApiApproveRVO.class);
			
	    	/*
	    	 * 5 step : 카카오  결제 REST API 응답 정보 갱신
	    	 */
			avDvo.setStlmMnsCd(result.getPaymentMethodType());
	    	if ( result.getAmount() != null ) {
		    	avDvo.setStlmTtAmt(Long.parseLong(result.getAmount().getTotal()));
		    	avDvo.setFrTaxAmt(Long.parseLong(result.getAmount().getTaxFree()));
		    	avDvo.setSrtxAmt(Long.parseLong(result.getAmount().getVat()));
		    	avDvo.setUsePnt(Long.parseLong(result.getAmount().getPoint()));
		    	avDvo.setDscnAmt(Long.parseLong(result.getAmount().getDiscount()));
	    	}
	    	if ( result.getCardInfo() != null ) {
		    	avDvo.setAcrKorNm(result.getCardInfo().getPurchaseCorp());
		    	avDvo.setAcrCd(result.getCardInfo().getPurchaseCorpCode());
		    	avDvo.setIssrKorNm(result.getCardInfo().getIssuerCorp());
		    	avDvo.setIssrCd(result.getCardInfo().getIssuerCorpCode());
		    	avDvo.setBinNo(result.getCardInfo().getBin());
		    	avDvo.setCardTypeCd(result.getCardInfo().getCardType());
		    	avDvo.setIsMcnt(Integer.parseInt(result.getCardInfo().getInstallMonth()));
		    	avDvo.setAvNo(result.getCardInfo().getApprovedId());
		    	avDvo.setAcrMcno(result.getCardInfo().getCardMid());
		    	avDvo.setIfimYn(result.getCardInfo().getInterestFreeInstall());
		    	avDvo.setCardPrdtCd(result.getCardInfo().getCardItemCode());
	    	}

	    	avDvo.setPrdtNm(result.getItemName());
	    	avDvo.setPrdtCd(result.getItemCode());
	    	if ( result.getQuantity() != null ) {
		    	avDvo.setPrdtQty(Integer.parseInt(result.getQuantity()));
	    	}
	    	avDvo.setPrprReqDttm(result.getCreatedAt());
	    	avDvo.setAvPcsnDttm(result.getApprovedAt());
	    	avDvo.setCancYn("N");

	    	if( result.getHttpCode() == HttpStatus.OK) {
	    		avDvo.setRspnCd("00");
	    		tbPgDvo.setLnkPrcRst("S");
	    	} else {
	    		avDvo.setRspnCd(result.getCode());
	    		avDvo.setRspnMsg(result.getMsg());
		    	if (result.getExtras() != null) {
		    		avDvo.setAddRspnCd(result.getExtras().getMethodResultCode());
		    		avDvo.setAddRspnMsg(result.getExtras().getMethodResultMessage());
		    	}
		    	tbPgDvo.setLnkPrcRst("F");
	    	}

	    	avDvo.setLstChUsid(String.valueOf(principal.getCnrSq()));

	    	
	    	if( updateTbKakaoPayAvLst(avDvo) < 1 ) {
	    		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	    	}
	    	
	    	/*
	    	 * 6 step : 충전내역 갱신
	    	 */
	    	if ("MONEY".equals(result.getPaymentMethodType()) ) {
	    		tbPgDvo.setStlmKn("04");
	    	} else {
	    		tbPgDvo.setStlmKn("01");
	    		tbPgDvo.setStlmMemo(result.getCardInfo().getBin() + "**********");
	    	}
	    	tbPgDvo.setLnkRstInfo(result.toString());
	    	tbPgDvo.setLstChUsid(String.valueOf(principal.getCnrSq()));
	    	tbPgDvo.setPgStlmSq(tbKakaoPayAvLstSeq);
	 		if ( pgInicisService.updateTbPgFlng(tbPgDvo) < 1 ) {
	 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	 		}
	 		
			httpSession.removeAttribute("kakao_tid");
			
			if ( "00".equals(avDvo.getRspnCd())) {
				
		    	/*
		    	 * 7 step : 토큰 충전
		    	 */
				BlockchainRetrunVO blockchainRetrunVO = blockchainService.walletTokenDeposit(principal.getOprGrpId(), principal.getWltAddr(), avDvo.getStlmTtAmt(), "21");
		 		//BlockchainTokenTxVO blockchainTokenTxVO = blockchainService.tokenTransferB2C(principal.getOprGrpId(), "0x8867eaca298d95f7be7f9231f69b35135deda362", Long.parseLong(request.getParameter("totalAmount")), "21", String.valueOf(principal.getCnrSq()));
				if ( ! blockchainRetrunVO.isSuccess()) {
					throw new CaasException( blockchainRetrunVO.getErrCd(), blockchainRetrunVO.getErrMsg());
				}
				
				
		 		tbPgDvo.setToknTrnfSq(blockchainRetrunVO.getToknTrnfSq());
		 		if ( pgInicisService.updateTbPgFlngForTknSq(tbPgDvo) < 1 ) {
		 			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
		 		}	
			}
	 		
	 		
		} catch (Exception e) {
			error(e.getMessage(), e);
			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
        }
		mav.addObject("process", "kakaoBll");
  		mav.addObject("resultcode", avDvo.getRspnCd());
  		mav.addObject("resultmsg", avDvo.getRspnMsg());
  		
  		return mav;
    }
}
