package net.e4net.caas.common.vo;

import java.util.Date;

import lombok.Data;

/**
 * 외부 인터페이스 연계 공통 정보 VO
 * @author KC.KIM
 *
 */
@Data
public class ExtLinkVO {
    private String	oprGrpId;		// 단체ID
    private String	lnkAgency;		// 연계Agency업체
    private Date	lnkPrcDt;		// 연계처리일시
    private String	lnkPrcRst;		// 연계처리 결과
    private String	lnkReqInfo;		// 연계요청정보 (JSON)
    private String	lnkRstInfo;		// 연계결과정보 (JSON) 

    private Data	fstRegDttm;		// 최초등록일시
    private String	fstRegUsid;		// 최초등록자ID
    private Data	lstChDttm;		// 최근갱신일시
    private String	lstChUsid;		// 최근등록자ID
}
