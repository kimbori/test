package net.e4net.caas.common.service;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.mapper.CmmCodeMapper;
import net.e4net.caas.common.mapper.CmmMapper;
import net.e4net.caas.common.vo.CmmnDetailCode;
import net.e4net.caas.common.vo.OprGrpVO;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.common.vo.dvo.TbDntLstDVO;
import net.e4net.caas.dntn.service.DntnInsService;
import net.e4net.caas.my.vo.FxprBllRVO;

@Slf4j
@Service
public class CmmService {

	@Autowired CmmMapper cmmMapper;
	@Autowired CmmCodeMapper cmmCodeMapper;
	
	//이니시스 서비스
    @Autowired
    private PgInicisService pgInicisService;
    
    @Autowired
    private DntnInsService dntnService;

	public static Map<String, OprGrpVO>  oprGrpInfoList = null;
	
	/* ==================================================================================
	 * 	운영단체 정보
	 */

	private void loadOprGrpList() {
		oprGrpInfoList = cmmMapper.selectOprGrp();
		for(String key : oprGrpInfoList.keySet()) {
			log.debug(String.format("[oprGrpInfoList] load (%s), %s", key, oprGrpInfoList.get(key)) );
		}
	}
	
	/**
	 * 운영단체정보를 주기적으로 갱신한다. 
	 */
	@Scheduled(fixedDelayString = "${auto.oprGrp.delay.ms}")
	public void autoLoadOprGrpList() {
		loadOprGrpList();
	}

	/**
	 * 운영단체정보를 조회한다.
	 * @param oprGrpId 운영단체ID
	 * @return 운영단체정보
	 */
	public OprGrpVO getOprGrpInfo(String oprGrpId) {
		if(oprGrpInfoList == null) {
			loadOprGrpList();
		}
		return oprGrpInfoList.get(oprGrpId);
	}

	/* ==================================================================================
	 * 	공통코드
	 */
	// 공통코드 상세목록 조회
	public List<CmmnDetailCode> selectCmmCodeDetail(String codeId) {
		return cmmCodeMapper.selectCmmCodeDetail(codeId);
	}
	
	/* ==================================================================================
	 * 	정기결제
	 */
	public UserVO getUserInfo(long cnrSq) {
		return cmmMapper.selectUserInfo(cnrSq);
	}
	
	/** 상태정보 업데이트
	 *  (캠페인정보 : DNMR_STAT, 정기결제내역 : FXPR_STAT)
	 * */
	@Scheduled(cron = "0 01 13 2,1,5,10,15,20,25,30 * ?")
	public void updStat() {
		/* STEP 1. 캠페인 상태정보 UPDATE */
		int cmpCnt = cmmMapper.updateDntnStat();
		log.debug("# 캠페인 상태정보 변경 ; " + cmpCnt);		
		
		/* STEP 2. 정기결제 상태정보 UPDATE */
		int fxprCnt = cmmMapper.updateFxprStat();
		log.debug("# 정기결제 상태정보 변경 : " + fxprCnt);
	}
	
	/** 정기결제 
     * 매월 1, 5, 10, 15, 20, 25, 30일 00:00시에 실행 */
    @Scheduled(cron = "0 01 13 2,1,5,10,15,20,25,30 * ?")
    public void autoFxpr() {
		/* STEP 1. 해당일 결제 대상건 조회 */
		List<FxprBllRVO> dvoLst = cmmMapper.selectFxprBllRcp();
		log.debug("# 총 정기기부 건수 : " + dvoLst.size());
		
    	/* STEP 2. 충전 -> 기부 */
		int totCnt = 0;
		for(int i=0; i<dvoLst.size(); i++) {
			try {
				FxprBllRVO dvo = dvoLst.get(i);
				/* STEP 2-1. (카드간편결제)빌링키로 결제 */
				UserVO uvo = getUserInfo(dvo.getCnrSq());
				String rltCode = pgInicisService.inicisReqBillFxpr(uvo, dvo);
				
				if(!"00".equals(rltCode)) {
					log.debug("충전 시 오류 발생\n"+dvo.toString());
				}else {
					/* STEP 2-2. 기부내역 등록 */
					TbDntLstDVO dntnPvo = new TbDntLstDVO();
					dntnPvo.setCmpSq(dvo.getCmpSq());
					dntnPvo.setCnrSq(dvo.getCnrSq());
					dntnPvo.setDntnKn("02"); //정기기부
					dntnPvo.setDntnAmt(dvo.getOtaAmt());
					dntnPvo.setWltAddr(dvo.getCmpWlt());
					
					int cnt = dntnService.insertDntn(uvo, dntnPvo);
					if(cnt != 1) {
						throw new CaasException("99999", "정기결제 시 오류 발생");
					}totCnt += cnt;
				}
			} catch (CaasException e) {
				e.printStackTrace();
			}
		}
		log.debug("#성공 정기기부 건수 : " + totCnt);
	}
}