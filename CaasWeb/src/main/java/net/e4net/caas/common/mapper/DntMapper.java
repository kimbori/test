package net.e4net.caas.common.mapper;

import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.dvo.TbDntPrsnDVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author yeongeun.seo 
 * @since 2018.11.30
 */ 
@Repository
public interface DntMapper {
	 
	/** 
     * 기부자 가입 시퀀스 조회
     */
    public long selectTbDntPrsnSeq();
    
    /** 
     * 기부자 회원 정보 조회
     */
    public TbDntPrsnDVO selectTbDntPrsnInfo(TbDntPrsnDVO dvo);
    
    /** 
     * 기부자 등록 
     */
    public int insertTbDntPrsnInfo(TbDntPrsnDVO dvo);
    
    /** 
     * 기부자 지갑정보 업데이트
     */
    public int updateTbDntPrsnWaltInfo(TbDntPrsnDVO dvo);
    
    /** 
     * 기부자 본인인증정보 업데이트
     */
    public int updateTbDntPrsnCertInfo(TbDntPrsnDVO dvo);
    
}
