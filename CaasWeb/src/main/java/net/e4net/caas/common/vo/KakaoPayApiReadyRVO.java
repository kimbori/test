package net.e4net.caas.common.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class KakaoPayApiReadyRVO extends KakaoRestVO {

	private static final long serialVersionUID = -5447950025980771248L;

	@JsonProperty("tid")
	String tid; 
	
	@JsonProperty("next_redirect_app_url")
	String nextRedirectAppUrl; 
	
	@JsonProperty("next_redirect_mobile_url")
	String nextRedirectMobileUrl; 
	
	@JsonProperty("next_redirect_pc_url")
	String nextRedirectPcUrl; 
	
	@JsonProperty("android_app_scheme")
	String androidAppScheme; 
	
	@JsonProperty("ios_app_scheme")
	String iosAppScheme; 
	
	@JsonProperty("created_at")
	String createdAt; 
}
