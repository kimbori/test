package net.e4net.caas.common.vo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class KakaoPayApiCancelPVO extends KakaoRestVO{
	private static final long serialVersionUID = 5552459882874180733L;
	
	
	@JsonIgnore
	private final transient Logger logger = LoggerFactory.getLogger(KakaoPayApiCancelPVO.class);
 
	//가맹점 코드. 10자.
	@JsonProperty("cid")
	String cid; 
	
	//결제 고유번호. 20자.
	@JsonProperty("tid")
	String tid;
	
	//취소 금액
	@JsonProperty("cancel_amount")
	String cancelAmount; 
	
	//취소 비과세 금액
	@JsonProperty("cancel_tax_free_amount")
	String cancelTaxFreeAmount; 
	
	//취소 부가세 금액(안보낼 경우 (취소 금액 - 취소 비과세 금
	@JsonProperty("cancel_vat_amount")
	String cancelVatAmount; 
	
	//취소 부가세 금액(안보낼 경우 (취소 금액 - 취소 비과세 금
	@JsonProperty("cancel_available_amount")
	String cancelAvailableAmount; 
	
	//해당 Request와 매핑해서 저장하고 싶은 값. 최대 200자
	@JsonProperty("payload")
	String payload; 
}
