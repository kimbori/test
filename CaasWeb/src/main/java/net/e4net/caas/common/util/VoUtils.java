package net.e4net.caas.common.util;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;

import java.util.Map;

/**
 * VO 관련 Util
 *
 * @author djim
 * @since  2018.11.01
 */
public class VoUtils {

	private final static transient Logger logger = LoggerFactory.getLogger(VoUtils.class);

	/**
	 * VO 를 Map 으로 변환한다.
	 *
	 * @param vo
	 * @return
	 */
	public static Map<String, Object> convertVoToMap(Object vo) {
		ObjectMapper om = new ObjectMapper();
		TypeReference<Map<String, Object>> typeReference = new TypeReference<Map<String, Object>>() {};

		return om.convertValue(vo, typeReference);
	}

	/**
	 * Map 을 VO 로 변환한다.
	 *
	 * @param map
	 * @param <T>
	 * @return
	 */
	public static <T> T convertMapToVo(Map<String, Object> map) {
		ObjectMapper om = new ObjectMapper();
		TypeReference<T> typeReference = new TypeReference<T>() {};

		return om.convertValue(map, typeReference);
	}

	/**
	 * Map 을 VO 로 변환한다.
	 *
	 * @param map
	 * @param resultClass
	 * @param <T>
	 * @return
	 */
	public static <T> T convertMapToVo(Map<String, Object> map, Class<T> resultClass) {
		ObjectMapper om = new ObjectMapper();
		return om.convertValue(map, resultClass);
	}

	/**
	 * VO를 복사한다. 대상은 내부 구조 및 변수가 같아야 한다.
	 *
	 * @param source
	 * @param target
	 */
	public static void copy(Object source, Object target) {
		if (source == null || target == null) {
			logger.error("{} is null.", source == null ? "source" : "target");
			return;
		}

		BeanUtils.copyProperties(source, target);
	}

}
