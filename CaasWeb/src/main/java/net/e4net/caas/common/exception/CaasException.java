package net.e4net.caas.common.exception;

import lombok.Getter;

public class CaasException extends Exception {

	private static final long serialVersionUID = -7014732141060156462L;

	@Getter
	private String message; 

	@Getter
	private String code;

	public CaasException(String code, String message) {
		super(message);

		this.code = code;
		this.message = message;
	}

	public CaasException(String code, String message, Throwable cause) {
		super(message, cause);

		this.code = code;
		this.message = message;
	}

	protected CaasException(String code, String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);

		this.code = code;
		this.message = message;
	}

}
