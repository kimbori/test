package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = false)
@ToString
public class SampleDVO extends PageVO {

	private String key;
	private int val1;
	private int val2;
	private String str1;

}
