package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbInipayAvLstDVO extends PageVO {

	private static final long serialVersionUID = 3732233262569652054L;
	
	private long inipayAvSq;          //시퀀스(승인일련번호)
	private String inipayStlmCd;      //지불수단
	private String avDt;              //승인일자
	private String inipayMrchId;      //이니페이아이디
	private String stlmNtno;		  //거래고유번호
	private long inipayCertSq;      //시퀀스(인증일련번호)
	private long stlmTtAmt;           //결제총액
	private long cnrSq;               //기부자SQ
	private String cnrNm;             //기부자명
	private String rspnCd;            //결과코드
	private String rspnMsg;           //결과메시지
	private String notiUrl;           //통지URL
	private String returnUrl;         //결과URL
	private String nxtUrl;            //다음URL
	private String cardNo;            //카드번호
	private String issrCd;            //발급사코드
	private String acrMcno;           //가맹점번호
	private String acrCd;             //매입사코드
	private String porCancPosbYn;     //부분취소 가능여부
	private String ifimYn;            //무이자할부여부
	private String chkCardYn;         //체크카드 여부
	private int isMcnt;            	  //할부개월수
	private String cardcCd;           //카드사코드
	private String fcEngCd;           //금융사영문코드
	private String avNo;              //승인번호
	private String vpCardCd;          //vp카드코드
	private String fcKorNm;           //금융사한글명
	private String evntCd;            //이벤트코드
	private String mbphCtcoCd;        //이동전화통신사코드
	private String stlmMbphNo;        //결제 휴대폰번호
	private String ewalletTypeCd;     //전자지갑종류
	private String appLnkgCd;         //앱연동여부
	private String cashRcepStatCd;    //현금영수증처리상태
	private String cashRcepStatMsg;   //현금영수증처리메시지
	private long cashRcepTtAmt;       //현금영수증 총금액
	private long cashRcepVrspAmt;     //현금영수증공급가액
	private long cashRcepTaxAmt;      //현금영수증세금
	private long cashRcepSrfeAmt;     //현금영수증봉사료
	private String cashRcepTypeCd;    //현금영수증용도구분
	private String cashRcepIsDttm;    //현금영수증발행시간
	private String cashRcepIsNo;      //현금영수증발행번호
	private String vaccNo;            //입금할 계좌번호
	private String rcptEndDt;         //입금마감일자
	private String rcptEndTm;         //입금마감시간
	private String acowNm;            //계좌주명
	private String vaccBankCd;        //은행코드
	private String fstRegDttm;        //최초등록일시
	private String fstRegUsid;        //최초등록자
	private String lstChDttm;         //최종변경일시
	private String lstChUsid;         //최종변경자
}