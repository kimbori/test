package net.e4net.caas.common.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.PgBllRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author djim
 * @since 2018.11.01
 */
@Repository
public interface FlngProcMapper {
	 
    /**
     * PG_BILL 조회
     */ 
    public List<PgBllRVO> selectBllList(long cnrSq);
    
    
    /**
     * BILL KEY 조회
     */ 
    public PgBllRVO selectBllKey(long bllAuthSq);

}
