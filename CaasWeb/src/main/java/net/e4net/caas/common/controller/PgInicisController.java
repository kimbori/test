package net.e4net.caas.common.controller;

import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.service.PgInicisService;

/**
 * PgInicis 컨트롤러
 *
 * @author djim
 * @since 2018.11.01
 * history  2018.11.01 최초작성
 */
@Controller
public class PgInicisController extends AbstractController {
	
	//이니시스 서비스
    @Autowired
    private PgInicisService pgInicisService;
	
	/**
	 * 빌링키 요청 페이지 (이니시스 호출)
	 * @param model
	 * @param request
	 * @return
	 * @throws Exception
	 */
    @RequestMapping(value = "/moveBillKeyReqPag")
    public String moveBillKeyReqPag(Model model, HttpServletRequest request) throws CaasException {
    	
		return pgInicisService.moveBillKeyReqPag(model, request);
    }
    
    /**
     * 이니시스 빌링키 발급결과 CALLBACK 
     * @param request 
     * @param model
     * @return
     * @throws Exception
     */
  	@RequestMapping(value="/public/inicisBillKeyResult")
  	public ModelAndView inipayReturn(HttpServletRequest request, ModelMap model) throws CaasException {
  		return pgInicisService.inipayReturn(request, model);
  	}
  	
  	
	 /**
	  *  이니시스 빌링 결제 요청
	  * @param request
	  * @param model
	  * @return
	  * @throws CaasException
	  */
 	@RequestMapping(value="/inicisReqBill")
 	public ModelAndView inicisReqBill(HttpServletRequest request, ModelMap model) throws CaasException {
 		return pgInicisService.inicisReqBill(request, model);
 	}
 	
 	/**
 	 * 이니시스 일반결제 페이지 호출
 	 * @param redirectAttributes
 	 * @param request
 	 * @return page
 	 * @throws CaasException
 	 * @throws UnsupportedEncodingException
 	 */
 	@RequestMapping("/moveIniPayMobile")
 	public String moveIniPayMobile(RedirectAttributes redirectAttributes, HttpServletRequest request) throws CaasException{
 		return pgInicisService.moveIniPayMobile(redirectAttributes, request);
 	}
 	
 	/**
 	 * 이니시스 다음 결제 URL
 	 * @param request
 	 * @return
 	 * @throws CaasException
 	 */
 	@RequestMapping("/public/iniPayNextUrl")
 	public ModelAndView iniPayNextUrl(HttpServletRequest request) throws CaasException{
 		return pgInicisService.iniPayNextUrl(request);
 	}
 	
 	
 	/**
 	 * 이니시스 NOTI
 	 * @param request
 	 * @return
 	 * @throws CaasException
 	 */
 	@RequestMapping("/public/iniPayNotiUrl")
 	@ResponseBody
 	public String iniPayNotiUrl(HttpServletRequest request) throws CaasException{
 		return pgInicisService.iniPayNotiUrl(request);
 	}
 	
 	
 	/**
 	 * 이니시스 RETURN URL
 	 * @param request
 	 * @return
 	 * @throws CaasException
 	 */
 	@RequestMapping("/public/iniPayReturnUrl")
 	public ModelAndView iniPayReturnUrl(HttpServletRequest request){
 		return pgInicisService.iniPayReturnUrl(request); 
 	}
 	
 	
}
