package net.e4net.caas.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.mapper.DntMapper;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.common.vo.CertInfoVO;
import net.e4net.caas.common.vo.dvo.TbCertiPrsnDVO;
import net.e4net.caas.common.vo.dvo.TbDntPrsnDVO;

/**
 * 기부자 가입 서비스
 *
 * @author yeongeun.seo
 * @since 2018.11.30
 */ 
@Slf4j
@Service
public class DntService extends AbstractDao {
    @Autowired
    private DntMapper dntMapper;
    
    @Autowired
    private BlockchainService blockchainService;
    
    // 기부자 정보 생성 처리
    public TbDntPrsnDVO createDntProc(CertInfoVO vo, TbCertiPrsnDVO dvo, String writerID) throws CaasException {
    	TbDntPrsnDVO dntPVO = new TbDntPrsnDVO();
    	
    	// 기부자 정보 등록
		long tbDntPrsnSeq = selectTbDntPrsnSeq();
		dntPVO.setCnrSq(tbDntPrsnSeq);							//기부자SQ
		dntPVO.setOprGrpId(vo.getOprGrpId());					//운영단체ID
		dntPVO.setCnrNm(vo.getName());							//기부자명
		dntPVO.setTrmlId(vo.getTrmlId());						//단말기ID
		dntPVO.setMailAddr(vo.getMailAddr());					//기부자메일주소 (KGC)
		dntPVO.setHpno(vo.getNo());								//휴대전화번호
		dntPVO.setPrsnAuthSq(dvo.getPrsnAuthSq());				//본인인증SQ
		dntPVO.setCnrCi(dvo.getCnrCi());						//기부자CI
//		dntPVO.setMailAddr();				//메일주소
//		dntPVO.setWltAddr(blockVO.getWltAddr());				//지갑주소
		dntPVO.setFstRegUsid(writerID);							//최초등록자
		dntPVO.setLstChUsid(writerID);							//최종변경자
  		
  		if( insertTbDntPrsnInfo(dntPVO) < 1 ) {
  			log.error("기부자정보 저장 처리 오류.", dntPVO);
    		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
    	}
  		
  		BlockchainRetrunVO retVo = blockchainService.createWalletDntPrsn(vo.getOprGrpId(), tbDntPrsnSeq);
  		
  		if(! retVo.isSuccess()) {
  			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
  		}
  		if(changeDntWaltProc(retVo.getWalletAddr(), dntPVO.getCnrSq(), writerID) < 1 ) {
  			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
  		}
  		
    	return dntPVO;
    }
    
    // 기부자 기존가입여부 확인
    public TbDntPrsnDVO checkDntProc(TbCertiPrsnDVO dvo) {
    	TbDntPrsnDVO dntPVO = new TbDntPrsnDVO();
    	    	
    	// 기부자 조회 조건
    	dntPVO.setOprGrpId(dvo.getOprGrpId());					//운영단체ID
    	dntPVO.setCnrCi(dvo.getCnrCi());						//기부자CI
  		
		TbDntPrsnDVO dntRVO = selectTbDntPrsnInfo(dntPVO);
				
    	return dntRVO;
    }
    
    // 기부자 본인인증서비스 SQ,지갑정보 업데이트 처리
    public int changeDntWaltProc(String wltAddr, long cnrSq, String writerID) {
    	TbDntPrsnDVO dntPVO = new TbDntPrsnDVO();
  		
    	dntPVO.setWltAddr(wltAddr);		// 지갑주소
    	dntPVO.setLstChUsid(writerID);	// 최종변경자
    	dntPVO.setCnrSq(cnrSq);			// 기부자SQ

    	return updateTbDntPrsnWaltInfo(dntPVO);
    }
    
    // 기부자 본인인증정보 업데이트 처리
    public int changeDntCertInfoProc(CertInfoVO certVO, TbDntPrsnDVO dntVO, String writerID) {
    	TbDntPrsnDVO dntPVO = new TbDntPrsnDVO();
  		
    	dntPVO.setCnrNm(certVO.getName());			//이름 
    	dntPVO.setTrmlId(certVO.getTrmlId());		//디바이스ID
    	dntPVO.setMailAddr(certVO.getMailAddr());	// 메일주소
    	dntPVO.setHpno(certVO.getNo());				//핸드폰번호
    	dntPVO.setLstChUsid(writerID);				//최종변경자
    	dntPVO.setCnrSq(dntVO.getCnrSq());			//기부자SQ

    	return updateTbDntPrsnCertInfo(dntPVO);
    }
    
    /**
     * 기부자 가입 준비 시퀀스 조회
     *
     * @return 기부자 가입 준비 시퀀스 조회
     */
    public long selectTbDntPrsnSeq() {
        return dntMapper.selectTbDntPrsnSeq();
    }
    
    /**
     * 기부자 정보 조회
     *
     * @param 기부자 가입 정보 조회 
     * @return 등록결과
     */
    public TbDntPrsnDVO selectTbDntPrsnInfo(TbDntPrsnDVO dvo) {
        return dntMapper.selectTbDntPrsnInfo(dvo);
    }
    
    /**
     * 기부자 가입 등록
     *
     * @param 기부자 가입 등록 정보 
     * @return 등록결과
     */
    public int insertTbDntPrsnInfo(TbDntPrsnDVO dvo) {
        return dntMapper.insertTbDntPrsnInfo(dvo);
    }
    
    /**
     * 기부자 본인인증정보 갱신
     *
     * @param 기부자 본인인증정보 
     * @return 수정결과
     */
    public int updateTbDntPrsnCertInfo(TbDntPrsnDVO dvo) {
        return dntMapper.updateTbDntPrsnCertInfo(dvo);
    }
    
    /**
     * 기부자 SQ, 지갑정보 정보 갱신
     *
     * @param 기부자 갱신 정보
     * @return 수정결과
     */
    public int updateTbDntPrsnWaltInfo(TbDntPrsnDVO dvo) {
        return dntMapper.updateTbDntPrsnWaltInfo(dvo);
    }
}
