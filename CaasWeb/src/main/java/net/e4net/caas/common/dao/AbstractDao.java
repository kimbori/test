package net.e4net.caas.common.dao;

import org.apache.ibatis.session.ExecutorType;
import org.mybatis.spring.SqlSessionTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;

import net.e4net.caas.common.base.BaseAbstractCommon;

@Service
public class AbstractDao extends BaseAbstractCommon {

	private PlatformTransactionManager transactionManager;

	@Autowired
	@Qualifier("transactionManager")
	protected void setTransactionManager(PlatformTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}

	private SqlSessionTemplate sqlSessionTemplate;

	@Autowired
	@Qualifier("sqlSessionTemplate")
	protected void setSqlSessionTemplate(SqlSessionTemplate sqlSessionTemplate) {
		this.sqlSessionTemplate = sqlSessionTemplate;
	}

	/**
	 * {@link TransactionStatus}를 가져온다.<br/>
	 * 기본 설정되는 전달행위(Propagation Behavior)는 {@link TransactionDefinition#PROPAGATION_REQUIRED} 이다
	 *
	 * @return
	 */
	protected TransactionStatus getTransactionStatus() {
		String methodName = new Object(){}.getClass().getEnclosingMethod().getName();

		return getTransactionStatus(methodName);
	}

	/**
	 * {@link TransactionStatus}를 가져온다.<br/>
	 * 기본 설정되는 전달행위(Propagation Behavior)는 {@link TransactionDefinition#PROPAGATION_REQUIRED} 이다
	 *
	 * @param transactionName 트랜젝션명
	 * @return
	 */
	protected TransactionStatus getTransactionStatus(String transactionName) {
		return getTransactionStatus(transactionName, TransactionDefinition.PROPAGATION_REQUIRED);
	}

	/**
	 * {@link TransactionStatus}를 가져온다.<br/>
	 * 전달행위(Propagation Behavior) 종류는 {@link TransactionDefinition} 참고
	 *
	 * @param transactionName 트랜젝션명
	 * @param propagationBehavior 전달행위
	 * @return
	 */
	protected TransactionStatus getTransactionStatus(String transactionName, int propagationBehavior) {
		return getTransactionStatus(transactionName, propagationBehavior, TransactionDefinition.ISOLATION_DEFAULT);
	}

	/**
	 * {@link TransactionStatus}를 가져온다.<br/>
	 * 전달행위(Propagation Behavior) 종류는 {@link TransactionDefinition} 참고<br/>
	 * 격리레벨(Isolation Level) 종류는 {@link TransactionDefinition} 참고
	 *
	 * @param transactionName 트랜젝션명
	 * @param propagationBehavior 전달행위
	 * @param isolationLevel 격리레벨
	 * @return
	 */
	protected TransactionStatus getTransactionStatus(String transactionName, int propagationBehavior, int isolationLevel) {
		DefaultTransactionDefinition dtd = new DefaultTransactionDefinition();
		dtd.setName(transactionName);
		dtd.setPropagationBehavior(propagationBehavior);
		dtd.setIsolationLevel(isolationLevel);
		dtd.setTimeout(20);
		return transactionManager.getTransaction(dtd);
	}

	/**
	 * 트랜젝션을 커밋한다.
	 *
	 * @param status
	 */
	protected void transactionCommit(TransactionStatus status) throws TransactionException  {
		try {
			if (status != null) transactionManager.commit(status);
		} catch (TransactionException e) {
			throw e;
		}
	}

	/**
	 * 트랜젝션을 롤백한다.
	 *
	 * @param status
	 */
	protected void transactionRollback(TransactionStatus status) throws TransactionException {
		try {
			if (status != null) transactionManager.rollback(status);
		} catch (TransactionException e) {
			throw e;
		}
	}

	/**
	 * 현재 Execytor Type을 리턴한다.
	 *
	 * @return
	 */
	protected ExecutorType getExecutorType() {
		return sqlSessionTemplate.getExecutorType();
	}

	/**
	 * 배치여부 확인
	 *
	 * @return
	 */
	protected boolean isBatch() {
		return getExecutorType().equals(ExecutorType.BATCH);
	}

}
