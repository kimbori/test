package net.e4net.caas.common;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.service.BlockchainService;
import net.e4net.caas.common.service.DntService;
import net.e4net.caas.common.service.LoginService;
import net.e4net.caas.common.util.EncodeUtilLib;
import net.e4net.caas.common.util.FileMngUtils;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.common.vo.UserVO;

@Slf4j
@Controller
public class IndexController extends AbstractController {
	
	//@Autowired private CmmService cmmService;
	@Autowired private LoginService loginService;
	@Autowired private BlockchainService blockchainService;
	@Autowired private DntService dntService;

	@Value("${spring.profiles.active}") private String pSpringProfilesActive;
	@Value("${callback.api.myHost}") private String pCallbackApiMyHost;
	@Value("${caas.web.login.enable}") private boolean pCaasWebLoginEnable;

	/**
	 * 모바일용 자동 로그인 처리
	 * 	(TODO) 접근에 대한 보안 대책 필요
	 * @param cnrCi
	 * @param oprGrpId
	 */
	@RequestMapping("/public/loginProc")       
	public String loginProc(@RequestParam String cnrCi, @RequestParam String oprGrpId, String redUrl, HttpServletRequest request) throws CaasException {
		log.debug("[loginProc] before. cnrCi=" + cnrCi);
		cnrCi = EncodeUtilLib.urlDecoder(cnrCi);  // (KGC)
		cnrCi = cnrCi.replace(" ", "+");			// (KGC)	
		log.debug("[loginProc] after. cnrCi=" + cnrCi);
		
		debug(cnrCi);
		debug(oprGrpId);

		UserVO userVO = loginService.selectUser(cnrCi, oprGrpId);
		
		if ( userVO == null) {
			throw new CaasException("9999", "인증 실패 하였습니다.");
		}

		// (KGC) 방어코드. 사용자등록은 되었는데 지갑주소가 없는 경우 생성해 준다.
		if(userVO.getWltAddr() == null || userVO.getWltAddr().isEmpty()) {
			BlockchainRetrunVO retVo = blockchainService.createWalletDntPrsn(oprGrpId, userVO.getCnrSq());
			if(retVo.isSuccess()) {
				dntService.changeDntWaltProc(retVo.getWalletAddr(), userVO.getCnrSq(), "system");
				userVO.setWltAddr(retVo.getWalletAddr());
			} else {
				log.error("지갑 생성 실패");	// (TODO) (KGC)실패 시 고려				
			}
		}

		userVO.setLoginYn(true);

        ArrayList<SimpleGrantedAuthority> authorities = new ArrayList<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_USER"));
        
        Authentication authentication  = new UsernamePasswordAuthenticationToken(userVO, null, authorities);
        SecurityContext securityContext = SecurityContextHolder.getContext();
        securityContext.setAuthentication(authentication);

        HttpSession session = request.getSession(true);
        session.setAttribute("SPRING_SECURITY_CONTEXT",securityContext);   // 세션에 spring security context 넣음

        // redirect url이 파라미터로 주어진 경우는 해당 URl로 이동하고
        //	미 지정인 경우는 default인 메인 페이지 화면으로 이동한다.
        //	SNS 캠페인 이동인 경우에 사용할 수 있다.
        // 관련 처리 : caas.app.js 파일 caasSns.shareCampaign() 함수 redUrl 값이 이 파라미터로 들어오게 된다.
        String url = null;
        if(redUrl != null && ! redUrl.isEmpty()) {
        	url = EncodeUtilLib.urlDecoder(redUrl);
    		//log.debug("[loginProc] redUrl=" + redUrl + ", url=" + url);
        } else {
        	url = "/moveMainPag";
        }
		return  "redirect:" + url;   
	}

	/* ==================================================================================
	 * 	테스트용 웹용 로그인 처리 - 개발 테스트 용도로만 사용한다.
	 */
	/**
	 * 로컬 테스트용 로그인 페이지. (외부 서비스용 아님)
	 * 	(*1) (보안) "local"인 경우만 접속을 허락한다. 서버에서 접속은 불허한다.
	 * 		"dev" 등 다른 경우는 hoem("/")으로 이동.
	 */
	@RequestMapping(value = {"/public/localLogin", "/moveLoginPag"})       
	public String loginPag(ModelMap model) { 	// (*1)
		if(! pCaasWebLoginEnable) {
			log.warn("웹(Web)  로그인 접속은 불허합니다.");
			return "redirect:/";
		}
		model.addAttribute("pSpringProfilesActive", pSpringProfilesActive);
		return "login";
	}

	/**
	 * 로컬 테스트 로그인 처리용. (외부 서비스용 아님)
	 * @param cnrCi
	 * @param oprGrpId
	 */
	@RequestMapping("/public/localLoginProc")       
	public String localLoginProc(@RequestParam String cnrCi, @RequestParam String oprGrpId, HttpServletRequest request) throws CaasException {         
		if(! pCaasWebLoginEnable) {
			log.warn("웹(Web)  로그인 접속은 불허합니다.");
			return "redirect:/";
		}
		return loginProc(cnrCi, oprGrpId, null, request);
	}

	/* ==================================================================================
	 * 	홈페이지 - 안드로이드 앱 다운로드 사이트로 사용.
	 */
	/**
	 * 홈 페이지.
	 */
	@RequestMapping("/")       
	public String index(ModelMap model) {              
		//log.debug("pSpringProfilesActive=" + pSpringProfilesActive + ", pCallbackApiMyHost=" + pCallbackApiMyHost);
		//OprGrpVO vo1 = cmmService.getOprGrpInfo("e4net");
		//log.debug("vo1=" + vo1);
		//UserVO user = getMyUserVO();
		//log.debug("user=" + user);
		return "index/index";
	}

	/**
	 * 안드로이드 앱 파일 다운로드
	 * 	예, "/public/download/CherryAndroid.apk"
	 */
	@RequestMapping("/public/download/{downFile}")       
	public void downCherryAndroid(@PathVariable String downFile, HttpServletResponse response) throws Exception {
		String downfile = "/home/caas/apk_download/" + downFile;
		FileMngUtils.downFile(response, downfile, downFile);
	} 

	/**
	 * 이 요청 URL은 앱이 설치된 경우에 앱(APP)에서만 인지하기 URL이다. 웹 접속은 허용하지 않는다. 
	 * 	앱이 미설치 된 경우에 웹으로 접속된다. 이에 대한 대응 페이지이다.
	 * 	테스트 URL 예, http://localhost:8080/public/sms/WTIT?redUrl=%2Fcampaign%2F111
	 */
	@RequestMapping("/public/sms/{oprGrpId}")       
	public String invalidPageFromSMS(@PathVariable String oprGrpId, String redUrl, ModelMap model) {
		model.addAttribute("urlType", "sms");
		model.addAttribute("oprGrpId", oprGrpId);
		model.addAttribute("redUrl", redUrl);
		log.debug("[invalidPageFromSMS] oprGrpId=" + oprGrpId + ", redUrl=" + redUrl);
		return "index/invalidPage";
	}

}

