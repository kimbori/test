package net.e4net.caas.common.adapter;

import org.springframework.util.concurrent.FailureCallback;
import org.springframework.util.concurrent.SuccessCallback;

public interface RestAdapterCallback<T> extends SuccessCallback<T>, FailureCallback {
}
