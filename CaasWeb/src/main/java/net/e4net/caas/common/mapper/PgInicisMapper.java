package net.e4net.caas.common.mapper;

import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.dvo.TbDntPrsnDVO;
import net.e4net.caas.common.vo.dvo.TbInipayAvLstDVO;
import net.e4net.caas.common.vo.dvo.TbInipayCertLstDVO;
import net.e4net.caas.common.vo.dvo.TbPgBllDVO;
import net.e4net.caas.common.vo.dvo.TbPgFlngDVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author djim
 * @since 2018.11.01
 */
@Repository
public interface PgInicisMapper {
	 
    /** 
     * PG BLL 등록
     */
    public int insertTbPgBll(TbPgBllDVO dvo);
    
    /**
     * PG BLL 수정
     */
    public int updateTbPgBll(TbPgBllDVO dvo);
    
    /**
     * PG BLL 시퀀스 조회
     */ 
    public long selectTbPgBllSeq();
    
    /** 
     * PG 빌링결제(충전) 등록
     */
    public int insertTbPgFlng(TbPgFlngDVO dvo);
    
    /**
     * PG 빌링결제(충전) 수정
     */
    public int updateTbPgFlng(TbPgFlngDVO dvo);
    
    /**
     * 충전 토큰정보 수정
     */
    public int updateTbPgFlngForTknSq(TbPgFlngDVO dvo);

    /**
     * PG BLL 결제 시퀀스 조회
     */ 
    public long selectTbPgFlngSeq();
    
    /**
     * PG 일반결제 인증 일련번호 조회
     */ 
    public long selectTbInipayCertSeq();
    
    /** 
     * PG 일반결제 인증 등록
     */
    public int insertTbInipayCertLst(TbInipayCertLstDVO dvo);
    
    /**
     * PG 일반결제 인증 수정
     */
    public int updateTbInipayCertLst(TbInipayCertLstDVO dvo);
    
    /**
     * PG 일반결제 인증 조회
     */
    public TbInipayCertLstDVO selectTbInipayCertLst(long inipayCertSq);
    
    /**
     * PG 일반결제 결제 일련번호 조회
     */ 
    public long selectTbInipayAvSeq();
    
    /** 
     * PG 일반결제 결제 등록
     */
    public int insertTbInipayAvLst(TbInipayAvLstDVO dvo);
    
    /**
     * PG 일반결제 결제 수정
     */
    public int updateTbInipayAvLst(TbInipayAvLstDVO dvo);
    
    /**
    * PG 일반결제 결제 정상결제건 조회
    */
    public String selectTbInipayAvLstCnt(String stlmNtno);
    
    /**
    * 기부자정보 조회
    */
    public TbDntPrsnDVO selectTbDntPrsn(long cnrSq);

}
