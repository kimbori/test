package net.e4net.caas.common.util;

import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class EncodeUtilLib {

	/**
	 * 문자열에 대한 SHA512 해시코드를 얻는다.
	 * @param src 소스 문자열
	 * @return 해시코드. null 이면 실패.
	 */
    public static String getHashSHA512(String src) {
		try {
			MessageDigest objSHA = MessageDigest.getInstance("SHA-512");
	        byte[] bytSHA = objSHA.digest(src.getBytes());
	        BigInteger intNumber = new BigInteger(1, bytSHA);
	        String strHashCode = intNumber.toString(16);
	
	        while (strHashCode.length() < 128) {
	            strHashCode = "0" + strHashCode;
	        }
	        return strHashCode;
	    } catch (NoSuchAlgorithmException e) {
	    	log.error("msg=" + e.getMessage());
	    }
        return null;
    }

    /**
     * URI에 대한 인코딩 처리. 한글이 포함된 경우에 사용.
     * 	Same as encodeURIComponent() in Javascript
     * @param url
     * @param enc
     * @return
     */
    public static String urlEncoder(String url, String enc) {
    	String retUrl = "";
    	try {
			retUrl = URLEncoder.encode(url, enc);
		} catch (UnsupportedEncodingException e) {
	    	log.error("url=" + url + ", msg=" + e.getMessage());
		}
    	return retUrl;
    }
    public static String urlEncoder(String url) {
    	return urlEncoder(url, "UTF-8");
    }

    /**
     * 	Same as decodeURIComponent() in Javascript
     * @param url
     * @param enc
     * @return
     */
    public static String urlDecoder(String url, String enc) {
    	String retUrl = "";
    	try {
			retUrl = URLDecoder.decode(url, enc);
		} catch (UnsupportedEncodingException e) {
	    	log.error("url=" + url + ", msg=" + e.getMessage());
		}
    	return retUrl;
    }
    public static String urlDecoder(String url) {
    	return urlDecoder(url, "UTF-8");
    }

    /**
     * 한글만 인코딩 처리
     * @param url
     * @param enc
     * @return
     */
    public static String urlHanEncoder(String url, String enc) {
		char[] txtChar = url.toCharArray();
		for(int j = 0; j < txtChar.length; j++) {
			if(txtChar[j] >= '\uAC00' && txtChar[j] <= '\uD7A3') {
				String targetText = String.valueOf(txtChar[j]);
				try {
					url = url.replace(targetText, URLEncoder.encode(targetText, enc));
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				}
			} 
		}

    	return url;
    }
    public static String urlHanEncoder(String url) {
    	return urlHanEncoder(url, "UTF-8");
    }

}