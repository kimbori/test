package net.e4net.caas.common.vo;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class KakaoPayApiReadyPVO extends KakaoRestVO{
	private static final long serialVersionUID = 5552459882874180733L;

	//가맹점 코드. 10자.
	@JsonProperty("cid")
	String cid; 
	
	//가맹점 주문번호. 최대 100자
	@JsonProperty("partner_order_id")
	String partnerOrderId;
	
	@JsonProperty("partner_user_id")
	String partnerUserId; 
	
	@JsonProperty("item_name")
	String itemName; 
	
	@JsonProperty("quantity")
	String quantity = "1"; 
	
	@JsonProperty("total_amount")
	String totalAmount; 
	
	@JsonProperty("tax_free_amount")
	String taxFreeAmount; 
	
	@JsonProperty("approval_url")
	String approvalUrl; 
	
	@JsonProperty("cancel_url")
	String cancelUrl; 
	
	@JsonProperty("fail_url")
	String failUrl; 
}
