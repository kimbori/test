package net.e4net.caas.common.service;

import java.security.MessageDigest;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Hex;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import mup.mcash.module.common.McashCipher;
import net.e4net.caas.common.base.BaseAbstractService;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.mapper.CertMapper;
import net.e4net.caas.common.util.JsonUtils;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.CertInfoVO;
import net.e4net.caas.common.vo.OprGrpVO;
import net.e4net.caas.common.vo.dvo.TbCertiPrsnDVO;
import net.e4net.caas.common.vo.dvo.TbDntPrsnDVO;

/**
 * Cert 서비스
 *
 * @author yeongeun.seo
 * @since 2018.11.30
 */ 
@Slf4j
@Service
public class CertService extends BaseAbstractService {
	
	@Autowired private CertMapper certMapper;
	@Autowired private DntService dntService;
    
	/**
	 * 본인인증서비스 결과 처리 (복화화 등)
	 * @param request
	 * @param oprGrp
	 * @return
	 * @throws Exception
		Return Paramameter
			Resultcd	4byte 고정	결과코드 (0000: 성공, 그 외: 실패)
			Resultmsg	100byte 	결과메세지
			Mobilid		15byte 고정	모빌리언스 거래번호
			Cryptyn		1byte 고정	암호화 사용 여부 “Y” 고정
			Keygb		1byte 고정	암호화key 지정 값
			MSTR		2000byte 	가맹점 전달 콜백변수
			Signdate	14byte 이하	인증일자
			Svcid		12byte 고정	서비스ID
			Tradeid		40byte 이하	상점거래번호
			Name		9byte 이하	사용자명 (암호화)
			No			12byte 이하	사용자 전화번호 (암호화)
			Commid		3byte 고정	이동통신사 (암호화)
			Socialno	13byte 이하	생년월일 (19800101) (암호화)
			Sex			1byte 고정	성별 (M:남성, F:여성) (암호화)
			Foreigner	1byte 고정	외국인여부 (Y:외국인 / N:내국인) (암호화)
			Ci			88byte 고정	Ci값 (암호화)
			Di			64byte 고정	Di값 (암호화)
			CI_Mode		2byte 고정	요청구분
			DI_Code		12byte 고정	웹사이트코드
			Mac			64byte 고정	검증키 (암호화)
		결과코드 (Resultcd)
			코드		설명
			0000	성공
			0011	해당하는 폰 번호가 없습니다. 통신사 구분 및 폰번호를 정확히 입력하십시오
			0012	생년월일(성별)이 일치하지 않습니다
			0013	인증 또는 인증번호 발송 실패입니다 (SKT 통신사만 해당)
			0017	선불요금제 사용자는 인증 불가합니다
			0018	제한요금제 사용자는 인증 불가합니다
			0024	명의자 이름이 일치하지 않습니다
			0025	법인 휴대폰은 인증 불가합니다
			0026	일시 정지 상태인 경우 인증 불가합니다
			0043	유효한인증내역(인증번호/유효시간)이 없습니다
			0071	등록되지 않은 서비스입니다. 서비스 제공업체에 문의하세요
			0081	현재 제공되지 않고 있는 통신사입니다
			0082	현재 제공되지 않고 있는 인증방식입니다
			0095	인증번호 발송이 실패하였으니 잠시 후 이용하세요.
			0097	요청자료에 문제가 있습니다. 서비스업체에 문의하세요
			0098	통신사 서버 점검중이므로 잠시후 사용하십시오
			0099	현재 사용자가 많아서 서비스가 지연되고 있습니다. 잠시 후 사용하세요
	 */
    public CertInfoVO certResultProc(HttpServletRequest request, OprGrpVO oprGrp) throws Exception {
    	String encKey = oprGrp.getAlterCtEncKey();
    	log.debug("oprGrp=" + oprGrp + ", encKey=" + encKey);
    	
    	String Svcid		= request.getParameter("Svcid");		//서비스아이디
    	String Mobilid 		= request.getParameter("Mobilid");		//모빌리언스 거래번호
    	String Signdate		= request.getParameter("Signdate");		//결제일자
    	String Tradeid		= request.getParameter("Tradeid");		//가맹점 거래번호
    	String Name			= request.getParameter("Name");			//이름
    	String No			= request.getParameter("No");			//휴대폰번호
    	String Commid		= request.getParameter("Commid");		//이동통신사
    	String Resultcd		= request.getParameter("Resultcd");		//결과코드
    	String Resultmsg	= new String(request.getParameter("Resultmsg").getBytes("iso-8859-1"), "euc-kr");	//결과메시지
    	String Cryptyn		= request.getParameter("Cryptyn");		//암호화 사용 여부 (default : Y)
    	String Keygb		= request.getParameter("Keygb");		//암호화 Key 구분 (0 : CI_SVCID 8자리, 1·2 : 가맹점 관리자 등록 후 사용)
    	String Socialno		= request.getParameter("Socialno");		//생년월일
    	String Sex			= request.getParameter("Sex");			//성별 (남성:M, 여성:F)
    	String Foreigner	= request.getParameter("Foreigner");	//외국인여부 (외국인 : Y)
    	String Ci			= request.getParameter("Ci");			//CI
    	String Di			= request.getParameter("Di");			//DI
    	String CI_Mode		= request.getParameter("CI_Mode");		//CI_Mode 41:LMS문구설정, 51:SMS문구설정, 61:SMS발송
    	String DI_Code		= request.getParameter("DI_Code");		//웹사이트코드
    	String Mac			= request.getParameter("Mac");			//검증키
    	String MSTR			= request.getParameter("MSTR");			//가맹점 확장 변수
		
		/*********************************************************************************
		' Okurl 암호화 사용 시 사용자정보 암호문 전달
		' 주) 고유KEY 설정 시 비밀키 : "가맹점 고유 KEY + CI_SVCID 앞 8자리" (16byte)    			// Keygb 1 or 2
		'			고유KEY 미 설정 시 비밀키 : "CI_SVCID 앞 8자리 + CI_SVCID 앞 8자리" (16byte)	// keygb 0
		'			encKey 항목에 key 세팅
		*********************************************************************************/
    	log.debug("암호화된 데이터");
    	log.debug("Name : " + Name);
    	log.debug("No : " + No);
    	log.debug("Commid : " + Commid);   	
    	log.debug("Socialno : " + Socialno);
    	log.debug("Sex : " + Sex);  	
    	log.debug("Foreigner : " + Foreigner);
    	log.debug("Ci : " + Ci);    	
    	log.debug("Di : " + Di);
    	log.debug("Resultmsg : " + Resultmsg);

    	Name		= McashCipher.decodeString( Name, 		encKey );
    	No			= McashCipher.decodeString( No, 		encKey );   	
    	Commid		= McashCipher.decodeString( Commid, 	encKey );
    	Socialno	= McashCipher.decodeString( Socialno,	encKey );
    	Sex			= McashCipher.decodeString( Sex, 		encKey );
    	Foreigner	= McashCipher.decodeString( Foreigner, 	encKey );
    	Ci			= McashCipher.decodeString( Ci, 		encKey );
    	Di			= McashCipher.decodeString( Di, 		encKey );
    	
		/*********************************************************************************
		* Mac값 위변조 여부 확인 SHA256(Signdate+Di+Ci+Mobilid+Svcid.substring(0, 8)+Svcid.substring(0, 8))
		**********************************************************************************/
    	String key = Signdate+Di+Ci+Mobilid+Svcid.substring(0, 8)+Svcid.substring(0, 8);
		MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
		sha256.update(key.getBytes());
		byte[] byteData = sha256.digest();
		
		String sha = Hex.encodeHexString(byteData);
		
		if( !Mac.equals(sha) ){
			log.error("데이터가 위·변조되었습니다.");
			// (TODO) 위변조시 처리 필요.
		}
		
    	CertInfoVO certVO = new CertInfoVO();
		certVO.setSvcId(Svcid);			//서비스아이디                                                 
		certVO.setMobilId(Mobilid);		//모빌리언스 거래번호                                             
		certVO.setSignDate(Signdate);	//결제일자                                                   
		certVO.setTradeId(Tradeid);		//가맹점 거래번호                                               
		certVO.setName(Name);			//이름                                                     
		certVO.setNo(No);				//휴대폰번호                                                  
		certVO.setCommId(Commid);		//이동통신사                                                  
		certVO.setResultCd(Resultcd);	//결과코드                                                   
		certVO.setResultMsg(Resultmsg);	//결과메시지                                                  
		certVO.setCryptYn(Cryptyn);		//암호화 사용 여부 (default : y)                                
		certVO.setKeyGb(Keygb);			//암호화 key 구분 (0 : ciSvcid 8자리, 1·2 : 가맹점 관리자 등록 후 사용)    
		certVO.setSocialNo(Socialno);	//생년월일                                                   
		certVO.setSex(Sex);				//성별 (남성:m, 여성:f)                                        
		certVO.setForeigner(Foreigner);	//외국인여부 (외국인 : y)                                        
		certVO.setCi(Ci);				//ci                                                     
		certVO.setDi(Di);				//di                                                     
		certVO.setCiMode(CI_Mode);		//ciMode 41:lms문구설정, 51:sms문구설정, 61:sms발송                
		certVO.setDiCode(DI_Code);		//웹사이트코드                                                 
		certVO.setMac(Mac);				//검증키                                                    
		certVO.setMstr(MSTR);			//가맹점 확장 변수
		
    	if(!StringUtils.isNullOrBlank(MSTR)) {
    		String[] mstrArry = MSTR.split("[|]");
    		certVO.setOprGrpId(mstrArry[0]);		//운영단체ID
    		certVO.setTrmlId(mstrArry[1]);			//디바이스ID
    		if(mstrArry.length == 3) {
    			certVO.setMailAddr(mstrArry[2]);	// 메일주소
    		}
    	}
    	return certVO;
    }

	public TbCertiPrsnDVO saveUserInfo(OprGrpVO oprGrp, CertInfoVO certInfoVO, String writerID) throws CaasException, JSONException {
  		// 본인인증서비스 결과 적재 처리
  		TbCertiPrsnDVO certDVO = createCertLogProc(certInfoVO, oprGrp, writerID);

  		/*
  		 * ***************************************************************************************************
  		 */
  		// 연계처리결과 S인경우 기부자정보 등록  		
  		if("S".equals(certDVO.getLnkPrcRst())) {
 		
			log.debug("############################## 기부자 기회원 조회 처리 ##############################");
			TbDntPrsnDVO dntDVO = dntService.checkDntProc(certDVO);
			
			if(dntDVO != null && ! StringUtils.isNullOrBlank(dntDVO.toString())) {
				log.debug("############################# 기회원 정보 업데이트 처리 ##############################");
				if( dntService.changeDntCertInfoProc(certInfoVO, dntDVO, writerID) < 1 ) {
					throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
				}
			} else {
				log.debug("########################## 기부자DB 생성 처리 (지갑생성포함) ##########################");
				dntDVO = dntService.createDntProc(certInfoVO, certDVO, writerID);
	  			log.debug("dntDVO : " + dntDVO.toString());
			}  			  			
			
			log.debug("########################## 본인인증DB 기부자SQ 업데이트 처리 ##########################");
			if(changeCertLogProc(dntDVO, writerID) < 1) {
	  			throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
	  		}
  		} else {
  			return null;
  		}
  		return certDVO;
	}
	
    // 본인인증서비스 결과 적재 처리
    public TbCertiPrsnDVO createCertLogProc(CertInfoVO vo, OprGrpVO oprGrp, String writerID) throws CaasException, JSONException {
    	TbCertiPrsnDVO certDVO = new TbCertiPrsnDVO();
    	JSONObject reqInfo = new JSONObject();
    	reqInfo.put("ciSvcid", oprGrp.getCtSvcId());
    	reqInfo.put("siteUrl", oprGrp.getHomeUrl());
    	String certRst = "0000".equals(vo.getResultCd()) ? "S" : "F";
    	    	
    	// 기부자 본인인증내역 적재
  		long tbCertSeq = selectTbCertiPrsnSeq();
  		certDVO.setPrsnAuthSq(tbCertSeq);			// 본인인증sq
  		certDVO.setOprGrpId(vo.getOprGrpId());		// 운영단체id
  		certDVO.setLnkAgency(oprGrp.getBcKd());		// 연계agency (02: kg-mobilians)
  		certDVO.setLnkPrcRst(certRst);				// 연계처리결과 (S:성공, F:실패)
  		certDVO.setLnkReqInfo(reqInfo.toString());	// 연계요청정보
  		certDVO.setLnkRstInfo(JsonUtils.convertvObjToJson(vo));				// 연계결과정보
  		//certDVO.setCnrSq();						// 기부자SQ - 업데이트처리
  		certDVO.setCnrCi(vo.getCi());				// 기부자CI
  		certDVO.setCnrDi(vo.getDi());				// 기부자DI
  		certDVO.setBrthDt(vo.getSocialNo());		// 생년월일
  		certDVO.setSex(vo.getSex());				// 성별
  		certDVO.setFrmnYn(vo.getForeigner());		// 외국인여부
  		certDVO.setFstRegUsid(writerID);			// 최초등록자
  		certDVO.setLstChUsid(writerID);				// 최종변경자
		
  		log.debug("###############################################################################"); 
  		log.debug("certDVO : " + certDVO.toString());
  		
  		if( insertTbCertiPrsnInfo(certDVO) < 1 ) {
    		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
    	}    	
    	
    	return certDVO;
    }
    
    // 본인인증서비스 기부자SQ 업데이트 처리
    public int changeCertLogProc(TbDntPrsnDVO vo, String writerID) throws CaasException {
  		TbCertiPrsnDVO certUpDvo = new TbCertiPrsnDVO();
  		
  		certUpDvo.setCnrSq(vo.getCnrSq());				// 기부자SQ
  		certUpDvo.setLstChUsid(writerID);				// 최종변경자
  		certUpDvo.setPrsnAuthSq(vo.getPrsnAuthSq());	// 본인인증SQ

    	return updateTbCertiPrsnInfo(certUpDvo);
    }
    
    /**
     * 본인인증 시퀀스 조회
     *
     * @return 본인인증 시퀀스 조회
     */
    public long selectTbCertiPrsnSeq() {
        return certMapper.selectTbCertiPrsnSeq();
    }
    
    /**
     * 본인인증 내역 등록 
     *
     * @param 본인인증 내역 등록 정보 
     * @return 등록결과
     */
    public int insertTbCertiPrsnInfo(TbCertiPrsnDVO dvo) {
        return certMapper.insertTbCertiPrsnInfo(dvo);
    }
    
    /**
     * 본인인증 정보 갱신
     *
     * @param 본인인증 갱신 정보
     * @return 수정결과
     */
    public int updateTbCertiPrsnInfo(TbCertiPrsnDVO dvo) {
        return certMapper.updateTbCertiPrsnInfo(dvo);
    }
}
