package net.e4net.caas.common.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;


/**
 * class 화면 전 후 처리를 위한 인터셉터
 *
 * @author djim
 * @since 2018-11-01
 */
public class CommonInterceptor extends HandlerInterceptorAdapter {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());


	//private final List<String> passUriList = Arrays.asList("/", "/login", "/logout", "/api/ping", "/error");

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		logger.debug("Interceptor > preHandle : [{}] [{}]", request.getRequestURI(), request.getRequestURL().toString());

		return super.preHandle(request, response, handler);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView mav) throws Exception {
		logger.debug("Interceptor > postHandle : [{}]", request.getRequestURI());

		super.postHandle(request, response, handler, mav);
	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
		logger.debug("Interceptor > afterCompletion : [{}]", request.getRequestURI());

		if (ex != null) {
			logger.error("afterCompletion Exception Catch", ex);
		}
		super.afterCompletion(request, response, handler, ex);
	}

}
