package net.e4net.caas.common.vo;

import groovy.transform.ToString;
import lombok.Data;

@ToString
@Data
public class BlockchainRetrunVO {
	private boolean	success;		// 성공여부
    private Object	result;			// API 별
    private String	errKind;		// 실패 종유("logic", "api", "db", "exception")
    private String	errCd;			// 실패인 경우 코드
    private String	errMsg;			// 실패인 경우 메시지

    /**
     * 지갑 잔고조회 성공인 경우에 잔고 조회.
     * 	대상 API : getWalletBalance()
     * @return 지갑 잔고. 오류면 -1
     */
    public long getBalance() {
    	return success ? (long)result : -1;
    }

    /**
     * 지갑생성 성공 시 지갑주소 조회.
     * 	대상 API : 
     * @return
     */
    public String getWalletAddr() {
    	return success ? ((BlockchainWalletVO)result).getWltAddr() : "";
    }

    public long getWalletAddrSq() {
    	return success || result != null ? ((BlockchainWalletVO)result).getWltAddrSq() : -1;
    }

    /**
     * 지갑토큰 이체 시 토큰전송SQ 조회.
     * 	대상 API : walletTokenDeposit(), walletTokenWithdraw(), walletTokenTransfer()
     * @return
     */
    public long getToknTrnfSq() {
    	return success ? ((BlockchainTokenTxVO)result).getToknTrnfSq() : -1;
    }

    /**
     * 성공 처리 결과 설정
     * @param result
     */
    public void setSuccess(Object result) {
    	this.success = true;
    	this.result = result;
    }

    /**
     * API 처리 결과 실패로 response된 경우 설정
     * @param errCd
     * @param errMsg
     */
    public void setFailApi(String errCd, String errMsg) {
    	this.success = false;
    	this.errKind = "api";
    	this.errCd = errCd;
    	this.errMsg = errMsg;
    }

    /**
     * Exception 실패인 경우 설정
     * @param e
     */
    public void setFailDb(String errMsg) {
    	this.success = false;
    	this.errKind = "db";
    	//this.errCd = "";
    	this.errMsg = errMsg;
    }

    /**
     * Exception 실패인 경우 설정
     * @param e
     */
    public void setFailException(Exception e) {
    	this.success = false;
    	this.errKind = "exception";
    	this.errCd = "";
    	this.errMsg = e.getMessage();
    }

}