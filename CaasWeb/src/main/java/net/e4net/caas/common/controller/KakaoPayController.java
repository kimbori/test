package net.e4net.caas.common.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.service.KakaoPayService;
import net.e4net.caas.common.vo.KakaoPayApiCancelPVO;
import net.e4net.caas.common.vo.KakaoPayApiReadyPVO;

/**
 * 카카오페이를 통한 충전 컨트롤러
 *
 * @author djim
 * @since 2018.11.01
 * history  2018.11.01 최초작성
 */
@Controller
public class KakaoPayController extends AbstractController {

	
    //kakaopay 준비내역 및 결제내역 서비스
    @Autowired
    private KakaoPayService kakaoPayService;
    
    
    /**
     * 카카오페이  준비요청 컨트롤러
     * 
     * @param HttpServletRequest request
     * @param KakaoPayApiReadyPVO pvo
     * @param HttpSession httpSession
     * @return ModelAndView 
     * @throws CaasException
     */
    @RequestMapping(value = "/kakaoPayReady")
    public ModelAndView ready(HttpServletRequest request, KakaoPayApiReadyPVO pvo, HttpSession httpSession) throws CaasException {
        
    	return kakaoPayService.ready(request, pvo, httpSession);
    }
    
    
    
    /**
     * 카카오 승인 호출 컨트롤러
     * @param httpSession
     * @param pgToken 카카오 거래 토큰정보
     * @return
     * @throws CaasException
     */
    @RequestMapping(value = "/public/kakaoPayApprove")
    public ModelAndView approve(HttpSession httpSession, @RequestParam(value="pg_token") String pgToken) throws CaasException {

    	return kakaoPayService.approve(httpSession, pgToken);
    }
    
	/**
	 * 사용자가 결제 취소 시 호출 되는 URL
	 * @param httpSession
	 * @param pvo
	 * @return
	 */
    @RequestMapping(value = "/public/kakaoPayCancel")
    public ModelAndView cancel(HttpSession httpSession, KakaoPayApiCancelPVO pvo) {
    	debug(pvo.toString());
    	ModelAndView mav = new ModelAndView("common/flngResultPag");
    	
		mav.addObject("process", "kakaoBll");
		mav.addObject("resultcode", "01");
		mav.addObject("resultmsg", "결제 취소");
		return mav;
    }
    
	/**
	 * 사용자가 결제 실패 시 호출 되는 URL
	 * @param httpSession
	 * @param pvo
	 * @return
	 */
    @RequestMapping(value = "/public/kakaoPayFail")
    public ModelAndView fail(HttpSession httpSession, KakaoPayApiCancelPVO pvo) {
    	ModelAndView mav = new ModelAndView("common/flngResultPag");
    	debug("kakaoPayFail");
    	debug(pvo.toString());
    	mav.addObject("process", "kakaoBll");
		return mav;
    }
}
