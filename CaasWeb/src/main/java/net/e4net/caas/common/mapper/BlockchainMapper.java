package net.e4net.caas.common.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import net.e4net.caas.common.vo.BlockchainTokenTxVO;
import net.e4net.caas.common.vo.BlockchainWalletVO;

@Repository
public interface BlockchainMapper {

	/*
	 * TB_GRP_OPR
	 */
	// 블록체인 접속정보 얻기
	//public BlockchainLinkInfoVO getBlockchainLinkInfo(String oprGrpId);

	/*
	 * TB_WLT_ADDR
	 */
	// 지갑 신규 저장
	public int insertBlockchainWallet(BlockchainWalletVO vo);
	// 지갑 정보 조회
	public BlockchainWalletVO getBlockchainWallet(long wltAddrSq);
	// 지갑 잔고 조회
	public long getWalletBalance(long wltAddrSq);
	// 지갑 잔고 갱신
	public int updateWalletBalance(BlockchainWalletVO vo);
	// 지갑 생성 done_yn = "Y" 갱신
	public int updateWalletDoneYes(long wltAddrSq);

	/*
	 * TB_TKN_SND
	 */
	// 토큰전송 트랜젝션 처리 결과 저장
	public int insertTokenSend(BlockchainTokenTxVO vo);
	// 토큰전송 내역 조건 조회
	public List<BlockchainTokenTxVO> selectTokenSend(BlockchainTokenTxVO vo);
	// 토큰전송 내역 건 조회
	public BlockchainTokenTxVO getTokenSend(long toknTrnfSq);
	// 트랜잭션 검증 결과 저장
	public int updateTxVerify(BlockchainTokenTxVO vo);
	// 토큰전송 done_yn = "Y" 갱신
	public int updateTokenSendDoneYes(long toknTrnfSq);
}
