package net.e4net.caas.common.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.mapper.FlngProcMapper;
import net.e4net.caas.common.vo.PgBllRVO;

/**
 * 충전 서비스
 *
 * @author djim
 * @since 2018.11.01
 */ 
@Service
public class FlngProcService extends AbstractDao {
    @Autowired
    private FlngProcMapper flngProcMapper;
    
    /**
     * PG 빌링키 목록 조회
     *
     * @return PG 빌링키 목록 조회
     */
    public List<PgBllRVO> selectBllList(long cnrSq) {
        return flngProcMapper.selectBllList(cnrSq);
    }
    
    /**
     * 빌링키 조회
     *
     * @return PG 빌링키 목록 조회
     */
    public PgBllRVO selectBllKey(long bllAuthSq) {
        return flngProcMapper.selectBllKey(bllAuthSq);
    }
}
