package net.e4net.caas.common.vo;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class KakaoRestVO extends RestVO {

	private static final long serialVersionUID = -485714499364856306L;

	@JsonIgnore
	HttpStatus httpCode;
	
	@JsonProperty("code")
	String code;
	
	@JsonProperty("msg")
	String msg; 
	
	@JsonProperty("extras")
	private Extras extras;
	
	@Data
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class Extras {
		@JsonProperty("method_result_code")
		private String methodResultCode;
		
		@JsonProperty("method_result_message")
		private String methodResultMessage;
	}
}
