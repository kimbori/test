package net.e4net.caas.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class BlockchainWalletVO extends ExtLinkVO {
    private long	wltAddrSq;		// 지갑주소SQ
    private String	mailAddr;		// 지갑생성 시 메일주소
    private String	wltAddr;		// 지갑주소
    private String	wltKn;			// 지갑종류
    private long	wltLv;			// 지갑잔고
}
