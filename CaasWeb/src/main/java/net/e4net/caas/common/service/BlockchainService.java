package net.e4net.caas.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.base.BaseAbstractService;
import net.e4net.caas.common.mapper.BlockchainMapper;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.sample.service.SampleService;

/**
 * 블록체인 공통 처리
 * 		BlockchainService		: 서비스 호출용 API
 * 			외부  메소드 트랜젝션 실패 여부를 구분처리하기 위해 지갑생성과 토큰이체 기록을 남긴다. (*1)
 * 		BlockchainTrsService	: 트랜젝션 별도 처리
 * 			외부 메소드 트랜젝션 실패와 상관없이 내부 메소드 트랜젝션 처리를 수행한다.
 * 			또한 토큰 이체 후 잔고 동기화를 ASYNC 처리를 수행한다.
 * 		BlockchainApiService'
 * 			실제 Blockchain 서버와의 인터페이스를 수행한다.
 * @author KC.KIM
 *
 */
@Service
@Slf4j
public class BlockchainService extends BaseAbstractService {

	@Autowired private BlockchainTrsService bcTrsService;
	@Autowired private BlockchainMapper mapper;

	/* ==================================================================================
	 * 블록체인 지갑 잔고 조회 API
	 */

	/**
	 * 지갑주소에 대한 잔고를 조회.
	 * @param oprGrpId 운영단체ID
	 * @param walletAddr 지갑주소
	 * @return retVo.isSuccess() : 성공여부, retVo.getBalance() : 잔고
	 */
	public BlockchainRetrunVO getWalletBalance(String oprGrpId, String walletAddr) {
		return bcTrsService.getWalletBalance(oprGrpId, walletAddr);
	}

	/* ==================================================================================
	 * 블록체인 토큰 전송 (이체) API
	 */
	/**
	 *	토큰입금(B2C) : 결제에 의한 토큰 입급 등에 사용 
	 *		운영단체-토큰 => 기부자-토큰, 가맹단체-토큰, 캠페인-토큰
	 *		운영단체지갑은 파라미터에서 제외
	 * @param oprGrpId 운영단체ID
	 * @param wltAddr 입금 지갑주소
	 * @param trnfTokn 이체 토큰 (금액 채리)
	 * @param trnfTp "21"(결제충전), "52"(Fiat정산취소)
	 * @return retVo.isSuccess() : 성공여부, retVo.getToknTrnfSq() : 토큰전송SQ
	 */
	public BlockchainRetrunVO walletTokenDeposit(String oprGrpId, String wltAddr, long trnfTokn, String trnfTp) {
		return walletTokenSub(oprGrpId, "B2C", null, wltAddr, trnfTokn, trnfTp);
	}

	/**
	 *	토큰출금(C2B) : Fiat 정산에 의한 토큰 출금 등에 사용
	 *		운영단체-토큰 <= 기부자-토큰, 가맹단체-토큰, 캠페인-토큰
	 *		운영단체지갑은 파라미터에서 제외
	 * @param oprGrpId 운영단체ID
	 * @param wltAddr  출금 지갑주소
	 * @param trnfTokn 이체 토큰 (금액 채리)
	 * @param trnfTp "22"(결제취소), "51"(Fiat정산요청)
	 * @return
	 */
	public BlockchainRetrunVO walletTokenWithdraw(String oprGrpId, String wltAddr, long trnfTokn, String trnfTp) {
		return walletTokenSub(oprGrpId, "C2B", wltAddr, null, trnfTokn, trnfTp);
	}

	/**
	 *	토큰이체 : 기부자-토큰, 가맹단체-토큰, 캠페인-토큰 상호간
	 * @param oprGrpId 운영단체ID
	 * @param frmWltAddr 출금 지갑주소
	 * @param toWltAddr 입금 지갑주소
	 * @param trnfTokn 이체 토큰 (금액 채리)
	 * @param trnfTp "31"(기부전송), "32"(기부취소), "41"(정산요청), "42"(정산취소)
	 * @return
	 */
	public BlockchainRetrunVO walletTokenTransfer(String oprGrpId, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp) {
		return walletTokenSub(oprGrpId, "C2C", frmWltAddr, toWltAddr, trnfTokn, trnfTp);
	}

	public BlockchainRetrunVO walletTokenSub(String oprGrpId, String transferType, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp) {
		BlockchainRetrunVO vo = bcTrsService.walletTokenTrans(oprGrpId, transferType, frmWltAddr, toWltAddr, trnfTokn, trnfTp);
		if(vo.getToknTrnfSq() != -1) {
			mapper.updateTokenSendDoneYes(vo.getToknTrnfSq());	// done_yn = "Y" 처리			
		}
		return vo;
	}

	/* ==================================================================================
	 * 블록체인 지갑생성 API - 운영단체만 제외하고 지갑이 생성된다.
	 *	운영단체의 경우는 B2C, C2B 이여서 지갑이 미리 생성된다. 운영자에서 등록한다.
	 */

	/**
	 * 가맹단체 지갑 만들기
	 * @param oprGrpId 운영단체ID
	 * @param mrchGrpId 가맹단체ID
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainRetrunVO createWalletGrpMct(String oprGrpId, String mrchGrpId) {
		return createWalletSub(oprGrpId, "02", mrchGrpId);
	}

	/**
	 * 캠페인 지갑 만들기
	 * @param oprGrpId 운영단체ID
	 * @param mrchGrpId 가맹단체ID
	 * @param cmpSq 캠페인SQ
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainRetrunVO createWalletCmpInf(String oprGrpId, String mrchGrpId, long cmpSq) {
		String emailGubun = mrchGrpId + "_" + String.valueOf(cmpSq);
		return createWalletSub(oprGrpId, "03", emailGubun);
	}

	/**
	 * 기부자 지갑 생성
	 * @param oprGrpId 운영단체ID
	 * @param 기부자SQ
	 * @param userid Writer ID
	 * @return
	 */
	public BlockchainRetrunVO createWalletDntPrsn(String oprGrpId, long cnrSq) {
		return createWalletSub(oprGrpId, "04", String.valueOf(cnrSq));
	}

	public BlockchainRetrunVO createWalletSub(String oprGrpId, String wltKn, String userid) {
		BlockchainRetrunVO vo = bcTrsService.createWallet(oprGrpId, wltKn, userid);
		if(vo.getWalletAddrSq() != -1) {
			mapper.updateWalletDoneYes(vo.getWalletAddrSq());	// done_yn = "Y" 처리
		}
		return vo;
	}

	/* ==================================================================================
	 * Rollback 테스트
	 */

	@SuppressWarnings("unused")
	public BlockchainRetrunVO createWalletDntPrsnFail(String oprGrpId, long cnrSq) {
		log.debug("지갑생성 트렌젝션 실패 테스트. Rollback 테스트");
		BlockchainRetrunVO vo = createWalletDntPrsn(oprGrpId, cnrSq);
		if(true) {
			throw new RuntimeException();			
		}
		return vo;
	}

	@SuppressWarnings("unused")
	public BlockchainRetrunVO walletTokenTransferFail(String oprGrpId, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp) {
		log.debug("토큰이체 트렌젝션 실패 테스트. Rollback 테스트");
		BlockchainRetrunVO vo = walletTokenTransfer(oprGrpId, frmWltAddr, toWltAddr, trnfTokn, trnfTp);
		if(true) {
			throw new RuntimeException();			
		}
		return vo;
	}

	/**
	 * rollback transaction 테스트 (단위 테스트)
	 */
	@Autowired private SampleService sampleService;
    public void transactionRollbackTest() {
    	sampleService.transactionRollbackTestModule("Middle", "TEST_ROLLBACK", 0, 1);
		bcTrsService.transactionRollbackTest();
     }

}