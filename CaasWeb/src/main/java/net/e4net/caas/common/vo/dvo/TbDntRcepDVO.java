package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
public class TbDntRcepDVO extends PageVO {
	private long dntnRcepSq;		//기부영수신청SEQ
	private long cnrSq;				//기부자SEQ
	private String pcsnStat;		//처리상태
	private String tgtStDt;			//대상시작일
	private String tgtEdDt;			//대상종료일
	private long dntnRcivCnt;		//기부영수건수
	private long dntnRcivSum;		//기부영수합계
	private String name;			//성명
	private String rrno;			//주민번호
	private String hpno;			//휴대폰번호
	private String issuMeth;		//발급방법
	private String mailAddr;		//메일주소
	private String fxno;			//팩스번호
	private String psad;			//우편번호
	private String addr;			//주소
	private String fstRegDttm;		//최초등록일시
	private String fstRegUsid;		//최초등록자
	private String lstChDttm;		//최종변경일시
	private String lstChUsid;		//최종변경자
}