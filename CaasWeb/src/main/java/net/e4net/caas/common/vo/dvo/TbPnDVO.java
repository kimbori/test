package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbPnDVO extends PageVO{
	
	private static final long serialVersionUID = -514783173873964722L;
	
	private long pnSq;				//공지사항SQ
	private String oprGrpId;		//운영단체ID
	private String title;			//제목
	private String ctnt;			//내용
	private String notcYn;			//알람여부
	private String fstRegDttm;		//최초등록일시
	private String fstRegUsid;		//최초등록자
	private String lstChDttm;		//최종변경일시
	private String lstChUsid;		//최종변경자

}
