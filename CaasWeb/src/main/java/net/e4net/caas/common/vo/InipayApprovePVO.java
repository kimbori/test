package net.e4net.caas.common.vo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class InipayApprovePVO extends RestVO{

	private static final long serialVersionUID = 3828433521165547129L;

	@JsonIgnore
	private final transient Logger logger = LoggerFactory.getLogger(InipayApprovePVO.class);
	
	@JsonProperty("P_TID")
	private String pTid; 
	@JsonProperty("P_MID")
	private String pMid; 
}
