package net.e4net.caas.common.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;
import net.e4net.caas.common.base.BaseAbstractService;
import net.e4net.caas.common.vo.BlockchainRetrunVO;
import net.e4net.caas.sample.service.SampleService;

@Service
@Slf4j
public class BlockchainTrsService extends BaseAbstractService {

	@Autowired private BlockchainApiService bcApiService;

	/**
	 * 지갑주소에 대한 잔고를 조회.
	 * @param oprGrpId 운영단체ID
	 * @param walletAddr 지갑주소
	 * @return retVo.isSuccess() : 성공여부, retVo.getBalance() : 잔고
	 */
	public BlockchainRetrunVO getWalletBalance(String oprGrpId, String walletAddr) {
		BlockchainRetrunVO retVo = bcApiService.getWalletBalanceSub(oprGrpId, walletAddr);
		return retVo;
	}

	/**
	 *	ASYNC 처리를 위해 이체처리와 잔고 동기화 처리를 별도 서비스(BlockchainApiService)에 두고 호출하였다.
	 *		같은 Service에 두는 경우는 ASYNC 처리가 안되기 때문이다.
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public BlockchainRetrunVO walletTokenTrans(String oprGrpId, String transferType, String frmWltAddr, String toWltAddr, long trnfTokn, String trnfTp) {
		BlockchainRetrunVO retVo = bcApiService.walletTokenSub(oprGrpId, transferType, frmWltAddr, toWltAddr, trnfTokn, trnfTp);

		if(retVo.isSuccess()) {
			// 이체 성공 후 DB 잔고를 갱신해 준다.
			log.debug("[walletTokenSub] 이체후 잔고 동기화 - Async 수행");
			bcApiService.asyncUpdateWalletBalance(oprGrpId, frmWltAddr, toWltAddr);				
		}
		return retVo;
	}

	/**
	 * 기부자 지갑 생성
	 * @param oprGrpId 운영단체ID
	 * @param wltKn 구분
	 * @param userid Writer ID
	 * @return
	 */
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
	public BlockchainRetrunVO createWallet(String oprGrpId, String wltKn, String userid) {
		return bcApiService.createWalletSub(oprGrpId, wltKn, userid);
	}

	/*
	 * 외부 메소드와 별도 트랜젝션으로 처리되기를 바라는 경우에 Propagation.NOT_SUPPORTED 지정. 
	 */
	@Autowired private SampleService sampleService;
	@Transactional(propagation = Propagation.NOT_SUPPORTED)
    public void transactionRollbackTest() {
    	sampleService.transactionRollbackTestModule("Internal", "TEST_ROLLBACK_I", 1, 0);
    }

}