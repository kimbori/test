package net.e4net.caas.common.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.env.Environment;

/**
 * Server 관련 Util
 *
 * @author djim
 * @since  2017.12.14
 *
 */
public class ServerUtils {
	private static final Logger logger = LoggerFactory.getLogger(ServerUtils.class);

	private static Environment env;
	private static String PROFILE;

	// was 기동시 listner 에서 세팅함
	public static void setEnvironment(Environment env){
		if( ServerUtils.env == null ){
			ServerUtils.env = env;
			ServerUtils.PROFILE = env.getProperty("spring.profiles.active");
			logger.info("ServerUtils init Start");
			logger.info("   set Environment value and PROFILE value..[{}]", ServerUtils.PROFILE);
			logger.info("ServerUtils init End");
		}
	}

	/**
	 * 서버 Production 여부 리턴
	 * @return
	 */
	public static boolean isProduction() {
		return PROFILE.equals("production");
	}

}
