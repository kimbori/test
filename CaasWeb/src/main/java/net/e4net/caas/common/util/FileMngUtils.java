package net.e4net.caas.common.util;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.util.FileCopyUtils;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class FileMngUtils {

	public static final int BUFF_SIZE = 2048;

	/**
	 * 서버 파일에 대하여 다운로드를 처리한다.
	 *
	 * @param response
	 * @param streFileNm 파일저장 경로가 포함된 형태  (from eGov)
	 * @param orignFileNm
	 * @throws Exception
	 */
	public static void downFile(HttpServletResponse response, String strFileNm, String orignFileNm) throws Exception {
		String downFileName = strFileNm;
		String orgFileName = orignFileNm;

		File file = new File(downFileName);
		
		if (!file.exists()) {
			throw new FileNotFoundException(downFileName);
		}

		if (!file.isFile()) {
			throw new FileNotFoundException(downFileName);
		}

		int fSize = (int) file.length();
		if (fSize > 0) {
			BufferedInputStream in = null;

			try {
				in = new BufferedInputStream(new FileInputStream(file));

				String mimetype = "application/x-msdownload";
				response.setContentType(mimetype);
				response.setHeader("Content-Disposition:", "attachment; filename=" + orgFileName);
				response.setContentLength(fSize);
				FileCopyUtils.copy(in, response.getOutputStream());
			} finally {
				closeAll(in);
			}
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
	}

	/**
	 * base64 문자열 이미지를 다운로드 한다.
	 * @param base64Img
	 * @param contentType "image/jpeg" (예)
	 * @param orignFileNm
	 */
	public static void downBase64Img(HttpServletResponse response, String base64Img, String contentType, String orignFileNm) throws Exception {
		byte[] byteImg = Base64.decodeBase64(base64Img);

		int fSize = byteImg.length;
		if(fSize > 0) {
			BufferedInputStream in = null;

			try {
				//String mimetype = "application/x-msdownload";
				response.setContentType(contentType);
				response.setHeader("Content-Disposition:", "attachment; filename=" + orignFileNm);
				response.setContentLength(fSize);
				FileCopyUtils.copy(byteImg, response.getOutputStream());
			} finally {
				closeAll(in);
			}
			response.getOutputStream().flush();
			response.getOutputStream().close();
		}
	}

	/**
	 * IMG 테그 src에 사용되는 형태의 base64 이미지를 다운로드 하는 경우에 사용
	 * 	(*1) BASE64 데이터 부분을 축출하기 위해, "data:image/jpeg;base64" 와 BASE64 부분을 분리한다.
	 * 	(*2) Content Type를 축출하기 위해, "data", "image/jpeg", "base64" 부분을 분리한다.
	 * @param base64ImgSrc 예, "data:image/jpeg;base64,/9j/4AAQSk....=="
	 * @param orignFileNm
	 * @throws Exception 
	 */
	public static void downBase64ImgSrc(HttpServletResponse response, String base64ImgSrc, String orignFileNm) throws Exception {
        String[] tk1 = base64ImgSrc.split(","); // (*1)
        String[] tk2 = tk1[0].split(":|;"); // (*2)
        String base64Img = tk1[1];
        String contentType = tk2[1];
		downBase64Img(response, base64Img, contentType, orignFileNm);
	}

	/**
	 * 서버의 파일을 다운로드한다. (from eGov)
	 *
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public static void downFile(HttpServletRequest request, HttpServletResponse response) throws Exception {

		String downFileName = "";
		String orgFileName = "";

		if ((String) request.getAttribute("downFile") == null) {
			downFileName = "";
		} else {
			downFileName = (String) request.getAttribute("downFile");
		}

		if ((String) request.getAttribute("orgFileName") == null) {
			orgFileName = "";
		} else {
			orgFileName = (String) request.getAttribute("orginFile");
		}

		orgFileName = orgFileName.replaceAll("\r", "").replaceAll("\n", "");

		File file = new File(downFileName);		// EgovWebUtil.filePathBlackList(downFileName)

		if (!file.exists()) {
			throw new FileNotFoundException(downFileName);
		}

		if (!file.isFile()) {
			throw new FileNotFoundException(downFileName);
		}

		byte[] buffer = new byte[BUFF_SIZE]; //buffer size 2K.

		response.setContentType("application/x-msdownload");
		response.setHeader("Content-Disposition:", "attachment; filename=" + new String(orgFileName.getBytes(), "UTF-8"));
		response.setHeader("Content-Transfer-Encoding", "binary");
		response.setHeader("Pragma", "no-cache");
		response.setHeader("Expires", "0");

		BufferedInputStream fin = null;
		BufferedOutputStream outs = null;

		try {
			fin = new BufferedInputStream(new FileInputStream(file));
			outs = new BufferedOutputStream(response.getOutputStream());
			int read = 0;

			while ((read = fin.read(buffer)) != -1) {
				outs.write(buffer, 0, read);
			}
		} finally {
			closeAll(outs, fin);
		}
	}

	public static void closeAll(Closeable  ... resources) {
		for (Closeable resource : resources) {
			if (resource != null) {
				try {
					resource.close();
				} catch (Exception ignore) {
					log.warn("Occurred Exception to close resource is ingored!!");
				}
			}
		}
	}

}
