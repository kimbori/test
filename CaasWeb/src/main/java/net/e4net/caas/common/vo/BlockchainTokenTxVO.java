package net.e4net.caas.common.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper=false)
public class BlockchainTokenTxVO extends ExtLinkVO {
    private long	toknTrnfSq;		// 토큰전송SQ
    private String	trnfTp;			// 토큰전송타입

    private String	frmWltAddr;		// From 지갑주소
    private String	toWltAddr;		// From 지갑주소
    private long	trnfTokn;		// 전송 토큰 수(= 기부 채리 금액)

    private String	txHash;			// 블록체인 트랜젝션 해시 (검증용)
    private String	txCfnRslt;		// 검증 결과
    private Data	txCfnDt;		// 검증 일시

    private String	doneYn;			// 완료처리 여부
}
