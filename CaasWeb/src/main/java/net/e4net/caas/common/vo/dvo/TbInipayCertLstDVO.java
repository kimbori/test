package net.e4net.caas.common.vo.dvo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbInipayCertLstDVO extends PageVO {

	private static final long serialVersionUID = -5936384602751754883L;
	
	private long   inipayCertSq; //이니페이인증내역SQ
	private String inipayStlmCd; //지불수단
	private long   stlmTtAmt;    //결제총액
	private String prdtNm;       //상품명
	private long   cnrSq;        //기부자SQ
	private String cnrNm;        //기부자명
	private String notiUrl;      //통지URL
	private String returnUrl;    //결과URL
	private String nxtUrl;       //다음URL
	private String stlmNtno;    //거래고유번호
	private String avUrl;        //요청URL
	private String rspnCd;       //결과코드
	private String rspnMsg;      //결과메시지
	private String fstRegDttm;   //최초등록일시
	private String fstRegUsid;   //최초등록자
	private String lstChDttm;    //최종변경일시
	private String lstChUsid;    //최종변경자
}