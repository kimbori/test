package net.e4net.caas.common.util;

import com.github.miemiedev.mybatis.paginator.domain.Order;
import com.github.miemiedev.mybatis.paginator.domain.Order.Direction;
import com.github.miemiedev.mybatis.paginator.domain.PageBounds;
import com.github.miemiedev.mybatis.paginator.domain.PageList;
import com.github.miemiedev.mybatis.paginator.domain.Paginator;

import net.e4net.caas.common.vo.PageVO;
import org.springframework.stereotype.Component;
import org.springframework.ui.ModelMap;

@Component
public class MybatisUtils {

	private static final int DEF_ROW_COUNT = 15;			// default
    //private static int MAX_ROW_COUNT;

    //@Value("#{global['mybatis.pagination.count.per.page']}")
    //public void setMaxRowCount(int maxRowCount) {
    //    MAX_ROW_COUNT = maxRowCount;
    //}

    /**
     * 	(*1) Unique 하지 않는 키로 order by 시 첫페이지에서 조회된 결과 일부가 두번째 페이지에서도 조회되어
     * 		중복 표시되는 일이 생긴다.
     * 		일반적인 처리를 위해 첫번째 오는 칼럼을 추가로 order by 주기로 한다.
     * 		대부분은 첫 컬럼이 PK인 경우가 대부분이기 때문이다.
     * @param offset
     * @param limit
     * @param order
     * @return
     */
    public static PageBounds pageBounds(Integer page, Integer limit, Order order) {
        if(page == null || page == 0) page = 1;			// default
        if(limit == null || limit == 0) limit = DEF_ROW_COUNT;	// default

        //if(limit > MAX_ROW_COUNT || limit <= 0) {
        //    limit = MAX_ROW_COUNT;
        //}
        
		Order orderApd = new Order("1", Direction.ASC, null); //(*1)
        return order != null ? new PageBounds(page, limit, order, orderApd) : new PageBounds(page, limit, orderApd); 
    }
 
    public static PageBounds pageBounds(Integer page, Integer limit) {
    	return pageBounds(page, limit, null); 
    }

//    public static PageBounds pageBounds(CommandMap param) {
//		log.debug("param = " + param.getMap().toString());
//    	Integer offset = param.getInt("page");
//    	Integer limit = param.getInt("rows");
//    	String sidx = param.getString("sidx");
//    	String sord = param.getString("sord");
//
//    	if(offset == 0 ) offset = param.getInt("offset");
//    	if(limit == 0 ) limit = param.getInt("limit");
//    	if(!sidx.isEmpty()) {
//    		Order order = new Order(sidx, sord.equals("desc") ? Direction.DESC : Direction.ASC, null);
//    		return pageBounds(offset, limit, order);
//    	} else {
//    		return pageBounds(offset, limit);    		
//    	}
//    }

    public static PageBounds pageBounds(PageVO param) {
    	String sidx = param.getOrderByColumn();
    	if(sidx != null && ! sidx.isEmpty()) {
       		Order order = new Order(sidx, param.getAscDesc().equals("desc") ? Direction.DESC : Direction.ASC, null);
    		return pageBounds(Integer.parseInt(param.getPageNo()), param.getCountPage(), order);    		
    	} else {
    		return pageBounds(Integer.parseInt(param.getPageNo()), param.getCountPage());    		
    	}
    }

    public static void putModelListPage(PageList<?> resultList, ModelMap model) {
        Paginator paginator = resultList.getPaginator();
    	model.put("paginator", paginator);
        model.put("resultList", resultList);
		//log.debug("list(" + list.size() + ") = " + list.toString());
		//log.debug("list size= " + list.size() + ", paginator= " + paginator.toString());
	}

}
