package net.e4net.caas.common.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class KakaoPayApiApproveRVO extends KakaoRestVO {

	private static final long serialVersionUID = -5447950025980771248L;

	@JsonProperty("aid")
	String aid; 
	
	@JsonProperty("tid")
	String tid; 
	
	@JsonProperty("cid")
	String cid; 
	
	@JsonProperty("sid")
	String sid; 
	
	@JsonProperty("partner_order_id")
	String partnerOrderId; 
	
	@JsonProperty("partner_user_id")
	String partnerUserId; 
	
	@JsonProperty("payment_method_type")
	String paymentMethodType; 
	
	@JsonProperty("amount")
	private Amount amount;
	
	@Data
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class Amount {
		@JsonProperty("total")
		private String total;
		
		@JsonProperty("tax_free")
		private String taxFree;
		
		@JsonProperty("vat")
		private String vat;
		
		@JsonProperty("point")
		private String point;
		
		@JsonProperty("discount")
		private String discount;
	}
	
	@JsonProperty("card_info")
	private CardInfo cardInfo; 
	
	@Data
	@JsonInclude(JsonInclude.Include.NON_NULL)
	public static class CardInfo {
		@JsonProperty("purchase_corp")
		private String purchaseCorp;
		
		@JsonProperty("purchase_corp_code")
		private String purchaseCorpCode;
		
		@JsonProperty("issuer_corp")
		private String issuerCorp;
		
		@JsonProperty("issuer_corp_code")
		private String issuerCorpCode;
		
		@JsonProperty("bin")
		private String bin;
		
		@JsonProperty("card_type")
		private String cardType;
		
		@JsonProperty("install_month")
		private String installMonth;
		
		@JsonProperty("approved_id")
		private String approvedId;
		
		@JsonProperty("card_mid")
		private String cardMid;
		
		@JsonProperty("interest_free_install")
		private String interestFreeInstall;
		
		@JsonProperty("card_item_code")
		private String cardItemCode;
	}
	
	@JsonProperty("item_name")
	String itemName; 
	
	@JsonProperty("item_code")
	String itemCode; 
	
	@JsonProperty("quantity")
	String quantity; 
	
	@JsonProperty("created_at")
	String createdAt;
	
	@JsonProperty("approved_at")
	String approvedAt; 
	
	@JsonProperty("payload")
	String payload; 
}
