package net.e4net.caas.my.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbPgBllDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class CardInfoPVO extends TbPgBllDVO{
	
	private static final long serialVersionUID = 662343427120116962L;
	
	
}
