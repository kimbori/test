package net.e4net.caas.my.service;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.util.MybatisUtils;
import net.e4net.caas.common.vo.dvo.TbDntRcepDVO;
import net.e4net.caas.my.mapper.DntnHstMapper;
import net.e4net.caas.my.vo.DntnHstPVO;
import net.e4net.caas.my.vo.DntnHstRVO;

/**
 * 기부내역 서비스
 *
 * @author hee.woo
 * @since 2018.11.27
 */
@Service
public class DntnHstService extends AbstractDao {
   
	@Autowired
    private DntnHstMapper dntnHstMapper;

    /**
     * 기부내역 리스트 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public List<DntnHstRVO> selectMyDntnList(DntnHstPVO pvo){
    	return dntnHstMapper.selectMyDntnList(pvo, MybatisUtils.pageBounds(pvo));
    }
    
    /**
     * 기부내역 집계
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public DntnHstRVO selectMyDntnSum(DntnHstPVO pvo){
    	return dntnHstMapper.selectMyDntnSum(pvo);
    }
    
    /**
     * 영수증신청
     *
     * @param pvo 신청정보
     * @return 신청건수
     */
    public int insertRcep(TbDntRcepDVO pvo) {
    	return dntnHstMapper.insertRcep(pvo);
    }
}