package net.e4net.caas.my.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.my.mapper.BllMgmtMapper;
import net.e4net.caas.my.vo.CardInfoPVO;
import net.e4net.caas.my.vo.CardInfoRVO;

/**
 * 기부내역 서비스
 *
 * @author hee.woo
 * @since 2018.12.03
 */
@Service
public class BllMgmtService extends AbstractDao {

	@Autowired
	private BllMgmtMapper bllMgmtMapper;

	/**
	 * 간편결제 카드목록 조회
	 *
	 * @param pvo 조회 조건
	 * @return 검색결과
	 */
	public List<CardInfoRVO> selectBllCardList(CardInfoPVO pvo) {
		return bllMgmtMapper.selectBllCardList(pvo);
	}

	/**
     * 간편결제 대표카드 조회
     */
	public CardInfoRVO selectBllCard(CardInfoPVO pvo) {
		return bllMgmtMapper.selectBllCard(pvo);
	}
	
	/**
	 * 간편결제 대표카드 변경
	 */
	public int updateRprsCard(CardInfoPVO pvo) throws Exception {
		int cnt = 0;

		// 기존 대표카드 제거
		CardInfoPVO delRprsVO = new CardInfoPVO();
		delRprsVO.setCnrSq(pvo.getCnrSq());
		delRprsVO.setRprsCardYn("N");
		bllMgmtMapper.updateRprsCard(delRprsVO);

		// 신규 대표카드 설정
		pvo.setRprsCardYn("Y");
		cnt = bllMgmtMapper.updateRprsCard(pvo);
		if (cnt != 1) {
			throw new CaasException("99999", "간편결제 대표카드 변경 오류");
		}

		return cnt;
	}
}