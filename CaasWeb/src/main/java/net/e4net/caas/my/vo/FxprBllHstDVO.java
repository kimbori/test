package net.e4net.caas.my.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.PageVO;

/**
 * @author D181
 *
 */
@Data
@ToString
@SuppressWarnings("serial")
@EqualsAndHashCode(callSuper = true)
public class FxprBllHstDVO extends PageVO {
	private long fxprHstSq;		//SEQ
	private long fxprDntnSq;	//정기기부 SEQ
	private String regDttm;		//등록일시
	private String fxprPcsnStat;//처리상태
	private long bllAuthSq;		//빌링인증 SEQ
	private String statYm;		//개시연월
	private String endYm;		//종료연월
	private String otaDt;		//출금일
	private int otaAmt;			//출금금액
	
	private long cnrSq;
}