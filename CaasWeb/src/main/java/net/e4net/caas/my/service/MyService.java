package net.e4net.caas.my.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.my.mapper.MyMapper;
import net.e4net.caas.my.vo.MyInfoPVO;
import net.e4net.caas.my.vo.MyInfoRVO;

/**
 * 마이페이지 서비스
 *
 * @author hee.woo
 * @since 2018.11.27
 */
@Service
public class MyService extends AbstractDao {
   
	@Autowired
    private MyMapper myMapper;

    
    /**
     * 마이페이지 회원정보 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public MyInfoRVO selectMyInfo(MyInfoPVO pvo) {
    	return myMapper.selectMyInfo(pvo);
    }
    
    /**
     * 마이페이지 - 프로필 정보 변경 
     *
     * @param pvo 변경정보
     * @return 변경결과
     */
    public int updatePrfInfo(MyInfoPVO pvo) {
    	return myMapper.updatePrfInfo(pvo);
    }
    
}
