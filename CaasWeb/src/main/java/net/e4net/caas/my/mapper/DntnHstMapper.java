package net.e4net.caas.my.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.common.vo.dvo.TbDntRcepDVO;
import net.e4net.caas.my.vo.DntnHstPVO;
import net.e4net.caas.my.vo.DntnHstRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author hee.woo
 * @since 2018.11.27
 */
@Repository
public interface DntnHstMapper {
	
    
    /**
     * 기부내역 리스트 조회
     */
    public List<DntnHstRVO> selectMyDntnList(DntnHstPVO pvo, PageBounds pageBounds);
	
    /**
     * 기부내역 집계
     */
    public DntnHstRVO selectMyDntnSum(DntnHstPVO pvo);
  
    /**
     * 영수증 신청
     */
    public int insertRcep(TbDntRcepDVO pvo);
}