package net.e4net.caas.my.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import net.e4net.caas.my.vo.FxprBllHstDVO;
import net.e4net.caas.my.vo.FxprBllPVO;
import net.e4net.caas.my.vo.FxprBllRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author hee.woo
 * @since 2018.12.04
 */
@Repository
public interface FxprBllMapper {
	
    
    /**
     * 정기결제 목록 조회
     */
    public List<FxprBllRVO> selectFxprBllList(FxprBllPVO pvo);
    
    /**
     * 정기결제 정보 조회
     */
    public FxprBllHstDVO selectFxprBllInfo(FxprBllPVO pvo);
    
    /**
     * 정기결제 정보 변경
     */
    public int updateFxprBll(FxprBllPVO pvo);
    
    /**
     * 정기결제 종료
     */
    public int deleteFxprBll(FxprBllPVO pvo);
    
    /**
     * 정기결제 이력 저장
     * */
    public int insertFxprBllHst(FxprBllHstDVO pvo);
}