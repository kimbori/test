package net.e4net.caas.my.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.my.vo.FlngHstPVO;
import net.e4net.caas.my.vo.FlngHstRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author hee.woo
 * @since 2018.11.28
 */
@Repository
public interface FlngHstMapper {
	
    
    /**
     * 충전내역 리스트 조회
     */
    public List<FlngHstRVO> selectMyFlngList(FlngHstPVO pvo, PageBounds pageBounds);
	
    /**
     * 충전내역 집계
     */
    public FlngHstRVO selectMyFlngSum(FlngHstPVO pvo);
  
}