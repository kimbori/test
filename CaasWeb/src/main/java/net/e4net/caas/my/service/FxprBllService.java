package net.e4net.caas.my.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;

import net.e4net.caas.common.dao.AbstractDao;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.my.mapper.FxprBllMapper;
import net.e4net.caas.my.vo.FxprBllHstDVO;
import net.e4net.caas.my.vo.FxprBllPVO;
import net.e4net.caas.my.vo.FxprBllRVO;

/**
 * 정기기부 결제관리 서비스
 *
 * @author hee.woo
 * @since 2018.12.04
 */
@Service
public class FxprBllService extends AbstractDao {
   
	@Autowired
    private FxprBllMapper fxprBllMapper;

	
    /**
     * 정기결제 목록 조회
     *
     * @param pvo 조회 조건
     * @return 검색결과
     */
    public List<FxprBllRVO> selectFxprBllList(FxprBllPVO pvo){
    	return fxprBllMapper.selectFxprBllList(pvo);
    }
    
    /**
     * 정기결제 정보 변경
     *
     * @param pvo 정기결제 정보
     * @return 업데이트건수
     */
    public int updateFxprBll(FxprBllPVO pvo) {
    	TransactionStatus stat = null;
    	int cnt = 0;
    	try {
    		stat = getTransactionStatus("updateFxprBll");
    		
    		if("02".equals(pvo.getFxprStat())) {
    			cnt = fxprBllMapper.deleteFxprBll(pvo);
    		}else {
    			cnt = fxprBllMapper.updateFxprBll(pvo);
    		}
    		
    		if(cnt != 1) {
    			throw new CaasException("9999", "정기결제 정보 변경/종료 시 오류발생");
    		}
    		
    		FxprBllHstDVO dvo = fxprBllMapper.selectFxprBllInfo(pvo);
    		dvo.setCnrSq(pvo.getCnrSq());
    		dvo.setFxprPcsnStat(pvo.getFxprPcsnStat());
    		cnt = fxprBllMapper.insertFxprBllHst(dvo);
    		if(cnt != 1) {
    			throw new CaasException("9999", "정기결제 이력 등록 시 오류발생");
    		}
    		transactionCommit(stat);
    	} catch (Exception e) {
    		error("updateFxprBll", e);
    		transactionRollback(stat);
    	}
    	return cnt;
    }
}
