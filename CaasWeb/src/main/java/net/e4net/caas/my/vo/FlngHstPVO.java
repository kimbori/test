package net.e4net.caas.my.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbPgFlngDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class FlngHstPVO extends TbPgFlngDVO{
	
	private static final long serialVersionUID = -1154542582102403164L;
	
	private String startDate;
	private String endDate;
	
}
