package net.e4net.caas.news.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import net.e4net.caas.common.controller.AbstractController;
import net.e4net.caas.common.exception.CaasException;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.UserVO;
import net.e4net.caas.news.service.PnService;
import net.e4net.caas.news.vo.TbPnPVO;
import net.e4net.caas.news.vo.TbPnRVO;

/**
 * 공지사항 컨트롤러
 *
 * @author hee.woo
 * @since 2018.11.26
 * history  2018.11.26 최초작성
 */
@Controller
public class PnController extends AbstractController {
	
    @Autowired
    private PnService pnService;
	
    
    /**
     * <pre>
     * 공지사항/FAQ 페이지 이동
     * </pre>
     *
     * @param model
     * @param request
     * @return
     */
    @RequestMapping(value = "/movePnFaqPag", method = RequestMethod.GET)
    public ModelAndView movePnFaqPag(HttpServletRequest request) throws Exception {
    	debug("===== move 공지사항 페이지 =====");
    	ModelAndView mav = new ModelAndView("news/pnFaqPag");
    
		return getOkModelAndView(mav);
    }
    
    
    /**
     * <pre>
     * 공지사항 목록 조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return List<TbPnRVO> 공지사항 리스트
     */
    @RequestMapping(value = "/news/selectPnList", method = RequestMethod.POST)
    public ModelAndView selectPnList(@ModelAttribute TbPnPVO pvo) throws Exception {
        debug("=====  select 공지사항 목록 =====");
        ModelAndView mav = new ModelAndView("news/pnFaqAjax :: pnList");
        
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setOprGrpId(uVO.getOprGrpId()); // 운영단체ID
    	pvo.setOrderByColumn("PN_SQ");
    	pvo.setAscDesc("desc");
        List<TbPnRVO> resultList = pnService.selectPnList(pvo);
        
        int totalCnt = resultList.size() == 0 ? 0 : resultList.get(0).getTotalCnt();
        int pageNo = StringUtils.parseInt(pvo.getPageNo(), 1);
        boolean hasNext = totalCnt-(pageNo*pvo.getCountPage()) > 0;
        
        mav.addObject("totalCnt", totalCnt);
        mav.addObject("resultList", resultList);
        mav.addObject("hasNext", hasNext);

        return getOkModelAndView(mav);
    }
    
    
    
    /**
     * <pre>
     * 공지사항 상세조회
     * </pre>
     *
     * @param pvo 조회조건 값
     * @return TbPnRVO 공지사항 정보
     */
    @RequestMapping(value = "/news/selectPnDetail", method = RequestMethod.GET)
    public ModelAndView selectPnDetail(@ModelAttribute TbPnPVO pvo) throws Exception {
    	debug("=====  select 공지사항 상세조회 =====");
    	//ModelAndView mav = new ModelAndView("news/pnDtlPag"); //페이지전환
    	ModelAndView mav = new ModelAndView("news/pnFaqAjax :: pnDetail");
    	
    	UserVO uVO = (UserVO)SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    	pvo.setOprGrpId(uVO.getOprGrpId()); // 운영단체ID
    	pvo.setPnSq(StringUtils.parseLong(pvo.getSrchPnSq(), 0));
    	
    	TbPnRVO result = pnService.selectPnDetail(pvo);
    	if(result==null) {
    		throw new CaasException("9999", "처리 중 오류가 발생하였습니다.");
    	}
    	
    	mav.addObject("result", result);
    	return getOkModelAndView(mav);
    }
    
    
    
}