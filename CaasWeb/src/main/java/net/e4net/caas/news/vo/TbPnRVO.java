package net.e4net.caas.news.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbPnDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbPnRVO extends TbPnDVO{
	
	private static final long serialVersionUID = 7896582958844813869L;
	
	private String regDttmFmt;
	private String icoNewYn;
	
}
