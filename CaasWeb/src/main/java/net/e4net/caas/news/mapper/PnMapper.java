package net.e4net.caas.news.mapper;

import java.util.List;

import org.springframework.stereotype.Repository;

import com.github.miemiedev.mybatis.paginator.domain.PageBounds;

import net.e4net.caas.news.vo.TbPnPVO;
import net.e4net.caas.news.vo.TbPnRVO;

/**
 * Mapper Xml 매핑을 위한 인터페이스
 *
 * @author hee.woo
 * @since 2018.11.26
 */
@Repository
public interface PnMapper {
	
	/**
     * 공지사항 목록 조회
     */
    public List<TbPnRVO> selectPnList(TbPnPVO pvo, PageBounds pageBounds);
    
    
    /**
     * 공지사항 상세조회
     */
    public TbPnRVO selectPnDetail(TbPnPVO pvo);
	
    
    
}