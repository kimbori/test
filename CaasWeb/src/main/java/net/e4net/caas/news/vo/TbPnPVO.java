package net.e4net.caas.news.vo;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import net.e4net.caas.common.vo.dvo.TbPnDVO;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString
public class TbPnPVO extends TbPnDVO{

	private static final long serialVersionUID = 4145344790937146943L;
	
	private String srchPnSq;
	
}
