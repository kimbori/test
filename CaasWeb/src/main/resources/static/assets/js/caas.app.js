/* **************************************************************************************
 * 	E4. 공통 App <-> Web View 인터페이스
 */
//;'use strict';
(function($) {

	function func2App() {
		alert("func2App");
	}

	isAndroid = function() {
		return typeof(Android)!= "undefined";
	}

	// Web View => App 호출
	caasToApp = {
			
		//return true if app is valid
		isValidApp() {
			if(! isAndroid()) return false;
			return Android.isValidApp();
		},
			
		//Refresh on Resume set
		setRefreshOnResume() {
			if(! isAndroid()) return;
			Android.setRefreshOnResume();
		},
			
		//푸시알림 setter
		//notiStatus == boolean
		setNotificationStatus(notiStatus){
			if(! isAndroid()) return;
			Android.setNotificationStatus(notiStatus);
		},
		
		//앱잠금 setter
		//lockStatus == boolean
		setAppLockStatus(lockStatus){
			if(! isAndroid()) return;
			Android.setAppLockStatus(lockStatus);
		},
		
		//Get status of whether user wants to receive notifications
		//returns boolean
		getNotificationStatus() {
			if(! isAndroid()) return false;
			return Android.getNotificationStatus();
		},
		
		//Get App lock status
		//returns boolean
		getAppLockStatus(){
			if(! isAndroid()) return false;
			return Android.getAppLockStatus();
		},

		// 테스트용
		getString : function(){
			var sampleStr = Android.sendString();
			console.log(sampleStr);
		},

		// 테스트용
		getJson : function() {
			var sample = Android.showToast2("herro");
			console.log(sample);
		},
		
		//CID opGroupID에 저장
		getCID : function() {
			if(! isAndroid()) return "";
			var CID = Android.getCID();
			console.log(CID);
			return CID;
		},
		
		//운영단체ID opGroupID에 저장
		getOpGroupID : function() {
			if(! isAndroid()) return "";
			var opGroupID = Android.getOpGroupID();
			console.log(opGroupID);
			return opGroupID;
		},
			
		//
		showAndroidToast : function() {
			if(! isAndroid()) return;
		    //var toast = document.getElementById("searchTxt").value;
			var toast = "Herro I am a toast"
		    Android.showToast(toast);        
		},
		
		//return firebase token string
		getFirebaseToken : function() {
			if(! isAndroid()) return "";
			return Android.getFirebaseToken(); 
		},
		
		//비밀번호 설정 화면 안드로이드에서 띄우기
		startPasswordSetActivity : function() {
			if(! isAndroid()) return;
			Android.startPasswordSet();
		},
		
		//비밀번호 입력(확인) 화면 안드로이드에서 띄우기
		startPasswordCheckActivity : function() {
			console.log("startPasswordCheckActivity");
			if(! isAndroid()) return;
			Android.startPasswordCheck();
		},
		
		//CID 안드로이드에 저장
		saveCIDToPhone : function(cid){
			if(! isAndroid()) return;
			Android.saveCIDToPhone(cid);
		},
		
		//DID 안드로이드에 저장
		saveDIDToPhone : function(did){
			if(! isAndroid()) return;
			Android.saveDIDToPhone(did);
		},

		shareFacebook : function(link, msg) {
			if(! isAndroid()) return;
			//content, url
			Android.shareFacebook("http://cherry.e4net.net/ \n기부하면 체리죠! \n지금 바로 기부해보세요 ^^", "http://cherry.e4net.net/");        
		},

		
		//Title: 제목
		//Description: 부가설명
		//BtnTitle: 버튼 텍스트
		//LikeCount, commentCount, shareCount, viewCount: BtnTitle밑에 표시될 숫자들
		//url: 버튼을 눌렀을때 앱으로 열어야할 URL
		//imgUrl: 표시할 이미지 (url형식이여야됨)
		shareKakaoTalk : function(title, description, btnTitle, likeCount, commentCount, shareCount, viewCount, url, imgUrl) {
			if(! isAndroid()) return;
			Android.shareKakaoTalkApi(title, description, btnTitle, likeCount, commentCount, shareCount, viewCount, url, imgUrl)
			//Android.shareKakaoTalk("http://cherry.e4net.net/ \n기부하면 체리죠! \n지금 바로 기부해보세요 ^^", "http://cherry.e4net.net/"); 
			
		},
		
		shareSMS : function(link, msg) {
			if(! isAndroid()) return;
			//PARAM: link, msg
			Android.shareSMS(link, msg);
		},
		
		sharePick : function() {
			if(! isAndroid()) return;
			Android.shareAppLink("http://cherry.e4net.net/ \n체리로 세상을 따듯하게 합니다")
		},
		
		getPic : function() {
			if(! isAndroid()) return;
			Android.getPic();        
		},
		
		//CallbackFunc == string
		//E.g. "caasFromApp.setPic"
		//Please omit brackets
		getPic2 : function(callbackFunc) {
			if(! isAndroid()) return;
			Android.getPic2(callbackFunc);        
		},
		
		getPic3 : function(callbackFunc) {
			if(! isAndroid()) return;
			Android.getPic3(callbackFunc);        
		},
		
		goToMyPage : function(url, title) {
			if(! isAndroid()) return;
			Android.goToPage(url,title);        
		},
		
		openSearch : function(url, title) {
			if(! isAndroid()) return;
			Android.openSearch(url);        
		}
	};

	// App => Web View 호출
	caasFromApp = {
		funcSearch : null,
		setFuncSearch : function(func) {
			caasFromApp.funcSearch = func;
		},

		buttonEvent : function(bType, url) {
			switch(bType) {
			case "SEARCH" : 
				caasToApp.openSearch(url + "/moveSrch");
				break;
			case "HOME" : 
				location.href = url + "/moveMainPag";	
				break;
			case "MYPAGE" :
				caasToApp.goToMyPage(url + "/moveMyPag", "마이페이지");
				//location.href = "https://cherry.e4net.net/moveMyPag";	
				break;
			case "ALERTS" : 
				caasToApp.goToMyPage(url + "/publish/CM-005.html", "알림");			
				break;
				
			default: alert("Invalid Button Type, bType=" + bType);
			}
		},
		
		setPic : function(base64pic) {
			$(document).ready(function() {
		    	$("#img_form_url").attr("src", "data:image/png;base64," + base64pic);
		    });
		}
	};

	// SNS 공유 표준 처리
	//	(*1) 비공개 URL이다. 앱 수행 후 이동할 캠페인 주소. (참조, "/public/loginProc" 의 "redUrl" 로 넘겨진다.
	//	(*2) 카카오톡 화면에 표시되는 이미지이다. 공개 URL이다.
	caasSns = {
		shareCampaign : function(snsType, cmpSq, cmpNm) {
			var url = "/campaign/" + cmpSq; // (*1)
			var redUrl = encodeURIComponent(url);
			var imgUrl = "/public/campaign/" + cmpSq + "/img"; // (*2)
			var description = "체리로 세상을 따듯하게 합니다!";
			//console.log("[fnGoSns] snsType=" + snsType + ", cmpSq=" + cmpSq + ", cmpNm=" + cmpNm + ", url=" + url);
			switch(snsType) {
			case "facebook" :	alert("준비중 입니다."); /*caasToApp.shareFacebook(url, msg);*/		break;
			case "kakaotalk" :	caasSns.shareKakaoTalk(cmpNm, description, redUrl, imgUrl);	break;
			case "sms" :
				caasToApp.shareSMS(redUrl, description + "\n" + cmpNm);
				break;
			}
		},

		shareKakaoTalk : function(title, description, url, imgUrl) {
			caasToApp.shareKakaoTalk(title, description, "체리앱에서 보기", -1, -1, -1, -1, url, imgUrl)
		},

	}
}(jQuery));

function func3App() {
	alert("func3App");
}

//검색 등록_srchKey : 검색키값
function fnInsSrch(srchKey, pageNo){
	pageNo = (pageNo == null || pageNo == 0) ? 1 : pageNo;
	$("#pageNo").val(pageNo);
	$("#srchKey", document.mainForm).val(srchKey);
	
	goAjax("/main/srchIns","POST", false, $("#mainForm").serialize(), fnInsSrchCallBack);
	
	function fnInsSrchCallBack(data){
		if(pageNo == 1){
			$("#dntnAjax").html(data);
		}else{
			$("#dntnAjax").append(data);
		}
		$("#btnDntnMore").click(function(){
			fnInsSrch(srchKey, Number(pageNo)+1);
		});
		
		fnGetSrchList();	//최근검색 리스트 조회 
	}
}