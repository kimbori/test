package net.e4net.caas.common.service;

import static org.junit.Assert.assertTrue;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;
import mup.mcash.module.common.McashCipher;
import net.e4net.caas.CaasApplicationTests;
import net.e4net.caas.common.util.StringUtils;
import net.e4net.caas.common.vo.CmmnDetailCode;
import net.e4net.caas.sample.service.SampleService;
import net.e4net.caas.sample.vo.SampleRVO;

@Slf4j
public class CmmServiceTest extends CaasApplicationTests {

	@Autowired private CmmService service;
    @Autowired private SampleService sampleService;

    /**
     * 트랜젝션 Rollback 단위 테스트
     */
	@Test
	public void testTransactionRollback() {
		testTransactionRollbackSub(false);		// 외부는 commit, 내부는 commit
		testTransactionRollbackSub(true);		// 외부는 rollback, 내부는 commit
	}
	public void testTransactionRollbackSub(boolean onfail) {
		boolean onSuccess = true;
		SampleRVO vo1 = sampleService.getTransactionRollbackTestVo("TEST_ROLLBACK");
		SampleRVO vo2 = sampleService.getTransactionRollbackTestVo("TEST_ROLLBACK_I");
		try {
			sampleService.transactionRollbackTest(onfail);			
		} catch(Exception e) {
			if(e.getMessage() == null) {
				log.error("Exception. err=" + e.getMessage());				
			} else {
				log.error("Exception. err=" + e.getMessage(), e);				
			}
			onSuccess = false;
		}
		sampleService.transactionRollbackTestVoLog("After. onSuccess=" + onSuccess, "TEST_ROLLBACK", vo1);
		sampleService.transactionRollbackTestVoLog("After. onSuccess=" + onSuccess, "TEST_ROLLBACK_I", vo2);
	}

	//@Test
	public void selectCmmCodeDetailTet() {
    	log.debug("test3");
		List<CmmnDetailCode> result = service.selectCmmCodeDetail("test");
		assertTrue(result.size() > 0);
	}

	//@Test
	public void test() throws UnsupportedEncodingException {
		String encKey = "1811260618112606";
		String decName = "00dfffbae3d95f257ad7f599670b4fe0";
		
	    String name = McashCipher.decodeString(decName, encKey);
	    //String name1 = McashSeed.decodeString(decName, McashCipher.getKey(encKey)).trim();
		log.debug("name= " + name);

		String defaultCharset = Charset.defaultCharset().name();
		log.debug("defaultCharset= " + defaultCharset);
		
		byte[] byteNameUtf = name.getBytes("utf-8");
		byte[] byteNameEuckr= name.getBytes("euc-kr");
		log.debug("bytes(utf-8) =" + name.getBytes("utf-8"));
		log.debug("bytes(euc-kr) =" + name.getBytes("euc-kr"));
		log.debug("bytes(utf-8) =" + StringUtils.byteArrayToHexString(byteNameUtf));
		log.debug("bytes(euc-kr) =" + StringUtils.byteArrayToHexString(byteNameEuckr));
	}

	public void test2() throws IOException {
    	String ekry ="1811260618112606";
    	String encStr = "00dfffbae3d95f257ad7f599670b4fe0";
    	String decStr = McashCipher.decodeString(encStr,ekry);
    	
		log.debug("decStr=" + decStr);
		
		//euc_kr_str - euc-kr 문자열

		CharBuffer cbuffer = CharBuffer.wrap((new String(StringUtils.parseByte(decStr), "EUC-KR").toCharArray()));

		Charset utf8charset = Charset.forName("UTF-8");

		ByteBuffer bbuffer = utf8charset.encode(cbuffer);

		//변환된 UTF-8 문자열

		String tmpDecode = new String(bbuffer.array());
		
		log.debug("TTTT : " + tmpDecode);
		
    	String word = decStr;
    	System.out.println("utf-8        			 : " + new String(word.getBytes("UTF-8")));
    	System.out.println("euc-kr       			 : " + new String(word.getBytes("EUC-KR")));
    	System.out.println("8859_1       			 : " + new String(word.getBytes("8859_1")));
    	
    	System.out.println("utf-8 -> euc-kr        : " + new String(word.getBytes("utf-8"), "euc-kr"));
    	System.out.println("utf-8 -> ksc5601       : " + new String(word.getBytes("utf-8"), "ksc5601"));
    	System.out.println("utf-8 -> x-windows-949 : " + new String(word.getBytes("utf-8"), "x-windows-949"));
    	System.out.println("utf-8 -> iso-8859-1    : " + new String(word.getBytes("utf-8"), "iso-8859-1"));
    	System.out.println("iso-8859-1 -> euc-kr        : " + new String(word.getBytes("iso-8859-1"), "euc-kr"));
    	System.out.println("iso-8859-1 -> ksc5601       : " + new String(word.getBytes("iso-8859-1"), "ksc5601"));
    	System.out.println("iso-8859-1 -> x-windows-949 : " + new String(word.getBytes("iso-8859-1"), "x-windows-949"));
    	System.out.println("iso-8859-1 -> utf-8         : " + new String(word.getBytes("iso-8859-1"), "utf-8"));
    	System.out.println("euc-kr -> utf-8         : " + new String(word.getBytes("euc-kr"), "utf-8"));
    	System.out.println("euc-kr -> ksc5601       : " + new String(word.getBytes("euc-kr"), "ksc5601"));
    	System.out.println("euc-kr -> x-windows-949 : " + new String(word.getBytes("euc-kr"), "x-windows-949"));
    	System.out.println("euc-kr -> iso-8859-1    : " + new String(word.getBytes("euc-kr"), "iso-8859-1"));
    	System.out.println("ksc5601 -> euc-kr        : " + new String(word.getBytes("ksc5601"), "euc-kr"));
    	System.out.println("ksc5601 -> utf-8         : " + new String(word.getBytes("ksc5601"), "utf-8"));
    	System.out.println("ksc5601 -> x-windows-949 : " + new String(word.getBytes("ksc5601"), "x-windows-949"));
    	System.out.println("ksc5601 -> iso-8859-1    : " + new String(word.getBytes("ksc5601"), "iso-8859-1"));
    	System.out.println("x-windows-949 -> euc-kr     : " + new String(word.getBytes("x-windows-949"), "euc-kr"));
    	System.out.println("x-windows-949 -> utf-8      : " + new String(word.getBytes("x-windows-949"), "utf-8"));
    	System.out.println("x-windows-949 -> ksc5601    : " + new String(word.getBytes("x-windows-949"), "ksc5601"));
    	System.out.println("x-windows-949 -> iso-8859-1 : " + new String(word.getBytes("x-windows-949"), "iso-8859-1"));
/*
    	String [] charSet = {"euc-kr", "ksc5601", "iso-8859-1", "8859_1", "ascii" , "MS949" ,"UTF-8"};
    	for (int i=0; i<charSet.length; i++) {
    	 for (int j=0; j<charSet.length; j++) {
    	  try {
    		  debug("[" + charSet[i] +"," + charSet[j] +"] = " + new String(Resultmsg.getBytes(charSet[i]), charSet[j]));
    	  } catch (UnsupportedEncodingException e) {
    	   e.printStackTrace();
    	  }
    	 }
    	}
    	debug(		request.getQueryString());
    	debug(		request.getQueryString());
    	debug(URLEncoder.encode(Resultmsg, "UTF-8"));
    	debug(URLEncoder.encode(Resultmsg, "EUC-KR"));
    	debug(URLDecoder.decode(Resultmsg, "UTF-8"));
    	debug(URLEncoder.encode(Resultmsg, "UTF-8"));
    	debug(URLEncoder.encode(Resultmsg, "EUC-KR"));
    	debug(URLDecoder.decode(URLEncoder.encode(Resultmsg, "UTF-8"), "EUC-KR"));
 */
		System.out.println("==== file.encoding===" + System.getProperty("file.encoding"));
		String aa = decStr;
		testEncoding(aa, "MS949");
		testEncoding(aa, "UTF-8");
		testEncoding(aa, "CP933");
		testEncoding(aa, "EUC-KR");
	}
	
	static String convert(String str, String encoding) throws IOException {
		  ByteArrayOutputStream requestOutputStream = new ByteArrayOutputStream();
		  requestOutputStream.write(str.getBytes(encoding));
		  return requestOutputStream.toString(encoding);
		 }

	 static String testEncoding(String str, String encoding) throws IOException {
	  String result = convert(str, encoding);
	  System.out.println(result + "=>encoding=" + encoding + ",length=("
	    + result.getBytes(encoding).length + ")");
	  return result;
	 }
}
