package net.e4net.caas.common.service;

import java.io.UnsupportedEncodingException;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import lombok.extern.slf4j.Slf4j;
import mup.mcash.module.common.McashCipher;
import net.e4net.caas.CaasApplicationTests;
import net.e4net.caas.common.vo.OprGrpVO;

@Slf4j
public class CertServiceTest extends CaasApplicationTests {

	@Autowired private CmmService cmmService;

	//@Test
	public void getAlterCtEncKeyTest() {
		getAlterCtEncKeyTestSub("e4net");
		getAlterCtEncKeyTestSub("WTIT");
	}
	private void getAlterCtEncKeyTestSub(String oprGrpId) {
		OprGrpVO oprGrp = cmmService.getOprGrpInfo(oprGrpId);
    	String alterEncKey = oprGrp.getAlterCtEncKey();
    	log.debug("oprGrpId=" + oprGrpId 
    			+ ", SvcId=" + oprGrp.getCtSvcId() + ", EncKey=" + oprGrp.getCtEncKey() 
    			+ ", alterEncKey=" + alterEncKey);
	}
	
	@Test
	public void decodeTest() throws UnsupportedEncodingException {
		OprGrpVO oprGrp = cmmService.getOprGrpInfo("WTIT");
    	String alterEncKey = oprGrp.getAlterCtEncKey();
		String encName = "3dbb56a54939d66a6b2f9de0712d3c7b";
		String decName = McashCipher.decodeString(encName, alterEncKey);
		log.debug("decName=" + decName + ", encName=" + encName + ", alterEncKey=" + alterEncKey);
	}
}
